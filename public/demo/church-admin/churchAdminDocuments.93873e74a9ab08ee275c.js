var churchAdminDocuments;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./ui/src/components/church-admin/confirm-dialog.tsx":
/*!***********************************************************!*\
  !*** ./ui/src/components/church-admin/confirm-dialog.tsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ./dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var ConfirmDialog = function (_a) {
    var isOpen = _a.isOpen, isProcessing = _a.isProcessing, title = _a.title, message = _a.message, action = _a.action, processingAction = _a.processingAction, onConfirm = _a.onConfirm, onCancel = _a.onCancel;
    var isDarkMode = React.useContext(dark_mode_context_1["default"]);
    return (React.createElement(core_1.Dialog, { isOpen: isOpen, title: title, className: isDarkMode ? core_1.Classes.DARK : '', onClose: onCancel },
        React.createElement("div", { className: core_1.Classes.DIALOG_BODY },
            React.createElement("p", { className: core_1.Classes.RUNNING_TEXT }, message)),
        React.createElement("div", { className: core_1.Classes.DIALOG_FOOTER },
            React.createElement("div", { className: core_1.Classes.DIALOG_FOOTER_ACTIONS },
                React.createElement(core_1.Button, { onClick: onCancel, disabled: isProcessing === true }, "Cancel"),
                React.createElement(core_1.Button, { onClick: onConfirm, disabled: isProcessing === true, intent: "primary" }, isProcessing === true ? processingAction : action)))));
};
exports["default"] = ConfirmDialog;


/***/ }),

/***/ "./ui/src/model/user.ts":
/*!******************************!*\
  !*** ./ui/src/model/user.ts ***!
  \******************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
exports.__esModule = true;
exports.User = void 0;
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var User = /** @class */ (function () {
    function User() {
        this.id = '';
        this.firstName = '';
        this.lastName = '';
        this.username = '';
        this.authLevel = 'admin';
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('id', String),
        __metadata("design:type", Object)
    ], User.prototype, "id");
    __decorate([
        (0, json2typescript_1.JsonProperty)('firstName', String),
        __metadata("design:type", Object)
    ], User.prototype, "firstName");
    __decorate([
        (0, json2typescript_1.JsonProperty)('lastName', String),
        __metadata("design:type", Object)
    ], User.prototype, "lastName");
    __decorate([
        (0, json2typescript_1.JsonProperty)('username', String),
        __metadata("design:type", Object)
    ], User.prototype, "username");
    __decorate([
        (0, json2typescript_1.JsonProperty)('authLevel', String),
        __metadata("design:type", Object)
    ], User.prototype, "authLevel");
    User = __decorate([
        (0, json2typescript_1.JsonObject)('User')
    ], User);
    return User;
}());
exports.User = User;


/***/ }),

/***/ "./ui/src/pages/church-admin/documents.styles.ts":
/*!*******************************************************!*\
  !*** ./ui/src/pages/church-admin/documents.styles.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    card: "\n    padding: 0;\n  ",
    headerRow: "\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    padding: 10px 16px;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n  ",
    header: "\n    text-align: left;\n    padding: 0;\n    width: auto;\n  ",
    empty: {
        margin: '40px 0',
        '& h4': "\n      margin-bottom: 20px;\n    "
    },
    body: "\n    padding: 16px;\n  ",
    docRow: {
        display: 'flex',
        alignItems: 'flex-start',
        marginBottom: '16px',
        '&:last-child': "\n      margin-bottom: 0;\n    "
    },
    avatar: "\n    background-color: ".concat(core_1.Colors.GRAY3, ";\n    padding: 6px;\n    border-radius: 2px;\n    margin: 4px 16px 0 0;\n  "),
    metaCol: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        marginRight: 'auto',
        '& a': "\n      display: flex;\n      align-items: center;\n    "
    },
    linkIcon: "\n    margin-left: 4px;\n    padding-bottom: 1px;\n  ",
    metaLine: "\n    display: flex;\n    font-size: 12px;\n    margin-top: 2px;\n  ",
    more: "\n    margin: 4px 0 0 8px;\n  "
});


/***/ }),

/***/ "./ui/src/pages/church-admin/documents.tsx":
/*!*************************************************!*\
  !*** ./ui/src/pages/church-admin/documents.tsx ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.init = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_1 = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var admin_page_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/admin-page */ "./ui/src/components/church-admin/admin-page.tsx"));
var center_card_layout_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/center-card-layout */ "./ui/src/components/church-admin/center-card-layout.tsx"));
var di_container_1 = __importDefault(__webpack_require__(/*! ../../di-container */ "./ui/src/di-container.ts"));
var local_storage_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/local-storage-data-access */ "./ui/src/services/local-storage-data-access.ts"));
var uploaded_document_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/uploaded-document-data-access */ "./ui/src/services/uploaded-document-data-access.ts"));
var bytes_formatter_1 = __webpack_require__(/*! bytes-formatter */ "./ui/node_modules/bytes-formatter/index.js");
var confirm_dialog_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/confirm-dialog */ "./ui/src/components/church-admin/confirm-dialog.tsx"));
var documents_styles_1 = __webpack_require__(/*! ./documents.styles */ "./ui/src/pages/church-admin/documents.styles.ts");
var ReactDOM = __importStar(__webpack_require__(/*! react-dom */ "./ui/node_modules/react-dom/index.js"));
var kerygma_configuration_1 = __webpack_require__(/*! ../../model/kerygma-configuration */ "./ui/src/model/kerygma-configuration.ts");
var toaster = core_1.Toaster.create({ position: 'top' });
var localStorageDataAccess = di_container_1["default"].resolve(local_storage_data_access_1["default"]);
var uploadedDocumentDataAccess = di_container_1["default"].resolve(uploaded_document_data_access_1["default"]);
var config = di_container_1["default"].resolve(kerygma_configuration_1.KerygmaConfiguration);
var _a = config.api, apiHost = _a.apiHost, mediaContextRoot = _a.mediaContextRoot;
var documentUri = "".concat(apiHost).concat(mediaContextRoot, "/documents/");
var Documents = function (_a) {
    var assetsRoot = _a.assetsRoot;
    var _b = react_1["default"].useState(), error = _b[0], setError = _b[1];
    var _c = (0, react_1.useState)(true), isLoading = _c[0], setIsLoading = _c[1];
    var _d = (0, react_1.useState)(), loginUser = _d[0], setLoginUser = _d[1];
    var _e = (0, react_1.useState)([]), documents = _e[0], setDocuments = _e[1];
    var _f = (0, react_1.useState)(false), isDeleteConfirmOpen = _f[0], setIsDeleteConfirmOpen = _f[1];
    var _g = react_1["default"].useState(null), docToDelete = _g[0], setDocToDelete = _g[1];
    var _h = (0, react_1.useState)(false), isProcessing = _h[0], setIsProcessing = _h[1];
    var classes = (0, documents_styles_1.useStyles)();
    var onLogin = function () {
        setError(undefined);
        loadDocuments().then();
    };
    var loadDocuments = function () { return __awaiter(void 0, void 0, void 0, function () {
        var response, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, 3, 4]);
                    return [4 /*yield*/, uploadedDocumentDataAccess.getDocuments()];
                case 1:
                    response = _a.sent();
                    setDocuments(response.documents);
                    setLoginUser(response.loginUser);
                    return [3 /*break*/, 4];
                case 2:
                    err_1 = _a.sent();
                    setError(err_1);
                    return [3 /*break*/, 4];
                case 3:
                    setIsLoading(false);
                    return [7 /*endfinally*/];
                case 4: return [2 /*return*/];
            }
        });
    }); };
    var onEditDoc = function (doc) {
        window.location.href = "document.html?docId=".concat(doc.id);
    };
    var onConfirmDeleteDoc = function (doc) {
        setDocToDelete(doc);
        setIsDeleteConfirmOpen(true);
    };
    var onDeleteDoc = function () { return __awaiter(void 0, void 0, void 0, function () {
        var err_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    setIsProcessing(true);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 4, 5, 6]);
                    return [4 /*yield*/, uploadedDocumentDataAccess.deleteDocument(docToDelete.id)];
                case 2:
                    _a.sent();
                    toaster.show({
                        message: "Document \"".concat(docToDelete.title, "\" (").concat(docToDelete.getFilenameWithSuffix(), ") was successfully deleted."),
                        intent: 'primary',
                        icon: 'small-tick'
                    });
                    return [4 /*yield*/, loadDocuments()];
                case 3:
                    _a.sent();
                    return [3 /*break*/, 6];
                case 4:
                    err_2 = _a.sent();
                    setError(err_2);
                    return [3 /*break*/, 6];
                case 5:
                    setIsDeleteConfirmOpen(false);
                    setIsProcessing(false);
                    setDocToDelete(null);
                    return [7 /*endfinally*/];
                case 6: return [2 /*return*/];
            }
        });
    }); };
    var onCancelDelete = function () {
        setIsDeleteConfirmOpen(false);
        setDocToDelete(null);
    };
    (0, react_1.useEffect)(function () {
        loadDocuments().then();
        var message = localStorageDataAccess.getToast();
        if (message) {
            toaster.show({
                message: message,
                intent: 'primary',
                icon: 'small-tick'
            });
        }
    }, []);
    return (react_1["default"].createElement(admin_page_1["default"], { assetsRoot: assetsRoot, error: error, clearError: function () { return setError(undefined); }, onLogin: onLogin, loginUser: loginUser },
        react_1["default"].createElement(center_card_layout_1["default"], null,
            react_1["default"].createElement(core_1.Card, { elevation: 1, className: classes.card },
                react_1["default"].createElement("div", { className: "".concat(classes.headerRow, " card-header") },
                    react_1["default"].createElement("h1", { className: classes.header }, "Documents"),
                    react_1["default"].createElement(core_1.AnchorButton, { icon: "document-open", text: "Add Document", href: "document.html" })),
                react_1["default"].createElement("div", { className: classes.body }, isLoading === true ? (react_1["default"].createElement("div", { className: classes.docRow },
                    react_1["default"].createElement("div", { className: "".concat(classes.avatar, " ").concat(core_1.Classes.SKELETON) },
                        react_1["default"].createElement(core_1.Icon, { icon: "document", color: "white" })),
                    react_1["default"].createElement("div", { className: classes.metaCol },
                        react_1["default"].createElement("div", { className: core_1.Classes.SKELETON }, "Sample Document Title"),
                        react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.SKELETON) }, "ID: sample-document-id"),
                        react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.SKELETON) }, "filename: sample-document-id.pdf (5MB)"),
                        react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.SKELETON) }, "created: Apr 25, 2022 8:34 PM")))) : (react_1["default"].createElement(react_1["default"].Fragment, null, documents.length === 0 ? (react_1["default"].createElement("div", null,
                    react_1["default"].createElement(core_1.NonIdealState, { icon: "folder-open", title: "No Documents", description: "No documents have been added yet.", className: classes.empty }))) : (react_1["default"].createElement(react_1["default"].Fragment, null, documents.map(function (doc) {
                    var docMenu = (react_1["default"].createElement(core_1.Menu, null,
                        react_1["default"].createElement(core_1.MenuItem, { icon: "edit", text: "Edit", onClick: function () { return onEditDoc(doc); } }),
                        react_1["default"].createElement(core_1.MenuItem, { icon: "remove", text: "Delete", onClick: function () { return onConfirmDeleteDoc(doc); } })));
                    return (react_1["default"].createElement("div", { className: classes.docRow, key: doc.id },
                        react_1["default"].createElement("div", { className: classes.avatar },
                            react_1["default"].createElement(core_1.Icon, { icon: "document", color: "white" })),
                        react_1["default"].createElement("div", { className: classes.metaCol },
                            react_1["default"].createElement("a", { href: "".concat(documentUri).concat(doc.getFilenameWithSuffix()), target: "_blank", rel: "noopener noreferrer" },
                                doc.title,
                                react_1["default"].createElement(core_1.Icon, { className: classes.linkIcon, icon: "share", size: 12 })),
                            react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                "ID: ",
                                doc.filename),
                            react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                doc.getFilenameWithSuffix(),
                                " (",
                                (0, bytes_formatter_1.formatBytes)(doc.size),
                                ")"),
                            react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                "created:",
                                ' ',
                                doc.uploadedTimestamp.format('MMM D, YYYY h:mm A'))),
                        react_1["default"].createElement(core_1.Popover, { placement: "bottom-end", content: docMenu },
                            react_1["default"].createElement(core_1.Button, { icon: "more", className: classes.more }))));
                })))))))),
        react_1["default"].createElement(confirm_dialog_1["default"], { isOpen: isDeleteConfirmOpen, isProcessing: isProcessing, title: "Confirm Delete Document", message: "Are you sure you want to delete the document \"".concat(docToDelete === null || docToDelete === void 0 ? void 0 : docToDelete.title, "\" (").concat(docToDelete === null || docToDelete === void 0 ? void 0 : docToDelete.getFilenameWithSuffix(), ")?"), action: "Delete Document", processingAction: "Deleting Document...", onConfirm: onDeleteDoc, onCancel: onCancelDelete })));
};
function init(target, assetsRoot) {
    ReactDOM.render(react_1["default"].createElement(Documents, { assetsRoot: assetsRoot }), document.getElementById(target));
}
exports.init = init;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	(() => {
/******/ 		// The chunk loading function for additional chunks
/******/ 		// Since all referenced chunks are already included
/******/ 		// in this file, this function is empty here.
/******/ 		__webpack_require__.e = () => (Promise.resolve());
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/harmony module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.hmd = (module) => {
/******/ 			module = Object.create(module);
/******/ 			if (!module.children) module.children = [];
/******/ 			Object.defineProperty(module, 'exports', {
/******/ 				enumerable: true,
/******/ 				set: () => {
/******/ 					throw new Error('ES Modules may not assign module.exports or exports.*, Use ESM export syntax, instead: ' + module.id);
/******/ 				}
/******/ 			});
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"churchAdminDocuments": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors","ui_src_common_global-constants_ts-ui_src_di-container_ts","ui_src_components_church-admin_dark-mode-context_ts-ui_src_services_data-access_ts-ui_src_ser-43f718","ui_src_components_church-admin_admin-page_tsx","ui_src_components_church-admin_center-card-layout_tsx-ui_src_services_uploaded-document-data--e4ccb5"], () => (__webpack_require__("./ui/src/pages/church-admin/documents.tsx")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	churchAdminDocuments = __webpack_exports__;
/******/ 	
/******/ })()
;