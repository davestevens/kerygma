var churchAdminUser;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./ui/src/components/church-admin/center-card-layout.styles.ts":
/*!*********************************************************************!*\
  !*** ./ui/src/components/church-admin/center-card-layout.styles.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var global_constants_1 = __webpack_require__(/*! ../../common/global-constants */ "./ui/src/common/global-constants.ts");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    centerContainer: "\n    display: flex;\n    justify-content: center;\n    padding: 0 16px;\n  ",
    centerContent: "\n    display: flex;\n    flex-direction: column;\n    max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ";\n    width: 100%;\n    margin-top: 30px;\n  "),
    centerContentBreadcrumb: "\n    margin-top: 16px;\n  ",
    breadcrumbs: "\n    padding: 0 0 8px 0;\n  "
});


/***/ }),

/***/ "./ui/src/components/church-admin/center-card-layout.tsx":
/*!***************************************************************!*\
  !*** ./ui/src/components/church-admin/center-card-layout.tsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var center_card_layout_styles_1 = __webpack_require__(/*! ./center-card-layout.styles */ "./ui/src/components/church-admin/center-card-layout.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var CenterCardLayout = function (_a) {
    var breadcrumbs = _a.breadcrumbs, children = _a.children;
    var classes = (0, center_card_layout_styles_1.useStyles)();
    return (React.createElement("div", { className: classes.centerContainer },
        React.createElement("div", { className: "\n          ".concat(classes.centerContent, "\n          ").concat(breadcrumbs && classes.centerContentBreadcrumb, "\n        ") },
            breadcrumbs && (React.createElement("div", { className: classes.breadcrumbs },
                React.createElement(core_1.Breadcrumbs, { items: breadcrumbs }))),
            children)));
};
exports["default"] = CenterCardLayout;


/***/ }),

/***/ "./ui/src/components/church-admin/page-form.styles.ts":
/*!************************************************************!*\
  !*** ./ui/src/components/church-admin/page-form.styles.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var global_constants_1 = __webpack_require__(/*! ../../common/global-constants */ "./ui/src/common/global-constants.ts");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    formContainer: "\n    display: flex;\n    flex-direction: column;\n    align-items: flex-start;\n    width: 100%;\n    max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ";\n  ")
});


/***/ }),

/***/ "./ui/src/components/church-admin/page-form.tsx":
/*!******************************************************!*\
  !*** ./ui/src/components/church-admin/page-form.tsx ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var page_form_styles_1 = __webpack_require__(/*! ./page-form.styles */ "./ui/src/components/church-admin/page-form.styles.ts");
var PageForm = function (_a) {
    var children = _a.children;
    var classes = (0, page_form_styles_1.useStyles)();
    return React.createElement("div", { className: classes.formContainer }, children);
};
exports["default"] = PageForm;


/***/ }),

/***/ "./ui/src/model/user-form.ts":
/*!***********************************!*\
  !*** ./ui/src/model/user-form.ts ***!
  \***********************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
exports.__esModule = true;
exports.UserForm = void 0;
var yup = __importStar(__webpack_require__(/*! yup */ "./ui/node_modules/yup/es/index.js"));
var user_1 = __webpack_require__(/*! ./user */ "./ui/src/model/user.ts");
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var passwordPattern = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).*$/;
var UserForm = /** @class */ (function (_super) {
    __extends(UserForm, _super);
    function UserForm() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.password = '';
        _this.passwordConfirm = '';
        _this.mode = 'new';
        // Field Validators
        _this.firstNameValidator = function () {
            return yup.string().trim().required().isValidSync(_this.firstName)
                ? ''
                : 'is required';
        };
        _this.lastNameValidator = function () {
            return yup.string().trim().required().isValidSync(_this.lastName)
                ? ''
                : 'is required';
        };
        _this.usernameValidator = function () {
            if (!yup.string().trim().required().isValidSync(_this.username)) {
                return 'is required';
            }
            return yup.string().min(8).isValidSync(_this.username)
                ? ''
                : 'must be at least 8 characters';
        };
        _this.passwordValidator = function () {
            if (_this.mode === 'edit' && !_this.isPasswordSet()) {
                // not required for edit mode
                return '';
            }
            if (!yup.string().trim().required().isValidSync(_this.password)) {
                return 'is required';
            }
            if (!yup.string().trim().min(8).isValidSync(_this.password)) {
                return 'must be at least 8 characters';
            }
            return yup
                .string()
                .trim()
                .matches(passwordPattern)
                .isValidSync(_this.password)
                ? ''
                : 'must contain uppercase, lowercase, and numeric characters';
        };
        _this.passwordConfirmValidator = function () {
            if (_this.mode === 'edit' && !_this.isPasswordSet()) {
                // not required for edit mode
                return '';
            }
            return _this.password === _this.passwordConfirm ? '' : 'does not match';
        };
        _this.authLevelValidator = function () {
            return _this.authLevel === 'admin' || _this.authLevel === 'sysadmin'
                ? ''
                : 'invalid auth level';
        };
        return _this;
    }
    UserForm_1 = UserForm;
    UserForm.prototype.isValid = function () {
        for (var key in this) {
            var validator = this[key];
            if (key.endsWith('Validator') &&
                typeof validator === 'function' &&
                validator() !== '') {
                return false;
            }
        }
        return true;
    };
    UserForm.prototype.isPasswordSet = function () {
        return this.password.trim().length > 0;
    };
    /*
     * This is necessary for the useState react hook to modify the state
     * of the form.
     */
    UserForm.prototype.clone = function () {
        var clone = new UserForm_1();
        for (var _i = 0, _a = Object.keys(this); _i < _a.length; _i++) {
            var key = _a[_i];
            if (typeof this[key] === 'string') {
                clone[key] = this[key];
            }
        }
        return clone;
    };
    var UserForm_1;
    __decorate([
        (0, json2typescript_1.JsonProperty)('password', String),
        __metadata("design:type", Object)
    ], UserForm.prototype, "password");
    __decorate([
        (0, json2typescript_1.JsonProperty)('passwordConfirm', String),
        __metadata("design:type", Object)
    ], UserForm.prototype, "passwordConfirm");
    UserForm = UserForm_1 = __decorate([
        (0, json2typescript_1.JsonObject)('UserForm')
    ], UserForm);
    return UserForm;
}(user_1.User));
exports.UserForm = UserForm;


/***/ }),

/***/ "./ui/src/pages/church-admin/user.styles.ts":
/*!**************************************************!*\
  !*** ./ui/src/pages/church-admin/user.styles.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    card: "\n    padding: 0;\n  ",
    headerRow: "\n    padding: 10px 16px;\n    border-bottom: 1px solid ".concat(core_1.Colors.LIGHT_GRAY1, ";\n    background-color: ").concat(core_1.Colors.LIGHT_GRAY4, ";\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n  "),
    header: "\n    text-align: left;\n    padding: 0;\n  ",
    body: "\n    padding: 16px;\n  ",
    errorMessage: "\n    font-size: 12px;\n    color: ".concat(core_1.Colors.RED3, ";\n    margin: -8px 0 8px 0;\n  "),
    buttonRow: "\n    display: flex;\n  ",
    buttonSpaced: "\n    margin-right: 16px;\n  "
});


/***/ }),

/***/ "./ui/src/pages/church-admin/user.tsx":
/*!********************************************!*\
  !*** ./ui/src/pages/church-admin/user.tsx ***!
  \********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.init = void 0;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var react_1 = __webpack_require__(/*! react */ "./ui/node_modules/react/index.js");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var admin_page_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/admin-page */ "./ui/src/components/church-admin/admin-page.tsx"));
var page_form_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/page-form */ "./ui/src/components/church-admin/page-form.tsx"));
var user_form_1 = __webpack_require__(/*! ../../model/user-form */ "./ui/src/model/user-form.ts");
var center_card_layout_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/center-card-layout */ "./ui/src/components/church-admin/center-card-layout.tsx"));
var validation_form_group_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/validation-form-group */ "./ui/src/components/church-admin/validation-form-group.tsx"));
var di_container_1 = __importDefault(__webpack_require__(/*! ../../di-container */ "./ui/src/di-container.ts"));
var user_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/user-data-access */ "./ui/src/services/user-data-access.ts"));
var http_status_codes_1 = __webpack_require__(/*! http-status-codes */ "./ui/node_modules/http-status-codes/build/es/index.js");
var local_storage_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/local-storage-data-access */ "./ui/src/services/local-storage-data-access.ts"));
var user_styles_1 = __webpack_require__(/*! ./user.styles */ "./ui/src/pages/church-admin/user.styles.ts");
var ReactDOM = __importStar(__webpack_require__(/*! react-dom */ "./ui/node_modules/react-dom/index.js"));
var userId = new URLSearchParams(window.location.search).get('userId');
var mode = userId ? 'edit' : 'new';
var title = "".concat(mode === 'new' ? 'Add' : 'Edit', " User");
var userDataAccess = di_container_1["default"].resolve(user_data_access_1["default"]);
var localStorageDataAccess = di_container_1["default"].resolve(local_storage_data_access_1["default"]);
var breadcrumbs = [
    { href: '/church-admin/users.html', icon: 'people', text: 'Users' },
    { icon: mode === 'new' ? 'new-person' : 'edit', text: title },
];
var User = function (_a) {
    var assetsRoot = _a.assetsRoot;
    var _b = React.useState(new user_form_1.UserForm()), userForm = _b[0], setUserForm = _b[1];
    var _c = React.useState(''), usernameError = _c[0], setUsernameError = _c[1];
    var _d = React.useState(), error = _d[0], setError = _d[1];
    var _e = React.useState(false), isProcessing = _e[0], setIsProcessing = _e[1];
    var _f = React.useState(true), isLoading = _f[0], setIsLoading = _f[1];
    var _g = (0, react_1.useState)(), loginUser = _g[0], setLoginUser = _g[1];
    var classes = (0, user_styles_1.useStyles)();
    var handleAuthLevelChange = function (event) {
        if (event.currentTarget.value === 'sysadmin') {
            userForm.authLevel = 'sysadmin';
        }
        else {
            userForm.authLevel = 'admin';
        }
        setUserForm(userForm.clone());
    };
    var onFieldChange = function (id, newValue) {
        userForm[id] = newValue;
        setUserForm(userForm.clone());
    };
    var handleSave = function () { return __awaiter(void 0, void 0, void 0, function () {
        var response, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    setUsernameError('');
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 6, 7, 8]);
                    setIsProcessing(true);
                    response = void 0;
                    if (!(mode === 'new')) return [3 /*break*/, 3];
                    return [4 /*yield*/, userDataAccess.addUser(userForm)];
                case 2:
                    response = _a.sent();
                    return [3 /*break*/, 5];
                case 3: return [4 /*yield*/, userDataAccess.saveUser(userForm)];
                case 4:
                    response = _a.sent();
                    _a.label = 5;
                case 5:
                    if (response.status === http_status_codes_1.StatusCodes.CONFLICT) {
                        setUsernameError('Userid already exists');
                    }
                    else {
                        localStorageDataAccess.setToast("User ".concat(userForm.firstName, " ").concat(userForm.lastName, " (").concat(userForm.username, ") was successfully ").concat(mode === 'edit' ? 'saved' : 'added', "."));
                        window.location.href = 'users.html';
                    }
                    return [3 /*break*/, 8];
                case 6:
                    err_1 = _a.sent();
                    setError(err_1);
                    return [3 /*break*/, 8];
                case 7:
                    setIsProcessing(false);
                    return [7 /*endfinally*/];
                case 8: return [2 /*return*/];
            }
        });
    }); };
    var clearError = function () {
        setError(undefined);
    };
    var preLoadForm = function () { return __awaiter(void 0, void 0, void 0, function () {
        var _a, user, loginUser_1, userForm_1, err_2;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, 3, 4]);
                    return [4 /*yield*/, userDataAccess.getUser(userId)];
                case 1:
                    _a = _b.sent(), user = _a.user, loginUser_1 = _a.loginUser;
                    userForm_1 = new user_form_1.UserForm();
                    userForm_1.mode = 'edit';
                    userForm_1.firstName = user.firstName;
                    userForm_1.lastName = user.lastName;
                    userForm_1.id = userId;
                    userForm_1.username = user.username;
                    userForm_1.authLevel = user.authLevel;
                    setUserForm(userForm_1);
                    setLoginUser(loginUser_1);
                    return [3 /*break*/, 4];
                case 2:
                    err_2 = _b.sent();
                    setError(err_2);
                    return [3 /*break*/, 4];
                case 3:
                    setIsLoading(false);
                    return [7 /*endfinally*/];
                case 4: return [2 /*return*/];
            }
        });
    }); };
    var verifyLogin = function () { return __awaiter(void 0, void 0, void 0, function () {
        var user, err_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, 3, 4]);
                    return [4 /*yield*/, userDataAccess.getUserLoggedIn()];
                case 1:
                    user = _a.sent();
                    setLoginUser(user);
                    return [3 /*break*/, 4];
                case 2:
                    err_3 = _a.sent();
                    setError(err_3);
                    return [3 /*break*/, 4];
                case 3:
                    setIsLoading(false);
                    return [7 /*endfinally*/];
                case 4: return [2 /*return*/];
            }
        });
    }); };
    React.useEffect(function () {
        if (mode === 'edit') {
            preLoadForm().then();
        }
        else {
            verifyLogin().then();
        }
    }, []);
    var actionLabel = mode === 'edit' ? 'Save' : 'Add User';
    var processingLabel = mode === 'edit' ? 'Saving...' : 'Adding User...';
    var actionIcon = mode === 'edit' ? 'small-tick' : 'plus';
    return (React.createElement(admin_page_1["default"], { assetsRoot: assetsRoot, error: error, clearError: clearError, loginUser: loginUser },
        React.createElement(center_card_layout_1["default"], { breadcrumbs: breadcrumbs },
            React.createElement(core_1.Card, { elevation: 1, className: classes.card },
                React.createElement("div", { className: "".concat(classes.headerRow, " card-header") },
                    React.createElement("h1", { className: classes.header }, title)),
                React.createElement("div", { className: classes.body },
                    React.createElement(page_form_1["default"], null,
                        React.createElement(validation_form_group_1["default"], { id: "firstName", value: userForm.firstName, validator: function () {
                                return userForm.firstNameValidator();
                            }, setter: onFieldChange, label: "First Name", labelInfo: "(required)", isLoading: isLoading }),
                        React.createElement(validation_form_group_1["default"], { id: "lastName", value: userForm.lastName, validator: function () {
                                return userForm.lastNameValidator();
                            }, setter: onFieldChange, label: "Last Name", labelInfo: "(required)", isLoading: isLoading }),
                        React.createElement(validation_form_group_1["default"], { id: "username", value: userForm.username, validator: function () {
                                return userForm.usernameValidator();
                            }, setter: onFieldChange, label: "Username", labelInfo: "(required)", helperText: "A valid email address.", isLoading: isLoading }),
                        usernameError && (React.createElement("div", { className: classes.errorMessage }, usernameError)),
                        React.createElement(validation_form_group_1["default"], { id: "password", value: userForm.password, validator: function () {
                                return userForm.passwordValidator();
                            }, setter: onFieldChange, label: "Password", labelInfo: mode === 'new' ? '(required)' : null, helperText: "Must be at least 8 characters and contain uppercase, lowercase and numeric characters.", inputType: "password", isLoading: isLoading }),
                        React.createElement(validation_form_group_1["default"], { id: "passwordConfirm", value: userForm.passwordConfirm, validator: function () {
                                return userForm.passwordConfirmValidator();
                            }, setter: onFieldChange, label: "Confirm Password", labelInfo: mode === 'new' ? '(required)' : null, inputType: "password", isLoading: isLoading }),
                        React.createElement(core_1.FormGroup, { label: "Authorization Level" },
                            React.createElement(core_1.RadioGroup, { onChange: handleAuthLevelChange, inline: true, selectedValue: userForm.authLevel, name: "auth-level", className: isLoading ? core_1.Classes.SKELETON : '' },
                                React.createElement(core_1.Radio, { label: "Admin", value: "admin" }),
                                React.createElement(core_1.Radio, { label: "System Admin", value: "sysadmin" }))),
                        React.createElement("div", { className: classes.buttonRow },
                            React.createElement(core_1.Button, { icon: actionIcon, text: isProcessing ? processingLabel : actionLabel, className: classes.buttonSpaced, onClick: handleSave, disabled: !userForm.isValid() || isProcessing }),
                            React.createElement(core_1.AnchorButton, { icon: "small-cross", text: "Cancel", href: "/church-admin/users.html" }))))))));
};
function init(target, assetsRoot) {
    ReactDOM.render(React.createElement(User, { assetsRoot: assetsRoot }), document.getElementById(target));
}
exports.init = init;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	(() => {
/******/ 		// The chunk loading function for additional chunks
/******/ 		// Since all referenced chunks are already included
/******/ 		// in this file, this function is empty here.
/******/ 		__webpack_require__.e = () => (Promise.resolve());
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/harmony module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.hmd = (module) => {
/******/ 			module = Object.create(module);
/******/ 			if (!module.children) module.children = [];
/******/ 			Object.defineProperty(module, 'exports', {
/******/ 				enumerable: true,
/******/ 				set: () => {
/******/ 					throw new Error('ES Modules may not assign module.exports or exports.*, Use ESM export syntax, instead: ' + module.id);
/******/ 				}
/******/ 			});
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"churchAdminUser": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors","ui_src_common_global-constants_ts-ui_src_di-container_ts","ui_src_components_church-admin_dark-mode-context_ts-ui_src_services_data-access_ts-ui_src_ser-43f718","ui_src_components_church-admin_admin-page_tsx","ui_src_services_user-data-access_ts"], () => (__webpack_require__("./ui/src/pages/church-admin/user.tsx")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	churchAdminUser = __webpack_exports__;
/******/ 	
/******/ })()
;