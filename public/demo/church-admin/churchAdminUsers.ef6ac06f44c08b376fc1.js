var churchAdminUsers;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./ui/src/components/church-admin/center-card-layout.styles.ts":
/*!*********************************************************************!*\
  !*** ./ui/src/components/church-admin/center-card-layout.styles.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var global_constants_1 = __webpack_require__(/*! ../../common/global-constants */ "./ui/src/common/global-constants.ts");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    centerContainer: "\n    display: flex;\n    justify-content: center;\n    padding: 0 16px;\n  ",
    centerContent: "\n    display: flex;\n    flex-direction: column;\n    max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ";\n    width: 100%;\n    margin-top: 30px;\n  "),
    centerContentBreadcrumb: "\n    margin-top: 16px;\n  ",
    breadcrumbs: "\n    padding: 0 0 8px 0;\n  "
});


/***/ }),

/***/ "./ui/src/components/church-admin/center-card-layout.tsx":
/*!***************************************************************!*\
  !*** ./ui/src/components/church-admin/center-card-layout.tsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var center_card_layout_styles_1 = __webpack_require__(/*! ./center-card-layout.styles */ "./ui/src/components/church-admin/center-card-layout.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var CenterCardLayout = function (_a) {
    var breadcrumbs = _a.breadcrumbs, children = _a.children;
    var classes = (0, center_card_layout_styles_1.useStyles)();
    return (React.createElement("div", { className: classes.centerContainer },
        React.createElement("div", { className: "\n          ".concat(classes.centerContent, "\n          ").concat(breadcrumbs && classes.centerContentBreadcrumb, "\n        ") },
            breadcrumbs && (React.createElement("div", { className: classes.breadcrumbs },
                React.createElement(core_1.Breadcrumbs, { items: breadcrumbs }))),
            children)));
};
exports["default"] = CenterCardLayout;


/***/ }),

/***/ "./ui/src/components/church-admin/confirm-dialog.tsx":
/*!***********************************************************!*\
  !*** ./ui/src/components/church-admin/confirm-dialog.tsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ./dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var ConfirmDialog = function (_a) {
    var isOpen = _a.isOpen, isProcessing = _a.isProcessing, title = _a.title, message = _a.message, action = _a.action, processingAction = _a.processingAction, onConfirm = _a.onConfirm, onCancel = _a.onCancel;
    var isDarkMode = React.useContext(dark_mode_context_1["default"]);
    return (React.createElement(core_1.Dialog, { isOpen: isOpen, title: title, className: isDarkMode ? core_1.Classes.DARK : '', onClose: onCancel },
        React.createElement("div", { className: core_1.Classes.DIALOG_BODY },
            React.createElement("p", { className: core_1.Classes.RUNNING_TEXT }, message)),
        React.createElement("div", { className: core_1.Classes.DIALOG_FOOTER },
            React.createElement("div", { className: core_1.Classes.DIALOG_FOOTER_ACTIONS },
                React.createElement(core_1.Button, { onClick: onCancel, disabled: isProcessing === true }, "Cancel"),
                React.createElement(core_1.Button, { onClick: onConfirm, disabled: isProcessing === true, intent: "primary" }, isProcessing === true ? processingAction : action)))));
};
exports["default"] = ConfirmDialog;


/***/ }),

/***/ "./ui/src/pages/church-admin/users.styles.ts":
/*!***************************************************!*\
  !*** ./ui/src/pages/church-admin/users.styles.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    card: "\n    padding: 0;\n  ",
    headerRow: "\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    padding: 10px 16px;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n  ",
    header: "\n    text-align: left;\n    padding: 0;\n    width: auto;\n  ",
    body: "\n    padding: 16px;\n  ",
    userRow: {
        display: 'flex',
        alignItems: 'center',
        marginBottom: '16px',
        '&:last-child': "\n      margin-bottom: 0;\n    "
    },
    avatar: "\n    background-color: ".concat(core_1.Colors.GRAY3, ";\n    padding: 6px;\n    border-radius: 2px;\n    margin-right: 16px;\n  "),
    nameCol: "\n    display: flex;\n    flex-direction: column;\n    align-items: flex-start;\n  ",
    email: "\n    font-size: 12px;\n    margin-top: 2px;\n  ",
    tag: "\n    margin-left: auto;\n  ",
    more: "\n    margin-left: 8px;\n  ",
    subText: "\n    font-size: 12px;\n  "
});


/***/ }),

/***/ "./ui/src/pages/church-admin/users.tsx":
/*!*********************************************!*\
  !*** ./ui/src/pages/church-admin/users.tsx ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.init = void 0;
var react_1 = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var admin_page_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/admin-page */ "./ui/src/components/church-admin/admin-page.tsx"));
var center_card_layout_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/center-card-layout */ "./ui/src/components/church-admin/center-card-layout.tsx"));
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var user_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/user-data-access */ "./ui/src/services/user-data-access.ts"));
var di_container_1 = __importDefault(__webpack_require__(/*! ../../di-container */ "./ui/src/di-container.ts"));
var local_storage_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/local-storage-data-access */ "./ui/src/services/local-storage-data-access.ts"));
var confirm_dialog_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/confirm-dialog */ "./ui/src/components/church-admin/confirm-dialog.tsx"));
var ReactDOM = __importStar(__webpack_require__(/*! react-dom */ "./ui/node_modules/react-dom/index.js"));
var users_styles_1 = __webpack_require__(/*! ./users.styles */ "./ui/src/pages/church-admin/users.styles.ts");
var toaster = core_1.Toaster.create({ position: 'top' });
var userDataAccess = di_container_1["default"].resolve(user_data_access_1["default"]);
var localStorageDataAccess = di_container_1["default"].resolve(local_storage_data_access_1["default"]);
var Users = function (_a) {
    var assetsRoot = _a.assetsRoot;
    var _b = (0, react_1.useState)(true), isLoading = _b[0], setIsLoading = _b[1];
    var _c = (0, react_1.useState)([]), users = _c[0], setUsers = _c[1];
    var _d = (0, react_1.useState)(), loginUser = _d[0], setLoginUser = _d[1];
    var _e = react_1["default"].useState(), error = _e[0], setError = _e[1];
    var _f = (0, react_1.useState)(false), isDeleteConfirmOpen = _f[0], setIsDeleteConfirmOpen = _f[1];
    var _g = react_1["default"].useState(null), userToDelete = _g[0], setUserToDelete = _g[1];
    var _h = (0, react_1.useState)(false), isProcessing = _h[0], setIsProcessing = _h[1];
    var classes = (0, users_styles_1.useStyles)();
    var loadUsers = function () { return __awaiter(void 0, void 0, void 0, function () {
        var response, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, 3, 4]);
                    return [4 /*yield*/, userDataAccess.getUsers()];
                case 1:
                    response = _a.sent();
                    setUsers(response.users);
                    setLoginUser(response.loginUser);
                    return [3 /*break*/, 4];
                case 2:
                    err_1 = _a.sent();
                    setError(err_1);
                    return [3 /*break*/, 4];
                case 3:
                    setIsLoading(false);
                    return [7 /*endfinally*/];
                case 4: return [2 /*return*/];
            }
        });
    }); };
    var clearError = function () {
        setError(undefined);
    };
    var onLogin = function () {
        setError(undefined);
        loadUsers().then();
    };
    var onEditUser = function (user) {
        window.location.href = "user.html?userId=".concat(user.id);
    };
    var onConfirmDeleteUser = function (user) {
        setUserToDelete(user);
        setIsDeleteConfirmOpen(true);
    };
    var onDeleteUser = function () { return __awaiter(void 0, void 0, void 0, function () {
        var err_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    setIsProcessing(true);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 4, 5, 6]);
                    return [4 /*yield*/, userDataAccess.deleteUser(userToDelete.id)];
                case 2:
                    _a.sent();
                    toaster.show({
                        message: "User ".concat(userToDelete.firstName, " ").concat(userToDelete.lastName, " (").concat(userToDelete.username, ") was successfully deleted."),
                        intent: 'primary',
                        icon: 'small-tick'
                    });
                    return [4 /*yield*/, loadUsers()];
                case 3:
                    _a.sent();
                    return [3 /*break*/, 6];
                case 4:
                    err_2 = _a.sent();
                    setError(err_2);
                    return [3 /*break*/, 6];
                case 5:
                    setIsDeleteConfirmOpen(false);
                    setIsProcessing(false);
                    setUserToDelete(null);
                    return [7 /*endfinally*/];
                case 6: return [2 /*return*/];
            }
        });
    }); };
    var onCancelDelete = function () {
        setIsDeleteConfirmOpen(false);
        setUserToDelete(null);
    };
    (0, react_1.useEffect)(function () {
        loadUsers().then();
        var message = localStorageDataAccess.getToast();
        if (message) {
            toaster.show({
                message: message,
                intent: 'primary',
                icon: 'small-tick'
            });
        }
    }, []);
    return (react_1["default"].createElement(admin_page_1["default"], { assetsRoot: assetsRoot, error: error, clearError: clearError, onLogin: onLogin, loginUser: loginUser },
        react_1["default"].createElement(center_card_layout_1["default"], null,
            react_1["default"].createElement(core_1.Card, { elevation: 1, className: classes.card },
                react_1["default"].createElement("div", { className: "".concat(classes.headerRow, " card-header") },
                    react_1["default"].createElement("h1", { className: classes.header }, "Users"),
                    react_1["default"].createElement(core_1.AnchorButton, { icon: "new-person", text: "Add User", href: "user.html" })),
                react_1["default"].createElement("div", { className: classes.body }, isLoading ? (react_1["default"].createElement("div", { className: classes.userRow },
                    react_1["default"].createElement("div", { className: "".concat(classes.avatar, " ").concat(core_1.Classes.SKELETON) },
                        react_1["default"].createElement(core_1.Icon, { icon: "person", color: "white" })),
                    react_1["default"].createElement("div", { className: classes.nameCol },
                        react_1["default"].createElement("div", { className: core_1.Classes.SKELETON }, "Sample Name Goes Here"),
                        react_1["default"].createElement("div", { className: "".concat(classes.email, " ").concat(core_1.Classes.SKELETON) }, "sample.email@some-domain.com")),
                    react_1["default"].createElement(core_1.Tag, { round: true, className: "".concat(classes.tag, " ").concat(core_1.Classes.SKELETON) }, "Admin"),
                    react_1["default"].createElement(core_1.Button, { icon: "more", className: "".concat(classes.more, " ").concat(core_1.Classes.SKELETON) }))) : (react_1["default"].createElement(react_1["default"].Fragment, null, users.map(function (user) {
                    var userMenu = (react_1["default"].createElement(core_1.Menu, null,
                        react_1["default"].createElement(core_1.MenuItem, { icon: "edit", text: "Edit", onClick: function () { return onEditUser(user); } }),
                        (user === null || user === void 0 ? void 0 : user.id) !== (loginUser === null || loginUser === void 0 ? void 0 : loginUser.id) && (react_1["default"].createElement(core_1.MenuItem, { icon: "remove", text: "Delete", onClick: function () { return onConfirmDeleteUser(user); } }))));
                    return (react_1["default"].createElement("div", { className: classes.userRow, key: user.id },
                        react_1["default"].createElement("div", { className: classes.avatar },
                            react_1["default"].createElement(core_1.Icon, { icon: "person", color: "white" })),
                        react_1["default"].createElement("div", { className: classes.nameCol },
                            react_1["default"].createElement("div", null,
                                user.firstName,
                                " ",
                                user.lastName,
                                (user === null || user === void 0 ? void 0 : user.id) === (loginUser === null || loginUser === void 0 ? void 0 : loginUser.id) && (react_1["default"].createElement("span", { className: "".concat(classes.subText, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    ' ',
                                    "(logged in as)"))),
                            react_1["default"].createElement("div", { className: "".concat(classes.email, " ").concat(core_1.Classes.TEXT_MUTED) }, user.username)),
                        react_1["default"].createElement(core_1.Tag, { round: true, className: classes.tag }, user.authLevel === 'sysadmin'
                            ? 'System Admin'
                            : 'Admin'),
                        react_1["default"].createElement(core_1.Popover, { placement: "bottom-end", content: userMenu },
                            react_1["default"].createElement(core_1.Button, { icon: "more", className: classes.more }))));
                })))))),
        react_1["default"].createElement(confirm_dialog_1["default"], { isOpen: isDeleteConfirmOpen, isProcessing: isProcessing, title: "Confirm Delete User", message: "Are you sure you want to delete user ".concat(userToDelete === null || userToDelete === void 0 ? void 0 : userToDelete.firstName, " ").concat(userToDelete === null || userToDelete === void 0 ? void 0 : userToDelete.lastName, " (").concat(userToDelete === null || userToDelete === void 0 ? void 0 : userToDelete.username, ")?"), action: "Delete User", processingAction: "Deleting User...", onConfirm: onDeleteUser, onCancel: onCancelDelete })));
};
function init(target, assetsRoot) {
    ReactDOM.render(react_1["default"].createElement(Users, { assetsRoot: assetsRoot }), document.getElementById(target));
}
exports.init = init;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	(() => {
/******/ 		// The chunk loading function for additional chunks
/******/ 		// Since all referenced chunks are already included
/******/ 		// in this file, this function is empty here.
/******/ 		__webpack_require__.e = () => (Promise.resolve());
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/harmony module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.hmd = (module) => {
/******/ 			module = Object.create(module);
/******/ 			if (!module.children) module.children = [];
/******/ 			Object.defineProperty(module, 'exports', {
/******/ 				enumerable: true,
/******/ 				set: () => {
/******/ 					throw new Error('ES Modules may not assign module.exports or exports.*, Use ESM export syntax, instead: ' + module.id);
/******/ 				}
/******/ 			});
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"churchAdminUsers": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors","ui_src_common_global-constants_ts-ui_src_di-container_ts","ui_src_components_church-admin_dark-mode-context_ts-ui_src_services_data-access_ts-ui_src_ser-43f718","ui_src_components_church-admin_admin-page_tsx","ui_src_services_user-data-access_ts"], () => (__webpack_require__("./ui/src/pages/church-admin/users.tsx")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	churchAdminUsers = __webpack_exports__;
/******/ 	
/******/ })()
;