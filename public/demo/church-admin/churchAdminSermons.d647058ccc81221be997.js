var churchAdminSermons;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./ui/src/components/church-admin/confirm-dialog.tsx":
/*!***********************************************************!*\
  !*** ./ui/src/components/church-admin/confirm-dialog.tsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ./dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var ConfirmDialog = function (_a) {
    var isOpen = _a.isOpen, isProcessing = _a.isProcessing, title = _a.title, message = _a.message, action = _a.action, processingAction = _a.processingAction, onConfirm = _a.onConfirm, onCancel = _a.onCancel;
    var isDarkMode = React.useContext(dark_mode_context_1["default"]);
    return (React.createElement(core_1.Dialog, { isOpen: isOpen, title: title, className: isDarkMode ? core_1.Classes.DARK : '', onClose: onCancel },
        React.createElement("div", { className: core_1.Classes.DIALOG_BODY },
            React.createElement("p", { className: core_1.Classes.RUNNING_TEXT }, message)),
        React.createElement("div", { className: core_1.Classes.DIALOG_FOOTER },
            React.createElement("div", { className: core_1.Classes.DIALOG_FOOTER_ACTIONS },
                React.createElement(core_1.Button, { onClick: onCancel, disabled: isProcessing === true }, "Cancel"),
                React.createElement(core_1.Button, { onClick: onConfirm, disabled: isProcessing === true, intent: "primary" }, isProcessing === true ? processingAction : action)))));
};
exports["default"] = ConfirmDialog;


/***/ }),

/***/ "./ui/src/components/church-admin/tag-dialog.styles.ts":
/*!*************************************************************!*\
  !*** ./ui/src/components/church-admin/tag-dialog.styles.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    tagContainer: "\n    display: flex;\n    flex-direction: column;\n    padding: 16px 0;\n  ",
    tagRow: "\n    display: flex;\n    align-items: center;\n    padding: 8px;\n    font-size: 14px;\n  ",
    tagSelect: "\n    display: flex;\n    margin-bottom: 0;\n  ",
    tagCount: "\n    margin-left: auto;\n    text-align: right;\n  ",
    tagCountLabel: "\n    margin-left: 4px;\n  "
});


/***/ }),

/***/ "./ui/src/components/church-admin/tag-dialog.tsx":
/*!*******************************************************!*\
  !*** ./ui/src/components/church-admin/tag-dialog.tsx ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var react_1 = __importDefault(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ./dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var tag_dialog_styles_1 = __webpack_require__(/*! ./tag-dialog.styles */ "./ui/src/components/church-admin/tag-dialog.styles.ts");
var TagDialog = function (_a) {
    var tags = _a.tags, isOpen = _a.isOpen, onCancel = _a.onCancel, onApply = _a.onApply, onToggleTag = _a.onToggleTag;
    var classes = (0, tag_dialog_styles_1.useStyles)();
    return (react_1["default"].createElement(dark_mode_context_1["default"].Consumer, null, function (isDarkMode) { return (react_1["default"].createElement(core_1.Dialog, { isOpen: isOpen, title: "Filter by Tag", className: isDarkMode ? core_1.Classes.DARK : '', onClose: onCancel },
        react_1["default"].createElement(core_1.DialogBody, { useOverflowScrollContainer: true }, tags.map(function (tag, i) { return (react_1["default"].createElement("div", { key: i, className: classes.tagRow },
            react_1["default"].createElement(core_1.Checkbox, { className: classes.tagSelect, inline: true, checked: tag.isSelected, onChange: function () { return onToggleTag(i); } }, tag.tag),
            react_1["default"].createElement("div", { className: classes.tagCount },
                tag.sermonCount,
                react_1["default"].createElement("span", { className: "".concat(classes.tagCountLabel, " bp5-text-muted bp5-text-small") }, "sermons")))); })),
        react_1["default"].createElement(core_1.DialogFooter, { actions: react_1["default"].createElement(react_1["default"].Fragment, null,
                react_1["default"].createElement(core_1.Button, { onClick: function () { return onCancel(); } }, "Cancel"),
                react_1["default"].createElement(core_1.Button, { onClick: function () { return onApply(); }, intent: "primary" }, "Apply")) }))); }));
};
exports["default"] = TagDialog;


/***/ }),

/***/ "./ui/src/components/list-pager.styles.ts":
/*!************************************************!*\
  !*** ./ui/src/components/list-pager.styles.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    buttonRow: "\n    display: flex;\n    justify-content: flex-end;\n    align-items: center;\n  ",
    label: "\n    font-size: 12px;\n    margin-right: 8px;\n  ",
    range: "\n    font-size: 12px;\n    margin: 0 4px 0 8px;\n  ",
    button: "\n    margin-left: 4px;\n  "
});


/***/ }),

/***/ "./ui/src/components/list-pager.tsx":
/*!******************************************!*\
  !*** ./ui/src/components/list-pager.tsx ***!
  \******************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
exports.perPageOptions = void 0;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var list_pager_styles_1 = __webpack_require__(/*! ./list-pager.styles */ "./ui/src/components/list-pager.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_1 = __webpack_require__(/*! react */ "./ui/node_modules/react/index.js");
exports.perPageOptions = [20, 50, 100];
var ListPager = function (props) {
    var totalRecords = props.totalRecords, onChange = props.onChange;
    var isMounted = (0, react_1.useRef)(false);
    var classes = (0, list_pager_styles_1.useStyles)();
    var _a = React.useState(props.page), page = _a[0], setPage = _a[1];
    var _b = React.useState(props.perPage), perPage = _b[0], setPerPage = _b[1];
    var onChangePerPage = function (event) {
        var perPage = parseInt(event.currentTarget.value);
        setPerPage(perPage);
        setPage(0);
    };
    var calcRange = function () {
        if (totalRecords === 0) {
            return 'no records';
        }
        var from = page * perPage + 1;
        var thru = Math.min(page * perPage + perPage, totalRecords);
        return "".concat(from, " - ").concat(thru, " of ").concat(totalRecords);
    };
    var onPrev = function () {
        if (page > 0) {
            setPage(page - 1);
        }
    };
    var maxPage = function () {
        return Math.ceil(totalRecords / perPage - 1);
    };
    var onNext = function () {
        if (page < maxPage()) {
            setPage(page + 1);
        }
    };
    var onFirst = function () {
        setPage(0);
    };
    var onLast = function () {
        setPage(maxPage());
    };
    (0, react_1.useEffect)(function () {
        // Using isMounted ref to not execute the onChange callback
        // when first mounting the component.
        if (isMounted.current) {
            onChange(page, perPage);
        }
        else {
            isMounted.current = true;
        }
    }, [page, perPage]);
    return (React.createElement("div", { className: classes.buttonRow },
        React.createElement("label", { className: "".concat(classes.label, " ").concat(core_1.Classes.TEXT_MUTED) }, "Items per page:"),
        React.createElement(core_1.HTMLSelect, { onChange: onChangePerPage, options: exports.perPageOptions, value: perPage }),
        React.createElement("span", { className: "".concat(classes.range, " ").concat(core_1.Classes.TEXT_MUTED) }, calcRange()),
        React.createElement(core_1.Button, { icon: "chevron-backward", className: classes.button, onClick: onFirst, disabled: page === 0 }),
        React.createElement(core_1.Button, { icon: "chevron-left", className: classes.button, onClick: onPrev, disabled: page === 0 }),
        React.createElement(core_1.Button, { icon: "chevron-right", className: classes.button, onClick: onNext, disabled: page >= maxPage() }),
        React.createElement(core_1.Button, { icon: "chevron-forward", className: classes.button, onClick: onLast, disabled: page >= maxPage() })));
};
exports["default"] = ListPager;


/***/ }),

/***/ "./ui/src/pages/church-admin/sermons.styles.ts":
/*!*****************************************************!*\
  !*** ./ui/src/pages/church-admin/sermons.styles.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var global_constants_1 = __webpack_require__(/*! ../../common/global-constants */ "./ui/src/common/global-constants.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    centerContainer: "\n    display: flex;\n    justify-content: center;\n    padding: 0 16px;\n  ",
    card: "\n    padding: 0;\n    max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ";\n    width: 100%;\n    margin-top: 30px;\n  "),
    cardSide: "\n    padding: 0;\n    max-width: 300px;\n    width: 100%;\n    margin: 30px 0 0 16px;\n  ",
    headerRow: "\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    padding: 10px 16px;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n  ",
    searchRow: "\n    display: flex;\n    padding: 4px 16px;\n    border-top: none !important;\n  ",
    tagFilterRow: "\n    display: flex;\n    padding: 4px 16px;\n    border-top: none !important;\n    align-items: center;\n  ",
    clearTags: "\n    margin-left: auto;\n  ",
    searchInput: "\n    flex: 1;\n  ",
    sort: "\n    margin: 0 0 0 8px;\n  ",
    header: "\n    text-align: left;\n    padding: 0;\n    width: auto;\n  ",
    empty: {
        margin: '40px 0',
        '& h4': "\n      margin-bottom: 20px;\n    "
    },
    body: "\n    padding: 16px;\n  ",
    docRow: {
        display: 'flex',
        alignItems: 'flex-start',
        marginBottom: '16px',
        '&:last-child': "\n      margin-bottom: 0;\n    "
    },
    avatar: "\n    display: flex;\n    background-color: ".concat(core_1.Colors.GRAY3, ";\n    padding: 6px;\n    border-radius: 2px;\n    margin: 4px 16px 0 0;\n  "),
    metaCol: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        marginRight: 'auto',
        flex: 1,
        '& a': "\n      display: flex;\n      align-items: center;\n    "
    },
    linkIcon: "\n    margin-left: 4px;\n    padding-bottom: 1px;\n  ",
    metaLine: "\n    display: flex;\n    font-size: 12px;\n    margin-top: 2px;\n  ",
    tagLine: "\n    display: flex;\n    margin-top: 4px;\n    align-items: center;\n  ",
    tag: "\n    margin-left: 4px;\n  ",
    more: "\n    margin: 4px 0 0 8px;\n  ",
    playLine: "\n    display: flex;\n    width: 100%;\n    font-size: 12px;\n    align-items: center;\n    margin-top: 2px;\n  ",
    playButton: "\n    margin-right: 4px;\n  ",
    sermonDate: "\n    white-space: nowrap;\n  ",
    time: "\n    white-space: nowrap;\n    margin: 0 8px;\n  ",
    tagContainer: "\n    display: flex;\n    flex-direction: column;\n    padding: 16px 0;\n  ",
    tagRow: "\n    display: flex;\n    align-items: center;\n    padding: 8px;\n  ",
    tagSelect: "\n    display: flex;\n    margin-bottom: 0;\n  ",
    tagCount: "\n    margin-left: auto;\n    text-align: right;\n  ",
    tagCountLabel: "\n    margin-left: 4px;\n  "
});


/***/ }),

/***/ "./ui/src/pages/church-admin/sermons.tsx":
/*!***********************************************!*\
  !*** ./ui/src/pages/church-admin/sermons.tsx ***!
  \***********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.init = void 0;
var react_1 = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var admin_page_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/admin-page */ "./ui/src/components/church-admin/admin-page.tsx"));
var sermons_styles_1 = __webpack_require__(/*! ./sermons.styles */ "./ui/src/pages/church-admin/sermons.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var di_container_1 = __importDefault(__webpack_require__(/*! ../../di-container */ "./ui/src/di-container.ts"));
var sermon_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/sermon-data-access */ "./ui/src/services/sermon-data-access.ts"));
var bible_book_data_yaml_1 = __importDefault(__webpack_require__(/*! ../../model/bible-book-data.yaml */ "./ui/src/model/bible-book-data.yaml"));
var ReactDOM = __importStar(__webpack_require__(/*! react-dom */ "./ui/node_modules/react-dom/index.js"));
var confirm_dialog_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/confirm-dialog */ "./ui/src/components/church-admin/confirm-dialog.tsx"));
var list_pager_1 = __importStar(__webpack_require__(/*! ../../components/list-pager */ "./ui/src/components/list-pager.tsx"));
var user_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/user-data-access */ "./ui/src/services/user-data-access.ts"));
var kerygma_configuration_1 = __webpack_require__(/*! ../../model/kerygma-configuration */ "./ui/src/model/kerygma-configuration.ts");
var tag_dialog_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/tag-dialog */ "./ui/src/components/church-admin/tag-dialog.tsx"));
var toaster = core_1.Toaster.create({ position: 'top' });
var bibleBooks = bible_book_data_yaml_1["default"].books;
var sermonDataAccess = di_container_1["default"].resolve(sermon_data_access_1["default"]);
var userDataAccess = di_container_1["default"].resolve(user_data_access_1["default"]);
var config = di_container_1["default"].resolve(kerygma_configuration_1.KerygmaConfiguration);
var _a = config.api, apiHost = _a.apiHost, mediaContextRoot = _a.mediaContextRoot;
var sermonsUri = "".concat(apiHost).concat(mediaContextRoot, "/sermons/");
var Sermons = function (_a) {
    var assetsRoot = _a.assetsRoot;
    var _b = react_1["default"].useState(), error = _b[0], setError = _b[1];
    var _c = (0, react_1.useState)(true), isLoading = _c[0], setIsLoading = _c[1];
    var _d = (0, react_1.useState)(), loginUser = _d[0], setLoginUser = _d[1];
    var _e = (0, react_1.useState)([]), sermons = _e[0], setSermons = _e[1];
    var _f = react_1["default"].useState([]), audioRefs = _f[0], setAudioRefs = _f[1];
    var _g = react_1["default"].useState([]), isActive = _g[0], setIsActive = _g[1];
    var _h = react_1["default"].useState(0), intervalId = _h[0], setIntervalId = _h[1];
    var _j = (0, react_1.useState)(0), currentTime = _j[0], setCurrentTime = _j[1];
    var _k = (0, react_1.useState)(0), duration = _k[0], setDuration = _k[1];
    var _l = (0, react_1.useState)(null), playingIndex = _l[0], setPlayingIndex = _l[1];
    var _m = (0, react_1.useState)(false), isDeleteConfirmOpen = _m[0], setIsDeleteConfirmOpen = _m[1];
    var _o = react_1["default"].useState(null), sermonToDelete = _o[0], setSermonToDelete = _o[1];
    var _p = (0, react_1.useState)(false), isProcessing = _p[0], setIsProcessing = _p[1];
    var _q = react_1["default"].useState(0), page = _q[0], setPage = _q[1];
    var _r = react_1["default"].useState(list_pager_1.perPageOptions[0]), perPage = _r[0], setPerPage = _r[1];
    var _s = react_1["default"].useState('latest'), sortOrder = _s[0], setSortOrder = _s[1];
    var _t = react_1["default"].useState(undefined), filter = _t[0], setFilter = _t[1];
    var _u = react_1["default"].useState([]), tagFilter = _u[0], setTagFilter = _u[1];
    var _v = react_1["default"].useState(0), totalSermons = _v[0], setTotalSermons = _v[1];
    var _w = react_1["default"].useState(false), tagDialogOpen = _w[0], setTagDialogOpen = _w[1];
    var _x = react_1["default"].useState(undefined), tags = _x[0], setTags = _x[1];
    var _y = react_1["default"].useState([]), tagsTemp = _y[0], setTagsTemp = _y[1];
    var classes = (0, sermons_styles_1.useStyles)();
    var onLogin = function () {
        setError(undefined);
        loadUser().then();
        loadSermons().then();
    };
    // We need this call because the getSermons call does not return the user
    // since it is also used by the public UI.
    var loadUser = function () { return __awaiter(void 0, void 0, void 0, function () {
        var loginUser_1, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, userDataAccess.getUserLoggedIn()];
                case 1:
                    loginUser_1 = _a.sent();
                    setLoginUser(loginUser_1);
                    return [3 /*break*/, 3];
                case 2:
                    err_1 = _a.sent();
                    setError(err_1);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); };
    var loadSermons = function () { return __awaiter(void 0, void 0, void 0, function () {
        var response, sermonRefs, playingStates, err_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    setIsLoading(true);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, 4, 5]);
                    return [4 /*yield*/, sermonDataAccess.debouncedGetSermons(page, perPage, sortOrder, filter, tagFilter)];
                case 2:
                    response = _a.sent();
                    setSermons(response.sermons);
                    setTotalSermons(response.totalRowCount);
                    sermonRefs = response.sermons.map(function () {
                        return react_1["default"].createRef();
                    });
                    playingStates = response.sermons.map(function () { return false; });
                    setAudioRefs(sermonRefs);
                    setIsActive(playingStates);
                    return [3 /*break*/, 5];
                case 3:
                    err_2 = _a.sent();
                    setError(err_2);
                    return [3 /*break*/, 5];
                case 4:
                    setIsLoading(false);
                    return [7 /*endfinally*/];
                case 5: return [2 /*return*/];
            }
        });
    }); };
    var loadTags = function () { return __awaiter(void 0, void 0, void 0, function () {
        var availableTags;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, sermonDataAccess.getSermonTags('sermonCount')];
                case 1:
                    availableTags = _a.sent();
                    setTags(availableTags);
                    return [2 /*return*/];
            }
        });
    }); };
    var onConfirmDeleteSermon = function (sermon) {
        setSermonToDelete(sermon);
        setIsDeleteConfirmOpen(true);
    };
    var onDeleteSermon = function () { return __awaiter(void 0, void 0, void 0, function () {
        var err_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    setIsProcessing(true);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 4, 5, 6]);
                    return [4 /*yield*/, sermonDataAccess.deleteSermon(sermonToDelete.id)];
                case 2:
                    _a.sent();
                    toaster.show({
                        message: "Sermon \"".concat(sermonToDelete.title, "\" (").concat(sermonToDelete.filename, ") was successfully deleted."),
                        intent: 'primary',
                        icon: 'small-tick'
                    });
                    return [4 /*yield*/, loadSermons()];
                case 3:
                    _a.sent();
                    return [3 /*break*/, 6];
                case 4:
                    err_3 = _a.sent();
                    setError(err_3);
                    return [3 /*break*/, 6];
                case 5:
                    setIsDeleteConfirmOpen(false);
                    setIsProcessing(false);
                    setSermonToDelete(null);
                    return [7 /*endfinally*/];
                case 6: return [2 /*return*/];
            }
        });
    }); };
    var onCancelDelete = function () {
        setIsDeleteConfirmOpen(false);
        setSermonToDelete(null);
    };
    (0, react_1.useEffect)(function () {
        if (!loginUser) {
            loadUser().then();
        }
        if (!tags) {
            loadTags().then();
        }
        loadSermons().then();
    }, [sortOrder, page, perPage, filter, tagFilter]);
    var onTogglePlay = function (index) {
        var _a;
        var playingStates = __spreadArray([], isActive, true);
        setPlayingIndex(index);
        var _loop_1 = function (i) {
            var audioRef = (_a = audioRefs[i]) === null || _a === void 0 ? void 0 : _a.current;
            if (i === index) {
                if (audioRef.paused) {
                    if (currentTime === audioRef.duration) {
                        audioRef.currentTime = 0;
                    }
                    audioRef.play().then();
                    playingStates[i] = true;
                    setDuration(audioRef.duration);
                    setCurrentTime(audioRef.currentTime);
                    clearInterval(intervalId);
                    var newIntervalId = window.setInterval(function () {
                        setCurrentTime(audioRef.currentTime);
                        if (audioRef.paused) {
                            var intervalPlayingStates = __spreadArray([], isActive, true);
                            intervalPlayingStates[i] = false;
                            setIsActive(intervalPlayingStates);
                            clearInterval(intervalId);
                        }
                    }, 1000);
                    setIntervalId(newIntervalId);
                }
                else {
                    audioRef.pause();
                    playingStates[i] = false;
                    clearInterval(intervalId);
                }
            }
            else {
                if (!audioRef.paused) {
                    audioRef.pause();
                    playingStates[i] = false;
                }
            }
        };
        for (var i = 0; i < audioRefs.length; i++) {
            _loop_1(i);
        }
        setIsActive(playingStates);
    };
    var onSliderChange = function (value) {
        var _a;
        var audioRef = (_a = audioRefs[playingIndex]) === null || _a === void 0 ? void 0 : _a.current;
        if (isActive[playingIndex] && !audioRef.paused) {
            onTogglePlay(playingIndex);
        }
        setCurrentTime(value);
    };
    var onSliderRelease = function (value) {
        var _a;
        setCurrentTime(value);
        var audioRef = (_a = audioRefs[playingIndex]) === null || _a === void 0 ? void 0 : _a.current;
        audioRef.currentTime = value;
        if (value < duration) {
            onTogglePlay(playingIndex);
        }
    };
    var onSearch = function (elem) {
        var _a;
        var newFilter = (_a = elem.target.value) !== null && _a !== void 0 ? _a : '';
        var trimmed = newFilter.trim();
        if (trimmed.length < 3) {
            setFilter(undefined);
            setPage(0);
        }
        else {
            setFilter(trimmed);
            setPage(0);
        }
    };
    var onPagerChanged = function (page, perPage) {
        setPage(page);
        setPerPage(perPage);
    };
    var formatTime = function (index, totalSeconds) {
        if (index !== playingIndex) {
            return '';
        }
        var hrs = Math.floor(totalSeconds / 3600);
        var hrsPrefix = hrs > 0 ? "".concat(hrs, ":") : '';
        totalSeconds = totalSeconds - hrs * 3600;
        var min = Math.floor(totalSeconds / 60);
        var minPadded = String(min).padStart(2, '0');
        var sec = Math.floor(totalSeconds - min * 60);
        var secPadded = String(sec).padStart(2, '0');
        return "".concat(hrsPrefix).concat(minPadded, ":").concat(secPadded);
    };
    var onEditSermon = function (sermon) {
        window.location.href = "sermon.html?sermonId=".concat(sermon.id);
    };
    var onOpenTagDialog = function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            setTagsTemp(tags.map(function (tag) { return tag.clone(); }));
            setTagDialogOpen(true);
            return [2 /*return*/];
        });
    }); };
    var onApplyTagChanges = function () {
        setTags(tagsTemp.map(function (tag) { return tag.clone(); }));
        setTagDialogOpen(false);
        var newFilterTags = tagsTemp
            .filter(function (tag) { return tag.isSelected; })
            .map(function (tag) { return tag.tag; });
        setTagFilter(newFilterTags);
    };
    var clearTagFilter = function () {
        setTagFilter([]);
        for (var _i = 0, tags_1 = tags; _i < tags_1.length; _i++) {
            var tag = tags_1[_i];
            tag.isSelected = false;
        }
    };
    var onToggleTag = function (i) {
        var tagsClone = tagsTemp.map(function (tag) { return tag.clone(); });
        tagsClone[i].isSelected = !tagsClone[i].isSelected;
        setTagsTemp(tagsClone);
    };
    var sortMenu = (react_1["default"].createElement(core_1.Menu, null,
        react_1["default"].createElement(core_1.MenuItem, { icon: "sort-asc", text: "Sort latest", active: sortOrder === 'latest', onClick: function () { return setSortOrder('latest'); } }),
        react_1["default"].createElement(core_1.MenuItem, { icon: "sort-desc", text: "Sort oldest", active: sortOrder === 'oldest', onClick: function () { return setSortOrder('oldest'); } })));
    return (react_1["default"].createElement(admin_page_1["default"], { assetsRoot: assetsRoot, error: error, clearError: function () { return setError(undefined); }, onLogin: onLogin, loginUser: loginUser },
        react_1["default"].createElement("div", { className: classes.centerContainer },
            react_1["default"].createElement(core_1.Card, { elevation: 1, className: classes.card },
                react_1["default"].createElement("div", { className: "".concat(classes.headerRow, " card-header") },
                    react_1["default"].createElement("h1", { className: classes.header }, "Sermons"),
                    react_1["default"].createElement(core_1.AnchorButton, { icon: "plus", text: "Add Sermon", href: "sermon.html" })),
                react_1["default"].createElement("div", { className: "".concat(classes.searchRow, " card-header") },
                    react_1["default"].createElement(core_1.InputGroup, { placeholder: "Search sermons on title, speaker, year or passage", type: "search", leftIcon: "search", className: classes.searchInput, onChange: onSearch }),
                    (!tags || tags.length > 0) && (react_1["default"].createElement(core_1.Button, { icon: "tag", className: classes.sort, onClick: onOpenTagDialog })),
                    react_1["default"].createElement(core_1.Popover, { placement: "bottom-end", content: sortMenu },
                        react_1["default"].createElement(core_1.Button, { icon: "sort", className: classes.sort }))),
                tagFilter.length > 0 && (react_1["default"].createElement("div", { className: "".concat(classes.tagFilterRow, " card-header") },
                    react_1["default"].createElement(core_1.Icon, { icon: "tag", size: 12 }),
                    tagFilter.map(function (tag, i) { return (react_1["default"].createElement(core_1.Tag, { key: i, minimal: true, round: true, className: classes.tag }, tag)); }),
                    react_1["default"].createElement(core_1.Button, { icon: "cross", minimal: true, className: classes.clearTags, onClick: clearTagFilter }))),
                react_1["default"].createElement("div", { className: classes.body }, isLoading === true ? (react_1["default"].createElement(react_1["default"].Fragment, null, Array(3)
                    .fill(0)
                    .map(function (_val, index) { return (react_1["default"].createElement("div", { className: classes.docRow, key: index },
                    react_1["default"].createElement("div", { className: "".concat(classes.avatar, " ").concat(core_1.Classes.SKELETON) },
                        react_1["default"].createElement(core_1.Icon, { icon: "document", color: "white" })),
                    react_1["default"].createElement("div", { className: classes.metaCol },
                        react_1["default"].createElement("a", { href: "#", className: core_1.Classes.SKELETON }, "Skeleton Sermon Title Goes Here"),
                        react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.SKELETON) }, "Speaker: speaker name here"),
                        react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.SKELETON) }, "service: Morning Worship"),
                        react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.SKELETON) }, "passage: Romans 8:1-8:2"),
                        react_1["default"].createElement("div", { className: classes.playLine },
                            react_1["default"].createElement(core_1.Button, { icon: "play", minimal: true, className: "".concat(classes.playButton, " ").concat(core_1.Classes.SKELETON) }),
                            react_1["default"].createElement("span", { className: "".concat(classes.sermonDate, " ").concat(core_1.Classes.SKELETON) }, "Mon Jul 4, 2022"))),
                    react_1["default"].createElement(core_1.Button, { icon: "more", className: "".concat(classes.more, " ").concat(core_1.Classes.SKELETON) }))); }))) : (react_1["default"].createElement(react_1["default"].Fragment, null, sermons.length === 0 ? (react_1["default"].createElement("div", null,
                    react_1["default"].createElement(core_1.NonIdealState, { icon: "folder-open", title: "No Sermons", description: "No sermons have been added yet.", className: classes.empty }))) : (react_1["default"].createElement(react_1["default"].Fragment, null,
                    sermons.map(function (sermon, i) {
                        var docMenu = (react_1["default"].createElement(core_1.Menu, null,
                            react_1["default"].createElement(core_1.MenuItem, { icon: "edit", text: "Edit", onClick: function () { return onEditSermon(sermon); } }),
                            react_1["default"].createElement(core_1.MenuItem, { icon: "remove", text: "Delete", onClick: function () { return onConfirmDeleteSermon(sermon); } })));
                        var passage = sermon.getPassage(bibleBooks);
                        return (react_1["default"].createElement("div", { className: classes.docRow, key: sermon.id },
                            react_1["default"].createElement("div", { className: classes.avatar },
                                react_1["default"].createElement(core_1.Icon, { icon: "headset", color: "white" })),
                            react_1["default"].createElement("div", { className: classes.metaCol },
                                react_1["default"].createElement("a", { href: "".concat(sermonsUri).concat(sermon.filename), target: "_blank", rel: "noopener noreferrer" },
                                    sermon.title,
                                    react_1["default"].createElement(core_1.Icon, { className: classes.linkIcon, icon: "share", size: 12 })),
                                react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    "Speaker: ",
                                    sermon.speakerTitle,
                                    ' ',
                                    sermon.speakerFirstName,
                                    " ",
                                    sermon.speakerLastName),
                                react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    "service: ",
                                    sermon.service),
                                passage && (react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    "passage: ",
                                    passage)),
                                sermon.tags && sermon.tags.length > 0 && (react_1["default"].createElement("div", { className: "".concat(classes.tagLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    react_1["default"].createElement(core_1.Icon, { icon: "tag", size: 12 }),
                                    sermon.tags.map(function (tag, i) { return (react_1["default"].createElement(core_1.Tag, { key: i, minimal: true, round: true, className: classes.tag }, tag)); }))),
                                react_1["default"].createElement("div", { className: "".concat(classes.playLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    !isActive[i] ? (react_1["default"].createElement(core_1.Button, { icon: "play", minimal: true, className: classes.playButton, onClick: function () { return onTogglePlay(i); } })) : (react_1["default"].createElement(core_1.Button, { icon: "pause", minimal: true, className: classes.playButton, onClick: function () { return onTogglePlay(i); } })),
                                    react_1["default"].createElement("span", { className: classes.sermonDate }, sermon
                                        .getSermonDate()
                                        .format('ddd MMM D, YYYY')),
                                    i === playingIndex && (react_1["default"].createElement(react_1["default"].Fragment, null,
                                        react_1["default"].createElement("span", { className: classes.time }, formatTime(i, currentTime)),
                                        react_1["default"].createElement(core_1.Slider, { min: 0, max: duration, labelStepSize: duration, value: currentTime, intent: "primary", onChange: onSliderChange, onRelease: onSliderRelease, labelRenderer: false }),
                                        react_1["default"].createElement("span", { className: classes.time }, formatTime(i, duration)))),
                                    react_1["default"].createElement("audio", { ref: audioRefs[i], src: "".concat(sermonsUri).concat(sermon.filename) }))),
                            react_1["default"].createElement(core_1.Popover, { placement: "bottom-end", content: docMenu },
                                react_1["default"].createElement(core_1.Button, { icon: "more", className: classes.more }))));
                    }),
                    react_1["default"].createElement(list_pager_1["default"], { page: page, perPage: perPage, totalRecords: totalSermons, onChange: onPagerChanged })))))))),
        react_1["default"].createElement(confirm_dialog_1["default"], { isOpen: isDeleteConfirmOpen, isProcessing: isProcessing, title: "Confirm Delete Sermon", message: "Are you sure you want to delete the sermon \"".concat(sermonToDelete === null || sermonToDelete === void 0 ? void 0 : sermonToDelete.title, "\" (").concat(sermonToDelete === null || sermonToDelete === void 0 ? void 0 : sermonToDelete.filename, ")?"), action: "Delete Sermon", processingAction: "Deleting Sermon...", onConfirm: onDeleteSermon, onCancel: onCancelDelete }),
        react_1["default"].createElement(tag_dialog_1["default"], { tags: tagsTemp, isOpen: tagDialogOpen, onCancel: function () { return setTagDialogOpen(false); }, onApply: onApplyTagChanges, onToggleTag: onToggleTag })));
};
function init(target, assetsRoot) {
    ReactDOM.render(react_1["default"].createElement(Sermons, { assetsRoot: assetsRoot }), document.getElementById(target));
}
exports.init = init;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	(() => {
/******/ 		// The chunk loading function for additional chunks
/******/ 		// Since all referenced chunks are already included
/******/ 		// in this file, this function is empty here.
/******/ 		__webpack_require__.e = () => (Promise.resolve());
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/harmony module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.hmd = (module) => {
/******/ 			module = Object.create(module);
/******/ 			if (!module.children) module.children = [];
/******/ 			Object.defineProperty(module, 'exports', {
/******/ 				enumerable: true,
/******/ 				set: () => {
/******/ 					throw new Error('ES Modules may not assign module.exports or exports.*, Use ESM export syntax, instead: ' + module.id);
/******/ 				}
/******/ 			});
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"churchAdminSermons": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors","ui_src_common_global-constants_ts-ui_src_di-container_ts","ui_src_components_church-admin_dark-mode-context_ts-ui_src_services_data-access_ts-ui_src_ser-43f718","ui_src_components_church-admin_admin-page_tsx","ui_src_services_user-data-access_ts","ui_src_services_sermon-data-access_ts-ui_src_model_bible-book-data_yaml"], () => (__webpack_require__("./ui/src/pages/church-admin/sermons.tsx")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	churchAdminSermons = __webpack_exports__;
/******/ 	
/******/ })()
;