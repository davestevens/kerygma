var churchAdminSermon;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./ui/src/components/church-admin/center-card-layout.styles.ts":
/*!*********************************************************************!*\
  !*** ./ui/src/components/church-admin/center-card-layout.styles.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var global_constants_1 = __webpack_require__(/*! ../../common/global-constants */ "./ui/src/common/global-constants.ts");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    centerContainer: "\n    display: flex;\n    justify-content: center;\n    padding: 0 16px;\n  ",
    centerContent: "\n    display: flex;\n    flex-direction: column;\n    max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ";\n    width: 100%;\n    margin-top: 30px;\n  "),
    centerContentBreadcrumb: "\n    margin-top: 16px;\n  ",
    breadcrumbs: "\n    padding: 0 0 8px 0;\n  "
});


/***/ }),

/***/ "./ui/src/components/church-admin/center-card-layout.tsx":
/*!***************************************************************!*\
  !*** ./ui/src/components/church-admin/center-card-layout.tsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var center_card_layout_styles_1 = __webpack_require__(/*! ./center-card-layout.styles */ "./ui/src/components/church-admin/center-card-layout.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var CenterCardLayout = function (_a) {
    var breadcrumbs = _a.breadcrumbs, children = _a.children;
    var classes = (0, center_card_layout_styles_1.useStyles)();
    return (React.createElement("div", { className: classes.centerContainer },
        React.createElement("div", { className: "\n          ".concat(classes.centerContent, "\n          ").concat(breadcrumbs && classes.centerContentBreadcrumb, "\n        ") },
            breadcrumbs && (React.createElement("div", { className: classes.breadcrumbs },
                React.createElement(core_1.Breadcrumbs, { items: breadcrumbs }))),
            children)));
};
exports["default"] = CenterCardLayout;


/***/ }),

/***/ "./ui/src/components/church-admin/page-form.styles.ts":
/*!************************************************************!*\
  !*** ./ui/src/components/church-admin/page-form.styles.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var global_constants_1 = __webpack_require__(/*! ../../common/global-constants */ "./ui/src/common/global-constants.ts");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    formContainer: "\n    display: flex;\n    flex-direction: column;\n    align-items: flex-start;\n    width: 100%;\n    max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ";\n  ")
});


/***/ }),

/***/ "./ui/src/components/church-admin/page-form.tsx":
/*!******************************************************!*\
  !*** ./ui/src/components/church-admin/page-form.tsx ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var page_form_styles_1 = __webpack_require__(/*! ./page-form.styles */ "./ui/src/components/church-admin/page-form.styles.ts");
var PageForm = function (_a) {
    var children = _a.children;
    var classes = (0, page_form_styles_1.useStyles)();
    return React.createElement("div", { className: classes.formContainer }, children);
};
exports["default"] = PageForm;


/***/ }),

/***/ "./ui/src/pages/church-admin/sermon.styles.ts":
/*!****************************************************!*\
  !*** ./ui/src/pages/church-admin/sermon.styles.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    card: "\n    padding: 0;\n  ",
    headerRow: "\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    padding: 10px 16px;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n  ",
    header: "\n    text-align: left;\n    padding: 0;\n    width: auto;\n  ",
    body: "\n    padding: 16px;\n  ",
    errorMessage: "\n    font-size: 12px;\n    color: ".concat(core_1.Colors.RED3, ";\n    margin: -8px 0 8px 0;\n  "),
    formGroup: "\n    width: 100%;\n  ",
    buttonRow: "\n    display: flex;\n  ",
    saveButton: "\n      margin-right: 16px;\n  ",
    dialogMessage: "\n    margin-top: 16px;\n  ",
    dialogMessageError: "\n    margin-top: 16px;\n    color: ".concat(core_1.Colors.RED3, ";\n  "),
    nameTitle: "\n    width: 50px;\n  "
});


/***/ }),

/***/ "./ui/src/pages/church-admin/sermon.tsx":
/*!**********************************************!*\
  !*** ./ui/src/pages/church-admin/sermon.tsx ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.init = void 0;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var react_1 = __webpack_require__(/*! react */ "./ui/node_modules/react/index.js");
var admin_page_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/admin-page */ "./ui/src/components/church-admin/admin-page.tsx"));
var di_container_1 = __importDefault(__webpack_require__(/*! ../../di-container */ "./ui/src/di-container.ts"));
var user_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/user-data-access */ "./ui/src/services/user-data-access.ts"));
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var sermon_styles_1 = __webpack_require__(/*! ./sermon.styles */ "./ui/src/pages/church-admin/sermon.styles.ts");
var center_card_layout_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/center-card-layout */ "./ui/src/components/church-admin/center-card-layout.tsx"));
var validation_form_group_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/validation-form-group */ "./ui/src/components/church-admin/validation-form-group.tsx"));
var page_form_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/page-form */ "./ui/src/components/church-admin/page-form.tsx"));
var sermon_1 = __webpack_require__(/*! ../../model/sermon */ "./ui/src/model/sermon.ts");
var datetime_1 = __webpack_require__(/*! @blueprintjs/datetime */ "./ui/node_modules/@blueprintjs/datetime/lib/esm/index.js");
var dayjs_1 = __importDefault(__webpack_require__(/*! dayjs */ "./ui/node_modules/dayjs/dayjs.min.js"));
var kerygma_configuration_1 = __webpack_require__(/*! ../../model/kerygma-configuration */ "./ui/src/model/kerygma-configuration.ts");
var bible_book_data_yaml_1 = __importDefault(__webpack_require__(/*! ../../model/bible-book-data.yaml */ "./ui/src/model/bible-book-data.yaml"));
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var http_status_codes_1 = __webpack_require__(/*! http-status-codes */ "./ui/node_modules/http-status-codes/build/es/index.js");
var sermon_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/sermon-data-access */ "./ui/src/services/sermon-data-access.ts"));
var ReactDOM = __importStar(__webpack_require__(/*! react-dom */ "./ui/node_modules/react-dom/index.js"));
var sermonDataAccess = di_container_1["default"].resolve(sermon_data_access_1["default"]);
var config = di_container_1["default"].resolve(kerygma_configuration_1.KerygmaConfiguration);
var bibleBooks = bible_book_data_yaml_1["default"].books;
var speakerOptions = config.defaultSpeakers.map(function (speaker, index) {
    var prefix = speaker.title ? "".concat(speaker.title, " ") : '';
    var label = "".concat(prefix).concat(speaker.firstName, " ").concat(speaker.lastName);
    var value = index;
    return { label: label, value: value };
});
var getSpeakerOption = function (sermon) {
    return speakerOptions.find(function (speakerOption) {
        var prefix = sermon.speakerTitle ? "".concat(sermon.speakerTitle, " ") : '';
        var label = "".concat(prefix).concat(sermon.speakerFirstName, " ").concat(sermon.speakerLastName);
        return speakerOption.label === label;
    });
};
var otherSpeaker = -1;
speakerOptions.push({ label: 'Other', value: otherSpeaker });
var defaultSpeaker = config.defaultSpeakers.findIndex(function (speaker) { return speaker["default"]; });
var initModel = new sermon_1.Sermon();
var speaker = config.defaultSpeakers[defaultSpeaker];
initModel.setSermonDate((0, dayjs_1["default"])());
initModel.speakerTitle = speaker.title;
initModel.speakerFirstName = speaker.firstName;
initModel.speakerLastName = speaker.lastName;
var serviceOptions = config.services.map(function (service) { return ({
    label: service,
    value: service
}); });
initModel.service = serviceOptions[0].value;
var bookOptions = __spreadArray([
    { label: '', value: 'Book' }
], bibleBooks.map(function (book) { return ({
    label: book.name,
    value: book.code
}); }), true);
var sermonIdStr = new URLSearchParams(window.location.search).get('sermonId');
var sermonId = sermonIdStr ? parseInt(sermonIdStr) : null;
var mode = sermonId ? 'edit' : 'new';
var title = "".concat(mode === 'new' ? 'Add' : 'Edit', " Sermon");
var userDataAccess = di_container_1["default"].resolve(user_data_access_1["default"]);
var breadcrumbs = [
    { href: '/church-admin/sermons.html', icon: 'headset', text: 'Sermons' },
    { icon: mode === 'new' ? 'plus' : 'edit', text: title },
];
function calcChapters(book) {
    return book
        ? Array.from(Array(book.chapters.length).keys()).map(function (chap) { return chap + 1; })
        : ['Chapter'];
}
function calcVerses(book, chapter) {
    return book && chapter
        ? Array.from(Array(book.chapters[chapter - 1]).keys()).map(function (chap) { return chap + 1; })
        : ['Verse'];
}
var Sermon = function (_a) {
    var assetsRoot = _a.assetsRoot;
    var _b = React.useState(), error = _b[0], setError = _b[1];
    var _c = (0, react_1.useState)(), loginUser = _c[0], setLoginUser = _c[1];
    var _d = React.useState(false), isProcessing = _d[0], setIsProcessing = _d[1];
    var _e = React.useState(false), isProgressDialogOpen = _e[0], setIsProgressDialogOpen = _e[1];
    var _f = React.useState(0.0), progress = _f[0], setProgress = _f[1];
    var _g = React.useState(true), isLoading = _g[0], setIsLoading = _g[1];
    var _h = React.useState(initModel), sermonModel = _h[0], setSermonModel = _h[1];
    var _j = React.useState(defaultSpeaker), speakerSelection = _j[0], setSpeakerSelection = _j[1];
    var _k = React.useState(serviceOptions[0].value), serviceSelection = _k[0], setServiceSelection = _k[1];
    var _l = React.useState(null), fromBook = _l[0], setFromBook = _l[1];
    var fromChapters = calcChapters(fromBook);
    var fromVerses = calcVerses(fromBook, sermonModel.fromChapter);
    var _m = React.useState(null), thruBook = _m[0], setThruBook = _m[1];
    var thruChapters = calcChapters(thruBook);
    var thruVerses = calcVerses(thruBook, sermonModel.thruChapter);
    var _o = React.useState(''), localFilename = _o[0], setLocalFilename = _o[1];
    var fileInputRef = React.useRef();
    var _p = React.useState(''), uploadError = _p[0], setUploadError = _p[1];
    var classes = (0, sermon_styles_1.useStyles)();
    var onFieldChange = function (id, newValue) {
        sermonModel[id] = newValue;
        setSermonModel(sermonModel.clone());
    };
    var onChangeDateInput = function (selectedDate) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            sermonModel.setSermonDate((0, dayjs_1["default"])(selectedDate));
            setSermonModel(sermonModel.clone());
            return [2 /*return*/];
        });
    }); };
    var onChangeTag = function (values) {
        if (values.length > 20)
            return;
        var tagSet = new Set(values.map(function (tag) { return "".concat(tag).toLowerCase(); }));
        sermonModel.tags = Array.from(tagSet);
        setSermonModel(sermonModel.clone());
    };
    var onChangeSpeaker = function (event) {
        var speakerValue = parseInt(event.currentTarget.value);
        setSpeakerSelection(speakerValue);
        if (speakerValue !== otherSpeaker) {
            var speaker_1 = config.defaultSpeakers[speakerValue];
            sermonModel.speakerTitle = speaker_1.title;
            sermonModel.speakerFirstName = speaker_1.firstName;
            sermonModel.speakerLastName = speaker_1.lastName;
        }
        else {
            sermonModel.speakerTitle = '';
            sermonModel.speakerFirstName = '';
            sermonModel.speakerLastName = '';
        }
        setSermonModel(sermonModel.clone());
    };
    var onChangeService = function (event) {
        var service = event.currentTarget.value;
        setServiceSelection(service);
        sermonModel.service = service;
        setSermonModel(sermonModel.clone());
    };
    var onChangeFromBook = function (event) {
        sermonModel.fromBook = event.currentTarget.value;
        if (sermonModel.fromBook !== 'Book') {
            var book = bibleBooks.filter(function (bk) { return bk.code === sermonModel.fromBook; })[0];
            setFromBook(book);
            sermonModel.fromChapter = 1;
            sermonModel.fromVerse = 1;
        }
        else {
            setFromBook(null);
            sermonModel.fromBook = undefined;
            sermonModel.fromChapter = undefined;
            sermonModel.fromVerse = undefined;
        }
        setSermonModel(sermonModel.clone());
        onChangeThruBook(event);
    };
    var onChangeThruBook = function (event) {
        sermonModel.thruBook = event.currentTarget.value;
        if (sermonModel.thruBook !== 'Book') {
            var book = bibleBooks.filter(function (bk) { return bk.code === sermonModel.thruBook; })[0];
            setThruBook(book);
            sermonModel.thruChapter = 1;
            sermonModel.thruVerse = 1;
        }
        else {
            setThruBook(null);
            sermonModel.thruBook = undefined;
            sermonModel.thruChapter = undefined;
            sermonModel.thruVerse = undefined;
        }
        setSermonModel(sermonModel.clone());
    };
    var onChangeFromChapter = function (event) {
        sermonModel.fromChapter = parseInt(event.currentTarget.value);
        setSermonModel(sermonModel.clone());
        onChangeThruChapter(event);
    };
    var onChangeFromVerse = function (event) {
        sermonModel.fromVerse = parseInt(event.currentTarget.value);
        setSermonModel(sermonModel.clone());
        onChangeThruVerse(event);
    };
    var onChangeThruChapter = function (event) {
        sermonModel.thruChapter = parseInt(event.currentTarget.value);
        setSermonModel(sermonModel.clone());
    };
    var onChangeThruVerse = function (event) {
        sermonModel.thruVerse = parseInt(event.currentTarget.value);
        setSermonModel(sermonModel.clone());
    };
    var onFileInputChange = function (event) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            setLocalFilename(event.currentTarget.value);
            return [2 /*return*/];
        });
    }); };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    var onUploadProgress = function (progressEvent) {
        var progress = Math.round((progressEvent.loaded * 100) / progressEvent.total) / 100;
        setProgress(progress);
    };
    var clearError = function () {
        setError(undefined);
    };
    var preLoadForm = function () { return __awaiter(void 0, void 0, void 0, function () {
        var sermon_2, speakerOption, fromBook_1, thruBook_1, err_1;
        var _a, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    _c.trys.push([0, 2, 3, 4]);
                    return [4 /*yield*/, sermonDataAccess.getSermon(sermonId)];
                case 1:
                    sermon_2 = _c.sent();
                    speakerOption = (_b = (_a = getSpeakerOption(sermon_2)) === null || _a === void 0 ? void 0 : _a.value) !== null && _b !== void 0 ? _b : otherSpeaker;
                    setSpeakerSelection(speakerOption);
                    setServiceSelection(sermon_2.service);
                    fromBook_1 = bibleBooks.filter(function (bk) { return bk.code === sermon_2.fromBook; })[0];
                    setFromBook(fromBook_1);
                    thruBook_1 = bibleBooks.filter(function (bk) { return bk.code === sermon_2.thruBook; })[0];
                    setThruBook(thruBook_1);
                    setSermonModel(sermon_2);
                    return [3 /*break*/, 4];
                case 2:
                    err_1 = _c.sent();
                    setError(err_1);
                    return [3 /*break*/, 4];
                case 3:
                    setIsLoading(false);
                    return [7 /*endfinally*/];
                case 4: return [2 /*return*/];
            }
        });
    }); };
    var onLogin = function () {
        setError(undefined);
        verifyLogin().then();
    };
    var verifyLogin = function () { return __awaiter(void 0, void 0, void 0, function () {
        var user, err_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, 3, 4]);
                    return [4 /*yield*/, userDataAccess.getUserLoggedIn()];
                case 1:
                    user = _a.sent();
                    setLoginUser(user);
                    return [3 /*break*/, 4];
                case 2:
                    err_2 = _a.sent();
                    setError(err_2);
                    return [3 /*break*/, 4];
                case 3:
                    setIsLoading(false);
                    return [7 /*endfinally*/];
                case 4: return [2 /*return*/];
            }
        });
    }); };
    var isFormValid = function () {
        return (sermonModel.isValid() &&
            (fileInputRef.current.files.length > 0 || mode === 'edit'));
    };
    var handleSave = function () { return __awaiter(void 0, void 0, void 0, function () {
        var file, response, err_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    setUploadError('');
                    if (fileInputRef.current.files.length === 0 && mode === 'new') {
                        return [2 /*return*/];
                    }
                    file = fileInputRef.current.files[0];
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 6, 7, 8]);
                    setIsProcessing(true);
                    setProgress(0);
                    setIsProgressDialogOpen(true);
                    response = void 0;
                    if (!(mode === 'new')) return [3 /*break*/, 3];
                    return [4 /*yield*/, sermonDataAccess.addSermon(sermonModel, file, onUploadProgress)];
                case 2:
                    response = _a.sent();
                    return [3 /*break*/, 5];
                case 3: return [4 /*yield*/, sermonDataAccess.saveSermon(sermonModel, file, onUploadProgress)];
                case 4:
                    response = _a.sent();
                    _a.label = 5;
                case 5:
                    if (response.status === http_status_codes_1.StatusCodes.CONFLICT) {
                        setUploadError('A file with that filename already exists');
                    }
                    else if (response.status === http_status_codes_1.StatusCodes.REQUEST_TOO_LONG) {
                        setUploadError('The size of the file exceeded the allowed max');
                    }
                    return [3 /*break*/, 8];
                case 6:
                    err_3 = _a.sent();
                    setError(err_3);
                    setUploadError('An error occurred while uploading.');
                    setProgress(1.0);
                    return [3 /*break*/, 8];
                case 7:
                    setIsProcessing(false);
                    return [7 /*endfinally*/];
                case 8: return [2 /*return*/];
            }
        });
    }); };
    function closeProgress() {
        setIsProgressDialogOpen(false);
        if (progress >= 1.0 && uploadError === '') {
            window.location.href = 'sermons.html';
        }
        else {
            setProgress(0.0);
        }
    }
    (0, react_1.useEffect)(function () {
        if (mode === 'edit') {
            preLoadForm().then();
        }
        verifyLogin().then();
    }, []);
    var actionLabel = mode === 'edit' ? 'Save' : 'Add Sermon';
    var processingLabel = mode === 'edit' ? 'Saving...' : 'Adding Sermon...';
    var actionIcon = mode === 'edit' ? 'small-tick' : 'plus';
    return (React.createElement(admin_page_1["default"], { assetsRoot: assetsRoot, error: error, clearError: clearError, onLogin: onLogin, loginUser: loginUser },
        React.createElement(center_card_layout_1["default"], { breadcrumbs: breadcrumbs },
            React.createElement(core_1.Card, { elevation: 1, className: classes.card },
                React.createElement("div", { className: "".concat(classes.headerRow, " card-header") },
                    React.createElement("h1", { className: classes.header }, title)),
                React.createElement("div", { className: classes.body },
                    React.createElement(page_form_1["default"], null,
                        React.createElement(core_1.FormGroup, { label: "Sermon Date", className: "".concat(classes.formGroup, " ").concat(isLoading && core_1.Classes.SKELETON) },
                            React.createElement(datetime_1.DateInput, { value: sermonModel.getSermonDate().toISOString(), onChange: onChangeDateInput, formatDate: function (date) { return date.toLocaleDateString(); }, parseDate: function (str) { return new Date(str); }, fill: true })),
                        React.createElement(core_1.FormGroup, { label: "Speaker", className: "".concat(classes.formGroup, " ").concat(isLoading && core_1.Classes.SKELETON) },
                            React.createElement(core_1.HTMLSelect, { onChange: onChangeSpeaker, options: speakerOptions, fill: true, value: speakerSelection })),
                        speakerSelection === otherSpeaker && (React.createElement(core_1.FormGroup, { label: "Other Speaker", className: "".concat(classes.formGroup, " ").concat(isLoading && core_1.Classes.SKELETON) },
                            React.createElement(core_1.ControlGroup, { fill: true },
                                React.createElement(validation_form_group_1["default"], { id: "speakerTitle", label: "Title", placeholder: "Title", value: sermonModel.speakerTitle, setter: onFieldChange, hideFormGroup: true, noFill: true, className: classes.nameTitle }),
                                React.createElement(validation_form_group_1["default"], { id: "speakerFirstName", label: "First Name", placeholder: "First Name", value: sermonModel.speakerFirstName, validator: sermonModel.speakerFirstNameValidator, setter: onFieldChange, hideFormGroup: true, noFill: true }),
                                React.createElement(validation_form_group_1["default"], { id: "speakerLastName", label: "Last Name", placeholder: "Last Name", value: sermonModel.speakerLastName, validator: sermonModel.speakerLastNameValidator, setter: onFieldChange, hideFormGroup: true, noFill: true })))),
                        React.createElement(validation_form_group_1["default"], { id: "title", value: sermonModel.title, validator: function () {
                                return sermonModel.titleValidator();
                            }, setter: onFieldChange, label: "Title", labelInfo: "(required)", isLoading: isLoading }),
                        React.createElement(core_1.FormGroup, { label: "Search Tags", className: "".concat(classes.formGroup, " ").concat(isLoading && core_1.Classes.SKELETON) },
                            React.createElement(core_1.TagInput, { onChange: onChangeTag, values: sermonModel.tags, fill: true, leftIcon: "tag", placeholder: "Separate each tag with a comma and hit enter...", tagProps: { round: true, minimal: true } })),
                        React.createElement(core_1.FormGroup, { label: "Service", className: "".concat(classes.formGroup, " ").concat(isLoading && core_1.Classes.SKELETON) },
                            React.createElement(core_1.HTMLSelect, { onChange: onChangeService, options: serviceOptions, fill: true, value: serviceSelection })),
                        React.createElement(core_1.FormGroup, { label: "From Passage", className: "".concat(classes.formGroup, " ").concat(isLoading && core_1.Classes.SKELETON) },
                            React.createElement(core_1.ControlGroup, { fill: true },
                                React.createElement(core_1.HTMLSelect, { onChange: onChangeFromBook, options: bookOptions, value: sermonModel.fromBook }),
                                React.createElement(core_1.HTMLSelect, { onChange: onChangeFromChapter, options: fromChapters, value: sermonModel.fromChapter }),
                                React.createElement(core_1.HTMLSelect, { onChange: onChangeFromVerse, options: fromVerses, value: sermonModel.fromVerse }))),
                        React.createElement(core_1.FormGroup, { label: "To Passage", className: "".concat(classes.formGroup, " ").concat(isLoading && core_1.Classes.SKELETON) },
                            React.createElement(core_1.ControlGroup, { fill: true },
                                React.createElement(core_1.HTMLSelect, { onChange: onChangeThruBook, options: bookOptions, value: sermonModel.thruBook }),
                                React.createElement(core_1.HTMLSelect, { onChange: onChangeThruChapter, options: thruChapters, value: sermonModel.thruChapter }),
                                React.createElement(core_1.HTMLSelect, { onChange: onChangeThruVerse, options: thruVerses, value: sermonModel.thruVerse }))),
                        React.createElement(core_1.FormGroup, { label: "Local File to Upload", labelInfo: "(must be of type MP3)", className: "".concat(classes.formGroup, " ").concat(isLoading ? core_1.Classes.SKELETON : '') },
                            React.createElement(core_1.FileInput, { text: localFilename, buttonText: "Browse", fill: true, onInputChange: onFileInputChange, inputProps: { ref: fileInputRef, accept: 'audio/mpeg' } }))),
                    React.createElement("div", { className: classes.buttonRow },
                        React.createElement(core_1.Button, { icon: actionIcon, text: isProcessing ? processingLabel : actionLabel, className: classes.saveButton, onClick: handleSave, disabled: !isFormValid() || isProcessing }),
                        React.createElement(core_1.AnchorButton, { icon: "small-cross", text: "Cancel", href: "/church-admin/sermons.html" }))))),
        React.createElement(dark_mode_context_1["default"].Consumer, null, function (isDarkMode) { return (React.createElement(core_1.Dialog, { isOpen: isProgressDialogOpen, title: "Uploading File", className: isDarkMode ? core_1.Classes.DARK : '', isCloseButtonShown: false, canEscapeKeyClose: false, canOutsideClickClose: false },
            React.createElement("div", { className: core_1.Classes.DIALOG_BODY },
                React.createElement(core_1.ProgressBar, { value: progress, intent: uploadError ? 'danger' : 'primary', stripes: false }),
                progress < 1.0 ? (React.createElement("p", { className: classes.dialogMessage },
                    mode === 'new' ? 'Uploading ' : 'Saving ',
                    localFilename,
                    "....")) : (React.createElement(React.Fragment, null, uploadError !== '' ? (React.createElement("p", { className: classes.dialogMessageError }, uploadError)) : (React.createElement("p", { className: classes.dialogMessage },
                    localFilename,
                    " was successfully",
                    mode === 'new' ? ' uploaded' : ' saved',
                    "."))))),
            progress >= 1.0 && (React.createElement("div", { className: core_1.Classes.DIALOG_FOOTER },
                React.createElement("div", { className: core_1.Classes.DIALOG_FOOTER_ACTIONS },
                    React.createElement(core_1.Button, { onClick: closeProgress }, "OK")))))); })));
};
function init(target, assetsRoot) {
    ReactDOM.render(React.createElement(Sermon, { assetsRoot: assetsRoot }), document.getElementById(target));
}
exports.init = init;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	(() => {
/******/ 		// The chunk loading function for additional chunks
/******/ 		// Since all referenced chunks are already included
/******/ 		// in this file, this function is empty here.
/******/ 		__webpack_require__.e = () => (Promise.resolve());
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/harmony module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.hmd = (module) => {
/******/ 			module = Object.create(module);
/******/ 			if (!module.children) module.children = [];
/******/ 			Object.defineProperty(module, 'exports', {
/******/ 				enumerable: true,
/******/ 				set: () => {
/******/ 					throw new Error('ES Modules may not assign module.exports or exports.*, Use ESM export syntax, instead: ' + module.id);
/******/ 				}
/******/ 			});
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"churchAdminSermon": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors","ui_src_common_global-constants_ts-ui_src_di-container_ts","ui_src_components_church-admin_dark-mode-context_ts-ui_src_services_data-access_ts-ui_src_ser-43f718","ui_src_components_church-admin_admin-page_tsx","ui_src_services_user-data-access_ts","ui_src_services_sermon-data-access_ts-ui_src_model_bible-book-data_yaml"], () => (__webpack_require__("./ui/src/pages/church-admin/sermon.tsx")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	churchAdminSermon = __webpack_exports__;
/******/ 	
/******/ })()
;