var churchAdminDocument;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./ui/src/components/church-admin/page-form.styles.ts":
/*!************************************************************!*\
  !*** ./ui/src/components/church-admin/page-form.styles.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var global_constants_1 = __webpack_require__(/*! ../../common/global-constants */ "./ui/src/common/global-constants.ts");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    formContainer: "\n    display: flex;\n    flex-direction: column;\n    align-items: flex-start;\n    width: 100%;\n    max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ";\n  ")
});


/***/ }),

/***/ "./ui/src/components/church-admin/page-form.tsx":
/*!******************************************************!*\
  !*** ./ui/src/components/church-admin/page-form.tsx ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var page_form_styles_1 = __webpack_require__(/*! ./page-form.styles */ "./ui/src/components/church-admin/page-form.styles.ts");
var PageForm = function (_a) {
    var children = _a.children;
    var classes = (0, page_form_styles_1.useStyles)();
    return React.createElement("div", { className: classes.formContainer }, children);
};
exports["default"] = PageForm;


/***/ }),

/***/ "./ui/src/pages/church-admin/document.styles.ts":
/*!******************************************************!*\
  !*** ./ui/src/pages/church-admin/document.styles.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    card: "\n    padding: 0;\n  ",
    headerRow: "\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    padding: 10px 16px;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n  ",
    header: "\n    text-align: left;\n    padding: 0;\n    width: auto;\n  ",
    body: "\n    padding: 16px;\n  ",
    errorMessage: "\n    font-size: 12px;\n    color: ".concat(core_1.Colors.RED3, ";\n    margin: -8px 0 8px 0;\n  "),
    formGroup: "\n    width: 100%;\n  ",
    buttonRow: "\n    display: flex;\n  ",
    primaryButton: "\n    margin-right: 16px;\n  ",
    dialogMessage: "\n    margin-top: 16px;\n  ",
    dialogMessageError: "\n    margin-top: 16px;\n    color: ".concat(core_1.Colors.RED3, ";\n  ")
});


/***/ }),

/***/ "./ui/src/pages/church-admin/document.tsx":
/*!************************************************!*\
  !*** ./ui/src/pages/church-admin/document.tsx ***!
  \************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.init = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var document_styles_1 = __webpack_require__(/*! ./document.styles */ "./ui/src/pages/church-admin/document.styles.ts");
var admin_page_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/admin-page */ "./ui/src/components/church-admin/admin-page.tsx"));
var center_card_layout_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/center-card-layout */ "./ui/src/components/church-admin/center-card-layout.tsx"));
var di_container_1 = __importDefault(__webpack_require__(/*! ../../di-container */ "./ui/src/di-container.ts"));
var react_1 = __webpack_require__(/*! react */ "./ui/node_modules/react/index.js");
var validation_form_group_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/validation-form-group */ "./ui/src/components/church-admin/validation-form-group.tsx"));
var page_form_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/page-form */ "./ui/src/components/church-admin/page-form.tsx"));
var uploaded_document_1 = __webpack_require__(/*! ../../model/uploaded-document */ "./ui/src/model/uploaded-document.ts");
var http_status_codes_1 = __webpack_require__(/*! http-status-codes */ "./ui/node_modules/http-status-codes/build/es/index.js");
var uploaded_document_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/uploaded-document-data-access */ "./ui/src/services/uploaded-document-data-access.ts"));
var user_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/user-data-access */ "./ui/src/services/user-data-access.ts"));
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ../../components/church-admin/dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var ReactDOM = __importStar(__webpack_require__(/*! react-dom */ "./ui/node_modules/react-dom/index.js"));
var docIdStr = new URLSearchParams(window.location.search).get('docId');
var docId = docIdStr ? parseInt(docIdStr) : null;
var mode = docId ? 'edit' : 'new';
var title = "".concat(mode === 'new' ? 'Add' : 'Edit', " Document");
var uploadedDocumentDataAccess = di_container_1["default"].resolve(uploaded_document_data_access_1["default"]);
var userDataAccess = di_container_1["default"].resolve(user_data_access_1["default"]);
var breadcrumbs = [
    { href: '/church-admin/documents.html', icon: 'document', text: 'Documents' },
    { icon: mode === 'new' ? 'document-open' : 'edit', text: title },
];
var Document = function (_a) {
    var assetsRoot = _a.assetsRoot;
    var _b = React.useState(), error = _b[0], setError = _b[1];
    var _c = (0, react_1.useState)(), loginUser = _c[0], setLoginUser = _c[1];
    var _d = (0, react_1.useState)(new uploaded_document_1.UploadedDocument()), uploadedDoc = _d[0], setUploadedDoc = _d[1];
    var _e = React.useState(''), localFilename = _e[0], setLocalFilename = _e[1];
    var _f = React.useState(false), isProcessing = _f[0], setIsProcessing = _f[1];
    var _g = React.useState(false), isProgressDialogOpen = _g[0], setIsProgressDialogOpen = _g[1];
    var _h = React.useState(0.0), progress = _h[0], setProgress = _h[1];
    var _j = React.useState(true), isLoading = _j[0], setIsLoading = _j[1];
    var fileInputRef = React.useRef();
    var _k = React.useState(''), uploadError = _k[0], setUploadError = _k[1];
    var classes = (0, document_styles_1.useStyles)();
    var onFieldChange = function (id, newValue) {
        uploadedDoc[id] = newValue;
        setUploadedDoc(uploadedDoc.clone());
        if (id === 'filename') {
            setUploadError('');
        }
    };
    var onFileInputChange = function (event) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            setLocalFilename(event.currentTarget.value);
            return [2 /*return*/];
        });
    }); };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    var onUploadProgress = function (progressEvent) {
        var progress = Math.round((progressEvent.loaded * 100) / progressEvent.total) / 100;
        setProgress(progress);
    };
    var handleSave = function () { return __awaiter(void 0, void 0, void 0, function () {
        var file, response, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    setUploadError('');
                    if (fileInputRef.current.files.length === 0 && mode === 'new') {
                        return [2 /*return*/];
                    }
                    file = fileInputRef.current.files[0];
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 6, 7, 8]);
                    setIsProcessing(true);
                    setProgress(0);
                    setIsProgressDialogOpen(true);
                    response = void 0;
                    if (!(mode === 'new')) return [3 /*break*/, 3];
                    return [4 /*yield*/, uploadedDocumentDataAccess.addDocument(uploadedDoc, file, onUploadProgress)];
                case 2:
                    response = _a.sent();
                    return [3 /*break*/, 5];
                case 3: return [4 /*yield*/, uploadedDocumentDataAccess.saveDocument(uploadedDoc, file, onUploadProgress)];
                case 4:
                    response = _a.sent();
                    _a.label = 5;
                case 5:
                    if (response.status === http_status_codes_1.StatusCodes.CONFLICT) {
                        setUploadError('A file with that filename already exists');
                    }
                    else if (response.status === http_status_codes_1.StatusCodes.REQUEST_TOO_LONG) {
                        setUploadError('The size of the file exceeded the allowed max');
                    }
                    return [3 /*break*/, 8];
                case 6:
                    err_1 = _a.sent();
                    setError(err_1);
                    setUploadError('An error occurred while uploading.');
                    setProgress(1.0);
                    return [3 /*break*/, 8];
                case 7:
                    setIsProcessing(false);
                    return [7 /*endfinally*/];
                case 8: return [2 /*return*/];
            }
        });
    }); };
    var clearError = function () {
        setError(undefined);
    };
    var preLoadForm = function () { return __awaiter(void 0, void 0, void 0, function () {
        var _a, document_1, loginUser_1, docForm, err_2;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, 3, 4]);
                    return [4 /*yield*/, uploadedDocumentDataAccess.getDocument(docId)];
                case 1:
                    _a = _b.sent(), document_1 = _a.document, loginUser_1 = _a.loginUser;
                    docForm = new uploaded_document_1.UploadedDocument();
                    docForm.filename = document_1.filename;
                    docForm.title = document_1.title;
                    docForm.id = docId;
                    setUploadedDoc(docForm);
                    setLoginUser(loginUser_1);
                    return [3 /*break*/, 4];
                case 2:
                    err_2 = _b.sent();
                    setError(err_2);
                    return [3 /*break*/, 4];
                case 3:
                    setIsLoading(false);
                    return [7 /*endfinally*/];
                case 4: return [2 /*return*/];
            }
        });
    }); };
    var onLogin = function () {
        setError(undefined);
        verifyLogin().then();
    };
    var verifyLogin = function () { return __awaiter(void 0, void 0, void 0, function () {
        var user, err_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, 3, 4]);
                    return [4 /*yield*/, userDataAccess.getUserLoggedIn()];
                case 1:
                    user = _a.sent();
                    setLoginUser(user);
                    return [3 /*break*/, 4];
                case 2:
                    err_3 = _a.sent();
                    setError(err_3);
                    return [3 /*break*/, 4];
                case 3:
                    setIsLoading(false);
                    return [7 /*endfinally*/];
                case 4: return [2 /*return*/];
            }
        });
    }); };
    var isFormValid = function () {
        return (uploadedDoc.isValid() &&
            (fileInputRef.current.files.length > 0 || mode === 'edit'));
    };
    (0, react_1.useEffect)(function () {
        if (mode === 'edit') {
            preLoadForm().then();
        }
        else {
            verifyLogin().then();
        }
    }, []);
    var actionLabel = mode === 'edit' ? 'Save' : 'Add Document';
    var processingLabel = mode === 'edit' ? 'Saving...' : 'Adding Document...';
    var actionIcon = mode === 'edit' ? 'small-tick' : 'plus';
    function closeProgress() {
        setIsProgressDialogOpen(false);
        if (progress >= 1.0 && uploadError === '') {
            window.location.href = 'documents.html';
        }
        else {
            setProgress(0.0);
        }
    }
    return (React.createElement(admin_page_1["default"], { assetsRoot: assetsRoot, error: error, clearError: clearError, onLogin: onLogin, loginUser: loginUser },
        React.createElement(center_card_layout_1["default"], { breadcrumbs: breadcrumbs },
            React.createElement(core_1.Card, { elevation: 1, className: classes.card },
                React.createElement("div", { className: "".concat(classes.headerRow, " card-header") },
                    React.createElement("h1", { className: classes.header }, title)),
                React.createElement("div", { className: classes.body },
                    React.createElement(page_form_1["default"], null,
                        React.createElement(validation_form_group_1["default"], { id: "filename", value: uploadedDoc.filename, validator: function () {
                                return uploadedDoc.filenameValidator();
                            }, setter: onFieldChange, label: "Web Filename", labelInfo: "(required)", isLoading: isLoading, rightText: ".pdf" }),
                        uploadError && (React.createElement("div", { className: classes.errorMessage }, uploadError)),
                        React.createElement(validation_form_group_1["default"], { id: "title", value: uploadedDoc.title, validator: function () {
                                return uploadedDoc.titleValidator();
                            }, setter: onFieldChange, label: "Title", labelInfo: "(required)", isLoading: isLoading }),
                        React.createElement(core_1.FormGroup, { label: "Local File to Upload", labelInfo: "(must be of type PDF)", className: "".concat(classes.formGroup, " ").concat(isLoading ? core_1.Classes.SKELETON : '') },
                            React.createElement(core_1.FileInput, { text: localFilename, buttonText: "Browse", fill: true, onInputChange: onFileInputChange, inputProps: { ref: fileInputRef, accept: 'application/pdf' } }))),
                    React.createElement("div", { className: classes.buttonRow },
                        React.createElement(core_1.Button, { className: classes.primaryButton, icon: actionIcon, text: isProcessing ? processingLabel : actionLabel, onClick: handleSave, disabled: !isFormValid() || isProcessing }),
                        React.createElement(core_1.AnchorButton, { icon: "small-cross", text: "Cancel", href: "/church-admin/documents.html" }))))),
        React.createElement(dark_mode_context_1["default"].Consumer, null, function (isDarkMode) { return (React.createElement(core_1.Dialog, { isOpen: isProgressDialogOpen, title: "Uploading File", className: isDarkMode ? core_1.Classes.DARK : '', isCloseButtonShown: false, canEscapeKeyClose: false, canOutsideClickClose: false },
            React.createElement("div", { className: core_1.Classes.DIALOG_BODY },
                React.createElement(core_1.ProgressBar, { value: progress, intent: uploadError ? 'danger' : 'primary', stripes: false }),
                progress < 1.0 ? (React.createElement("p", { className: classes.dialogMessage },
                    mode === 'new' ? 'Uploading ' : 'Saving ',
                    uploadedDoc.filename,
                    ".pdf....")) : (React.createElement(React.Fragment, null, uploadError !== '' ? (React.createElement("p", { className: classes.dialogMessageError }, uploadError)) : (React.createElement("p", { className: classes.dialogMessage },
                    uploadedDoc.filename,
                    ".pdf was successfully",
                    mode === 'new' ? ' uploaded' : ' saved',
                    "."))))),
            progress >= 1.0 && (React.createElement("div", { className: core_1.Classes.DIALOG_FOOTER },
                React.createElement("div", { className: core_1.Classes.DIALOG_FOOTER_ACTIONS },
                    React.createElement(core_1.Button, { onClick: closeProgress }, "OK")))))); })));
};
function init(target, assetsRoot) {
    ReactDOM.render(React.createElement(Document, { assetsRoot: assetsRoot }), document.getElementById(target));
}
exports.init = init;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	(() => {
/******/ 		// The chunk loading function for additional chunks
/******/ 		// Since all referenced chunks are already included
/******/ 		// in this file, this function is empty here.
/******/ 		__webpack_require__.e = () => (Promise.resolve());
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/harmony module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.hmd = (module) => {
/******/ 			module = Object.create(module);
/******/ 			if (!module.children) module.children = [];
/******/ 			Object.defineProperty(module, 'exports', {
/******/ 				enumerable: true,
/******/ 				set: () => {
/******/ 					throw new Error('ES Modules may not assign module.exports or exports.*, Use ESM export syntax, instead: ' + module.id);
/******/ 				}
/******/ 			});
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"churchAdminDocument": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors","ui_src_common_global-constants_ts-ui_src_di-container_ts","ui_src_components_church-admin_dark-mode-context_ts-ui_src_services_data-access_ts-ui_src_ser-43f718","ui_src_components_church-admin_admin-page_tsx","ui_src_services_user-data-access_ts","ui_src_components_church-admin_center-card-layout_tsx-ui_src_services_uploaded-document-data--e4ccb5"], () => (__webpack_require__("./ui/src/pages/church-admin/document.tsx")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	churchAdminDocument = __webpack_exports__;
/******/ 	
/******/ })()
;