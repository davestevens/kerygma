var contactUs;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./ui/src/components/lazy-image.styles.ts":
/*!************************************************!*\
  !*** ./ui/src/components/lazy-image.styles.ts ***!
  \************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.createClasses = void 0;
var global_constants_1 = __webpack_require__(/*! ../common/global-constants */ "./ui/src/common/global-constants.ts");
var jss_preset_default_1 = __importDefault(__webpack_require__(/*! jss-preset-default */ "./ui/node_modules/jss-preset-default/dist/jss-preset-default.esm.js"));
var jss_1 = __importDefault(__webpack_require__(/*! jss */ "./ui/node_modules/jss/dist/jss.esm.js"));
/*
 * full = 100%
 * wide = full size of reading area with rounded corners
 * half = 50% of reading area with rounded corners
 */
var containerBase = {
    display: 'flex !important',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    backgroundSize: 'cover !important',
    backgroundPosition: 'center !important'
};
jss_1["default"].setup((0, jss_preset_default_1["default"])());
var createClasses = function (objectPosition) {
    var imageBase = {
        objectFit: 'cover',
        width: '100%',
        height: '100%'
    };
    if (objectPosition) {
        imageBase.objectPosition = objectPosition;
    }
    var classes = jss_1["default"]
        .createStyleSheet({
        containerFull: __assign(__assign({}, containerBase), { width: '100%', '& img': imageBase }),
        containerPadding: "\n      display: flex;\n      padding: 0 16px;\n    ",
        containerWide: __assign(__assign({}, containerBase), { maxWidth: global_constants_1.globalConstants.textMaxWidth, borderRadius: '4px', '& img': imageBase }),
        containerHalf: __assign(__assign({}, containerBase), { width: 'calc(${globalConstants.textMaxWidth} / 2)', borderRadius: '4px', '& img': imageBase })
    })
        .attach().classes;
    return classes;
};
exports.createClasses = createClasses;


/***/ }),

/***/ "./ui/src/components/lazy-image.tsx":
/*!******************************************!*\
  !*** ./ui/src/components/lazy-image.tsx ***!
  \******************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var lazy_image_styles_1 = __webpack_require__(/*! ./lazy-image.styles */ "./ui/src/components/lazy-image.styles.ts");
var react_lazy_load_image_component_1 = __webpack_require__(/*! react-lazy-load-image-component */ "./ui/node_modules/react-lazy-load-image-component/build/index.js");
var LazyImage = function (_a) {
    var src = _a.src, ht = _a.ht, width = _a.width, position = _a.position;
    var classes = (0, lazy_image_styles_1.createClasses)(position);
    var containerClass = width === 'half'
        ? classes.containerHalf
        : width === 'wide'
            ? classes.containerWide
            : classes.containerFull;
    var placeholder = src
        .replace('.jpg', '-placeholder.jpg')
        .replace('.jpeg', '-placeholder.jpeg');
    var image = (React.createElement(react_lazy_load_image_component_1.LazyLoadImage, { src: src, placeholderSrc: placeholder, effect: "blur", threshold: 100, height: ht, wrapperClassName: containerClass }));
    return (React.createElement(React.Fragment, null, width === 'full' ? (image) : (React.createElement("div", { className: classes.containerPadding }, image))));
};
exports["default"] = LazyImage;


/***/ }),

/***/ "./ui/src/pages/contact-us.tsx":
/*!*************************************!*\
  !*** ./ui/src/pages/contact-us.tsx ***!
  \*************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.init = void 0;
var react_1 = __importDefault(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var ReactDOM = __importStar(__webpack_require__(/*! react-dom */ "./ui/node_modules/react-dom/index.js"));
var lazy_image_1 = __importDefault(__webpack_require__(/*! ../components/lazy-image */ "./ui/src/components/lazy-image.tsx"));
var public_layout_1 = __importDefault(__webpack_require__(/*! ../layouts/public-layout */ "./ui/src/layouts/public-layout.tsx"));
var libphonenumber_js_1 = __webpack_require__(/*! libphonenumber-js */ "./ui/node_modules/libphonenumber-js/index.cjs");
var react_obfuscate_1 = __importDefault(__webpack_require__(/*! react-obfuscate */ "./ui/node_modules/react-obfuscate/dist/obfuscate.js"));
var di_container_1 = __importDefault(__webpack_require__(/*! ../di-container */ "./ui/src/di-container.ts"));
var kerygma_configuration_1 = __webpack_require__(/*! ../model/kerygma-configuration */ "./ui/src/model/kerygma-configuration.ts");
var config = di_container_1["default"].resolve(kerygma_configuration_1.KerygmaConfiguration);
var kerygmaVersion = di_container_1["default"].resolve('kerygmaVersion');
/*
 * Had to make this page a component instead of an MDX file
 * because MDX does not support React hooks and the Obfuscate
 * component uses React hooks.
 */
var ContactUs = function (_a) {
    var assetsRoot = _a.assetsRoot;
    var frontMatter = {
        title: 'Contact Us',
        description: "Contact Information for ".concat(config.churchNameShort)
    };
    var phoneNumber = (0, libphonenumber_js_1.parsePhoneNumberFromString)(config.phone, 'US');
    return (react_1["default"].createElement(public_layout_1["default"], { frontmatter: frontMatter, assetsRoot: assetsRoot, config: config, kerygmaVersion: kerygmaVersion, pathHome: "" },
        react_1["default"].createElement("br", null),
        react_1["default"].createElement(lazy_image_1["default"], { src: "./assets/img/hello-phone.jpg", ht: "400", width: "wide" }),
        react_1["default"].createElement("h1", null, "Contact Us"),
        react_1["default"].createElement("p", { style: { textAlign: 'center' } }, "Have a question? Give us a call or send us an email."),
        react_1["default"].createElement("br", null),
        react_1["default"].createElement(react_obfuscate_1["default"], { href: phoneNumber.getURI() }, phoneNumber.formatNational()),
        react_1["default"].createElement(react_obfuscate_1["default"], { email: config.email }, config.email),
        react_1["default"].createElement("br", null)));
};
function init(target, assetsRoot) {
    ReactDOM.render(react_1["default"].createElement(ContactUs, { assetsRoot: assetsRoot }), document.getElementById(target));
}
exports.init = init;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	(() => {
/******/ 		// The chunk loading function for additional chunks
/******/ 		// Since all referenced chunks are already included
/******/ 		// in this file, this function is empty here.
/******/ 		__webpack_require__.e = () => (Promise.resolve());
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/harmony module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.hmd = (module) => {
/******/ 			module = Object.create(module);
/******/ 			if (!module.children) module.children = [];
/******/ 			Object.defineProperty(module, 'exports', {
/******/ 				enumerable: true,
/******/ 				set: () => {
/******/ 					throw new Error('ES Modules may not assign module.exports or exports.*, Use ESM export syntax, instead: ' + module.id);
/******/ 				}
/******/ 			});
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"contactUs": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors","ui_src_common_global-constants_ts-ui_src_di-container_ts","ui_src_layouts_public-layout_tsx"], () => (__webpack_require__("./ui/src/pages/contact-us.tsx")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	contactUs = __webpack_exports__;
/******/ 	
/******/ })()
;