var aboutPurpose;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./pages/about/purpose.mdx":
/*!*********************************!*\
  !*** ./pages/about/purpose.mdx ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ MDXContent)
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var kerygma__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! kerygma */ "./node_modules/kerygma/ui/lib.js");
/*
title: Our Purpose
layout: PublicLayout
description: >
The purpose of Trinity Sample Church can
be divided into three categories: divine worship, mutual edification
and gospel witness.
*/


function _createMdxContent(props) {
  const _components = {
    a: "a",
    h1: "h1",
    h2: "h2",
    hr: "hr",
    p: "p",
    ...props.components
  };
  return (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: ["\n", "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(kerygma__WEBPACK_IMPORTED_MODULE_1__.LazyImage, {
      src: "../assets/img/open_bible.jpg",
      ht: 400,
      width: "full"
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 12,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.h1, {
      children: "Purpose"
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 14,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
      children: ["Much of the following purpose statement is included with\ngreater detail and clarity within our\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.a, {
        href: "https://opc.org/BCO/FG.html#Chapter_I",
        children: "book of church order - chapter 1"
      }, undefined, false, {
        fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
        lineNumber: 18,
        columnNumber: 1
      }, this)]
    }, undefined, true, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 16,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.h2, {
      children: "Our Head"
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 20,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
      children: "There is but one King and Head of the church,\nthe only Mediator between God and man, Jesus Christ,\nwho rules in his church by his Word and Spirit."
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 22,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.h2, {
      children: "Our Standard"
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 26,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
      children: "Christ orders his church by the rule of his Word."
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 28,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.h2, {
      children: "Our Power"
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 30,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
      children: "Jesus Christ, having ascended into heaven, abides in his church by the Holy Spirit whom he has sent."
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 32,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.hr, {}, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 34,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.h2, {
      children: "1. Our Mission - Divine Worship"
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 36,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
      children: "Our primary focus will be the service of God by our private\nand corporate worship. Our primary concern should not be what\nwe receive from our worship but that God is pleased with our\nworship."
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 38,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("small", {
      children: [(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
        children: "Psa. 19:14 - Let the words of my mouth and the meditation of my heart be acceptable in Your sight, O Lord, my rock and my Redeemer."
      }, undefined, false, {
        fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
        lineNumber: 44,
        columnNumber: 3
      }, this), (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
        children: "John 4:23 - But an hour is coming, and now is, when the true worshipers will worship the Father in spirit and truth; for such people the Father seeks to be His worshipers."
      }, undefined, false, {
        fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
        lineNumber: 46,
        columnNumber: 3
      }, this), (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
        children: "1Pet. 2:4-5 - And coming to Him as to a living stone which has been rejected by men, but is choice and precious in the sight of God, you also, as living stones, are being built up as a spiritual house for a holy priesthood, to offer up spiritual sacrifices acceptable to God through Jesus Christ."
      }, undefined, false, {
        fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
        lineNumber: 48,
        columnNumber: 3
      }, this)]
    }, undefined, true, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 42,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.h2, {
      children: "2. Our Mission - Mutual Edification"
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 51,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
      children: "We are Christ’s body, the church. As such, we should be known\nby our love for one another. Our actions should lead toward the\nmutual edification of the body."
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 53,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("small", {
      children: [(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
        children: "Rom. 15:1-2 - Now we who are strong ought to bear the weaknesses of those without strength and not just please ourselves. Each of us is to please his neighbor for his good, to his edification."
      }, undefined, false, {
        fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
        lineNumber: 59,
        columnNumber: 3
      }, this), (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
        children: "1Cor. 12:4,7 - Now there are varieties of gifts, but the same Spirit…But to each one is given the manifestation of the Spirit for the common good."
      }, undefined, false, {
        fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
        lineNumber: 61,
        columnNumber: 3
      }, this), (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
        children: "Rom. 14:19 - So then we pursue the things which make for peace and the building up of one another."
      }, undefined, false, {
        fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
        lineNumber: 63,
        columnNumber: 3
      }, this)]
    }, undefined, true, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 57,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.h2, {
      children: "3. Our Mission - Gospel Witness"
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 66,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
      children: "We should be about the mission that Christ gave the church to\ndisciple the nations through the faithful preaching of the gospel."
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 68,
      columnNumber: 1
    }, this), "\n", (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("small", {
      children: [(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
        children: "Matt. 28:19-20 - Go therefore and make disciples of all the nations, baptizing them in the name of the Father and the Son and the Holy Spirit, teaching them to observe all that I commanded you; and lo, I am with you always, even to the end of the age.”"
      }, undefined, false, {
        fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
        lineNumber: 72,
        columnNumber: 3
      }, this), (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components.p, {
        children: "Matt. 5:14-15 - “You are the light of the world. A city set on a hill cannot be hidden; nor does anyone light a lamp and put it under a basket, but on the lampstand, and it gives light to all who are in the house."
      }, undefined, false, {
        fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
        lineNumber: 74,
        columnNumber: 3
      }, this)]
    }, undefined, true, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
      lineNumber: 70,
      columnNumber: 1
    }, this)]
  }, undefined, true, {
    fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx",
    lineNumber: 1,
    columnNumber: 1
  }, this);
}
function MDXContent(props = {}) {
  const {wrapper: MDXLayout} = props.components || ({});
  return MDXLayout ? (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(MDXLayout, {
    ...props,
    children: (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_createMdxContent, {
      ...props
    }, undefined, false, {
      fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx"
    }, this)
  }, undefined, false, {
    fileName: "/home/dave/projects/kerygma/pages/about/purpose.mdx"
  }, this) : _createMdxContent(props);
}


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	(() => {
/******/ 		// The chunk loading function for additional chunks
/******/ 		// Since all referenced chunks are already included
/******/ 		// in this file, this function is empty here.
/******/ 		__webpack_require__.e = () => (Promise.resolve());
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/harmony module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.hmd = (module) => {
/******/ 			module = Object.create(module);
/******/ 			if (!module.children) module.children = [];
/******/ 			Object.defineProperty(module, 'exports', {
/******/ 				enumerable: true,
/******/ 				set: () => {
/******/ 					throw new Error('ES Modules may not assign module.exports or exports.*, Use ESM export syntax, instead: ' + module.id);
/******/ 				}
/******/ 			});
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"aboutPurpose": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors"], () => (__webpack_require__("./pages/about/purpose.mdx")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	aboutPurpose = __webpack_exports__;
/******/ 	
/******/ })()
;