"use strict";
(self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || []).push([["ui_src_components_church-admin_dark-mode-context_ts-ui_src_services_data-access_ts-ui_src_ser-43f718"],{

/***/ "./ui/src/components/church-admin/dark-mode-context.ts":
/*!*************************************************************!*\
  !*** ./ui/src/components/church-admin/dark-mode-context.ts ***!
  \*************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var DarkModeContext = React.createContext(false);
exports["default"] = DarkModeContext;


/***/ }),

/***/ "./ui/src/model/unauthorized-error.ts":
/*!********************************************!*\
  !*** ./ui/src/model/unauthorized-error.ts ***!
  \********************************************/
/***/ (function(__unused_webpack_module, exports) {


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var UnauthorizedError = /** @class */ (function (_super) {
    __extends(UnauthorizedError, _super);
    function UnauthorizedError(message) {
        var _this = _super.call(this, message) || this;
        Object.setPrototypeOf(_this, UnauthorizedError.prototype);
        return _this;
    }
    return UnauthorizedError;
}(Error));
exports["default"] = UnauthorizedError;


/***/ }),

/***/ "./ui/src/services/data-access.ts":
/*!****************************************!*\
  !*** ./ui/src/services/data-access.ts ***!
  \****************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var unauthorized_error_1 = __importDefault(__webpack_require__(/*! ../model/unauthorized-error */ "./ui/src/model/unauthorized-error.ts"));
var http_status_codes_1 = __webpack_require__(/*! http-status-codes */ "./ui/node_modules/http-status-codes/build/es/index.js");
var DataAccess = /** @class */ (function () {
    function DataAccess(config, fetcher, localStorageDataAccess) {
        this.config = config;
        this.fetcher = fetcher;
        this.localStorageDataAccess = localStorageDataAccess;
    }
    DataAccess.prototype.get = function (path, acceptableStatuses, bearerToken, params) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        return __awaiter(this, void 0, void 0, function () {
            var method, settings, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        method = 'GET';
                        settings = {
                            method: method,
                            params: params,
                            withCredentials: true
                        };
                        if (bearerToken) {
                            settings.headers = {
                                Accept: 'application/json',
                                'content-type': 'application/json',
                                Authorization: "Bearer ".concat(bearerToken)
                            };
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.fetcher("".concat(this.config.api.apiHost).concat(path), settings)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        error_1 = _a.sent();
                        if (error_1.response) {
                            return [2 /*return*/, DataAccess.verifyResponse(error_1.response, method, path, acceptableStatuses)];
                        }
                        throw error_1;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DataAccess.prototype.getAuthorized = function (path, acceptableStatuses, params) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        return __awaiter(this, void 0, void 0, function () {
            var token;
            return __generator(this, function (_a) {
                token = this.localStorageDataAccess.getToken();
                return [2 /*return*/, this.get(path, acceptableStatuses, token, params)];
            });
        });
    };
    DataAccess.prototype.post = function (path, body, acceptableStatuses, bearerToken) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.postOrPut('POST', path, body, acceptableStatuses, bearerToken)];
            });
        });
    };
    DataAccess.prototype.put = function (path, body, acceptableStatuses, bearerToken) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.postOrPut('PUT', path, body, acceptableStatuses, bearerToken)];
            });
        });
    };
    DataAccess.prototype.postOrPut = function (method, path, data, acceptableStatuses, bearerToken) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        return __awaiter(this, void 0, void 0, function () {
            var csrfToken, settings, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        csrfToken = this.localStorageDataAccess.getCsrfToken();
                        settings = {
                            method: method,
                            data: data,
                            headers: {
                                Accept: 'application/json',
                                Authorization: bearerToken ? "Bearer ".concat(bearerToken) : '',
                                _csrf: csrfToken
                            },
                            withCredentials: true
                        };
                        if (data) {
                            settings.headers['Content-Type'] = 'application/json';
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.fetcher("".concat(this.config.api.apiHost).concat(path), settings)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        error_2 = _a.sent();
                        if (error_2.response) {
                            return [2 /*return*/, DataAccess.verifyResponse(error_2.response, method, path, acceptableStatuses)];
                        }
                        throw error_2;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DataAccess.prototype.postFormDataAuthorized = function (method, path, data, acceptableStatuses, onUploadProgress) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        return __awaiter(this, void 0, void 0, function () {
            var token, csrfToken, contentType, settings, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        token = this.localStorageDataAccess.getToken();
                        csrfToken = this.localStorageDataAccess.getCsrfToken();
                        contentType = data.has('file')
                            ? 'multipart/form-data'
                            : 'application/x-www-form-urlencoded';
                        settings = {
                            method: method,
                            data: data,
                            headers: {
                                Accept: 'application/json',
                                Authorization: "Bearer ".concat(token),
                                'content-type': contentType,
                                _csrf: csrfToken
                            },
                            withCredentials: true,
                            onUploadProgress: onUploadProgress
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.fetcher("".concat(this.config.api.apiHost).concat(path), settings)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        error_3 = _a.sent();
                        if (error_3.response) {
                            return [2 /*return*/, DataAccess.verifyResponse(error_3.response, method, path, acceptableStatuses)];
                        }
                        throw error_3;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DataAccess.prototype["delete"] = function (path, acceptableStatuses, bearerToken) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        return __awaiter(this, void 0, void 0, function () {
            var csrfToken, method, settings, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        csrfToken = this.localStorageDataAccess.getCsrfToken();
                        method = 'DELETE';
                        settings = {
                            method: method,
                            headers: {
                                Accept: 'application/json',
                                Authorization: bearerToken ? "Bearer ".concat(bearerToken) : '',
                                _csrf: csrfToken
                            },
                            withCredentials: true
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.fetcher("".concat(this.config.api.apiHost).concat(path), settings)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        error_4 = _a.sent();
                        if (error_4.response) {
                            return [2 /*return*/, DataAccess.verifyResponse(error_4.response, method, path, acceptableStatuses)];
                        }
                        throw error_4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DataAccess.prototype.postAuthorized = function (path, body, acceptableStatuses) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        return __awaiter(this, void 0, void 0, function () {
            var token;
            return __generator(this, function (_a) {
                token = this.localStorageDataAccess.getToken();
                return [2 /*return*/, this.post(path, body, acceptableStatuses, token)];
            });
        });
    };
    DataAccess.prototype.putAuthorized = function (path, body, acceptableStatuses) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        return __awaiter(this, void 0, void 0, function () {
            var token;
            return __generator(this, function (_a) {
                token = this.localStorageDataAccess.getToken();
                return [2 /*return*/, this.put(path, body, acceptableStatuses, token)];
            });
        });
    };
    DataAccess.prototype.deleteAuthorized = function (path, acceptableStatuses) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        return __awaiter(this, void 0, void 0, function () {
            var token;
            return __generator(this, function (_a) {
                token = this.localStorageDataAccess.getToken();
                return [2 /*return*/, this["delete"](path, acceptableStatuses, token)];
            });
        });
    };
    DataAccess.verifyResponse = function (response, method, path, acceptableStatuses) {
        if (acceptableStatuses === void 0) { acceptableStatuses = []; }
        var statusWasAcceptable = acceptableStatuses.filter(function (status) { return status === response.status; }).length >
            0;
        if (response.status === http_status_codes_1.StatusCodes.UNAUTHORIZED) {
            throw new unauthorized_error_1["default"]();
        }
        if (!statusWasAcceptable && response.status >= 300) {
            throw new Error("Non 200 level status (".concat(response.status, ") returned from ").concat(method, " ").concat(path));
        }
        return response;
    };
    return DataAccess;
}());
exports["default"] = DataAccess;


/***/ }),

/***/ "./ui/src/services/local-storage-data-access.ts":
/*!******************************************************!*\
  !*** ./ui/src/services/local-storage-data-access.ts ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./ui/node_modules/tsyringe/dist/esm5/index.js");
var tokenKey = 'kerygma-api-token';
var csrfTokenKey = 'kerygma-csrf-token';
var settingsKey = 'kerygma-settings';
var toastKey = 'kerygma-toast';
var LocalStorageDataAccess = /** @class */ (function () {
    function LocalStorageDataAccess(localStorage, sessionStorage) {
        this.localStorage = localStorage;
        this.sessionStorage = sessionStorage;
    }
    LocalStorageDataAccess.prototype.saveToken = function (token) {
        this.sessionStorage.setItem(tokenKey, token);
    };
    LocalStorageDataAccess.prototype.getToken = function () {
        return this.sessionStorage.getItem(tokenKey);
    };
    LocalStorageDataAccess.prototype.saveCsrfToken = function (csrfToken) {
        this.sessionStorage.setItem(csrfTokenKey, csrfToken);
    };
    LocalStorageDataAccess.prototype.getCsrfToken = function () {
        return this.sessionStorage.getItem(csrfTokenKey);
    };
    LocalStorageDataAccess.prototype.saveDarkMode = function (isDarkMode) {
        var settingsValue = JSON.stringify({ isDarkMode: isDarkMode });
        this.localStorage.setItem(settingsKey, settingsValue);
    };
    LocalStorageDataAccess.prototype.getDarkMode = function () {
        var _a;
        var settings = JSON.parse(this.localStorage.getItem(settingsKey));
        return (_a = settings === null || settings === void 0 ? void 0 : settings.isDarkMode) !== null && _a !== void 0 ? _a : false;
    };
    LocalStorageDataAccess.prototype.setToast = function (message) {
        this.sessionStorage.setItem(toastKey, message);
    };
    // this is a destructive read
    LocalStorageDataAccess.prototype.getToast = function () {
        var _a;
        var message = (_a = this.sessionStorage.getItem(toastKey)) !== null && _a !== void 0 ? _a : '';
        this.sessionStorage.setItem(toastKey, '');
        return message;
    };
    LocalStorageDataAccess = __decorate([
        (0, tsyringe_1.injectable)(),
        __param(0, (0, tsyringe_1.inject)('localStorage')),
        __param(1, (0, tsyringe_1.inject)('sessionStorage')),
        __metadata("design:paramtypes", [Storage,
            Storage])
    ], LocalStorageDataAccess);
    return LocalStorageDataAccess;
}());
exports["default"] = LocalStorageDataAccess;


/***/ })

}]);