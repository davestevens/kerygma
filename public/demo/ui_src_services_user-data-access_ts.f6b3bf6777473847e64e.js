"use strict";
(self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || []).push([["ui_src_services_user-data-access_ts"],{

/***/ "./ui/src/model/user.ts":
/*!******************************!*\
  !*** ./ui/src/model/user.ts ***!
  \******************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
exports.__esModule = true;
exports.User = void 0;
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var User = /** @class */ (function () {
    function User() {
        this.id = '';
        this.firstName = '';
        this.lastName = '';
        this.username = '';
        this.authLevel = 'admin';
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('id', String),
        __metadata("design:type", Object)
    ], User.prototype, "id");
    __decorate([
        (0, json2typescript_1.JsonProperty)('firstName', String),
        __metadata("design:type", Object)
    ], User.prototype, "firstName");
    __decorate([
        (0, json2typescript_1.JsonProperty)('lastName', String),
        __metadata("design:type", Object)
    ], User.prototype, "lastName");
    __decorate([
        (0, json2typescript_1.JsonProperty)('username', String),
        __metadata("design:type", Object)
    ], User.prototype, "username");
    __decorate([
        (0, json2typescript_1.JsonProperty)('authLevel', String),
        __metadata("design:type", Object)
    ], User.prototype, "authLevel");
    User = __decorate([
        (0, json2typescript_1.JsonObject)('User')
    ], User);
    return User;
}());
exports.User = User;


/***/ }),

/***/ "./ui/src/services/user-data-access.ts":
/*!*********************************************!*\
  !*** ./ui/src/services/user-data-access.ts ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var user_1 = __webpack_require__(/*! ../model/user */ "./ui/src/model/user.ts");
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var data_access_1 = __importDefault(__webpack_require__(/*! ./data-access */ "./ui/src/services/data-access.ts"));
var local_storage_data_access_1 = __importDefault(__webpack_require__(/*! ./local-storage-data-access */ "./ui/src/services/local-storage-data-access.ts"));
var http_status_codes_1 = __webpack_require__(/*! http-status-codes */ "./ui/node_modules/http-status-codes/build/es/index.js");
var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./ui/node_modules/tsyringe/dist/esm5/index.js");
var kerygma_configuration_1 = __webpack_require__(/*! ../model/kerygma-configuration */ "./ui/src/model/kerygma-configuration.ts");
var UserDataAccess = /** @class */ (function (_super) {
    __extends(UserDataAccess, _super);
    function UserDataAccess(jsonConvert, config, fetcher, localStorageDataAccess) {
        var _this = _super.call(this, config, fetcher, localStorageDataAccess) || this;
        _this.jsonConvert = jsonConvert;
        return _this;
    }
    UserDataAccess.prototype.getUsers = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, body, users, loginUser;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getAuthorized('api/users')];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.data];
                    case 2:
                        body = _a.sent();
                        users = this.jsonConvert.deserializeArray(body.users, user_1.User);
                        loginUser = this.jsonConvert.deserializeObject(body.loginUser, user_1.User);
                        return [2 /*return*/, { users: users, loginUser: loginUser }];
                }
            });
        });
    };
    UserDataAccess.prototype.getUser = function (userId) {
        return __awaiter(this, void 0, void 0, function () {
            var response, body, user, loginUser;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getAuthorized("api/users/".concat(userId))];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.data];
                    case 2:
                        body = _a.sent();
                        user = this.jsonConvert.deserializeObject(body.user, user_1.User);
                        loginUser = this.jsonConvert.deserializeObject(body.loginUser, user_1.User);
                        return [2 /*return*/, { user: user, loginUser: loginUser }];
                }
            });
        });
    };
    UserDataAccess.prototype.addUser = function (userForm) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.postAuthorized('api/users', JSON.stringify(userForm), [
                            http_status_codes_1.StatusCodes.CONFLICT,
                        ])];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UserDataAccess.prototype.saveUser = function (userForm) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.putAuthorized("api/users/".concat(userForm.id), JSON.stringify(userForm), [http_status_codes_1.StatusCodes.CONFLICT])];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UserDataAccess.prototype.deleteUser = function (userId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.deleteAuthorized("api/users/".concat(userId))];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UserDataAccess.prototype.getUserLoggedIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, body;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getAuthorized('api/users/logged-in')];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.data];
                    case 2:
                        body = _a.sent();
                        return [2 /*return*/, this.jsonConvert.deserializeObject(body.user, user_1.User)];
                }
            });
        });
    };
    UserDataAccess = __decorate([
        (0, tsyringe_1.injectable)(),
        __param(2, (0, tsyringe_1.inject)('fetcher')),
        __metadata("design:paramtypes", [json2typescript_1.JsonConvert,
            kerygma_configuration_1.KerygmaConfiguration, Function, local_storage_data_access_1["default"]])
    ], UserDataAccess);
    return UserDataAccess;
}(data_access_1["default"]));
exports["default"] = UserDataAccess;


/***/ })

}]);