"use strict";
(self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || []).push([["ui_src_services_sermon-data-access_ts-ui_src_model_bible-book-data_yaml"],{

/***/ "./ui/src/model/date-converter.ts":
/*!****************************************!*\
  !*** ./ui/src/model/date-converter.ts ***!
  \****************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var dayjs_1 = __importDefault(__webpack_require__(/*! dayjs */ "./ui/node_modules/dayjs/dayjs.min.js"));
var DateConverter = /** @class */ (function () {
    function DateConverter() {
    }
    DateConverter.prototype.deserialize = function (data) {
        return (0, dayjs_1["default"])(data);
    };
    DateConverter.prototype.serialize = function (data) {
        return data.toISOString();
    };
    DateConverter = __decorate([
        json2typescript_1.JsonConverter
    ], DateConverter);
    return DateConverter;
}());
exports["default"] = DateConverter;


/***/ }),

/***/ "./ui/src/model/sermon-tag.ts":
/*!************************************!*\
  !*** ./ui/src/model/sermon-tag.ts ***!
  \************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
exports.__esModule = true;
exports.SermonTag = void 0;
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var SermonTag = /** @class */ (function () {
    function SermonTag() {
        this.tag = '';
        this.sermonCount = 0;
        this.isSelected = false;
    }
    SermonTag_1 = SermonTag;
    SermonTag.prototype.clone = function () {
        var clone = new SermonTag_1();
        clone.tag = this.tag;
        clone.sermonCount = this.sermonCount;
        clone.isSelected = this.isSelected;
        return clone;
    };
    var SermonTag_1;
    __decorate([
        (0, json2typescript_1.JsonProperty)('tag', String),
        __metadata("design:type", Object)
    ], SermonTag.prototype, "tag");
    __decorate([
        (0, json2typescript_1.JsonProperty)('sermonCount', Number),
        __metadata("design:type", Object)
    ], SermonTag.prototype, "sermonCount");
    SermonTag = SermonTag_1 = __decorate([
        (0, json2typescript_1.JsonObject)('SermonTag')
    ], SermonTag);
    return SermonTag;
}());
exports.SermonTag = SermonTag;


/***/ }),

/***/ "./ui/src/model/sermon.ts":
/*!********************************!*\
  !*** ./ui/src/model/sermon.ts ***!
  \********************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.Sermon = void 0;
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var yup = __importStar(__webpack_require__(/*! yup */ "./ui/node_modules/yup/es/index.js"));
var date_converter_1 = __importDefault(__webpack_require__(/*! ./date-converter */ "./ui/src/model/date-converter.ts"));
var dayjs_1 = __importDefault(__webpack_require__(/*! dayjs */ "./ui/node_modules/dayjs/dayjs.min.js"));
var Sermon = /** @class */ (function () {
    function Sermon() {
        var _this = this;
        this.id = 0;
        this.sermonDate = '';
        this.speakerTitle = '';
        this.speakerFirstName = '';
        this.speakerLastName = '';
        this.title = '';
        this.tags = [];
        this.service = '';
        this.fromBook = '';
        this.fromChapter = 0;
        this.fromVerse = 0;
        this.thruBook = '';
        this.thruChapter = 0;
        this.thruVerse = 0;
        this.filename = '';
        this.uploadedTimestamp = (0, dayjs_1["default"])();
        this.size = 0;
        this.speakerFirstNameValidator = function () {
            return !yup.string().trim().required().isValidSync(_this.speakerFirstName)
                ? 'is required'
                : '';
        };
        this.speakerLastNameValidator = function () {
            return !yup.string().trim().required().isValidSync(_this.speakerLastName)
                ? 'is required'
                : '';
        };
        this.titleValidator = function () {
            return !yup.string().trim().required().isValidSync(_this.title)
                ? 'is required'
                : '';
        };
    }
    Sermon_1 = Sermon;
    Sermon.prototype.setSermonDate = function (dt) {
        this.sermonDate = dt.format('YYYY-MM-DD');
    };
    Sermon.prototype.getSermonDate = function () {
        return (0, dayjs_1["default"])(this.sermonDate, 'YYYY-MM-DD');
    };
    Sermon.prototype.isValid = function () {
        for (var key in this) {
            var validator = this[key];
            if (key.endsWith('Validator') &&
                typeof validator === 'function' &&
                validator() !== '') {
                return false;
            }
        }
        return true;
    };
    Sermon.prototype.getFilename = function (bibleBooks) {
        var _a;
        var dt = this.getSermonDate().format('YYYY-MM-DD');
        var svc = this.service.replace(' ', '_').toLowerCase();
        var suffix = null;
        var fromBook = this.getFromBook(bibleBooks);
        if (fromBook) {
            suffix = "".concat(fromBook.abbrev).concat((_a = this.fromChapter) !== null && _a !== void 0 ? _a : '', "v").concat(this.fromVerse, "-").concat(this.thruChapter, "v").concat(this.thruVerse);
        }
        else {
            suffix = this.title.replace(/\W/g, '');
        }
        return "".concat(dt, "_").concat(this.speakerFirstName, "_").concat(this.speakerLastName, "_").concat(svc, "_").concat(suffix, ".mp3");
    };
    Sermon.prototype.getPassage = function (bibleBooks) {
        var fromBook = this.getFromBook(bibleBooks);
        if (!fromBook) {
            return null;
        }
        return "".concat(fromBook.name, " ").concat(this.fromChapter, ":").concat(this.fromVerse, "-").concat(this.thruChapter, ":").concat(this.thruVerse);
    };
    Sermon.prototype.getFromBook = function (bibleBooks) {
        var _this = this;
        if (!this.fromBook) {
            return null;
        }
        var fromBooks = bibleBooks.filter(function (book) { return book.code === _this.fromBook; });
        return fromBooks.length > 0 ? fromBooks[0] : null;
    };
    /*
     * This is necessary for the useState react hook to modify the state
     * of the form.
     */
    Sermon.prototype.clone = function () {
        var clone = new Sermon_1();
        for (var _i = 0, _a = Object.keys(this); _i < _a.length; _i++) {
            var key = _a[_i];
            if (typeof this[key] === 'string' || typeof this[key] === 'number') {
                clone[key] = this[key];
            }
            else if (Array.isArray(this[key])) {
                clone[key] = __spreadArray([], this[key], true);
            }
            else if (dayjs_1["default"].isDayjs(this[key])) {
                clone[key] = this[key].clone();
            }
        }
        return clone;
    };
    var Sermon_1;
    __decorate([
        (0, json2typescript_1.JsonProperty)('id', Number),
        __metadata("design:type", Object)
    ], Sermon.prototype, "id");
    __decorate([
        (0, json2typescript_1.JsonProperty)('sermonDate', String),
        __metadata("design:type", Object)
    ], Sermon.prototype, "sermonDate");
    __decorate([
        (0, json2typescript_1.JsonProperty)('speakerTitle', String, true),
        __metadata("design:type", Object)
    ], Sermon.prototype, "speakerTitle");
    __decorate([
        (0, json2typescript_1.JsonProperty)('speakerFirstName', String),
        __metadata("design:type", Object)
    ], Sermon.prototype, "speakerFirstName");
    __decorate([
        (0, json2typescript_1.JsonProperty)('speakerLastName', String),
        __metadata("design:type", Object)
    ], Sermon.prototype, "speakerLastName");
    __decorate([
        (0, json2typescript_1.JsonProperty)('title', String),
        __metadata("design:type", Object)
    ], Sermon.prototype, "title");
    __decorate([
        (0, json2typescript_1.JsonProperty)('tags', [String], true),
        __metadata("design:type", Array)
    ], Sermon.prototype, "tags");
    __decorate([
        (0, json2typescript_1.JsonProperty)('service', String, true),
        __metadata("design:type", Object)
    ], Sermon.prototype, "service");
    __decorate([
        (0, json2typescript_1.JsonProperty)('fromBook', String, true),
        __metadata("design:type", Object)
    ], Sermon.prototype, "fromBook");
    __decorate([
        (0, json2typescript_1.JsonProperty)('fromChapter', Number, true),
        __metadata("design:type", Object)
    ], Sermon.prototype, "fromChapter");
    __decorate([
        (0, json2typescript_1.JsonProperty)('fromVerse', Number, true),
        __metadata("design:type", Object)
    ], Sermon.prototype, "fromVerse");
    __decorate([
        (0, json2typescript_1.JsonProperty)('thruBook', String, true),
        __metadata("design:type", Object)
    ], Sermon.prototype, "thruBook");
    __decorate([
        (0, json2typescript_1.JsonProperty)('thruChapter', Number, true),
        __metadata("design:type", Object)
    ], Sermon.prototype, "thruChapter");
    __decorate([
        (0, json2typescript_1.JsonProperty)('thruVerse', Number, true),
        __metadata("design:type", Object)
    ], Sermon.prototype, "thruVerse");
    __decorate([
        (0, json2typescript_1.JsonProperty)('filename', String),
        __metadata("design:type", Object)
    ], Sermon.prototype, "filename");
    __decorate([
        (0, json2typescript_1.JsonProperty)('uploadedTimestamp', date_converter_1["default"], true),
        __metadata("design:type", Object)
    ], Sermon.prototype, "uploadedTimestamp");
    __decorate([
        (0, json2typescript_1.JsonProperty)('size', Number, true),
        __metadata("design:type", Object)
    ], Sermon.prototype, "size");
    Sermon = Sermon_1 = __decorate([
        (0, json2typescript_1.JsonObject)('Sermon')
    ], Sermon);
    return Sermon;
}());
exports.Sermon = Sermon;


/***/ }),

/***/ "./ui/src/services/sermon-data-access.ts":
/*!***********************************************!*\
  !*** ./ui/src/services/sermon-data-access.ts ***!
  \***********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var data_access_1 = __importDefault(__webpack_require__(/*! ./data-access */ "./ui/src/services/data-access.ts"));
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var local_storage_data_access_1 = __importDefault(__webpack_require__(/*! ./local-storage-data-access */ "./ui/src/services/local-storage-data-access.ts"));
var http_status_codes_1 = __webpack_require__(/*! http-status-codes */ "./ui/node_modules/http-status-codes/build/es/index.js");
var sermon_1 = __webpack_require__(/*! ../model/sermon */ "./ui/src/model/sermon.ts");
var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./ui/node_modules/tsyringe/dist/esm5/index.js");
var p_debounce_1 = __importDefault(__webpack_require__(/*! p-debounce */ "./ui/node_modules/p-debounce/index.js"));
var kerygma_configuration_1 = __webpack_require__(/*! ../model/kerygma-configuration */ "./ui/src/model/kerygma-configuration.ts");
var sermon_tag_1 = __webpack_require__(/*! ../model/sermon-tag */ "./ui/src/model/sermon-tag.ts");
var SermonDataAccess = /** @class */ (function (_super) {
    __extends(SermonDataAccess, _super);
    function SermonDataAccess(jsonConvert, config, fetcher, localStorageDataAccess) {
        var _this = _super.call(this, config, fetcher, localStorageDataAccess) || this;
        _this.jsonConvert = jsonConvert;
        _this.debouncedGetSermons = (0, p_debounce_1["default"])(_this.getSermons, 500);
        return _this;
    }
    SermonDataAccess.prototype.addSermon = function (sermon, file, onUploadProgress) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = new FormData();
                        data.append('sermonDate', sermon.sermonDate);
                        data.append('speakerTitle', sermon.speakerTitle);
                        data.append('speakerFirstName', sermon.speakerFirstName);
                        data.append('speakerLastName', sermon.speakerLastName);
                        data.append('title', sermon.title);
                        data.append('tags', JSON.stringify(sermon.tags));
                        data.append('service', sermon.service);
                        data.append('fromBook', sermon.fromBook);
                        data.append('fromChapter', "".concat(sermon.fromChapter));
                        data.append('fromVerse', "".concat(sermon.fromVerse));
                        data.append('thruBook', sermon.thruBook);
                        data.append('thruChapter', "".concat(sermon.thruChapter));
                        data.append('thruVerse', "".concat(sermon.thruVerse));
                        data.append('file', file);
                        return [4 /*yield*/, this.postFormDataAuthorized('POST', 'api/sermons', data, [http_status_codes_1.StatusCodes.CONFLICT, http_status_codes_1.StatusCodes.REQUEST_TOO_LONG], onUploadProgress)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SermonDataAccess.prototype.saveSermon = function (sermon, file, onUploadProgress) {
        return __awaiter(this, void 0, void 0, function () {
            var data, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!file) return [3 /*break*/, 2];
                        data = new FormData();
                        data.append('sermonDate', sermon.sermonDate);
                        data.append('speakerTitle', sermon.speakerTitle);
                        data.append('speakerFirstName', sermon.speakerFirstName);
                        data.append('speakerLastName', sermon.speakerLastName);
                        data.append('title', sermon.title);
                        data.append('tags', JSON.stringify(sermon.tags));
                        data.append('service', sermon.service);
                        data.append('fromBook', sermon.fromBook);
                        data.append('fromChapter', "".concat(sermon.fromChapter));
                        data.append('fromVerse', "".concat(sermon.fromVerse));
                        data.append('thruBook', sermon.thruBook);
                        data.append('thruChapter', "".concat(sermon.thruChapter));
                        data.append('thruVerse', "".concat(sermon.thruVerse));
                        data.append('file', file);
                        return [4 /*yield*/, this.postFormDataAuthorized('PUT', "api/sermons/".concat(sermon.id), data, [http_status_codes_1.StatusCodes.CONFLICT, http_status_codes_1.StatusCodes.REQUEST_TOO_LONG], onUploadProgress)];
                    case 1: return [2 /*return*/, _a.sent()];
                    case 2: return [4 /*yield*/, this.putAuthorized("api/sermons/".concat(sermon.id), JSON.stringify(sermon), [http_status_codes_1.StatusCodes.CONFLICT])];
                    case 3:
                        response = _a.sent();
                        onUploadProgress({ loaded: 10, total: 10 });
                        return [2 /*return*/, response];
                }
            });
        });
    };
    SermonDataAccess.prototype.getSermons = function (page, perPage, sortOrder, filter, tags) {
        return __awaiter(this, void 0, void 0, function () {
            var params, response, body, sermons;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            page: page,
                            perPage: perPage,
                            sortOrder: sortOrder,
                            filter: filter,
                            tags: tags
                        };
                        return [4 /*yield*/, this.get('api/sermons', [], undefined, params)];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.data];
                    case 2:
                        body = _a.sent();
                        sermons = this.jsonConvert.deserializeArray(body.sermons, sermon_1.Sermon);
                        return [2 /*return*/, { sermons: sermons, totalRowCount: body.totalRowCount }];
                }
            });
        });
    };
    SermonDataAccess.prototype.getSermon = function (sermonId) {
        return __awaiter(this, void 0, void 0, function () {
            var response, body;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get("api/sermons/".concat(sermonId))];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.data];
                    case 2:
                        body = _a.sent();
                        return [2 /*return*/, this.jsonConvert.deserializeObject(body.sermon, sermon_1.Sermon)];
                }
            });
        });
    };
    SermonDataAccess.prototype.deleteSermon = function (sermonId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.deleteAuthorized("api/sermons/".concat(sermonId))];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SermonDataAccess.prototype.getSermonTags = function (sortOrder) {
        return __awaiter(this, void 0, void 0, function () {
            var params, response, body;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = { sortOrder: sortOrder };
                        return [4 /*yield*/, this.get('api/sermon-tags', [], undefined, params)];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.data];
                    case 2:
                        body = _a.sent();
                        return [2 /*return*/, this.jsonConvert.deserializeArray(body.tags, sermon_tag_1.SermonTag)];
                }
            });
        });
    };
    SermonDataAccess = __decorate([
        (0, tsyringe_1.injectable)(),
        __param(2, (0, tsyringe_1.inject)('fetcher')),
        __metadata("design:paramtypes", [json2typescript_1.JsonConvert,
            kerygma_configuration_1.KerygmaConfiguration, Function, local_storage_data_access_1["default"]])
    ], SermonDataAccess);
    return SermonDataAccess;
}(data_access_1["default"]));
exports["default"] = SermonDataAccess;


/***/ }),

/***/ "./ui/src/model/bible-book-data.yaml":
/*!*******************************************!*\
  !*** ./ui/src/model/bible-book-data.yaml ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({books:[{name:'Genesis',abbrev:'Gen',code:'01_Gen',chapters:[31,25,24,26,32,22,24,22,29,32,32,20,18,24,21,16,27,33,38,18,34,24,20,67,34,35,46,22,35,43,55,32,20,31,29,43,36,30,23,23,57,38,34,34,28,34,31,22,33,26]},{name:'Exodus',abbrev:'Exod',code:'02_Exod',chapters:[22,25,22,31,23,30,25,32,35,29,10,51,22,31,27,36,16,27,25,26,36,31,33,18,40,37,21,43,46,38,18,35,23,35,35,38,29,31,43,38]},{name:'Leviticus',abbrev:'Lev',code:'03_Lev',chapters:[17,16,17,35,19,30,38,36,24,20,47,8,59,57,33,34,16,30,37,27,24,33,44,23,55,46,34]},{name:'Numbers',abbrev:'Num',code:'04_Num',chapters:[54,34,51,49,31,27,89,26,23,36,35,16,33,45,41,50,13,32,22,29,35,41,30,25,18,65,23,31,40,16,54,42,56,29,34,13]},{name:'Deuteronomy',abbrev:'Deut',code:'05_Deut',chapters:[46,37,29,49,33,25,26,20,29,22,32,32,18,29,23,22,20,22,21,20,23,30,25,22,19,19,26,68,29,20,30,52,29,12]},{name:'Joshua',abbrev:'Josh',code:'06_Josh',chapters:[18,24,17,24,15,27,26,35,27,43,23,24,33,15,63,10,18,28,51,9,45,34,16,33]},{name:'Judges',abbrev:'Judg',code:'07_Judg',chapters:[36,23,31,24,31,40,25,35,57,18,40,15,25,20,20,31,13,31,30,48,25]},{name:'Ruth',abbrev:'Ruth',code:'08_Ruth',chapters:[22,23,18,22]},{name:'1 Samuel',abbrev:'1Sam',code:'09_1Sam',chapters:[28,36,21,22,12,21,17,22,27,27,15,25,23,52,35,23,58,30,24,42,15,23,29,22,44,25,12,25,11,31,13]},{name:'2 Samuel',abbrev:'2Sam',code:'10_2Sam',chapters:[27,32,39,12,25,23,29,18,13,19,27,31,39,33,37,23,29,33,43,26,22,51,39,25]},{name:'1 Kings',abbrev:'1Kgs',code:'11_1Kgs',chapters:[53,46,28,34,18,38,51,66,28,29,43,33,34,31,34,34,24,46,21,43,29,53]},{name:'2 Kings',abbrev:'2Kgs',code:'12_2Kgs',chapters:[18,25,27,44,27,33,20,29,37,36,21,21,25,29,38,20,41,37,37,21,26,20,37,20,30]},{name:'1 Chronicles',abbrev:'1Chr',code:'13_1Chr',chapters:[54,55,24,43,26,81,40,40,44,14,47,40,14,17,29,43,27,17,19,8,30,19,32,31,31,32,34,21,30]},{name:'2 Chronicles',abbrev:'2Chr',code:'14_2Chr',chapters:[17,18,17,22,14,42,22,18,31,19,23,16,22,15,19,14,19,34,11,37,20,12,21,27,28,23,9,27,36,27,21,33,25,33,27,23]},{name:'Ezra',abbrev:'Ezra',code:'15_Ezra',chapters:[11,70,13,24,17,22,28,36,15,44]},{name:'Nehemiah',abbrev:'Neh',code:'16_Neh',chapters:[11,20,32,23,19,19,73,18,38,39,36,47,31]},{name:'Esther',abbrev:'Esth',code:'17_Esth',chapters:[22,23,15,17,14,14,10,17,32,3]},{name:'Job',abbrev:'Job',code:'18_Job',chapters:[22,13,26,21,27,30,21,22,35,22,20,25,28,22,35,22,16,21,29,29,34,30,17,25,6,14,23,28,25,31,40,22,33,37,16,33,24,41,30,24,34,17]},{name:'Psalm',abbrev:'Ps',code:'19_Ps',chapters:[6,12,8,8,12,10,17,9,20,18,7,8,6,7,5,11,15,50,14,9,13,31,6,10,22,12,14,9,11,12,24,11,22,22,28,12,40,22,13,17,13,11,5,26,17,11,9,14,20,23,19,9,6,7,23,13,11,11,17,12,8,12,11,10,13,20,7,35,36,5,24,20,28,23,10,12,20,72,13,19,16,8,18,12,13,17,7,18,52,17,16,15,5,23,11,13,12,9,9,5,8,28,22,35,45,48,43,13,31,7,10,10,9,8,18,19,2,29,176,7,8,9,4,8,5,6,5,6,8,8,3,18,3,3,21,26,9,8,24,13,10,7,12,15,21,10,20,14,9,6]},{name:'Proverbs',abbrev:'Prov',code:'20_Prov',chapters:[33,22,35,27,23,35,27,36,18,32,31,28,25,35,33,33,28,24,29,30,31,29,35,34,28,28,27,28,27,33,31]},{name:'Ecclesiastes',abbrev:'Ecc',code:'21_Ecc',chapters:[18,26,22,16,20,12,29,17,18,20,10,14]},{name:'Song of Songs',abbrev:'Song',code:'22_Song',chapters:[17,17,11,16,16,13,13,14]},{name:'Isaiah',abbrev:'Isa',code:'23_Isa',chapters:[31,22,26,6,30,13,25,22,21,34,16,6,22,32,9,14,14,7,25,6,17,25,18,23,12,21,13,29,24,33,9,20,24,17,10,22,38,22,8,31,29,25,28,28,25,13,15,22,26,11,23,15,12,17,13,12,21,14,21,22,11,12,19,12,25,24]},{name:'Jeremiah',abbrev:'Jer',code:'24_Jer',chapters:[19,37,25,31,31,30,34,22,26,25,23,17,27,22,21,21,27,23,15,18,14,30,40,10,38,24,22,17,32,24,40,44,26,22,19,32,21,28,18,16,18,22,13,30,5,28,7,47,39,46,64,34]},{name:'Lamentations',abbrev:'Lam',code:'25_Lam',chapters:[22,22,66,22,22]},{name:'Ezekiel',abbrev:'Ezek',code:'26_Ezek',chapters:[28,10,27,17,17,14,27,18,11,22,25,28,23,23,8,63,24,32,14,49,32,31,49,27,17,21,36,26,21,26,18,32,33,31,15,38,28,23,29,49,26,20,27,31,25,24,23,35]},{name:'Daniel',abbrev:'Dan',code:'27_Dan',chapters:[21,49,30,37,31,28,28,27,27,21,45,13]},{name:'Hosea',abbrev:'Hos',code:'28_Hos',chapters:[11,23,5,19,15,11,16,14,17,15,12,14,16,9]},{name:'Joel',abbrev:'Joel',code:'29_Joel',chapters:[20,32,21]},{name:'Amos',abbrev:'Amos',code:'30_Amos',chapters:[15,16,15,13,27,14,17,14,15]},{name:'Obadiah',abbrev:'Obad',code:'31_Obad',chapters:[21]},{name:'Jonah',abbrev:'Jonah',code:'32_Jonah',chapters:[17,10,10,11]},{name:'Micah',abbrev:'Mic',code:'33_Mic',chapters:[16,13,12,13,15,16,20]},{name:'Nahum',abbrev:'Nah',code:'34_Nah',chapters:[15,13,19]},{name:'Habakkuk',abbrev:'Hab',code:'35_Hab',chapters:[17,20,19]},{name:'Zephaniah',abbrev:'Zeph',code:'36_Zeph',chapters:[18,15,20]},{name:'Haggia',abbrev:'Hag',code:'37_Hag',chapters:[15,23]},{name:'Zechariah',abbrev:'Zech',code:'38_Zech',chapters:[21,13,10,14,11,15,14,23,17,12,17,14,9,21]},{name:'Malachi',abbrev:'Mal',code:'39_Mal',chapters:[14,17,18,6]},{name:'Matthew',abbrev:'Matt',code:'40_Matt',chapters:[25,23,17,25,48,34,29,34,38,42,30,50,58,36,39,28,27,35,30,34,46,46,39,51,46,75,66,20]},{name:'Mark',abbrev:'Mark',code:'41_Mark',chapters:[45,28,35,41,43,56,37,38,50,52,33,44,37,72,47,20]},{name:'Luke',abbrev:'Luke',code:'42_Luke',chapters:[80,52,38,44,39,49,50,56,62,42,54,59,35,35,32,31,37,43,48,47,38,71,56,53]},{name:'John',abbrev:'John',code:'43_John',chapters:[51,25,36,54,47,71,53,59,41,42,57,50,38,31,27,33,26,40,42,31,25]},{name:'Acts',abbrev:'Acts',code:'44_Acts',chapters:[26,47,26,37,42,15,60,40,43,48,30,25,52,28,41,40,34,28,41,38,40,30,35,27,27,32,44,31]},{name:'Romans',abbrev:'Rom',code:'45_Rom',chapters:[32,29,31,25,21,23,25,39,33,21,36,21,14,23,33,27]},{name:'1 Corinthians',abbrev:'1Cor',code:'46_1Cor',chapters:[31,16,23,21,13,20,40,13,27,33,34,31,13,40,58,24]},{name:'2 Corinthians',abbrev:'2Cor',code:'47_2Cor',chapters:[24,17,18,18,21,18,16,24,15,18,33,21,14]},{name:'Galatians',abbrev:'Gal',code:'48_Gal',chapters:[24,21,29,31,26,18]},{name:'Ephesians',abbrev:'Eph',code:'49_Eph',chapters:[23,22,21,32,33,24]},{name:'Philippians',abbrev:'Phil',code:'50_Phil',chapters:[30,30,21,23]},{name:'Colossians',abbrev:'Col',code:'51_Col',chapters:[29,23,25,18]},{name:'1 Thessalonians',abbrev:'1Thess',code:'52_1Thess',chapters:[10,20,13,18,28]},{name:'2 Thessalonians',abbrev:'2Thess',code:'53_2Thess',chapters:[12,17,18]},{name:'1 Timothy',abbrev:'1Tim',code:'54_1Tim',chapters:[20,15,16,16,25,21]},{name:'2 Timothy',abbrev:'2Tim',code:'55_2Tim',chapters:[18,26,17,22]},{name:'Titus',abbrev:'Titus',code:'56_Titus',chapters:[16,15,15]},{name:'Philemon',abbrev:'Phlm',code:'57_Phlm',chapters:[25]},{name:'Hebrews',abbrev:'Heb',code:'58_Heb',chapters:[14,18,19,16,14,20,28,13,28,39,40,29,25]},{name:'James',abbrev:'Jas',code:'59_Jas',chapters:[27,26,18,17,20]},{name:'1 Peter',abbrev:'1Pet',code:'60_1Pet',chapters:[25,25,22,19,14]},{name:'2 Peter',abbrev:'2Pet',code:'61_2Pet',chapters:[21,22,18]},{name:'1 John',abbrev:'1John',code:'62_1John',chapters:[10,29,24,21,21]},{name:'2 John',abbrev:'2John',code:'63_2John',chapters:[13]},{name:'3 John',abbrev:'3John',code:'64_3John',chapters:[14]},{name:'Jude',abbrev:'Jude',code:'65_Jude',chapters:[25]},{name:'Revelation',abbrev:'Rev',code:'66_Rev',chapters:[20,29,22,11,14,17,17,13,21,11,19,17,18,20,8,21,18,24,21,15,27,20]}]});

/***/ })

}]);