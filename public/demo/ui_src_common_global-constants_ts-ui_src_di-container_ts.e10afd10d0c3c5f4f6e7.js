"use strict";
(self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || []).push([["ui_src_common_global-constants_ts-ui_src_di-container_ts"],{

/***/ "./ui/src/common/declare-build-constants.ts":
/*!**************************************************!*\
  !*** ./ui/src/common/declare-build-constants.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, exports) => {


exports.__esModule = true;
exports.kerygmaVersion = exports.kerygmaConfig = void 0;
exports.kerygmaConfig = {"generator":{"outDir":"public/demo","pagesDir":"pages","assetsDir":"assets"},"api":{"apiHost":"/","mediaContextRoot":"media"},"churchNameFull":"Trinity Sample Church","churchNameShort":"Trinity Sample Church","phone":"970-555-0100","email":"office@trinity-sample-church.org","footerText":"© Copyright 2024 Trinity Sample Church. All rights reserved.","logo":{"path":"img/church-logo.svg","height":"70px"},"favicon":{"path":"img/favicon.png","fileType":"image/png"},"worshipLocation":{"street1":"101 1st Street","street2":"","city":"Busy Town","state":"CO","postalCode":"80550","directionsUrl":"https://www.openstreetmap.org/#map=19/40.47984/-104.90587","location":{"lat":40.479953,"lng":-104.905926},"zoom":16},"defaultSpeakers":[{"title":"Pastor","firstName":"John","lastName":"Chrysostom","default":true},{"title":"","firstName":"Another","lastName":"Speaker","default":false}],"services":["Morning Worship","Evening Worship","Sunday School","Other"],"podcast":{"length":50,"siteUrl":"https://trinity-sample-church.org","title":"Trinity Sample Church","description":"Listen to the latest sermons from Trinity Sample Church. We seek to proclaim the richness of God’s grace in the gospel of Jesus Christ.\n","author":"John Chrysostom","copyright":"© Copyright 2024 Trinity Sample Church. All rights reserved.","language":"en","categories":["Religion & Spirituality","Christianity"]},"theme":{"fonts":{"default":{"url":"https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;600&display=swap","family":"'Ubuntu', 'Helvetica', sans-serif"},"titles":{"url":"https://fonts.googleapis.com/css2?family=Aleo:ital,wght@0,400;0,600;1,400;1,600&display=swap","family":"'Aleo', serif"},"weightDefault":300,"weightHeaders":400,"weightFooter":300,"sizeDefault":"17px","textTransformHeaders":"none"},"colors":{"primary":"#0F4D7F","primaryText":"white","links":"#285ab6","headerBackground":"white","bodyBackground":"white","footerBackground":"#20262f","footerFont":"#a2aec2","footerFontLink":"white","hr":"#e0e0e0","fontDefault":"#444","fontHeaders":"#285ab6","fontHeadersSmaller":"#888","border":"#d0d0d0"}},"navigation":[{"label":"About","children":[{"label":"Purpose","path":"about/purpose.html","children":[]},{"label":"Leadership","path":"about/leadership.html","children":[]},{"label":"What We Believe","path":"about/what-we-believe.html","children":[]},{"label":"What To Expect When Visiting","path":"about/what-to-expect.html","children":[]},{"label":"Church Life","path":"about/church-life.html","children":[]}]},{"label":"Sermons","path":"sermons.html","children":[]},{"label":"Resources","path":"resources.html","children":[]},{"label":"Contact Us","path":"contact-us.html","children":[]}]};
var kerygmaVersionObj = {"version":"1.0.0"};
exports.kerygmaVersion = kerygmaVersionObj.version;


/***/ }),

/***/ "./ui/src/common/global-constants.ts":
/*!*******************************************!*\
  !*** ./ui/src/common/global-constants.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, exports) => {


exports.__esModule = true;
exports.globalConstants = void 0;
exports.globalConstants = {
    textMaxWidth: '680px',
    formMaxWidth: '400px',
    bp: {
        phoneMax: '576px',
        tabletMin: '577px',
        tabletMax: '768px',
        mdMin: '769px',
        mdMax: '991px',
        lgMin: '992px',
        lgMax: '1199px',
        xlMin: '1200px',
        xlMax: '1399px',
        xxlMin: '1400px'
    },
    churchAdmin: {
        appName: 'Kerygma',
        version: '0.1 beta'
    },
    kerygmaUrl: 'https://gitlab.com/davestevens/kerygma'
};


/***/ }),

/***/ "./ui/src/di-container.ts":
/*!********************************!*\
  !*** ./ui/src/di-container.ts ***!
  \********************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
__webpack_require__(/*! reflect-metadata */ "./ui/node_modules/reflect-metadata/Reflect.js");
var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./ui/node_modules/tsyringe/dist/esm5/index.js");
var axios_1 = __importDefault(__webpack_require__(/*! axios */ "./ui/node_modules/axios/dist/browser/axios.cjs"));
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var kerygma_configuration_1 = __webpack_require__(/*! ./model/kerygma-configuration */ "./ui/src/model/kerygma-configuration.ts");
var declare_build_constants_1 = __webpack_require__(/*! ./common/declare-build-constants */ "./ui/src/common/declare-build-constants.ts");
var fetcher = function (url, settings) {
    return (0, axios_1["default"])(url, settings);
};
tsyringe_1.container.register('fetcher', { useValue: fetcher });
tsyringe_1.container.register('localStorage', { useValue: localStorage });
tsyringe_1.container.register('sessionStorage', { useValue: sessionStorage });
tsyringe_1.container.register(json2typescript_1.JsonConvert, { useValue: new json2typescript_1.JsonConvert() });
tsyringe_1.container.register(kerygma_configuration_1.KerygmaConfiguration, { useValue: declare_build_constants_1.kerygmaConfig });
tsyringe_1.container.register('kerygmaVersion', { useValue: declare_build_constants_1.kerygmaVersion });
exports["default"] = tsyringe_1.container;


/***/ }),

/***/ "./ui/src/model/kerygma-configuration.ts":
/*!***********************************************!*\
  !*** ./ui/src/model/kerygma-configuration.ts ***!
  \***********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
exports.__esModule = true;
exports.KerygmaConfiguration = exports.Navigation = exports.Podcast = exports.KerygmaTheme = exports.KerygmaThemeColors = exports.KerygmaThemeFonts = exports.FontFamily = exports.ConfigSpeaker = exports.ConfigLocation = exports.LatLng = exports.ConfigFavicon = exports.ConfigLogo = exports.ApiSettings = exports.GeneratorSettings = void 0;
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var GeneratorSettings = /** @class */ (function () {
    function GeneratorSettings() {
        this.outDir = '';
        this.pagesDir = '';
        this.assetsDir = '';
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('outDir', String),
        __metadata("design:type", Object)
    ], GeneratorSettings.prototype, "outDir");
    __decorate([
        (0, json2typescript_1.JsonProperty)('pagesDir', String),
        __metadata("design:type", Object)
    ], GeneratorSettings.prototype, "pagesDir");
    __decorate([
        (0, json2typescript_1.JsonProperty)('assetsDir', String),
        __metadata("design:type", Object)
    ], GeneratorSettings.prototype, "assetsDir");
    GeneratorSettings = __decorate([
        (0, json2typescript_1.JsonObject)('GeneratorSettings')
    ], GeneratorSettings);
    return GeneratorSettings;
}());
exports.GeneratorSettings = GeneratorSettings;
var ApiSettings = /** @class */ (function () {
    function ApiSettings() {
        this.apiHost = '';
        this.mediaContextRoot = '';
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('apiHost', String),
        __metadata("design:type", Object)
    ], ApiSettings.prototype, "apiHost");
    __decorate([
        (0, json2typescript_1.JsonProperty)('mediaContextRoot', String),
        __metadata("design:type", Object)
    ], ApiSettings.prototype, "mediaContextRoot");
    ApiSettings = __decorate([
        (0, json2typescript_1.JsonObject)('ApiSettings')
    ], ApiSettings);
    return ApiSettings;
}());
exports.ApiSettings = ApiSettings;
var ConfigLogo = /** @class */ (function () {
    function ConfigLogo() {
        this.path = '';
        this.height = '';
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('path', String),
        __metadata("design:type", Object)
    ], ConfigLogo.prototype, "path");
    __decorate([
        (0, json2typescript_1.JsonProperty)('height', String),
        __metadata("design:type", Object)
    ], ConfigLogo.prototype, "height");
    ConfigLogo = __decorate([
        (0, json2typescript_1.JsonObject)('ConfigLogo')
    ], ConfigLogo);
    return ConfigLogo;
}());
exports.ConfigLogo = ConfigLogo;
var ConfigFavicon = /** @class */ (function () {
    function ConfigFavicon() {
        this.path = '';
        this.fileType = '';
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('path', String),
        __metadata("design:type", Object)
    ], ConfigFavicon.prototype, "path");
    __decorate([
        (0, json2typescript_1.JsonProperty)('fileType', String),
        __metadata("design:type", Object)
    ], ConfigFavicon.prototype, "fileType");
    ConfigFavicon = __decorate([
        (0, json2typescript_1.JsonObject)('ConfigFavicon')
    ], ConfigFavicon);
    return ConfigFavicon;
}());
exports.ConfigFavicon = ConfigFavicon;
var LatLng = /** @class */ (function () {
    function LatLng() {
        this.lat = 0;
        this.lng = 0;
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('lat', Number),
        __metadata("design:type", Object)
    ], LatLng.prototype, "lat");
    __decorate([
        (0, json2typescript_1.JsonProperty)('lng', Number),
        __metadata("design:type", Object)
    ], LatLng.prototype, "lng");
    LatLng = __decorate([
        (0, json2typescript_1.JsonObject)('LatLng')
    ], LatLng);
    return LatLng;
}());
exports.LatLng = LatLng;
var ConfigLocation = /** @class */ (function () {
    function ConfigLocation() {
        this.street1 = '';
        this.street2 = '';
        this.city = '';
        this.state = '';
        this.postalCode = '';
        this.directionsUrl = '';
        this.location = undefined;
        this.zoom = 16;
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('street1', String),
        __metadata("design:type", Object)
    ], ConfigLocation.prototype, "street1");
    __decorate([
        (0, json2typescript_1.JsonProperty)('street2', String, true),
        __metadata("design:type", Object)
    ], ConfigLocation.prototype, "street2");
    __decorate([
        (0, json2typescript_1.JsonProperty)('city', String),
        __metadata("design:type", Object)
    ], ConfigLocation.prototype, "city");
    __decorate([
        (0, json2typescript_1.JsonProperty)('state', String),
        __metadata("design:type", Object)
    ], ConfigLocation.prototype, "state");
    __decorate([
        (0, json2typescript_1.JsonProperty)('postalCode', String),
        __metadata("design:type", Object)
    ], ConfigLocation.prototype, "postalCode");
    __decorate([
        (0, json2typescript_1.JsonProperty)('directionsUrl', String),
        __metadata("design:type", Object)
    ], ConfigLocation.prototype, "directionsUrl");
    __decorate([
        (0, json2typescript_1.JsonProperty)('location', LatLng),
        __metadata("design:type", LatLng)
    ], ConfigLocation.prototype, "location");
    __decorate([
        (0, json2typescript_1.JsonProperty)('zoom', Number),
        __metadata("design:type", Object)
    ], ConfigLocation.prototype, "zoom");
    ConfigLocation = __decorate([
        (0, json2typescript_1.JsonObject)('ConfigLocation')
    ], ConfigLocation);
    return ConfigLocation;
}());
exports.ConfigLocation = ConfigLocation;
var ConfigSpeaker = /** @class */ (function () {
    function ConfigSpeaker() {
        this.title = '';
        this.firstName = '';
        this.lastName = '';
        this["default"] = false;
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('title', String),
        __metadata("design:type", Object)
    ], ConfigSpeaker.prototype, "title");
    __decorate([
        (0, json2typescript_1.JsonProperty)('firstName', String),
        __metadata("design:type", Object)
    ], ConfigSpeaker.prototype, "firstName");
    __decorate([
        (0, json2typescript_1.JsonProperty)('lastName', String),
        __metadata("design:type", Object)
    ], ConfigSpeaker.prototype, "lastName");
    __decorate([
        (0, json2typescript_1.JsonProperty)('default', Boolean, true),
        __metadata("design:type", Object)
    ], ConfigSpeaker.prototype, "default");
    ConfigSpeaker = __decorate([
        (0, json2typescript_1.JsonObject)('ConfigSpeaker')
    ], ConfigSpeaker);
    return ConfigSpeaker;
}());
exports.ConfigSpeaker = ConfigSpeaker;
var FontFamily = /** @class */ (function () {
    function FontFamily() {
        this.url = '';
        this.family = '';
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('url', String),
        __metadata("design:type", Object)
    ], FontFamily.prototype, "url");
    __decorate([
        (0, json2typescript_1.JsonProperty)('family', String),
        __metadata("design:type", Object)
    ], FontFamily.prototype, "family");
    FontFamily = __decorate([
        (0, json2typescript_1.JsonObject)('FontFamily')
    ], FontFamily);
    return FontFamily;
}());
exports.FontFamily = FontFamily;
var KerygmaThemeFonts = /** @class */ (function () {
    function KerygmaThemeFonts() {
        this["default"] = undefined;
        this.titles = undefined;
        this.weightDefault = 300;
        this.weightHeaders = 400;
        this.weightFooter = 300;
        this.sizeDefault = '';
        this.textTransformHeaders = 'none';
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('default', FontFamily),
        __metadata("design:type", FontFamily)
    ], KerygmaThemeFonts.prototype, "default");
    __decorate([
        (0, json2typescript_1.JsonProperty)('titles', FontFamily),
        __metadata("design:type", FontFamily)
    ], KerygmaThemeFonts.prototype, "titles");
    __decorate([
        (0, json2typescript_1.JsonProperty)('weightDefault', Number, true),
        __metadata("design:type", Object)
    ], KerygmaThemeFonts.prototype, "weightDefault");
    __decorate([
        (0, json2typescript_1.JsonProperty)('weightHeaders', Number, true),
        __metadata("design:type", Object)
    ], KerygmaThemeFonts.prototype, "weightHeaders");
    __decorate([
        (0, json2typescript_1.JsonProperty)('weightFooter', Number, true),
        __metadata("design:type", Object)
    ], KerygmaThemeFonts.prototype, "weightFooter");
    __decorate([
        (0, json2typescript_1.JsonProperty)('sizeDefault', String, true),
        __metadata("design:type", Object)
    ], KerygmaThemeFonts.prototype, "sizeDefault");
    __decorate([
        (0, json2typescript_1.JsonProperty)('textTransformHeaders', String, true),
        __metadata("design:type", Object)
    ], KerygmaThemeFonts.prototype, "textTransformHeaders");
    KerygmaThemeFonts = __decorate([
        (0, json2typescript_1.JsonObject)('KerygmaThemeFonts')
    ], KerygmaThemeFonts);
    return KerygmaThemeFonts;
}());
exports.KerygmaThemeFonts = KerygmaThemeFonts;
var KerygmaThemeColors = /** @class */ (function () {
    function KerygmaThemeColors() {
        this.primary = '';
        this.primaryText = '';
        this.links = '';
        this.headerBackground = '';
        this.bodyBackground = '';
        this.footerBackground = '';
        this.footerFont = '';
        this.footerFontLink = '';
        this.hr = '';
        this.fontDefault = '';
        this.fontHeaders = '';
        this.fontHeadersSmaller = '';
        this.border = '';
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('primary', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "primary");
    __decorate([
        (0, json2typescript_1.JsonProperty)('primaryText', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "primaryText");
    __decorate([
        (0, json2typescript_1.JsonProperty)('links', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "links");
    __decorate([
        (0, json2typescript_1.JsonProperty)('headerBackground', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "headerBackground");
    __decorate([
        (0, json2typescript_1.JsonProperty)('bodyBackground', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "bodyBackground");
    __decorate([
        (0, json2typescript_1.JsonProperty)('footerBackground', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "footerBackground");
    __decorate([
        (0, json2typescript_1.JsonProperty)('footerFont', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "footerFont");
    __decorate([
        (0, json2typescript_1.JsonProperty)('footerFontLink', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "footerFontLink");
    __decorate([
        (0, json2typescript_1.JsonProperty)('hr', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "hr");
    __decorate([
        (0, json2typescript_1.JsonProperty)('fontDefault', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "fontDefault");
    __decorate([
        (0, json2typescript_1.JsonProperty)('fontHeaders', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "fontHeaders");
    __decorate([
        (0, json2typescript_1.JsonProperty)('fontHeadersSmaller', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "fontHeadersSmaller");
    __decorate([
        (0, json2typescript_1.JsonProperty)('border', String),
        __metadata("design:type", Object)
    ], KerygmaThemeColors.prototype, "border");
    KerygmaThemeColors = __decorate([
        (0, json2typescript_1.JsonObject)('KerygmaThemeColors')
    ], KerygmaThemeColors);
    return KerygmaThemeColors;
}());
exports.KerygmaThemeColors = KerygmaThemeColors;
var KerygmaTheme = /** @class */ (function () {
    function KerygmaTheme() {
        this.fonts = undefined;
        this.colors = undefined;
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('fonts', KerygmaThemeFonts),
        __metadata("design:type", KerygmaThemeFonts)
    ], KerygmaTheme.prototype, "fonts");
    __decorate([
        (0, json2typescript_1.JsonProperty)('colors', KerygmaThemeColors),
        __metadata("design:type", KerygmaThemeColors)
    ], KerygmaTheme.prototype, "colors");
    KerygmaTheme = __decorate([
        (0, json2typescript_1.JsonObject)('KerygmaTheme')
    ], KerygmaTheme);
    return KerygmaTheme;
}());
exports.KerygmaTheme = KerygmaTheme;
var Podcast = /** @class */ (function () {
    function Podcast() {
        this.length = undefined;
        this.siteUrl = undefined;
        this.title = undefined;
        this.description = undefined;
        this.author = undefined;
        this.copyright = undefined;
        this.language = undefined;
        this.categories = [];
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('length', Number),
        __metadata("design:type", Number)
    ], Podcast.prototype, "length");
    __decorate([
        (0, json2typescript_1.JsonProperty)('siteUrl', String),
        __metadata("design:type", String)
    ], Podcast.prototype, "siteUrl");
    __decorate([
        (0, json2typescript_1.JsonProperty)('title', String),
        __metadata("design:type", String)
    ], Podcast.prototype, "title");
    __decorate([
        (0, json2typescript_1.JsonProperty)('description', String),
        __metadata("design:type", String)
    ], Podcast.prototype, "description");
    __decorate([
        (0, json2typescript_1.JsonProperty)('author', String),
        __metadata("design:type", String)
    ], Podcast.prototype, "author");
    __decorate([
        (0, json2typescript_1.JsonProperty)('copyright', String),
        __metadata("design:type", String)
    ], Podcast.prototype, "copyright");
    __decorate([
        (0, json2typescript_1.JsonProperty)('language', String),
        __metadata("design:type", String)
    ], Podcast.prototype, "language");
    __decorate([
        (0, json2typescript_1.JsonProperty)('categories', [String]),
        __metadata("design:type", Array)
    ], Podcast.prototype, "categories");
    Podcast = __decorate([
        (0, json2typescript_1.JsonObject)('Podcast')
    ], Podcast);
    return Podcast;
}());
exports.Podcast = Podcast;
var Navigation = /** @class */ (function () {
    function Navigation() {
        this.label = undefined;
        this.path = undefined;
        this.children = [];
    }
    Navigation_1 = Navigation;
    var Navigation_1;
    __decorate([
        (0, json2typescript_1.JsonProperty)('label', String),
        __metadata("design:type", String)
    ], Navigation.prototype, "label");
    __decorate([
        (0, json2typescript_1.JsonProperty)('path', String, true),
        __metadata("design:type", String)
    ], Navigation.prototype, "path");
    __decorate([
        (0, json2typescript_1.JsonProperty)('children', [Navigation_1], true),
        __metadata("design:type", Array)
    ], Navigation.prototype, "children");
    Navigation = Navigation_1 = __decorate([
        (0, json2typescript_1.JsonObject)('Navigation')
    ], Navigation);
    return Navigation;
}());
exports.Navigation = Navigation;
var KerygmaConfiguration = /** @class */ (function () {
    function KerygmaConfiguration() {
        this.generator = undefined;
        this.api = undefined;
        this.churchNameFull = '';
        this.churchNameShort = '';
        this.phone = '';
        this.email = '';
        this.footerText = '';
        this.logo = undefined;
        this.favicon = undefined;
        this.worshipLocation = undefined;
        this.defaultSpeakers = [];
        this.services = ['Morning Worship'];
        this.podcast = undefined;
        this.theme = undefined;
        this.navigation = [];
    }
    __decorate([
        (0, json2typescript_1.JsonProperty)('generator', GeneratorSettings),
        __metadata("design:type", GeneratorSettings)
    ], KerygmaConfiguration.prototype, "generator");
    __decorate([
        (0, json2typescript_1.JsonProperty)('api', ApiSettings),
        __metadata("design:type", ApiSettings)
    ], KerygmaConfiguration.prototype, "api");
    __decorate([
        (0, json2typescript_1.JsonProperty)('churchNameFull', String),
        __metadata("design:type", Object)
    ], KerygmaConfiguration.prototype, "churchNameFull");
    __decorate([
        (0, json2typescript_1.JsonProperty)('churchNameShort', String),
        __metadata("design:type", Object)
    ], KerygmaConfiguration.prototype, "churchNameShort");
    __decorate([
        (0, json2typescript_1.JsonProperty)('phone', String),
        __metadata("design:type", Object)
    ], KerygmaConfiguration.prototype, "phone");
    __decorate([
        (0, json2typescript_1.JsonProperty)('email', String),
        __metadata("design:type", Object)
    ], KerygmaConfiguration.prototype, "email");
    __decorate([
        (0, json2typescript_1.JsonProperty)('footerText', String),
        __metadata("design:type", Object)
    ], KerygmaConfiguration.prototype, "footerText");
    __decorate([
        (0, json2typescript_1.JsonProperty)('logo', ConfigLogo),
        __metadata("design:type", ConfigLogo)
    ], KerygmaConfiguration.prototype, "logo");
    __decorate([
        (0, json2typescript_1.JsonProperty)('favicon', ConfigFavicon),
        __metadata("design:type", ConfigFavicon)
    ], KerygmaConfiguration.prototype, "favicon");
    __decorate([
        (0, json2typescript_1.JsonProperty)('worshipLocation', ConfigLocation),
        __metadata("design:type", ConfigLocation)
    ], KerygmaConfiguration.prototype, "worshipLocation");
    __decorate([
        (0, json2typescript_1.JsonProperty)('defaultSpeakers', [ConfigSpeaker]),
        __metadata("design:type", Array)
    ], KerygmaConfiguration.prototype, "defaultSpeakers");
    __decorate([
        (0, json2typescript_1.JsonProperty)('services', [String]),
        __metadata("design:type", Object)
    ], KerygmaConfiguration.prototype, "services");
    __decorate([
        (0, json2typescript_1.JsonProperty)('podcast', Podcast),
        __metadata("design:type", Podcast)
    ], KerygmaConfiguration.prototype, "podcast");
    __decorate([
        (0, json2typescript_1.JsonProperty)('theme', KerygmaTheme),
        __metadata("design:type", KerygmaTheme)
    ], KerygmaConfiguration.prototype, "theme");
    __decorate([
        (0, json2typescript_1.JsonProperty)('navigation', [Navigation]),
        __metadata("design:type", Array)
    ], KerygmaConfiguration.prototype, "navigation");
    KerygmaConfiguration = __decorate([
        (0, json2typescript_1.JsonObject)('KerygmaConfiguration')
    ], KerygmaConfiguration);
    return KerygmaConfiguration;
}());
exports.KerygmaConfiguration = KerygmaConfiguration;


/***/ })

}]);