"use strict";
(self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || []).push([["ui_src_components_church-admin_admin-page_tsx"],{

/***/ "./ui/src/components/church-admin/admin-breadcrumbs.styles.ts":
/*!********************************************************************!*\
  !*** ./ui/src/components/church-admin/admin-breadcrumbs.styles.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    breadcrumbs: "\n    padding: 8px 16px;\n    border-bottom: 1px solid ".concat(core_1.Colors.LIGHT_GRAY1, ";\n    margin-bottom: 16px;\n  ")
});


/***/ }),

/***/ "./ui/src/components/church-admin/admin-breadcrumbs.tsx":
/*!**************************************************************!*\
  !*** ./ui/src/components/church-admin/admin-breadcrumbs.tsx ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var admin_breadcrumbs_styles_1 = __webpack_require__(/*! ./admin-breadcrumbs.styles */ "./ui/src/components/church-admin/admin-breadcrumbs.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var AdminBreadcrumbs = function (_a) {
    var items = _a.items;
    var classes = (0, admin_breadcrumbs_styles_1.useStyles)();
    return (React.createElement("div", { className: classes.breadcrumbs },
        React.createElement(core_1.Breadcrumbs, { items: items })));
};
exports["default"] = AdminBreadcrumbs;


/***/ }),

/***/ "./ui/src/components/church-admin/admin-navbar.styles.ts":
/*!***************************************************************!*\
  !*** ./ui/src/components/church-admin/admin-navbar.styles.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    homeLink: {
        '&:hover': "\n      text-decoration: none;\n    "
    },
    homeLinkSpan: "\n    color: ".concat(core_1.Colors.LIGHT_GRAY5, ";\n  "),
    navbarCustom: "\n    box-shadow: none !important;\n  ",
    navContainer: "\n    display: flex;\n    justify-content: space-between;\n  ",
    buttonRM: "\n    margin-right: 16px;\n  ",
    user: "\n    display: flex;\n    font-size: 12px;\n    margin-right: 16px;\n    align-items: center;\n  ",
    icon: "\n    margin-right: 8px;\n  "
});


/***/ }),

/***/ "./ui/src/components/church-admin/admin-navbar.tsx":
/*!*********************************************************!*\
  !*** ./ui/src/components/church-admin/admin-navbar.tsx ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var di_container_1 = __importDefault(__webpack_require__(/*! ../../di-container */ "./ui/src/di-container.ts"));
var admin_navbar_styles_1 = __webpack_require__(/*! ./admin-navbar.styles */ "./ui/src/components/church-admin/admin-navbar.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var auth_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/auth-data-access */ "./ui/src/services/auth-data-access.ts"));
var authDataAccess = di_container_1["default"].resolve(auth_data_access_1["default"]);
var AdminNavbar = function (_a) {
    var isDarkMode = _a.isDarkMode, onSwitchMode = _a.onSwitchMode, loginUser = _a.loginUser, hideNavLinks = _a.hideNavLinks;
    var _b = React.useState(false), isProcessing = _b[0], setIsProcessing = _b[1];
    var classes = (0, admin_navbar_styles_1.useStyles)();
    var onLogout = function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    setIsProcessing(true);
                    return [4 /*yield*/, authDataAccess.logout()];
                case 1:
                    _a.sent();
                    setIsProcessing(false);
                    window.location.href = 'logged-out.html';
                    return [2 /*return*/];
            }
        });
    }); };
    var loginUserSection = (React.createElement("div", { className: "".concat(classes.user, " ").concat(core_1.Classes.TEXT_MUTED) }, loginUser !== undefined && (React.createElement(React.Fragment, null,
        React.createElement(core_1.Icon, { icon: "person", size: 10, className: classes.icon }), loginUser === null || loginUser === void 0 ? void 0 :
        loginUser.firstName,
        " ", loginUser === null || loginUser === void 0 ? void 0 :
        loginUser.lastName))));
    return (React.createElement(core_1.Navbar, { fixedToTop: true, className: "".concat(core_1.Classes.DARK, " ").concat(classes.navbarCustom) },
        React.createElement("div", { className: classes.navContainer },
            React.createElement(core_1.NavbarGroup, { align: "left" },
                React.createElement(core_1.NavbarHeading, null,
                    React.createElement("a", { className: classes.homeLink, href: "index.html" },
                        React.createElement("span", { className: classes.homeLinkSpan }, "Church Admin"))),
                !hideNavLinks && (React.createElement(React.Fragment, null,
                    React.createElement(core_1.NavbarDivider, null),
                    React.createElement(core_1.AnchorButton, { href: "sermons.html", icon: "headset", text: "Sermons", minimal: true }),
                    React.createElement(core_1.AnchorButton, { href: "documents.html", icon: "document", text: "Documents", minimal: true }),
                    loginUser && loginUser.authLevel === 'sysadmin' && (React.createElement(core_1.AnchorButton, { href: "users.html", icon: "people", text: "Users", minimal: true }))))),
            React.createElement(core_1.NavbarGroup, { align: "right" },
                React.createElement(core_1.Button, { icon: isDarkMode ? 'flash' : 'moon', minimal: true, className: classes.buttonRM, title: isDarkMode ? 'Light Mode' : 'Dark Mode', onClick: onSwitchMode, outlined: false }),
                !hideNavLinks && (React.createElement(React.Fragment, null,
                    loginUserSection,
                    React.createElement(core_1.Button, { icon: "log-out", text: isProcessing ? 'Logging out...' : 'Logout', minimal: true, onClick: onLogout, disabled: isProcessing })))))));
};
exports["default"] = AdminNavbar;


/***/ }),

/***/ "./ui/src/components/church-admin/admin-page.styles.ts":
/*!*************************************************************!*\
  !*** ./ui/src/components/church-admin/admin-page.styles.ts ***!
  \*************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var _a, _b;
exports.__esModule = true;
exports.useStyles = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
var header = {
    fontFamily: "'Ubuntu', sans-serif",
    textTransform: 'none',
    margin: 0
};
exports.useStyles = (0, react_jss_1.createUseStyles)({
    pageContainer: (_a = {
            display: 'flex',
            flexDirection: 'column',
            fontSize: '14px',
            minHeight: '100vh',
            fontFamily: "'Ubuntu', sans-serif"
        },
        _a["&.".concat(core_1.Classes.DARK)] = "\n      background-color: ".concat(core_1.Colors.DARK_GRAY2, ";\n    "),
        _a["&.".concat(core_1.Classes.DARK, " .admin-footer")] = "\n      border-top: 1px solid ".concat(core_1.Colors.GRAY1, ";\n      background-color: ").concat(core_1.Colors.DARK_GRAY5, ";\n    "),
        _a),
    '@global': (_b = {
            body: "\n      margin: 0;\n    "
        },
        _b[".".concat(core_1.Classes.DARK)] = {
            '& .card-header': "\n        background-color: ".concat(core_1.Colors.DARK_GRAY5, ";\n        border: 1px solid #555;\n      ")
        },
        _b.h1 = __assign(__assign({}, header), { fontSize: '24px' }),
        _b.h2 = __assign(__assign({}, header), { fontSize: '22px' }),
        _b.h3 = __assign(__assign({}, header), { fontSize: '20px' }),
        _b.h4 = __assign(__assign({}, header), { fontSize: '18px' }),
        _b.h5 = __assign(__assign({}, header), { fontSize: '16px', fontWeight: 'bold' }),
        _b['.card-header'] = "\n      border-bottom: 1px solid ".concat(core_1.Colors.LIGHT_GRAY1, ";\n      background-color: ").concat(core_1.Colors.LIGHT_GRAY4, ";\n      border-top-left-radius: 2px;\n      border-top-right-radius: 2px;\n    "),
        _b['.card-header h1'] = { fontSize: '18px' },
        _b['.card-header h2'] = { fontSize: '18px' },
        _b),
    contentBody: "\n    display: flex;\n    flex-direction: column;\n    padding: 50px 0 20px;\n  ",
    footer: "\n    margin-top: auto;\n    background-color: ".concat(core_1.Colors.LIGHT_GRAY4, ";\n    padding: 8px 16px;\n    border-top: 1px solid ").concat(core_1.Colors.LIGHT_GRAY1, ";\n    font-size: 12px;\n    font-style: italic;\n  ")
});


/***/ }),

/***/ "./ui/src/components/church-admin/admin-page.tsx":
/*!*******************************************************!*\
  !*** ./ui/src/components/church-admin/admin-page.tsx ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var admin_navbar_1 = __importDefault(__webpack_require__(/*! ./admin-navbar */ "./ui/src/components/church-admin/admin-navbar.tsx"));
var admin_breadcrumbs_1 = __importDefault(__webpack_require__(/*! ./admin-breadcrumbs */ "./ui/src/components/church-admin/admin-breadcrumbs.tsx"));
var admin_page_styles_1 = __webpack_require__(/*! ./admin-page.styles */ "./ui/src/components/church-admin/admin-page.styles.ts");
var global_constants_1 = __webpack_require__(/*! ../../common/global-constants */ "./ui/src/common/global-constants.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_helmet_1 = __webpack_require__(/*! react-helmet */ "./ui/node_modules/react-helmet/es/Helmet.js");
var error_boundary_1 = __importDefault(__webpack_require__(/*! ../error-boundary */ "./ui/src/components/error-boundary.tsx"));
var di_container_1 = __importDefault(__webpack_require__(/*! ../../di-container */ "./ui/src/di-container.ts"));
var local_storage_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/local-storage-data-access */ "./ui/src/services/local-storage-data-access.ts"));
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ./dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var localStorageDataAccess = di_container_1["default"].resolve(local_storage_data_access_1["default"]);
var kerygmaVersion = di_container_1["default"].resolve('kerygmaVersion');
var initDarkMode = localStorageDataAccess.getDarkMode();
var AdminPage = function (_a) {
    var breadcrumbs = _a.breadcrumbs, error = _a.error, clearError = _a.clearError, onLogin = _a.onLogin, children = _a.children, loginUser = _a.loginUser, hideNavLinks = _a.hideNavLinks;
    // replace this with local storage
    var _b = React.useState(initDarkMode), isDarkMode = _b[0], setIsDarkMode = _b[1];
    var classes = (0, admin_page_styles_1.useStyles)();
    var onSwitchMode = function () {
        localStorageDataAccess.saveDarkMode(!isDarkMode);
        setIsDarkMode(!isDarkMode);
    };
    return (React.createElement("div", { className: "".concat(classes.pageContainer, " ").concat(isDarkMode && core_1.Classes.DARK) },
        React.createElement(dark_mode_context_1["default"].Provider, { value: isDarkMode },
            React.createElement(error_boundary_1["default"], { error: error, clearError: clearError, onLogin: onLogin },
                React.createElement(react_helmet_1.Helmet, null,
                    React.createElement("meta", { charSet: "utf-8" }),
                    React.createElement("title", null,
                        "Church Admin - ",
                        global_constants_1.globalConstants.churchAdmin.appName),
                    React.createElement("meta", { name: "description", content: "This is where you manage your church content." }),
                    React.createElement("meta", { name: "robots", content: "noindex" })),
                React.createElement(admin_navbar_1["default"], { hideNavLinks: hideNavLinks, isDarkMode: isDarkMode, onSwitchMode: onSwitchMode, loginUser: loginUser }),
                React.createElement("div", { className: classes.contentBody },
                    breadcrumbs && React.createElement(admin_breadcrumbs_1["default"], { items: breadcrumbs }),
                    children),
                React.createElement("div", { className: "".concat(classes.footer, " admin-footer") },
                    "Powered by",
                    ' ',
                    React.createElement("a", { href: global_constants_1.globalConstants.kerygmaUrl }, global_constants_1.globalConstants.churchAdmin.appName),
                    ' ',
                    "vs. ",
                    kerygmaVersion,
                    ".")))));
};
exports["default"] = AdminPage;


/***/ }),

/***/ "./ui/src/components/church-admin/login-dialog.styles.ts":
/*!***************************************************************!*\
  !*** ./ui/src/components/church-admin/login-dialog.styles.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    error: "\n    color: ".concat(core_1.Colors.RED2, ";\n    font-size: 14px;\n    margin-bottom: 16px;\n  "),
    loginButton: "\n    margin-bottom: 16px;\n  "
});


/***/ }),

/***/ "./ui/src/components/church-admin/login-dialog.tsx":
/*!*********************************************************!*\
  !*** ./ui/src/components/church-admin/login-dialog.tsx ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var react_1 = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var validation_form_group_1 = __importDefault(__webpack_require__(/*! ./validation-form-group */ "./ui/src/components/church-admin/validation-form-group.tsx"));
var login_form_1 = __webpack_require__(/*! ../../model/login-form */ "./ui/src/model/login-form.ts");
var di_container_1 = __importDefault(__webpack_require__(/*! ../../di-container */ "./ui/src/di-container.ts"));
var auth_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/auth-data-access */ "./ui/src/services/auth-data-access.ts"));
var local_storage_data_access_1 = __importDefault(__webpack_require__(/*! ../../services/local-storage-data-access */ "./ui/src/services/local-storage-data-access.ts"));
var login_dialog_styles_1 = __webpack_require__(/*! ./login-dialog.styles */ "./ui/src/components/church-admin/login-dialog.styles.ts");
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ./dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var http_status_codes_1 = __webpack_require__(/*! http-status-codes */ "./ui/node_modules/http-status-codes/build/es/index.js");
var authDataAccess = di_container_1["default"].resolve(auth_data_access_1["default"]);
var localStorageDataAccess = di_container_1["default"].resolve(local_storage_data_access_1["default"]);
var LoginDialog = function (_a) {
    var isOpen = _a.isOpen, onLogin = _a.onLogin;
    var classes = (0, login_dialog_styles_1.useStyles)();
    var _b = react_1["default"].useState(new login_form_1.LoginForm()), loginForm = _b[0], setLoginForm = _b[1];
    var _c = react_1["default"].useState(false), isProcessing = _c[0], setIsProcessing = _c[1];
    var _d = react_1["default"].useState(null), usernameRef = _d[0], setUsernameRef = _d[1];
    var _e = react_1["default"].useState(''), errorMessage = _e[0], setErrorMessage = _e[1];
    var isDarkMode = react_1["default"].useContext(dark_mode_context_1["default"]);
    var onFieldChange = function (id, newValue) {
        loginForm[id] = newValue;
        setLoginForm(loginForm.clone());
        setErrorMessage('');
    };
    var handleLogin = function () { return __awaiter(void 0, void 0, void 0, function () {
        var token;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    setErrorMessage('');
                    setIsProcessing(true);
                    return [4 /*yield*/, authDataAccess.login(loginForm.username, loginForm.password)];
                case 1:
                    token = _a.sent();
                    setIsProcessing(false);
                    switch (token) {
                        case http_status_codes_1.StatusCodes.FORBIDDEN:
                            setErrorMessage('Incorrect username or password.');
                            break;
                        case http_status_codes_1.StatusCodes.LOCKED:
                            setErrorMessage('Username has been suspended for 30 minutes due to failed login attempts.');
                            break;
                        default: {
                            localStorageDataAccess.saveToken(token);
                            onLogin();
                        }
                    }
                    return [2 /*return*/];
            }
        });
    }); };
    var focusOnUsername = function () {
        if (usernameRef) {
            usernameRef.focus();
        }
    };
    (0, react_1.useEffect)(function () {
        if (isOpen) {
            authDataAccess.generateCsrfToken().then(function (csrfToken) {
                localStorageDataAccess.saveCsrfToken(csrfToken);
            });
        }
    }, [isOpen]);
    return (react_1["default"].createElement(core_1.Dialog, { isOpen: isOpen, title: "Church Admin Login", isCloseButtonShown: false, onOpened: focusOnUsername, className: isDarkMode ? core_1.Classes.DARK : '' },
        react_1["default"].createElement("form", { className: core_1.Classes.DIALOG_BODY, style: { marginBottom: 0 } },
            react_1["default"].createElement(validation_form_group_1["default"], { id: "username", value: loginForm.username, validator: function () {
                    return loginForm.usernameValidator();
                }, setter: onFieldChange, placeholder: "Username", leftIcon: "person", setInputRef: setUsernameRef }),
            react_1["default"].createElement(validation_form_group_1["default"], { id: "password", value: loginForm.password, validator: function () {
                    return loginForm.passwordValidator();
                }, setter: onFieldChange, placeholder: "Password", leftIcon: "lock", inputType: "password" }),
            errorMessage !== '' && (react_1["default"].createElement("div", { className: classes.error }, errorMessage)),
            react_1["default"].createElement(core_1.Button, { text: isProcessing ? 'Logging In...' : 'Log In', onClick: handleLogin, disabled: !loginForm.isValid() || isProcessing, fill: true, large: true, intent: "primary", type: "submit", className: classes.loginButton }))));
};
exports["default"] = LoginDialog;


/***/ }),

/***/ "./ui/src/components/church-admin/validation-form-group.styles.ts":
/*!************************************************************************!*\
  !*** ./ui/src/components/church-admin/validation-form-group.styles.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    formGroup: "\n    width: 100%;\n  ",
    toolTip: "\n    font-size: 12px;\n  ",
    errorMessage: "\n    font-size: 12px;\n    color: ".concat(core_1.Colors.RED3, ";\n    margin-top: 5px;\n  "),
    errorMessageDark: "\n    font-size: 12px;\n    color: ".concat(core_1.Colors.RED5, ";\n    margin-top: 5px;\n  ")
});


/***/ }),

/***/ "./ui/src/components/church-admin/validation-form-group.tsx":
/*!******************************************************************!*\
  !*** ./ui/src/components/church-admin/validation-form-group.tsx ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var react_1 = __importDefault(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var validation_form_group_styles_1 = __webpack_require__(/*! ./validation-form-group.styles */ "./ui/src/components/church-admin/validation-form-group.styles.ts");
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ./dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var ValidationFormGroup = function (_a) {
    var id = _a.id, value = _a.value, validator = _a.validator, setter = _a.setter, label = _a.label, labelInfo = _a.labelInfo, helperText = _a.helperText, inputType = _a.inputType, placeholder = _a.placeholder, leftIcon = _a.leftIcon, setInputRef = _a.setInputRef, isLoading = _a.isLoading, rightText = _a.rightText, noFill = _a.noFill, hideFormGroup = _a.hideFormGroup, className = _a.className;
    var classes = (0, validation_form_group_styles_1.useStyles)();
    var _b = react_1["default"].useState(false), showPassword = _b[0], setShowPassword = _b[1];
    var _c = react_1["default"].useState(''), errorMessage = _c[0], setErrorMessage = _c[1];
    var helper = errorMessage === '' ? helperText : errorMessage;
    var isDarkMode = react_1["default"].useContext(dark_mode_context_1["default"]);
    var errorClass = isDarkMode
        ? classes.errorMessageDark
        : classes.errorMessage;
    var handleLockClick = function () {
        if (showPassword) {
            setShowPassword(false);
        }
        else {
            setShowPassword(true);
        }
    };
    var toolTip = (react_1["default"].createElement("div", { className: classes.toolTip }, "".concat(showPassword ? 'Hide' : 'Show', " Password")));
    var showButton = (react_1["default"].createElement(core_1.Tooltip, { content: toolTip, usePortal: false },
        react_1["default"].createElement(core_1.Button, { icon: showPassword ? 'eye-open' : 'eye-off', minimal: true, tabIndex: 999, onClick: handleLockClick })));
    var onChangeInput = function (event) { return __awaiter(void 0, void 0, void 0, function () {
        var errorMessage, prefix;
        return __generator(this, function (_a) {
            setter(id, event.currentTarget.value);
            errorMessage = validator ? validator() : '';
            if (errorMessage !== '') {
                prefix = label ? label : placeholder;
                setErrorMessage("".concat(prefix, " ").concat(errorMessage));
            }
            else {
                setErrorMessage('');
            }
            return [2 /*return*/];
        });
    }); };
    var fieldType = inputType === 'password' ? (showPassword ? 'text' : 'password') : 'text';
    var rightTag = inputType === 'password' ? (showButton) : rightText ? (react_1["default"].createElement(core_1.Tag, { minimal: true }, rightText)) : undefined;
    var inputGroup = (react_1["default"].createElement(core_1.InputGroup, { id: id, onChange: onChangeInput, intent: errorMessage ? 'danger' : 'none', value: value, type: fieldType, rightElement: rightTag, fill: !noFill, placeholder: placeholder, leftIcon: leftIcon, inputRef: function (input) {
            if (setInputRef) {
                setInputRef(input);
            }
            return input;
        } }));
    if (hideFormGroup) {
        return (react_1["default"].createElement("div", { className: className },
            inputGroup,
            errorMessage && react_1["default"].createElement("div", { className: errorClass }, errorMessage)));
    }
    return (react_1["default"].createElement(core_1.FormGroup, { label: label, labelInfo: labelInfo, intent: errorMessage ? 'danger' : 'none', helperText: helper, className: "\n        ".concat(classes.formGroup, "\n        ").concat(isLoading && core_1.Classes.SKELETON, "\n        ").concat(className !== null && className !== void 0 ? className : '', "\n      ") }, inputGroup));
};
exports["default"] = ValidationFormGroup;


/***/ }),

/***/ "./ui/src/components/error-boundary.tsx":
/*!**********************************************!*\
  !*** ./ui/src/components/error-boundary.tsx ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var unauthorized_error_1 = __importDefault(__webpack_require__(/*! ../model/unauthorized-error */ "./ui/src/model/unauthorized-error.ts"));
var login_dialog_1 = __importDefault(__webpack_require__(/*! ./church-admin/login-dialog */ "./ui/src/components/church-admin/login-dialog.tsx"));
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ./church-admin/dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var ErrorBoundary = /** @class */ (function (_super) {
    __extends(ErrorBoundary, _super);
    function ErrorBoundary(props) {
        var _this = _super.call(this, props) || this;
        _this.cancel = function () {
            _this.props.clearError();
        };
        _this.state = { showError: false, showLogin: false };
        return _this;
    }
    ErrorBoundary.getDerivedStateFromProps = function (props) {
        if (props.error) {
            if (props.error instanceof unauthorized_error_1["default"]) {
                return { showLogin: true };
            }
            else {
                console.error('ErrorBoundary was passed the following error. Showing error alert.');
                console.error(props.error);
                return { showError: true };
            }
        }
        return { showLogin: false, showError: false };
    };
    ErrorBoundary.prototype.componentDidCatch = function (error, info) {
        console.error('ErrorBoundary caught the following error. Showing error alert.');
        console.error(error);
        console.error(info);
        this.setState({ showError: true });
    };
    ErrorBoundary.prototype.render = function () {
        var _this = this;
        return (React.createElement(React.Fragment, null,
            React.createElement(dark_mode_context_1["default"].Consumer, null, function (isDarkMode) { return (React.createElement(core_1.Alert, { isOpen: _this.state.showError, onConfirm: _this.cancel, icon: "error", className: isDarkMode ? core_1.Classes.DARK : '' }, "Oops! Something went wrong with that last request.")); }),
            React.createElement(login_dialog_1["default"], { isOpen: this.state.showLogin, onLogin: this.props.onLogin }),
            this.props.children));
    };
    return ErrorBoundary;
}(React.Component));
exports["default"] = ErrorBoundary;


/***/ }),

/***/ "./ui/src/model/login-form.ts":
/*!************************************!*\
  !*** ./ui/src/model/login-form.ts ***!
  \************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
exports.LoginForm = void 0;
var yup = __importStar(__webpack_require__(/*! yup */ "./ui/node_modules/yup/es/index.js"));
var LoginForm = /** @class */ (function () {
    function LoginForm() {
        var _this = this;
        this.username = '';
        this.password = '';
        this.usernameValidator = function () {
            return yup.string().trim().required().isValidSync(_this.username)
                ? ''
                : 'is required';
        };
        this.passwordValidator = function () {
            return yup.string().trim().required().isValidSync(_this.password)
                ? ''
                : 'is required';
        };
    }
    LoginForm.prototype.isValid = function () {
        for (var key in this) {
            var validator = this[key];
            if (key.endsWith('Validator') &&
                typeof validator === 'function' &&
                validator() !== '') {
                return false;
            }
        }
        return true;
    };
    /*
     * This is necessary for the useState react hook to modify the state
     * of the form.
     */
    LoginForm.prototype.clone = function () {
        var clone = new LoginForm();
        for (var _i = 0, _a = Object.keys(this); _i < _a.length; _i++) {
            var key = _a[_i];
            if (typeof this[key] === 'string') {
                clone[key] = this[key];
            }
        }
        return clone;
    };
    return LoginForm;
}());
exports.LoginForm = LoginForm;


/***/ }),

/***/ "./ui/src/services/auth-data-access.ts":
/*!*********************************************!*\
  !*** ./ui/src/services/auth-data-access.ts ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var data_access_1 = __importDefault(__webpack_require__(/*! ./data-access */ "./ui/src/services/data-access.ts"));
var local_storage_data_access_1 = __importDefault(__webpack_require__(/*! ./local-storage-data-access */ "./ui/src/services/local-storage-data-access.ts"));
var http_status_codes_1 = __webpack_require__(/*! http-status-codes */ "./ui/node_modules/http-status-codes/build/es/index.js");
var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./ui/node_modules/tsyringe/dist/esm5/index.js");
var kerygma_configuration_1 = __webpack_require__(/*! ../model/kerygma-configuration */ "./ui/src/model/kerygma-configuration.ts");
var AuthDataAccess = /** @class */ (function (_super) {
    __extends(AuthDataAccess, _super);
    function AuthDataAccess(config, fetcher, localStorageDataAccess) {
        return _super.call(this, config, fetcher, localStorageDataAccess) || this;
    }
    AuthDataAccess.prototype.generateCsrfToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('api/generate-csrf')];
                    case 1:
                        response = _a.sent();
                        if (response.status === http_status_codes_1.StatusCodes.OK) {
                            return [2 /*return*/, response.data.token];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthDataAccess.prototype.login = function (username, password) {
        return __awaiter(this, void 0, void 0, function () {
            var response, loginBody;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.post('api/login', JSON.stringify({ username: username, password: password }), [http_status_codes_1.StatusCodes.FORBIDDEN, http_status_codes_1.StatusCodes.LOCKED])];
                    case 1:
                        response = _a.sent();
                        if (!(response.status === http_status_codes_1.StatusCodes.OK)) return [3 /*break*/, 3];
                        return [4 /*yield*/, response.data];
                    case 2:
                        loginBody = _a.sent();
                        return [2 /*return*/, loginBody.token];
                    case 3: return [2 /*return*/, response.status === http_status_codes_1.StatusCodes.LOCKED
                            ? http_status_codes_1.StatusCodes.LOCKED
                            : http_status_codes_1.StatusCodes.FORBIDDEN];
                }
            });
        });
    };
    AuthDataAccess.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.deleteAuthorized('api/logout')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthDataAccess = __decorate([
        (0, tsyringe_1.injectable)(),
        __param(1, (0, tsyringe_1.inject)('fetcher')),
        __metadata("design:paramtypes", [kerygma_configuration_1.KerygmaConfiguration, Function, local_storage_data_access_1["default"]])
    ], AuthDataAccess);
    return AuthDataAccess;
}(data_access_1["default"]));
exports["default"] = AuthDataAccess;


/***/ })

}]);