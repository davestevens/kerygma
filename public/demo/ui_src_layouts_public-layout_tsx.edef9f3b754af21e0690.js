"use strict";
(self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || []).push([["ui_src_layouts_public-layout_tsx"],{

/***/ "./ui/src/components/footer.styles.ts":
/*!********************************************!*\
  !*** ./ui/src/components/footer.styles.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStylesThemed = void 0;
var global_constants_1 = __webpack_require__(/*! ../common/global-constants */ "./ui/src/common/global-constants.ts");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
var useStylesThemed = function (theme) {
    var _a, _b, _c, _d, _e;
    return (0, react_jss_1.createUseStyles)({
        footer: "\n      display: flex;\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n      width: 100%;\n      margin-top: auto;\n      min-height: 300px;\n      background-color: ".concat(theme.colors.footerBackground, ";\n      color: ").concat(theme.colors.footerFont, ";\n      font-weight: ").concat(theme.fonts.weightFooter, ";\n    "),
        address: (_a = {
                fontSize: '14px',
                marginBottom: '8px',
                textAlign: 'center'
            },
            _a["@media (max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ")")] = {
                fontSize: '16px'
            },
            _a),
        addressPart: (_b = {
                display: 'inline-block'
            },
            _b["@media (max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ")")] = {
                display: 'block'
            },
            _b),
        spacer: (_c = {
                margin: '0 15px',
                display: 'inline-block'
            },
            _c["@media (max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ")")] = {
                display: 'none'
            },
            _c),
        contactLink: {
            display: 'inline'
        },
        obfuscate: {
            color: theme.colors.footerFont,
            textDecoration: 'none',
            '&:hover': {
                color: theme.colors.footerFontLink
            }
        },
        footerText: (_d = {
                fontSize: '12px',
                marginBottom: '8px',
                textAlign: 'center'
            },
            _d["@media (max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ")")] = {
                fontSize: '14px',
                padding: '0 16px'
            },
            _d),
        attributionText: (_e = {
                fontSize: '12px',
                textAlign: 'center'
            },
            _e["@media (max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ")")] = {
                fontSize: '14px',
                padding: '0 16px'
            },
            _e),
        attributionLink: {
            color: theme.colors.footerFont,
            textDecoration: 'none',
            '&:hover': {
                color: theme.colors.footerFontLink
            }
        }
    });
};
exports.useStylesThemed = useStylesThemed;


/***/ }),

/***/ "./ui/src/components/footer.tsx":
/*!**************************************!*\
  !*** ./ui/src/components/footer.tsx ***!
  \**************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var footer_styles_1 = __webpack_require__(/*! ./footer.styles */ "./ui/src/components/footer.styles.ts");
var libphonenumber_js_1 = __webpack_require__(/*! libphonenumber-js */ "./ui/node_modules/libphonenumber-js/index.cjs");
var react_obfuscate_1 = __importDefault(__webpack_require__(/*! react-obfuscate */ "./ui/node_modules/react-obfuscate/dist/obfuscate.js"));
var global_constants_1 = __webpack_require__(/*! ../common/global-constants */ "./ui/src/common/global-constants.ts");
var Footer = function (_a) {
    var config = _a.config, kerygmaVersion = _a.kerygmaVersion;
    var classes = (0, footer_styles_1.useStylesThemed)(config.theme)();
    var phoneNumber = (0, libphonenumber_js_1.parsePhoneNumberFromString)(config.phone, 'US');
    return (React.createElement("div", { className: classes.footer },
        React.createElement("div", { className: classes.address },
            React.createElement("span", { className: classes.addressPart },
                config.worshipLocation.street1,
                ",\u00A0",
                config.worshipLocation.street2 && (React.createElement(React.Fragment, null,
                    config.worshipLocation.street2,
                    "\u00A0")),
                config.worshipLocation.city,
                "\u00A0",
                config.worshipLocation.state,
                "\u00A0",
                config.worshipLocation.postalCode),
            phoneNumber !== undefined && (React.createElement("div", { className: classes.addressPart },
                React.createElement("div", { className: classes.spacer }, "\u2022"),
                React.createElement("div", { className: classes.contactLink },
                    React.createElement(react_obfuscate_1["default"], { href: phoneNumber.getURI(), className: classes.obfuscate }, phoneNumber.formatNational())))),
            React.createElement("div", { className: classes.addressPart },
                React.createElement("div", { className: classes.spacer }, "\u2022"),
                React.createElement("div", { className: classes.contactLink },
                    React.createElement(react_obfuscate_1["default"], { email: config.email, className: classes.obfuscate }, config.email)))),
        React.createElement("div", { className: classes.footerText }, config.footerText),
        React.createElement("div", { className: classes.attributionText },
            "Powered by",
            ' ',
            React.createElement("a", { href: global_constants_1.globalConstants.kerygmaUrl, className: classes.attributionLink }, global_constants_1.globalConstants.churchAdmin.appName),
            ' ',
            "vs. ",
            kerygmaVersion,
            ".")));
};
exports["default"] = Footer;


/***/ }),

/***/ "./ui/src/components/header-nav.styles.ts":
/*!************************************************!*\
  !*** ./ui/src/components/header-nav.styles.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStylesThemed = void 0;
var global_constants_1 = __webpack_require__(/*! ../common/global-constants */ "./ui/src/common/global-constants.ts");
var polished_1 = __webpack_require__(/*! polished */ "./ui/node_modules/polished/dist/polished.esm.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
var useStylesThemed = function (theme) {
    var _a, _b, _c;
    return (0, react_jss_1.createUseStyles)({
        container: "\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      width: 100%;\n    ",
        logoRow: "\n      display: flex;\n      justify-content: center;\n      align-items: center;\n      height: 120px;\n      width: 100%;\n      background-color: ".concat(theme.colors.headerBackground, ";\n    "),
        navRow: (_a = {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '40px',
                width: '100%',
                backgroundColor: theme.colors.primary,
                color: theme.colors.primaryText
            },
            _a["@media (max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ")")] = {
                height: '8px'
            },
            _a),
        navLink: (_b = {
                color: theme.colors.primaryText,
                padding: '0 16px',
                height: '40px',
                display: 'flex',
                alignItems: 'center',
                fontSize: '16px',
                fontWeight: theme.fonts.weightDefault,
                textDecoration: 'none',
                '&:hover': {
                    textDecoration: 'none',
                    color: theme.colors.primaryText,
                    backgroundColor: (0, polished_1.lighten)(0.1, theme.colors.primary)
                }
            },
            _b["@media (max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ")")] = {
                display: 'none'
            },
            _b),
        navButton: (_c = {
                display: 'none',
                position: 'absolute',
                left: '16px',
                top: '16px'
            },
            _c["@media (max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ")")] = {
                display: 'flex'
            },
            _c),
        navMenu: "\n      font-size: 14px;\n    "
    });
};
exports.useStylesThemed = useStylesThemed;


/***/ }),

/***/ "./ui/src/components/header-nav.tsx":
/*!******************************************!*\
  !*** ./ui/src/components/header-nav.tsx ***!
  \******************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var header_nav_styles_1 = __webpack_require__(/*! ./header-nav.styles */ "./ui/src/components/header-nav.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var NavigationMenuItems = function (_a) {
    var navigations = _a.navigations, classes = _a.classes, pathHome = _a.pathHome;
    return (React.createElement(core_1.Menu, { className: classes.navMenu }, navigations.map(function (navigation, i) {
        if (navigation.path) {
            var path = "".concat(pathHome).concat(navigation.path);
            return React.createElement(core_1.MenuItem, { text: navigation.label, href: path, key: i });
        }
        else {
            return (React.createElement(core_1.MenuItem, { text: navigation.label, key: i },
                React.createElement(NavigationMenuItems, { navigations: navigation.children, classes: classes, pathHome: pathHome })));
        }
    })));
};
var NavigationItems = function (_a) {
    var navigations = _a.navigations, classes = _a.classes, pathHome = _a.pathHome;
    return (React.createElement(React.Fragment, null, navigations.map(function (navigation, i) {
        if (navigation.path) {
            var path = "".concat(pathHome).concat(navigation.path);
            return (React.createElement("a", { className: classes.navLink, href: path, key: i }, navigation.label));
        }
        else {
            var childrenMenu = (React.createElement(NavigationMenuItems, { navigations: navigation.children, classes: classes, pathHome: pathHome }));
            return (React.createElement(core_1.Popover, { content: childrenMenu, position: core_1.Position.BOTTOM_LEFT, modifiers: { arrow: { enabled: false } }, usePortal: false, key: i },
                React.createElement("a", { className: classes.navLink }, navigation.label)));
        }
    })));
};
var HeaderNav = function (_a) {
    var assetsRoot = _a.assetsRoot, config = _a.config, pathHome = _a.pathHome;
    var classes = (0, header_nav_styles_1.useStylesThemed)(config.theme)();
    var fullMenu = (React.createElement(NavigationMenuItems, { navigations: config.navigation, classes: classes, pathHome: pathHome }));
    return (React.createElement("div", { className: classes.container },
        React.createElement("div", { className: classes.logoRow },
            React.createElement("a", { href: "".concat(pathHome, "index.html") },
                React.createElement("img", { alt: "Church Logo", src: "".concat(pathHome).concat(assetsRoot).concat(config.logo.path), style: { height: config.logo.height } })),
            React.createElement("div", { className: classes.navButton },
                React.createElement(core_1.Popover, { content: fullMenu, position: core_1.Position.BOTTOM_LEFT },
                    React.createElement(core_1.Button, { icon: "menu", text: "Menu" })))),
        React.createElement("div", { className: classes.navRow },
            React.createElement(NavigationItems, { navigations: config.navigation, classes: classes, pathHome: pathHome }))));
};
exports["default"] = HeaderNav;


/***/ }),

/***/ "./ui/src/layouts/public-layout.styles.ts":
/*!************************************************!*\
  !*** ./ui/src/layouts/public-layout.styles.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStylesThemed = void 0;
var global_constants_1 = __webpack_require__(/*! ../common/global-constants */ "./ui/src/common/global-constants.ts");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
var useStylesThemed = function (theme) {
    var _a;
    var header = {
        fontFamily: theme.fonts.titles.family,
        fontWeight: theme.fonts.weightHeaders,
        color: theme.colors.fontHeaders,
        textTransform: theme.fonts.textTransformHeaders,
        width: '100%',
        boxSizing: 'border-box',
        maxWidth: global_constants_1.globalConstants.textMaxWidth,
        padding: '0 16px'
    };
    return (0, react_jss_1.createUseStyles)({
        '@global': {
            body: (_a = {
                    display: 'flex',
                    flexDirection: 'column',
                    margin: 0,
                    fontFamily: theme.fonts["default"].family,
                    fontSize: theme.fonts.sizeDefault,
                    minHeight: '100vh',
                    color: theme.colors.fontDefault
                },
                _a["@media (max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ")")] = {
                    fontSize: "calc(".concat(theme.fonts.sizeDefault, " + 2px)")
                },
                _a),
            h1: { extend: header, textAlign: 'center' },
            h2: { extend: header },
            h3: { extend: header },
            h4: { extend: header },
            p: {
                width: '100%',
                boxSizing: 'border-box',
                maxWidth: global_constants_1.globalConstants.textMaxWidth,
                padding: '0 20px',
                margin: '16px 0',
                lineHeight: '1.5',
                color: theme.colors.fontDefault,
                fontWeight: theme.fonts.weightDefault
            },
            small: {
                width: '100%',
                boxSizing: 'border-box',
                maxWidth: global_constants_1.globalConstants.textMaxWidth,
                padding: '0 20px',
                margin: '16px 0',
                fontSize: '13px',
                lineHeight: '1.5'
            },
            center: {
                width: '100%',
                boxSizing: 'border-box',
                maxWidth: global_constants_1.globalConstants.textMaxWidth,
                padding: '0 20px',
                margin: '16px 0',
                lineHeight: '1.5',
                color: theme.colors.fontDefault,
                fontWeight: theme.fonts.weightDefault,
                textAlign: 'center'
            },
            ul: {
                width: '100%',
                boxSizing: 'border-box',
                maxWidth: global_constants_1.globalConstants.textMaxWidth,
                '& li': {
                    lineHeight: '1.5',
                    color: theme.colors.fontDefault,
                    fontWeight: theme.fonts.weightDefault
                }
            }
        },
        container: "\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      min-height: 100vh;\n    ",
        contentBody: "\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      width: 100%;\n    ",
        padContentBody: "\n      padding-bottom: 20px;\n    "
    });
};
exports.useStylesThemed = useStylesThemed;


/***/ }),

/***/ "./ui/src/layouts/public-layout.tsx":
/*!******************************************!*\
  !*** ./ui/src/layouts/public-layout.tsx ***!
  \******************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var public_layout_styles_1 = __webpack_require__(/*! ./public-layout.styles */ "./ui/src/layouts/public-layout.styles.ts");
var footer_1 = __importDefault(__webpack_require__(/*! ../components/footer */ "./ui/src/components/footer.tsx"));
var header_nav_1 = __importDefault(__webpack_require__(/*! ../components/header-nav */ "./ui/src/components/header-nav.tsx"));
var react_helmet_1 = __webpack_require__(/*! react-helmet */ "./ui/node_modules/react-helmet/es/Helmet.js");
var PublicLayout = function (_a) {
    var children = _a.children, frontmatter = _a.frontmatter, assetsRoot = _a.assetsRoot, config = _a.config, kerygmaVersion = _a.kerygmaVersion, pathHome = _a.pathHome;
    var classes = (0, public_layout_styles_1.useStylesThemed)(config.theme)();
    var bodyClasses = (frontmatter === null || frontmatter === void 0 ? void 0 : frontmatter.suppressBottomMargin) === true
        ? classes.contentBody
        : "".concat(classes.contentBody, " ").concat(classes.padContentBody);
    var title = (frontmatter === null || frontmatter === void 0 ? void 0 : frontmatter.title)
        ? "".concat(frontmatter === null || frontmatter === void 0 ? void 0 : frontmatter.title, " - ").concat(config.churchNameShort)
        : config.churchNameShort;
    var assetsPath = "".concat(pathHome).concat(assetsRoot);
    return (React.createElement(React.Fragment, null,
        React.createElement(react_helmet_1.Helmet, null,
            React.createElement("meta", { charSet: "utf-8" }),
            React.createElement("title", null, title),
            frontmatter.description && (React.createElement("meta", { name: "description", content: frontmatter.description })),
            React.createElement("link", { rel: "apple-touch-icon", href: "".concat(assetsPath).concat(config.favicon.path) }),
            React.createElement("link", { rel: "shortcut icon", type: config.favicon.fileType, href: "".concat(assetsPath).concat(config.favicon.path) }),
            React.createElement("style", { type: "text/css" }, "\n          @import url('".concat(config.theme.fonts["default"].url, "');\n          @import url('").concat(config.theme.fonts.titles.url, "');\n        "))),
        React.createElement("div", { className: classes.container },
            React.createElement(header_nav_1["default"], { assetsRoot: assetsRoot, config: config, pathHome: pathHome }),
            React.createElement("div", { className: bodyClasses }, children),
            React.createElement(footer_1["default"], { config: config, kerygmaVersion: kerygmaVersion }))));
};
exports["default"] = PublicLayout;


/***/ })

}]);