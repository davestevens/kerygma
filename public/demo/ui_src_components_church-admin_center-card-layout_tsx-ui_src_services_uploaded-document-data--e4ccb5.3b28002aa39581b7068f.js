"use strict";
(self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || []).push([["ui_src_components_church-admin_center-card-layout_tsx-ui_src_services_uploaded-document-data--e4ccb5"],{

/***/ "./ui/src/components/church-admin/center-card-layout.styles.ts":
/*!*********************************************************************!*\
  !*** ./ui/src/components/church-admin/center-card-layout.styles.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var global_constants_1 = __webpack_require__(/*! ../../common/global-constants */ "./ui/src/common/global-constants.ts");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    centerContainer: "\n    display: flex;\n    justify-content: center;\n    padding: 0 16px;\n  ",
    centerContent: "\n    display: flex;\n    flex-direction: column;\n    max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ";\n    width: 100%;\n    margin-top: 30px;\n  "),
    centerContentBreadcrumb: "\n    margin-top: 16px;\n  ",
    breadcrumbs: "\n    padding: 0 0 8px 0;\n  "
});


/***/ }),

/***/ "./ui/src/components/church-admin/center-card-layout.tsx":
/*!***************************************************************!*\
  !*** ./ui/src/components/church-admin/center-card-layout.tsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var center_card_layout_styles_1 = __webpack_require__(/*! ./center-card-layout.styles */ "./ui/src/components/church-admin/center-card-layout.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var CenterCardLayout = function (_a) {
    var breadcrumbs = _a.breadcrumbs, children = _a.children;
    var classes = (0, center_card_layout_styles_1.useStyles)();
    return (React.createElement("div", { className: classes.centerContainer },
        React.createElement("div", { className: "\n          ".concat(classes.centerContent, "\n          ").concat(breadcrumbs && classes.centerContentBreadcrumb, "\n        ") },
            breadcrumbs && (React.createElement("div", { className: classes.breadcrumbs },
                React.createElement(core_1.Breadcrumbs, { items: breadcrumbs }))),
            children)));
};
exports["default"] = CenterCardLayout;


/***/ }),

/***/ "./ui/src/model/date-converter.ts":
/*!****************************************!*\
  !*** ./ui/src/model/date-converter.ts ***!
  \****************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var dayjs_1 = __importDefault(__webpack_require__(/*! dayjs */ "./ui/node_modules/dayjs/dayjs.min.js"));
var DateConverter = /** @class */ (function () {
    function DateConverter() {
    }
    DateConverter.prototype.deserialize = function (data) {
        return (0, dayjs_1["default"])(data);
    };
    DateConverter.prototype.serialize = function (data) {
        return data.toISOString();
    };
    DateConverter = __decorate([
        json2typescript_1.JsonConverter
    ], DateConverter);
    return DateConverter;
}());
exports["default"] = DateConverter;


/***/ }),

/***/ "./ui/src/model/uploaded-document.ts":
/*!*******************************************!*\
  !*** ./ui/src/model/uploaded-document.ts ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.UploadedDocument = void 0;
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var yup = __importStar(__webpack_require__(/*! yup */ "./ui/node_modules/yup/es/index.js"));
var date_converter_1 = __importDefault(__webpack_require__(/*! ./date-converter */ "./ui/src/model/date-converter.ts"));
var dayjs_1 = __importDefault(__webpack_require__(/*! dayjs */ "./ui/node_modules/dayjs/dayjs.min.js"));
var UploadedDocument = /** @class */ (function () {
    function UploadedDocument() {
        var _this = this;
        this.id = 0;
        this.filename = '';
        this.type = 'pdf';
        this.title = '';
        this.url = '';
        this.size = 0;
        this.uploadedTimestamp = (0, dayjs_1["default"])();
        // Field Validators
        this.filenameValidator = function () {
            if (!yup.string().trim().required().isValidSync(_this.filename)) {
                return 'is required';
            }
            if (!yup.string().trim().min(3).isValidSync(_this.filename)) {
                return 'must be at least 3 characters';
            }
            return !yup
                .string()
                .trim()
                .matches(/[^a-z0-9_-]/)
                .isValidSync(_this.filename)
                ? ''
                : 'can only contain lowercase alpha numeric characters, underscores or dashes.';
        };
        this.titleValidator = function () {
            return !yup.string().trim().required().isValidSync(_this.title)
                ? 'is required'
                : '';
        };
    }
    UploadedDocument_1 = UploadedDocument;
    UploadedDocument.prototype.isValid = function () {
        return this.filenameValidator() === '' && this.titleValidator() === '';
    };
    UploadedDocument.prototype.getFilenameWithSuffix = function () {
        return "".concat(this.filename, ".").concat(this.type);
    };
    /*
     * This is necessary for the useState react hook to modify the state
     * of the form.
     */
    UploadedDocument.prototype.clone = function () {
        var clone = new UploadedDocument_1();
        for (var _i = 0, _a = Object.keys(this); _i < _a.length; _i++) {
            var key = _a[_i];
            if (typeof this[key] === 'string' || typeof this[key] === 'number') {
                clone[key] = this[key];
            }
        }
        return clone;
    };
    var UploadedDocument_1;
    __decorate([
        (0, json2typescript_1.JsonProperty)('id', Number),
        __metadata("design:type", Object)
    ], UploadedDocument.prototype, "id");
    __decorate([
        (0, json2typescript_1.JsonProperty)('filename', String),
        __metadata("design:type", Object)
    ], UploadedDocument.prototype, "filename");
    __decorate([
        (0, json2typescript_1.JsonProperty)('type', String),
        __metadata("design:type", Object)
    ], UploadedDocument.prototype, "type");
    __decorate([
        (0, json2typescript_1.JsonProperty)('title', String),
        __metadata("design:type", Object)
    ], UploadedDocument.prototype, "title");
    __decorate([
        (0, json2typescript_1.JsonProperty)('url', String, true),
        __metadata("design:type", Object)
    ], UploadedDocument.prototype, "url");
    __decorate([
        (0, json2typescript_1.JsonProperty)('size', Number, true),
        __metadata("design:type", Object)
    ], UploadedDocument.prototype, "size");
    __decorate([
        (0, json2typescript_1.JsonProperty)('uploadedTimestamp', date_converter_1["default"], true),
        __metadata("design:type", Object)
    ], UploadedDocument.prototype, "uploadedTimestamp");
    UploadedDocument = UploadedDocument_1 = __decorate([
        (0, json2typescript_1.JsonObject)('Document')
    ], UploadedDocument);
    return UploadedDocument;
}());
exports.UploadedDocument = UploadedDocument;


/***/ }),

/***/ "./ui/src/services/uploaded-document-data-access.ts":
/*!**********************************************************!*\
  !*** ./ui/src/services/uploaded-document-data-access.ts ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var data_access_1 = __importDefault(__webpack_require__(/*! ./data-access */ "./ui/src/services/data-access.ts"));
var json2typescript_1 = __webpack_require__(/*! json2typescript */ "./ui/node_modules/json2typescript/lib/esm/index.js");
var local_storage_data_access_1 = __importDefault(__webpack_require__(/*! ./local-storage-data-access */ "./ui/src/services/local-storage-data-access.ts"));
var uploaded_document_1 = __webpack_require__(/*! ../model/uploaded-document */ "./ui/src/model/uploaded-document.ts");
var http_status_codes_1 = __webpack_require__(/*! http-status-codes */ "./ui/node_modules/http-status-codes/build/es/index.js");
var user_1 = __webpack_require__(/*! ../model/user */ "./ui/src/model/user.ts");
var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./ui/node_modules/tsyringe/dist/esm5/index.js");
var kerygma_configuration_1 = __webpack_require__(/*! ../model/kerygma-configuration */ "./ui/src/model/kerygma-configuration.ts");
var UploadedDocumentDataAccess = /** @class */ (function (_super) {
    __extends(UploadedDocumentDataAccess, _super);
    function UploadedDocumentDataAccess(jsonConvert, config, fetcher, localStorageDataAccess) {
        var _this = _super.call(this, config, fetcher, localStorageDataAccess) || this;
        _this.jsonConvert = jsonConvert;
        return _this;
    }
    UploadedDocumentDataAccess.prototype.addDocument = function (document, file, onUploadProgress) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = new FormData();
                        data.append('filename', document.filename);
                        data.append('title', document.title);
                        data.append('file', file);
                        return [4 /*yield*/, this.postFormDataAuthorized('POST', 'api/documents', data, [http_status_codes_1.StatusCodes.CONFLICT, http_status_codes_1.StatusCodes.REQUEST_TOO_LONG], onUploadProgress)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UploadedDocumentDataAccess.prototype.getDocuments = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, body, documents, loginUser;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getAuthorized('api/documents')];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.data];
                    case 2:
                        body = _a.sent();
                        documents = this.jsonConvert.deserializeArray(body.documents, uploaded_document_1.UploadedDocument);
                        loginUser = this.jsonConvert.deserializeObject(body.loginUser, user_1.User);
                        return [2 /*return*/, { documents: documents, loginUser: loginUser }];
                }
            });
        });
    };
    UploadedDocumentDataAccess.prototype.getDocument = function (documentId) {
        return __awaiter(this, void 0, void 0, function () {
            var response, body, document, loginUser;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getAuthorized("api/documents/".concat(documentId))];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.data];
                    case 2:
                        body = _a.sent();
                        document = this.jsonConvert.deserializeObject(body.document, uploaded_document_1.UploadedDocument);
                        loginUser = this.jsonConvert.deserializeObject(body.loginUser, user_1.User);
                        return [2 /*return*/, { document: document, loginUser: loginUser }];
                }
            });
        });
    };
    UploadedDocumentDataAccess.prototype.deleteDocument = function (docId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.deleteAuthorized("api/documents/".concat(docId))];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UploadedDocumentDataAccess.prototype.saveDocument = function (document, file, onUploadProgress) {
        return __awaiter(this, void 0, void 0, function () {
            var data, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!file) return [3 /*break*/, 2];
                        data = new FormData();
                        data.append('id', "".concat(document.id));
                        data.append('filename', document.filename);
                        data.append('title', document.title);
                        data.append('file', file);
                        return [4 /*yield*/, this.postFormDataAuthorized('PUT', "api/documents/".concat(document.id), data, [http_status_codes_1.StatusCodes.CONFLICT, http_status_codes_1.StatusCodes.REQUEST_TOO_LONG], onUploadProgress)];
                    case 1: return [2 /*return*/, _a.sent()];
                    case 2: return [4 /*yield*/, this.putAuthorized("api/documents/".concat(document.id), JSON.stringify(document), [http_status_codes_1.StatusCodes.CONFLICT])];
                    case 3:
                        response = _a.sent();
                        onUploadProgress({ loaded: 10, total: 10 });
                        return [2 /*return*/, response];
                }
            });
        });
    };
    UploadedDocumentDataAccess = __decorate([
        (0, tsyringe_1.injectable)(),
        __param(2, (0, tsyringe_1.inject)('fetcher')),
        __metadata("design:paramtypes", [json2typescript_1.JsonConvert,
            kerygma_configuration_1.KerygmaConfiguration, Function, local_storage_data_access_1["default"]])
    ], UploadedDocumentDataAccess);
    return UploadedDocumentDataAccess;
}(data_access_1["default"]));
exports["default"] = UploadedDocumentDataAccess;


/***/ })

}]);