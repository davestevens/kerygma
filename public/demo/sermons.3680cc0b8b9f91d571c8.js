var sermons;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./ui/src/components/church-admin/tag-dialog.styles.ts":
/*!*************************************************************!*\
  !*** ./ui/src/components/church-admin/tag-dialog.styles.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    tagContainer: "\n    display: flex;\n    flex-direction: column;\n    padding: 16px 0;\n  ",
    tagRow: "\n    display: flex;\n    align-items: center;\n    padding: 8px;\n    font-size: 14px;\n  ",
    tagSelect: "\n    display: flex;\n    margin-bottom: 0;\n  ",
    tagCount: "\n    margin-left: auto;\n    text-align: right;\n  ",
    tagCountLabel: "\n    margin-left: 4px;\n  "
});


/***/ }),

/***/ "./ui/src/components/church-admin/tag-dialog.tsx":
/*!*******************************************************!*\
  !*** ./ui/src/components/church-admin/tag-dialog.tsx ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var react_1 = __importDefault(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var dark_mode_context_1 = __importDefault(__webpack_require__(/*! ./dark-mode-context */ "./ui/src/components/church-admin/dark-mode-context.ts"));
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var tag_dialog_styles_1 = __webpack_require__(/*! ./tag-dialog.styles */ "./ui/src/components/church-admin/tag-dialog.styles.ts");
var TagDialog = function (_a) {
    var tags = _a.tags, isOpen = _a.isOpen, onCancel = _a.onCancel, onApply = _a.onApply, onToggleTag = _a.onToggleTag;
    var classes = (0, tag_dialog_styles_1.useStyles)();
    return (react_1["default"].createElement(dark_mode_context_1["default"].Consumer, null, function (isDarkMode) { return (react_1["default"].createElement(core_1.Dialog, { isOpen: isOpen, title: "Filter by Tag", className: isDarkMode ? core_1.Classes.DARK : '', onClose: onCancel },
        react_1["default"].createElement(core_1.DialogBody, { useOverflowScrollContainer: true }, tags.map(function (tag, i) { return (react_1["default"].createElement("div", { key: i, className: classes.tagRow },
            react_1["default"].createElement(core_1.Checkbox, { className: classes.tagSelect, inline: true, checked: tag.isSelected, onChange: function () { return onToggleTag(i); } }, tag.tag),
            react_1["default"].createElement("div", { className: classes.tagCount },
                tag.sermonCount,
                react_1["default"].createElement("span", { className: "".concat(classes.tagCountLabel, " bp5-text-muted bp5-text-small") }, "sermons")))); })),
        react_1["default"].createElement(core_1.DialogFooter, { actions: react_1["default"].createElement(react_1["default"].Fragment, null,
                react_1["default"].createElement(core_1.Button, { onClick: function () { return onCancel(); } }, "Cancel"),
                react_1["default"].createElement(core_1.Button, { onClick: function () { return onApply(); }, intent: "primary" }, "Apply")) }))); }));
};
exports["default"] = TagDialog;


/***/ }),

/***/ "./ui/src/components/list-pager.styles.ts":
/*!************************************************!*\
  !*** ./ui/src/components/list-pager.styles.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStyles = void 0;
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
exports.useStyles = (0, react_jss_1.createUseStyles)({
    buttonRow: "\n    display: flex;\n    justify-content: flex-end;\n    align-items: center;\n  ",
    label: "\n    font-size: 12px;\n    margin-right: 8px;\n  ",
    range: "\n    font-size: 12px;\n    margin: 0 4px 0 8px;\n  ",
    button: "\n    margin-left: 4px;\n  "
});


/***/ }),

/***/ "./ui/src/components/list-pager.tsx":
/*!******************************************!*\
  !*** ./ui/src/components/list-pager.tsx ***!
  \******************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
exports.perPageOptions = void 0;
var React = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var list_pager_styles_1 = __webpack_require__(/*! ./list-pager.styles */ "./ui/src/components/list-pager.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_1 = __webpack_require__(/*! react */ "./ui/node_modules/react/index.js");
exports.perPageOptions = [20, 50, 100];
var ListPager = function (props) {
    var totalRecords = props.totalRecords, onChange = props.onChange;
    var isMounted = (0, react_1.useRef)(false);
    var classes = (0, list_pager_styles_1.useStyles)();
    var _a = React.useState(props.page), page = _a[0], setPage = _a[1];
    var _b = React.useState(props.perPage), perPage = _b[0], setPerPage = _b[1];
    var onChangePerPage = function (event) {
        var perPage = parseInt(event.currentTarget.value);
        setPerPage(perPage);
        setPage(0);
    };
    var calcRange = function () {
        if (totalRecords === 0) {
            return 'no records';
        }
        var from = page * perPage + 1;
        var thru = Math.min(page * perPage + perPage, totalRecords);
        return "".concat(from, " - ").concat(thru, " of ").concat(totalRecords);
    };
    var onPrev = function () {
        if (page > 0) {
            setPage(page - 1);
        }
    };
    var maxPage = function () {
        return Math.ceil(totalRecords / perPage - 1);
    };
    var onNext = function () {
        if (page < maxPage()) {
            setPage(page + 1);
        }
    };
    var onFirst = function () {
        setPage(0);
    };
    var onLast = function () {
        setPage(maxPage());
    };
    (0, react_1.useEffect)(function () {
        // Using isMounted ref to not execute the onChange callback
        // when first mounting the component.
        if (isMounted.current) {
            onChange(page, perPage);
        }
        else {
            isMounted.current = true;
        }
    }, [page, perPage]);
    return (React.createElement("div", { className: classes.buttonRow },
        React.createElement("label", { className: "".concat(classes.label, " ").concat(core_1.Classes.TEXT_MUTED) }, "Items per page:"),
        React.createElement(core_1.HTMLSelect, { onChange: onChangePerPage, options: exports.perPageOptions, value: perPage }),
        React.createElement("span", { className: "".concat(classes.range, " ").concat(core_1.Classes.TEXT_MUTED) }, calcRange()),
        React.createElement(core_1.Button, { icon: "chevron-backward", className: classes.button, onClick: onFirst, disabled: page === 0 }),
        React.createElement(core_1.Button, { icon: "chevron-left", className: classes.button, onClick: onPrev, disabled: page === 0 }),
        React.createElement(core_1.Button, { icon: "chevron-right", className: classes.button, onClick: onNext, disabled: page >= maxPage() }),
        React.createElement(core_1.Button, { icon: "chevron-forward", className: classes.button, onClick: onLast, disabled: page >= maxPage() })));
};
exports["default"] = ListPager;


/***/ }),

/***/ "./ui/src/pages/sermons.styles.ts":
/*!****************************************!*\
  !*** ./ui/src/pages/sermons.styles.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


exports.__esModule = true;
exports.useStylesThemed = void 0;
var global_constants_1 = __webpack_require__(/*! ../common/global-constants */ "./ui/src/common/global-constants.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var react_jss_1 = __webpack_require__(/*! react-jss */ "./ui/node_modules/react-jss/dist/react-jss.esm.js");
var useStylesThemed = function (theme) {
    return (0, react_jss_1.createUseStyles)({
        '@global': {
            h4: {
                fontFamily: theme.fonts["default"].family
            }
        },
        centerContainer: "\n      display: flex;\n      justify-content: center;\n      padding: 0 16px; \n      width: 100%;\n    ",
        card: "\n      padding: 0;\n      max-width: ".concat(global_constants_1.globalConstants.bp.phoneMax, ";\n      width: 100%;\n      margin-bottom: 30px;\n    "),
        cardSide: "\n      padding: 0;\n      max-width: 300px;\n      width: 100%;\n      margin: 30px 0 0 16px;\n    ",
        headerRow: "\n      display: flex;\n      justify-content: space-between;\n      align-items: center;\n      padding: 10px 16px;\n      border-top-left-radius: 3px;\n      border-top-right-radius: 3px;\n      border-bottom: 1px solid ".concat(core_1.Colors.LIGHT_GRAY1, ";\n      background-color: ").concat(core_1.Colors.LIGHT_GRAY4, ";\n    "),
        searchRow: "\n      display: flex;\n      padding: 4px 16px;\n    ",
        tagFilterRow: "\n      display: flex;\n      padding: 4px 16px;\n      border-top: none !important;\n      align-items: center;\n      border-bottom: 1px solid ".concat(core_1.Colors.LIGHT_GRAY1, ";\n      background-color: ").concat(core_1.Colors.LIGHT_GRAY4, ";\n    "),
        clearTags: "\n      margin-left: auto;\n    ",
        searchInput: "\n      flex: 1;\n    ",
        sort: "\n      margin: 0 0 0 8px;\n    ",
        header: "\n      text-align: left;\n      padding: 0;\n      width: auto;\n    ",
        empty: {
            margin: '40px 0',
            '& h4': "\n        margin-bottom: 20px;\n      "
        },
        body: "\n      padding: 16px;\n    ",
        docRow: {
            display: 'flex',
            alignItems: 'flex-start',
            marginBottom: '16px',
            '&:last-child': "\n        margin-bottom: 0;\n      "
        },
        avatar: "\n      display: flex;\n      background-color: ".concat(core_1.Colors.GRAY3, ";\n      padding: 6px;\n      border-radius: 2px;\n      margin: 4px 16px 0 0;\n      line-height: 1;\n    "),
        metaCol: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            marginRight: 'auto',
            flex: 1,
            '& a': "\n        display: flex;\n        align-items: center;\n      "
        },
        linkIcon: "\n      margin-left: 4px;\n      padding-bottom: 1px;\n    ",
        metaLine: "\n      display: flex;\n      font-size: 12px;\n      margin-top: 2px;\n    ",
        tagLine: "\n      display: flex;\n      margin-top: 4px;\n      align-items: center;\n    ",
        tag: "\n      margin-left: 4px;\n    ",
        more: "\n      margin: 4px 0 0 8px;\n    ",
        playLine: "\n      display: flex;\n      width: 100%;\n      font-size: 12px;\n      align-items: center;\n      margin-top: 2px;\n    ",
        playButton: "\n      margin-right: 4px;\n    ",
        sermonDate: "\n      white-space: nowrap;\n    ",
        time: "\n      white-space: nowrap;\n      margin: 0 8px;\n    "
    });
};
exports.useStylesThemed = useStylesThemed;


/***/ }),

/***/ "./ui/src/pages/sermons.tsx":
/*!**********************************!*\
  !*** ./ui/src/pages/sermons.tsx ***!
  \**********************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.init = void 0;
var react_1 = __importStar(__webpack_require__(/*! react */ "./ui/node_modules/react/index.js"));
var ReactDOM = __importStar(__webpack_require__(/*! react-dom */ "./ui/node_modules/react-dom/index.js"));
var public_layout_1 = __importDefault(__webpack_require__(/*! ../layouts/public-layout */ "./ui/src/layouts/public-layout.tsx"));
var sermons_styles_1 = __webpack_require__(/*! ./sermons.styles */ "./ui/src/pages/sermons.styles.ts");
var core_1 = __webpack_require__(/*! @blueprintjs/core */ "./ui/node_modules/@blueprintjs/core/lib/esm/index.js");
var list_pager_1 = __importStar(__webpack_require__(/*! ../components/list-pager */ "./ui/src/components/list-pager.tsx"));
var di_container_1 = __importDefault(__webpack_require__(/*! ../di-container */ "./ui/src/di-container.ts"));
var sermon_data_access_1 = __importDefault(__webpack_require__(/*! ../services/sermon-data-access */ "./ui/src/services/sermon-data-access.ts"));
var bible_book_data_yaml_1 = __importDefault(__webpack_require__(/*! ../model/bible-book-data.yaml */ "./ui/src/model/bible-book-data.yaml"));
var kerygma_configuration_1 = __webpack_require__(/*! ../model/kerygma-configuration */ "./ui/src/model/kerygma-configuration.ts");
var tag_dialog_1 = __importDefault(__webpack_require__(/*! ../components/church-admin/tag-dialog */ "./ui/src/components/church-admin/tag-dialog.tsx"));
var config = di_container_1["default"].resolve(kerygma_configuration_1.KerygmaConfiguration);
var kerygmaVersion = di_container_1["default"].resolve('kerygmaVersion');
var _a = config.api, apiHost = _a.apiHost, mediaContextRoot = _a.mediaContextRoot;
var sermonsUri = "".concat(apiHost).concat(mediaContextRoot, "/sermons/");
var bibleBooks = bible_book_data_yaml_1["default"].books;
var sermonDataAccess = di_container_1["default"].resolve(sermon_data_access_1["default"]);
var Sermons = function (_a) {
    var assetsRoot = _a.assetsRoot;
    var frontMatter = {
        title: 'Sermons',
        description: 'Where you can go to listen to our sermons.'
    };
    var _b = (0, react_1.useState)(true), isLoading = _b[0], setIsLoading = _b[1];
    var _c = (0, react_1.useState)([]), sermons = _c[0], setSermons = _c[1];
    var _d = react_1["default"].useState([]), audioRefs = _d[0], setAudioRefs = _d[1];
    var _e = react_1["default"].useState([]), isActive = _e[0], setIsActive = _e[1];
    var _f = react_1["default"].useState(0), intervalId = _f[0], setIntervalId = _f[1];
    var _g = (0, react_1.useState)(0), currentTime = _g[0], setCurrentTime = _g[1];
    var _h = (0, react_1.useState)(0), duration = _h[0], setDuration = _h[1];
    var _j = (0, react_1.useState)(null), playingIndex = _j[0], setPlayingIndex = _j[1];
    var _k = react_1["default"].useState(0), page = _k[0], setPage = _k[1];
    var _l = react_1["default"].useState(list_pager_1.perPageOptions[0]), perPage = _l[0], setPerPage = _l[1];
    var _m = react_1["default"].useState('latest'), sortOrder = _m[0], setSortOrder = _m[1];
    var _o = react_1["default"].useState(''), filterInputValue = _o[0], setFilterInputValue = _o[1];
    var _p = react_1["default"].useState(undefined), filter = _p[0], setFilter = _p[1];
    var _q = react_1["default"].useState([]), tagFilter = _q[0], setTagFilter = _q[1];
    var _r = react_1["default"].useState(0), totalSermons = _r[0], setTotalSermons = _r[1];
    var _s = react_1["default"].useState(false), tagDialogOpen = _s[0], setTagDialogOpen = _s[1];
    var _t = react_1["default"].useState(undefined), tags = _t[0], setTags = _t[1];
    var _u = react_1["default"].useState([]), tagsTemp = _u[0], setTagsTemp = _u[1];
    var _v = react_1["default"].useState(true), firstLoad = _v[0], setFirstLoad = _v[1];
    var classes = (0, sermons_styles_1.useStylesThemed)(config.theme)();
    var loadSermons = function () { return __awaiter(void 0, void 0, void 0, function () {
        var response, sermonRefs, playingStates, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    setIsLoading(true);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, 4, 5]);
                    return [4 /*yield*/, sermonDataAccess.debouncedGetSermons(page, perPage, sortOrder, filter, tagFilter)];
                case 2:
                    response = _a.sent();
                    setSermons(response.sermons);
                    setTotalSermons(response.totalRowCount);
                    sermonRefs = response.sermons.map(function () {
                        return react_1["default"].createRef();
                    });
                    playingStates = response.sermons.map(function () { return false; });
                    setAudioRefs(sermonRefs);
                    setIsActive(playingStates);
                    return [3 /*break*/, 5];
                case 3:
                    err_1 = _a.sent();
                    console.error({ msg: 'Error encountered loading sermons', err: err_1 });
                    return [3 /*break*/, 5];
                case 4:
                    setIsLoading(false);
                    return [7 /*endfinally*/];
                case 5: return [2 /*return*/];
            }
        });
    }); };
    var loadTags = function () { return __awaiter(void 0, void 0, void 0, function () {
        var availableTags, _loop_1, _i, availableTags_1, tag;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, sermonDataAccess.getSermonTags('sermonCount')];
                case 1:
                    availableTags = _a.sent();
                    if (tagFilter.length > 0) {
                        _loop_1 = function (tag) {
                            tag.isSelected = tagFilter.some(function (t) { return t === tag.tag; });
                        };
                        for (_i = 0, availableTags_1 = availableTags; _i < availableTags_1.length; _i++) {
                            tag = availableTags_1[_i];
                            _loop_1(tag);
                        }
                    }
                    setTags(availableTags);
                    return [2 /*return*/];
            }
        });
    }); };
    (0, react_1.useEffect)(function () {
        syncQueryString();
        if (!tags) {
            loadTags().then();
        }
        loadSermons().then();
        setFirstLoad(false);
    }, [sortOrder, page, perPage, filter, tagFilter]);
    var onTogglePlay = function (index) {
        var _a;
        var playingStates = __spreadArray([], isActive, true);
        setPlayingIndex(index);
        var _loop_2 = function (i) {
            var audioRef = (_a = audioRefs[i]) === null || _a === void 0 ? void 0 : _a.current;
            if (i === index) {
                if (audioRef.paused) {
                    if (currentTime === audioRef.duration) {
                        audioRef.currentTime = 0;
                    }
                    audioRef.play().then();
                    playingStates[i] = true;
                    setDuration(audioRef.duration);
                    setCurrentTime(audioRef.currentTime);
                    clearInterval(intervalId);
                    var newIntervalId = window.setInterval(function () {
                        setCurrentTime(audioRef.currentTime);
                        if (audioRef.paused) {
                            var intervalPlayingStates = __spreadArray([], isActive, true);
                            intervalPlayingStates[i] = false;
                            setIsActive(intervalPlayingStates);
                            clearInterval(intervalId);
                        }
                    }, 1000);
                    setIntervalId(newIntervalId);
                }
                else {
                    audioRef.pause();
                    playingStates[i] = false;
                    clearInterval(intervalId);
                }
            }
            else {
                if (!audioRef.paused) {
                    audioRef.pause();
                    playingStates[i] = false;
                }
            }
        };
        for (var i = 0; i < audioRefs.length; i++) {
            _loop_2(i);
        }
        setIsActive(playingStates);
    };
    var onSliderChange = function (value) {
        var _a;
        var audioRef = (_a = audioRefs[playingIndex]) === null || _a === void 0 ? void 0 : _a.current;
        if (isActive[playingIndex] && !audioRef.paused) {
            onTogglePlay(playingIndex);
        }
        setCurrentTime(value);
    };
    var onSliderRelease = function (value) {
        var _a;
        setCurrentTime(value);
        var audioRef = (_a = audioRefs[playingIndex]) === null || _a === void 0 ? void 0 : _a.current;
        audioRef.currentTime = value;
        if (value < duration) {
            onTogglePlay(playingIndex);
        }
    };
    var onSearch = function (elem) {
        var _a;
        var newFilter = (_a = elem.target.value) !== null && _a !== void 0 ? _a : '';
        setFilterInputValue(newFilter);
        var trimmed = newFilter.trim();
        if (trimmed.length < 3) {
            setFilter(undefined);
            setPage(0);
        }
        else {
            setFilter(trimmed);
            setPage(0);
        }
    };
    var syncQueryString = function () {
        if (firstLoad) {
            var initialParams = new URLSearchParams(window.location.search);
            var initialFilter = initialParams.get('s');
            var initialTagFilter = initialParams.getAll('t');
            var initialSortOrder = initialParams.get('o');
            if (initialFilter) {
                setFilterInputValue(initialFilter);
                setFilter(initialFilter);
            }
            if (initialTagFilter.length > 0) {
                setTagFilter(initialTagFilter);
            }
            if (initialSortOrder === 'oldest') {
                setSortOrder('oldest');
            }
            return;
        }
        if (window.history.replaceState) {
            var url = window.location.protocol;
            url += '//';
            url += window.location.host;
            url += window.location.pathname;
            if (filter || sortOrder === 'oldest' || tagFilter.length > 0) {
                var searchParams = new URLSearchParams(window.location.search);
                if (filter) {
                    searchParams.set('s', filter);
                }
                else {
                    searchParams["delete"]('s');
                }
                searchParams["delete"]('t');
                for (var _i = 0, tagFilter_1 = tagFilter; _i < tagFilter_1.length; _i++) {
                    var tag = tagFilter_1[_i];
                    searchParams.append('t', tag);
                }
                if (sortOrder === 'oldest') {
                    searchParams.set('o', 'oldest');
                }
                else {
                    searchParams["delete"]('o');
                }
                url += '?';
                url += searchParams.toString();
            }
            window.history.replaceState({ path: url }, '', url);
        }
    };
    var onPagerChanged = function (page, perPage) {
        setPage(page);
        setPerPage(perPage);
    };
    var formatTime = function (index, totalSeconds) {
        if (index !== playingIndex) {
            return '';
        }
        var hrs = Math.floor(totalSeconds / 3600);
        var hrsPrefix = hrs > 0 ? "".concat(hrs, ":") : '';
        totalSeconds = totalSeconds - hrs * 3600;
        var min = Math.floor(totalSeconds / 60);
        var minPadded = String(min).padStart(2, '0');
        var sec = Math.floor(totalSeconds - min * 60);
        var secPadded = String(sec).padStart(2, '0');
        return "".concat(hrsPrefix).concat(minPadded, ":").concat(secPadded);
    };
    var onOpenTagDialog = function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            setTagsTemp(tags.map(function (tag) { return tag.clone(); }));
            setTagDialogOpen(true);
            return [2 /*return*/];
        });
    }); };
    var onApplyTagChanges = function () {
        setTags(tagsTemp.map(function (tag) { return tag.clone(); }));
        setTagDialogOpen(false);
        var newFilterTags = tagsTemp
            .filter(function (tag) { return tag.isSelected; })
            .map(function (tag) { return tag.tag; });
        setTagFilter(newFilterTags);
    };
    var clearTagFilter = function () {
        setTagFilter([]);
        for (var _i = 0, tags_1 = tags; _i < tags_1.length; _i++) {
            var tag = tags_1[_i];
            tag.isSelected = false;
        }
    };
    var onToggleTag = function (i) {
        var tagsClone = tagsTemp.map(function (tag) { return tag.clone(); });
        tagsClone[i].isSelected = !tagsClone[i].isSelected;
        setTagsTemp(tagsClone);
    };
    var sortMenu = (react_1["default"].createElement(core_1.Menu, null,
        react_1["default"].createElement(core_1.MenuItem, { icon: "sort-asc", text: "Sort latest", active: sortOrder === 'latest', onClick: function () { return setSortOrder('latest'); } }),
        react_1["default"].createElement(core_1.MenuItem, { icon: "sort-desc", text: "Sort oldest", active: sortOrder === 'oldest', onClick: function () { return setSortOrder('oldest'); } })));
    return (react_1["default"].createElement(public_layout_1["default"], { frontmatter: frontMatter, assetsRoot: assetsRoot, config: config, kerygmaVersion: kerygmaVersion, pathHome: "" },
        react_1["default"].createElement("h1", null, "Sermons"),
        react_1["default"].createElement("div", { className: classes.centerContainer },
            react_1["default"].createElement(core_1.Card, { elevation: 1, className: classes.card },
                react_1["default"].createElement("div", { className: "".concat(classes.headerRow, " card-header") },
                    react_1["default"].createElement(core_1.InputGroup, { placeholder: "Search sermons on title, speaker, year or passage", type: "search", leftIcon: "search", className: classes.searchInput, onChange: onSearch, value: filterInputValue }),
                    (!tags || tags.length > 0) && (react_1["default"].createElement(core_1.Button, { icon: "tag", className: classes.sort, onClick: onOpenTagDialog })),
                    react_1["default"].createElement(core_1.Popover, { placement: "bottom-end", content: sortMenu },
                        react_1["default"].createElement(core_1.Button, { icon: "sort", className: classes.sort }))),
                tagFilter.length > 0 && (react_1["default"].createElement("div", { className: "".concat(classes.tagFilterRow, " card-header") },
                    react_1["default"].createElement(core_1.Icon, { icon: "tag", size: 12 }),
                    tagFilter.map(function (tag, i) { return (react_1["default"].createElement(core_1.Tag, { key: i, minimal: true, round: true, className: classes.tag }, tag)); }),
                    react_1["default"].createElement(core_1.Button, { icon: "cross", minimal: true, className: classes.clearTags, onClick: clearTagFilter }))),
                react_1["default"].createElement("div", { className: classes.body }, isLoading === true ? (react_1["default"].createElement(react_1["default"].Fragment, null, Array(3)
                    .fill(0)
                    .map(function (_val, index) { return (react_1["default"].createElement("div", { className: classes.docRow, key: index },
                    react_1["default"].createElement("div", { className: "".concat(classes.avatar, " ").concat(core_1.Classes.SKELETON) },
                        react_1["default"].createElement(core_1.Icon, { icon: "document", color: "white" })),
                    react_1["default"].createElement("div", { className: classes.metaCol },
                        react_1["default"].createElement("a", { href: "#", className: core_1.Classes.SKELETON }, "Skeleton Sermon Title Goes Here"),
                        react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.SKELETON) }, "Speaker: speaker name here"),
                        react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.SKELETON) }, "service: Morning Worship"),
                        react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.SKELETON) }, "passage: Romans 8:1-8:2"),
                        react_1["default"].createElement("div", { className: classes.playLine },
                            react_1["default"].createElement(core_1.Button, { icon: "play", minimal: true, className: "".concat(classes.playButton, " ").concat(core_1.Classes.SKELETON) }),
                            react_1["default"].createElement("span", { className: "".concat(classes.sermonDate, " ").concat(core_1.Classes.SKELETON) }, "Mon Jul 4, 2022"))),
                    react_1["default"].createElement(core_1.Button, { icon: "more", className: "".concat(classes.more, " ").concat(core_1.Classes.SKELETON) }))); }))) : (react_1["default"].createElement(react_1["default"].Fragment, null, sermons.length === 0 ? (react_1["default"].createElement("div", null,
                    react_1["default"].createElement(core_1.NonIdealState, { icon: "folder-open", title: "No Sermons", description: "No sermons have been added yet.", className: classes.empty }))) : (react_1["default"].createElement(react_1["default"].Fragment, null,
                    sermons.map(function (sermon, i) {
                        var passage = sermon.getPassage(bibleBooks);
                        return (react_1["default"].createElement("div", { className: classes.docRow, key: sermon.id },
                            react_1["default"].createElement("div", { className: classes.avatar },
                                react_1["default"].createElement(core_1.Icon, { icon: "headset", color: "white" })),
                            react_1["default"].createElement("div", { className: classes.metaCol },
                                react_1["default"].createElement("a", { href: "".concat(sermonsUri).concat(sermon.filename), target: "_blank", rel: "noopener noreferrer" },
                                    sermon.title,
                                    react_1["default"].createElement(core_1.Icon, { className: classes.linkIcon, icon: "share", size: 12 })),
                                react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    "Speaker: ",
                                    sermon.speakerTitle,
                                    ' ',
                                    sermon.speakerFirstName,
                                    " ",
                                    sermon.speakerLastName),
                                react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    "service: ",
                                    sermon.service),
                                passage && (react_1["default"].createElement("div", { className: "".concat(classes.metaLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    "passage: ",
                                    passage)),
                                sermon.tags && sermon.tags.length > 0 && (react_1["default"].createElement("div", { className: "".concat(classes.tagLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    react_1["default"].createElement(core_1.Icon, { icon: "tag", size: 12 }),
                                    sermon.tags.map(function (tag, i) { return (react_1["default"].createElement(core_1.Tag, { key: i, minimal: true, round: true, className: classes.tag }, tag)); }))),
                                react_1["default"].createElement("div", { className: "".concat(classes.playLine, " ").concat(core_1.Classes.TEXT_MUTED) },
                                    !isActive[i] ? (react_1["default"].createElement(core_1.Button, { icon: "play", minimal: true, className: classes.playButton, onClick: function () { return onTogglePlay(i); } })) : (react_1["default"].createElement(core_1.Button, { icon: "pause", minimal: true, className: classes.playButton, onClick: function () { return onTogglePlay(i); } })),
                                    react_1["default"].createElement("span", { className: classes.sermonDate }, sermon
                                        .getSermonDate()
                                        .format('ddd MMM D, YYYY')),
                                    i === playingIndex && (react_1["default"].createElement(react_1["default"].Fragment, null,
                                        react_1["default"].createElement("span", { className: classes.time }, formatTime(i, currentTime)),
                                        react_1["default"].createElement(core_1.Slider, { min: 0, max: duration, labelStepSize: duration, value: currentTime, intent: "primary", onChange: onSliderChange, onRelease: onSliderRelease, labelRenderer: false }),
                                        react_1["default"].createElement("span", { className: classes.time }, formatTime(i, duration)))),
                                    react_1["default"].createElement("audio", { ref: audioRefs[i], src: "".concat(sermonsUri).concat(sermon.filename) })))));
                    }),
                    react_1["default"].createElement(list_pager_1["default"], { page: page, perPage: perPage, totalRecords: totalSermons, onChange: onPagerChanged })))))))),
        react_1["default"].createElement(tag_dialog_1["default"], { tags: tagsTemp, isOpen: tagDialogOpen, onCancel: function () { return setTagDialogOpen(false); }, onApply: onApplyTagChanges, onToggleTag: onToggleTag })));
};
function init(target, assetsRoot) {
    ReactDOM.render(react_1["default"].createElement(Sermons, { assetsRoot: assetsRoot }), document.getElementById(target));
}
exports.init = init;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	(() => {
/******/ 		// The chunk loading function for additional chunks
/******/ 		// Since all referenced chunks are already included
/******/ 		// in this file, this function is empty here.
/******/ 		__webpack_require__.e = () => (Promise.resolve());
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/harmony module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.hmd = (module) => {
/******/ 			module = Object.create(module);
/******/ 			if (!module.children) module.children = [];
/******/ 			Object.defineProperty(module, 'exports', {
/******/ 				enumerable: true,
/******/ 				set: () => {
/******/ 					throw new Error('ES Modules may not assign module.exports or exports.*, Use ESM export syntax, instead: ' + module.id);
/******/ 				}
/******/ 			});
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"sermons": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkkerygma"] = self["webpackChunkkerygma"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendors","ui_src_common_global-constants_ts-ui_src_di-container_ts","ui_src_components_church-admin_dark-mode-context_ts-ui_src_services_data-access_ts-ui_src_ser-43f718","ui_src_services_sermon-data-access_ts-ui_src_model_bible-book-data_yaml","ui_src_layouts_public-layout_tsx"], () => (__webpack_require__("./ui/src/pages/sermons.tsx")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	sermons = __webpack_exports__;
/******/ 	
/******/ })()
;