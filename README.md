<div align="center">
<img src="https://gitlab.com/davestevens/kerygma/-/raw/master/logo.png" alt="Kerygma Logo" width="140"/>
</div>

<br>

Kerygma is a static site generator for churches.
It generates HTML and React JS from markdown.
It also adds sermon and document management
functionality geared specifically for churches.

The Greek noun kerygma (κήρυγμα) means preaching
or proclamation.


> For after that in the wisdom of God the world
knew not God, it pleased God by the foolishness
of preaching (κήρυγμα) to save them that believe. ~ 1 Cor. 1:21

## Features

- Static site generation based on mark down files
  - Mark down can include any of the following components
    - LazyImage
    - LocationMap
- Sermon administration
- Document administration
- User administration
- Podcast RSS feed that works with Apple podcasts

## Documentation

- Visit the [demo church site](https://davestevens.gitlab.io/kerygma/demo/index.html) for a demo of static content that you can generate using Markdown.
- [Documentation Site](https://davestevens.gitlab.io/kerygma)

## Install

```
npm i kerygma
```

---

## Usage

```
npx kerygma init      # use this to create a new site from scratch
npx kerygma generate  # generates your static content
npx kerygma migrate   # creates the necessary tables in the database
npx kerygma start     # serve up your site
npx kerygma --help
npx kerygma --version
```

## Future Features

- [ ] Change mobile menu to be a slide out
- [ ] Video sermon support
- [ ] Public announcements
- [ ] Add user profile with change password functionality

## Recently Completed Features

- [x] Tags on sermons
- [x] Documentation
- [x] Add init command to set up the config file, pages dir and assets dir
- [x] Apple podcast feed (completed with v0.1.54)
- [x] Make navigation data driven (completed with v0.1.55)
- [x] Custom sermon ordering by sermon type on same day (completed with v0.1.54)
