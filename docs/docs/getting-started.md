---
sidebar_position: 3
---

# Get started

Add the Package to Your Node Application

```
npm i kerygma
```

## Usage

Kergyma comes with a command line interface (CLI) to help you build and manage your church website.
Here are some of the commands you can run from the command line.

```
npx kerygma init      # use this to create a new site from scratch
npx kerygma generate  # generates your static content
npx kerygma migrate   # creates the necessary tables in the database
npx kerygma start     # serve up your site
npx kerygma --help
npx kerygma --version
```

Initialize a new church website from scratch using our demo for example pages.

```
npx kerygma init
```

Make changes to the `kerygma-config.yaml` file to configure various metadata
about your church, the colors, fonts and navigation for your new church website.

Generate the static html pages from your markdown (MDX) files and start your server.

```
npx kerygma generate
npx kerygma start
```

## Setting Up Your Database

1. If you are using Windows you may want to consider using Ubuntu running under
   Windows Subsystem for Linux v2 (WSL2).
1. Install the PostgreSQL database from https://www.postgresql.org/
1. Create the kerygma database and user. Of course for your production environment
   you will want to use a different username and password.
    ```
    $ sudo -u postgres psql
    postgres=# create database kerygma;
    postgres=# create user kerygma with encrypted password 'kerygma';
    postgres=# grant all privileges on database kerygma to kerygma;
    postgres=# exit
    $ sudo -u postgres psql -d kerygma
    postgres=# CREATE EXTENSION IF NOT EXISTS 'uuid-ossp';
    postgres=# CREATE EXTENSION IF NOT EXISTS 'pgcrypto';
    postgres=# GRANT ALL ON SCHEMA public TO 'kerygma';
    postgres=# exit
    ```
1. Test out the newly created database and user.
    ```
    $ psql --host=localhost --dbname=kerygma --username=kerygma
    ```
1. Update your `.env` file if you changed the kerygma username or password.
1. Run the following to create your tables in your new kerygma database.
    ```
    npx kerygma migrate
    ```
1. Start up your server.
    ```
    npx kerygma start
    ```
1. Then visit [http://localhost:9000/](http://localhost:9000/) for the public facing site
   and [http://localhost:9000/church-admin/](http://localhost:9000/church-admin/)
   for the church admin site. Login with the following user on the church admin site.
   In a production environment you will want to add a new system admin user and delete this default user.
    * Username: `adminuser`
    * Password: `ChangeMe123`
