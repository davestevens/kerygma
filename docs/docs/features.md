---
sidebar_position: 2
---
# Features

- :zap: Generate static HTML pages from easy to write markdown ([MDX](https://mdxjs.com/)).
- :art: You can use the following components within your MDX markdown files.
    - LazyImage - allows you to add an image from your assets folder at full, wide or half width. The image
      loads a small blurred placeholder image first for performance purposes.
    - LocationMap - adds a map showing the location of your church.
- :microphone: Sermon management system
    - Upload, edit or delete sermons that can be played on your site.
    - All sermons are searchable by title, speaker, year or passage.
- :page_facing_up: Document management system
    - Upload pdfs to your site and link to them from any markdown page (e.g. a sample bulletin).
- :lock: User management system
    - Control who can upload and manage sermons and documents.
    - Both system admin (can manage users) and admin roles are supported.
- :wrench: Fully configurable using a [yaml](https://yaml.org/) file.
- :iphone: Create an Apple podcast for free using the podcast feed URL.
- :bookmark: Ability to tag sermons so you can create a link to a sermon series, etc.

## Some Screenshots of the Sermon Management System
![Main Admin Page](./admin-screenshot.png)
![Add Sermon Admin Page](./add-sermon-screenshot.png)
