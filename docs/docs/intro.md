---
sidebar_position: 1
---
# Introduction

Kerygma is a tool to generate static marketing pages for a church that sit side by side
with a sermon management web app written in Node.js and PostgreSQL. You can use the
static site generation apart from or together with the sermon management web app.

## Why Kerygma?

### Why not build a church web site using a tool like WordPress or Wix? Wouldn't it be easier?

- WordPress is great in that it is popular and it is easy to find people skilled in
  using it. However, WordPress is a victim of its own success in that it has become
  a large target for hackers. Also, dynamic sites running off of a database are slower
  and easier to hack.
- Wix is a large company. All large companies are subject to pressure from woke
  activism that can easily shut down a church web site if the hosting company
  disagrees with what is being preached. This really goes for all cloud based companies.
  I originally stored all of my church's sermons on Amazon Storage because it was
  so inexpensive. Then Amazon started censoring books. Why should churches support
  companies that hate Christianity and the Bible?

### Why not host a church site on something like SermonAudio?

- [SermonAudio](https://www.sermonaudio.com/) has become a great service to the church
  in getting the gospel out while charging very little for churches to use it.
  However, SermonAudio is also susceptible to hackers because, as a SAAS (software as a service) company,
  it is a single point of failure.
  The idea behind kerygma is to encourage churches to have their technology infrastructure
  as decentralized as possible and therefore as resistant to hackers and censorship as possible.
  I encourage all churches to run Kerygma on two separate low end, low cost VPS (virtual private servers)
  services. In that way, if your site is censored or hacked you can quickly move your domain to
  your backup server and start shopping for another vendor. Worse comes to worse, you can even run Kerygma on a small PC
  running at your home if you get a static IP address from your ISP.

### What if they come after your domain registrar?

- If it ever comes to that churches may need to start using TOR .onion addresses.

## The Purpose of Kerygma

- Always open source and free for any church to use.
- To provide churches a way to leave big tech with its anti Christian, immoral, woke ideologies.
- To provide churches with a way to make their marketing sites and sermons less susceptible to hackers and censorship.

## What Does Kerygma mean?

- The Greek noun κήρυγμα refers to that which is preached or heralded. It seemed an appropriate
  name for this project, which was started to help churches manage their web sites and make their
  preaching available to the world.
