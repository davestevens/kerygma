import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import Heading from '@theme/Heading';

import styles from './index.module.css';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero', styles.heroBanner)}>
      <div className="container">
        <Heading as="h1" className="hero__title">
          {siteConfig.title}
        </Heading>
        <p className="hero__subtitle">{siteConfig.tagline}</p>
        <div className={styles.buttons}>
          <Link
            className={`${styles.marginButton} button button--secondary button--lg`}
            to="/docs/intro">
            Introduction
          </Link>
          <Link
            className="button button--secondary button--lg"
            to="/docs/getting-started">
            Getting Started
          </Link>
        </div>
        <div className={styles.descriptionContainer}>
          <div className={styles.description}>
            Kerygma is a tool to generate static marketing pages for a church that sit side by side
            with a sermon management web app written in Node.js and PostgreSQL. You can use the
            static site generation apart from or together with the sermon management web app.
          </div>
        </div>
      </div>
    </header>
  );
}

export default function Home(): JSX.Element {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Kerygma is a tool to generate static marketing sites for churches.">
      <HomepageHeader />
    </Layout>
  );
}
