#!/usr/bin/env node
'use strict';

const package_json = require('../package.json')
const { Command } = require('commander')
const Confirm = require('prompt-confirm')
const program = new Command()
const fs = require('fs')

program.name('kerygma')
  .description('CLI for Kerygma - a static site generator for churches.')
  .version(package_json.version)

program.command('start')
  .description('Run the kerygma API for church admin functionality.')
  .option('-c, --config <path>', 'specify the kerygma config yaml file', './kerygma-config.yaml')
  .action((options) => {
      console.log(`Starting kerygma API version ${package_json.version}...`)
      const {startServer} = require('../api/dist/index')
      startServer(options.config).then()
  })

program.command('generate')
  .description('Generate static html pages.')
  .option('-c, --config <path>', 'specify the kerygma config yaml file', './kerygma-config.yaml')
  .action((options) => {
    console.log(`Starting kerygma static site generator version ${package_json.version}...`)
    const {generateStaticPages} = require('../generator/dist/index')
    generateStaticPages(options.config).then()
  })

program.command('migrate')
  .description('Setup or migrate your database to the currently installed version.')
  .option('-u, --up', 'Executes any remaining un-executed migrations')
  .option('-d, --down', 'Executes the latest down migration (undo prior migration)')
  .action((options) => {
    console.log(`Kerygma database setup or migration tool version ${package_json.version}...`)
    console.warn('WARNING: Please be sure to backup your database before proceeding if this is an upgrade.')
    const prompt = new Confirm('Do you wish to proceed?')
    prompt.ask(answer => {
      if (answer === true) {
        const {migrateDatabase} = require('../api/dist/index')
        if (options.down === true) {
          migrateDatabase('down')
        } else {
          migrateDatabase('up')
        }
      } else {
        console.log('Setup or migration canceled.')
      }
    })
  })

program.command('init')
  .description('Initialize a new church web site from scratch using our demo for example pages.')
  .option('-c, --config <path>', 'specify an alternate name for your kerygma config file.', 'kerygma-config.yaml')
  .option('-p, --pages <path>', 'specify the directory you want to create for your pages.', 'pages')
  .option('-a, --assets <path>', 'specify the directory you want to create for you assets (images, etc).', 'assets')
  .option('-m, --media <path>', 'specify the directory you want to create for you media (audio mp3 files, pdfs, etc).', 'media')
  .action((options) => {
    console.log(`Creating a new Kerygma version ${package_json.version} church web site...`)
    console.log(`  kerygma config file to create: ${options.config}`)
    console.log('  api config file to create: .env')
    console.log(`  pages folder to create: ${options.pages}`)
    console.log(`  assets folder to create: ${options.assets}`)
    console.log(`  media folder to create: ${options.media}`)
    if (fs.existsSync(options.config)) {
      console.warn(`The config file ${options.config} already exists. Please rename or delete this file before proceeding. Init action canceled.`)
      return;
    }
    if (fs.existsSync(options.pages)) {
      console.warn(`The pages directory ${options.pages} already exists. Please rename or delete this path before proceeding. Init action canceled.`)
      return;
    }
    if (fs.existsSync(options.assets)) {
      console.warn(`The assets directory ${options.assets} already exists. Please rename or delete this path before proceeding. Init action canceled.`)
      return;
    }
    if (fs.existsSync(options.media)) {
      console.warn(`The media directory ${options.media} already exists. Please rename or delete this path before proceeding. Init action canceled.`)
      return;
    }
    if (fs.existsSync('.env')) {
      console.warn(`The .env file already exists. Please rename or delete this path before proceeding. Init action canceled.`)
      return;
    }
    const prompt = new Confirm('Do you wish to proceed?')
    prompt.ask(answer => {
      if (answer === true) {
        const {initWebSite} = require('../api/dist/index')
        initWebSite(options.config, options.pages, options.assets, options.media)
      } else {
        console.log('Init action canceled.')
      }
    })
  })

program.parse()
