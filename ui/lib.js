const LazyImage = require('./dist/components/lazy-image')
const LocationMap = require('./dist/components/location-map')
const { kerygmaConfig } = require('./dist/common/declare-build-constants')

exports.LazyImage = LazyImage.default
exports.LocationMap = LocationMap.default
exports.kerygmaConfig = kerygmaConfig
