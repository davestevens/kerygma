// eslint-disable-next-line no-undef
module.exports = {
  root: true,
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error',
  },
  env: { browser: true, node: true, es6: true },
  ignorePatterns: ['dist', 'coverage', 'types'],
  overrides: [
    {
      files: ['**/*.ts', '**/*.tsx'],
      extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'prettier',
      ],
      plugins: ['@typescript-eslint'],
    },
    {
      files: ['**/*.js', '**/*.mjs'],
      extends: ['eslint:recommended', 'prettier'],
      parserOptions: {
        sourceType: 'module',
        ecmaVersion: 2020,
      },
    },
  ],
}
