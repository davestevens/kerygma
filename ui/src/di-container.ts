import 'reflect-metadata'
import { container } from 'tsyringe'
import { Fetcher } from './services/data-access'
import axios from 'axios'
import { JsonConvert } from 'json2typescript'
import { KerygmaConfiguration } from './model/kerygma-configuration'
import { kerygmaConfig, kerygmaVersion } from './common/declare-build-constants'

const fetcher: Fetcher = (url, settings) => {
  return axios(url, settings)
}
container.register<Fetcher>('fetcher', { useValue: fetcher })
container.register<Storage>('localStorage', { useValue: localStorage })
container.register<Storage>('sessionStorage', { useValue: sessionStorage })
container.register(JsonConvert, { useValue: new JsonConvert() })
container.register(KerygmaConfiguration, { useValue: kerygmaConfig })
container.register<string>('kerygmaVersion', { useValue: kerygmaVersion })

export default container
