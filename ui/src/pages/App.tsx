import * as React from 'react'
import * as ReactDOM from 'react-dom'
import layoutMap from '../layouts/layout-map'
import PageFrontmatter from '../common/page-frontmatter'
import LayoutProps from '../layouts/layout-props'
import { FocusStyleManager } from '@blueprintjs/core'
import container from '../di-container'
import { KerygmaConfiguration } from '../model/kerygma-configuration'

FocusStyleManager.onlyShowFocusOnTabs()
const config = container.resolve(KerygmaConfiguration)
const kerygmaVersion = container.resolve<string>('kerygmaVersion')

export function init(
  Page: React.FC<LayoutProps>,
  target: string,
  frontMatterEncoded: string,
  assetsRoot: string,
  pathHome: string
): void {
  const frontMatterStr = window.atob(frontMatterEncoded)
  const frontMatterObj = JSON.parse(frontMatterStr)
  const frontMatter: PageFrontmatter = frontMatterObj?.data
  let RootComponent: JSX.Element

  if (frontMatter.layout && layoutMap[frontMatter.layout]) {
    const Layout = layoutMap[frontMatter.layout] as React.FC<LayoutProps>
    RootComponent = (
      <Layout
        frontmatter={frontMatter}
        assetsRoot={assetsRoot}
        config={config}
        kerygmaVersion={kerygmaVersion}
        pathHome={pathHome}
      >
        <Page />
      </Layout>
    )
  } else {
    RootComponent = <Page assetsRoot={assetsRoot} />
  }
  ReactDOM.render(RootComponent, document.getElementById(target))
}
