import { globalConstants } from '../common/global-constants'
import { Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'
import { KerygmaTheme } from '../model/kerygma-configuration'

export const useStylesThemed = (theme: KerygmaTheme) => {
  return createUseStyles({
    '@global': {
      h4: {
        fontFamily: theme.fonts.default.family,
      },
    },
    centerContainer: `
      display: flex;
      justify-content: center;
      padding: 0 16px; 
      width: 100%;
    `,
    card: `
      padding: 0;
      max-width: ${globalConstants.bp.phoneMax};
      width: 100%;
      margin-bottom: 30px;
    `,
    cardSide: `
      padding: 0;
      max-width: 300px;
      width: 100%;
      margin: 30px 0 0 16px;
    `,
    headerRow: `
      display: flex;
      justify-content: space-between;
      align-items: center;
      padding: 10px 16px;
      border-top-left-radius: 3px;
      border-top-right-radius: 3px;
      border-bottom: 1px solid ${Colors.LIGHT_GRAY1};
      background-color: ${Colors.LIGHT_GRAY4};
    `,
    searchRow: `
      display: flex;
      padding: 4px 16px;
    `,
    tagFilterRow: `
      display: flex;
      padding: 4px 16px;
      border-top: none !important;
      align-items: center;
      border-bottom: 1px solid ${Colors.LIGHT_GRAY1};
      background-color: ${Colors.LIGHT_GRAY4};
    `,
    clearTags: `
      margin-left: auto;
    `,
    searchInput: `
      flex: 1;
    `,
    sort: `
      margin: 0 0 0 8px;
    `,
    header: `
      text-align: left;
      padding: 0;
      width: auto;
    `,
    empty: {
      margin: '40px 0',
      '& h4': `
        margin-bottom: 20px;
      `,
    },
    body: `
      padding: 16px;
    `,
    docRow: {
      display: 'flex',
      alignItems: 'flex-start',
      marginBottom: '16px',
      '&:last-child': `
        margin-bottom: 0;
      `,
    },
    avatar: `
      display: flex;
      background-color: ${Colors.GRAY3};
      padding: 6px;
      border-radius: 2px;
      margin: 4px 16px 0 0;
      line-height: 1;
    `,
    metaCol: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      marginRight: 'auto',
      flex: 1,
      '& a': `
        display: flex;
        align-items: center;
      `,
    },
    linkIcon: `
      margin-left: 4px;
      padding-bottom: 1px;
    `,
    metaLine: `
      display: flex;
      font-size: 12px;
      margin-top: 2px;
    `,
    tagLine: `
      display: flex;
      margin-top: 4px;
      align-items: center;
    `,
    tag: `
      margin-left: 4px;
    `,
    more: `
      margin: 4px 0 0 8px;
    `,
    playLine: `
      display: flex;
      width: 100%;
      font-size: 12px;
      align-items: center;
      margin-top: 2px;
    `,
    playButton: `
      margin-right: 4px;
    `,
    sermonDate: `
      white-space: nowrap;
    `,
    time: `
      white-space: nowrap;
      margin: 0 8px;
    `,
  })
}
