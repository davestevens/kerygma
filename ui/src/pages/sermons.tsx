import React, { ChangeEvent, RefObject, useEffect, useState } from 'react'
import * as ReactDOM from 'react-dom'
import PublicLayout from '../layouts/public-layout'
import PageFrontmatter from '../common/page-frontmatter'
import LayoutProps from '../layouts/layout-props'
import { useStylesThemed } from './sermons.styles'
import {
  Button,
  Card,
  Classes,
  Icon,
  InputGroup,
  Menu,
  MenuItem,
  NonIdealState,
  Popover,
  Slider,
  Tag,
} from '@blueprintjs/core'
import { Sermon } from '../model/sermon'
import ListPager, { perPageOptions } from '../components/list-pager'
import container from '../di-container'
import SermonDataAccess from '../services/sermon-data-access'
import bibleBookData from '../model/bible-book-data.yaml'
import { BibleBook } from '../model/bible-book'
import { KerygmaConfiguration } from '../model/kerygma-configuration'
import { SermonTag } from '../model/sermon-tag'
import TagDialog from '../components/church-admin/tag-dialog'

const config = container.resolve(KerygmaConfiguration)
const kerygmaVersion = container.resolve<string>('kerygmaVersion')
const { apiHost, mediaContextRoot } = config.api
const sermonsUri = `${apiHost}${mediaContextRoot}/sermons/`
const bibleBooks = bibleBookData.books as BibleBook[]

const sermonDataAccess = container.resolve(SermonDataAccess)

const Sermons: React.FC<LayoutProps> = ({ assetsRoot }) => {
  const frontMatter: PageFrontmatter = {
    title: 'Sermons',
    description: 'Where you can go to listen to our sermons.',
  }
  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [sermons, setSermons] = useState<Sermon[]>([])
  const [audioRefs, setAudioRefs] = React.useState<
    RefObject<HTMLMediaElement>[]
  >([])
  const [isActive, setIsActive] = React.useState<boolean[]>([])
  const [intervalId, setIntervalId] = React.useState<number>(0)
  const [currentTime, setCurrentTime] = useState(0)
  const [duration, setDuration] = useState(0)
  const [playingIndex, setPlayingIndex] = useState<number>(null)
  const [page, setPage] = React.useState<number>(0)
  const [perPage, setPerPage] = React.useState<number>(perPageOptions[0])
  const [sortOrder, setSortOrder] = React.useState<'latest' | 'oldest'>(
    'latest'
  )
  const [filterInputValue, setFilterInputValue] = React.useState<string>('')
  const [filter, setFilter] = React.useState<string | undefined>(undefined)
  const [tagFilter, setTagFilter] = React.useState<string[]>([])
  const [totalSermons, setTotalSermons] = React.useState<number>(0)
  const [tagDialogOpen, setTagDialogOpen] = React.useState<boolean>(false)
  const [tags, setTags] = React.useState<SermonTag[] | undefined>(undefined)
  const [tagsTemp, setTagsTemp] = React.useState<SermonTag[]>([])
  const [firstLoad, setFirstLoad] = React.useState<boolean>(true)

  const classes = useStylesThemed(config.theme)()

  const loadSermons = async (): Promise<void> => {
    setIsLoading(true)
    try {
      const response = await sermonDataAccess.debouncedGetSermons(
        page,
        perPage,
        sortOrder,
        filter,
        tagFilter
      )
      setSermons(response.sermons)
      setTotalSermons(response.totalRowCount)
      const sermonRefs = response.sermons.map(() => {
        return React.createRef<HTMLMediaElement>()
      })
      const playingStates = response.sermons.map(() => false)
      setAudioRefs(sermonRefs)
      setIsActive(playingStates)
    } catch (err) {
      console.error({ msg: 'Error encountered loading sermons', err })
    } finally {
      setIsLoading(false)
    }
  }

  const loadTags = async (): Promise<void> => {
    const availableTags = await sermonDataAccess.getSermonTags('sermonCount')
    if (tagFilter.length > 0) {
      for (const tag of availableTags) {
        tag.isSelected = tagFilter.some((t) => t === tag.tag)
      }
    }
    setTags(availableTags)
  }

  useEffect(() => {
    syncQueryString()
    if (!tags) {
      loadTags().then()
    }
    loadSermons().then()
    setFirstLoad(false)
  }, [sortOrder, page, perPage, filter, tagFilter])

  const onTogglePlay = (index: number) => {
    const playingStates = [...isActive]
    setPlayingIndex(index)
    for (let i = 0; i < audioRefs.length; i++) {
      const audioRef = audioRefs[i]?.current
      if (i === index) {
        if (audioRef.paused) {
          if (currentTime === audioRef.duration) {
            audioRef.currentTime = 0
          }
          audioRef.play().then()
          playingStates[i] = true
          setDuration(audioRef.duration)
          setCurrentTime(audioRef.currentTime)
          clearInterval(intervalId)
          const newIntervalId = window.setInterval(() => {
            setCurrentTime(audioRef.currentTime)
            if (audioRef.paused) {
              const intervalPlayingStates = [...isActive]
              intervalPlayingStates[i] = false
              setIsActive(intervalPlayingStates)
              clearInterval(intervalId)
            }
          }, 1000)
          setIntervalId(newIntervalId)
        } else {
          audioRef.pause()
          playingStates[i] = false
          clearInterval(intervalId)
        }
      } else {
        if (!audioRef.paused) {
          audioRef.pause()
          playingStates[i] = false
        }
      }
    }
    setIsActive(playingStates)
  }

  const onSliderChange = (value: number) => {
    const audioRef = audioRefs[playingIndex]?.current
    if (isActive[playingIndex] && !audioRef.paused) {
      onTogglePlay(playingIndex)
    }
    setCurrentTime(value)
  }

  const onSliderRelease = (value: number) => {
    setCurrentTime(value)
    const audioRef = audioRefs[playingIndex]?.current
    audioRef.currentTime = value
    if (value < duration) {
      onTogglePlay(playingIndex)
    }
  }

  const onSearch = (elem: ChangeEvent<HTMLInputElement>) => {
    const newFilter = elem.target.value ?? ''
    setFilterInputValue(newFilter)
    const trimmed = newFilter.trim()
    if (trimmed.length < 3) {
      setFilter(undefined)
      setPage(0)
    } else {
      setFilter(trimmed)
      setPage(0)
    }
  }

  const syncQueryString = () => {
    if (firstLoad) {
      const initialParams = new URLSearchParams(window.location.search)
      const initialFilter = initialParams.get('s')
      const initialTagFilter = initialParams.getAll('t')
      const initialSortOrder = initialParams.get('o')
      if (initialFilter) {
        setFilterInputValue(initialFilter)
        setFilter(initialFilter)
      }
      if (initialTagFilter.length > 0) {
        setTagFilter(initialTagFilter)
      }
      if (initialSortOrder === 'oldest') {
        setSortOrder('oldest')
      }
      return
    }
    if (window.history.replaceState) {
      let url = window.location.protocol
      url += '//'
      url += window.location.host
      url += window.location.pathname
      if (filter || sortOrder === 'oldest' || tagFilter.length > 0) {
        const searchParams = new URLSearchParams(window.location.search)
        if (filter) {
          searchParams.set('s', filter)
        } else {
          searchParams.delete('s')
        }
        searchParams.delete('t')
        for (const tag of tagFilter) {
          searchParams.append('t', tag)
        }
        if (sortOrder === 'oldest') {
          searchParams.set('o', 'oldest')
        } else {
          searchParams.delete('o')
        }
        url += '?'
        url += searchParams.toString()
      }
      window.history.replaceState({ path: url }, '', url)
    }
  }

  const onPagerChanged = (page: number, perPage: number) => {
    setPage(page)
    setPerPage(perPage)
  }

  const formatTime = (index: number, totalSeconds: number): string => {
    if (index !== playingIndex) {
      return ''
    }
    const hrs = Math.floor(totalSeconds / 3600)
    const hrsPrefix = hrs > 0 ? `${hrs}:` : ''
    totalSeconds = totalSeconds - hrs * 3600
    const min = Math.floor(totalSeconds / 60)
    const minPadded = String(min).padStart(2, '0')
    const sec = Math.floor(totalSeconds - min * 60)
    const secPadded = String(sec).padStart(2, '0')
    return `${hrsPrefix}${minPadded}:${secPadded}`
  }

  const onOpenTagDialog = async () => {
    setTagsTemp(tags.map((tag) => tag.clone()))
    setTagDialogOpen(true)
  }

  const onApplyTagChanges = () => {
    setTags(tagsTemp.map((tag) => tag.clone()))
    setTagDialogOpen(false)
    const newFilterTags = tagsTemp
      .filter((tag) => tag.isSelected)
      .map((tag) => tag.tag)
    setTagFilter(newFilterTags)
  }

  const clearTagFilter = () => {
    setTagFilter([])
    for (const tag of tags) {
      tag.isSelected = false
    }
  }

  const onToggleTag = (i: number) => {
    const tagsClone = tagsTemp.map((tag) => tag.clone())
    tagsClone[i].isSelected = !tagsClone[i].isSelected
    setTagsTemp(tagsClone)
  }

  const sortMenu = (
    <Menu>
      <MenuItem
        icon="sort-asc"
        text="Sort latest"
        active={sortOrder === 'latest'}
        onClick={() => setSortOrder('latest')}
      />
      <MenuItem
        icon="sort-desc"
        text="Sort oldest"
        active={sortOrder === 'oldest'}
        onClick={() => setSortOrder('oldest')}
      />
    </Menu>
  )

  return (
    <PublicLayout
      frontmatter={frontMatter}
      assetsRoot={assetsRoot}
      config={config}
      kerygmaVersion={kerygmaVersion}
      pathHome=""
    >
      <h1>Sermons</h1>
      <div className={classes.centerContainer}>
        <Card elevation={1} className={classes.card}>
          <div className={`${classes.headerRow} card-header`}>
            <InputGroup
              placeholder="Search sermons on title, speaker, year or passage"
              type="search"
              leftIcon="search"
              className={classes.searchInput}
              onChange={onSearch}
              value={filterInputValue}
            ></InputGroup>
            {(!tags || tags.length > 0) && (
              <Button
                icon="tag"
                className={classes.sort}
                onClick={onOpenTagDialog}
              />
            )}
            <Popover placement="bottom-end" content={sortMenu}>
              <Button icon="sort" className={classes.sort} />
            </Popover>
          </div>
          {tagFilter.length > 0 && (
            <div className={`${classes.tagFilterRow} card-header`}>
              <Icon icon="tag" size={12} />
              {tagFilter.map((tag, i) => (
                <Tag key={i} minimal round className={classes.tag}>
                  {tag}
                </Tag>
              ))}
              <Button
                icon="cross"
                minimal
                className={classes.clearTags}
                onClick={clearTagFilter}
              />
            </div>
          )}
          <div className={classes.body}>
            {isLoading === true ? (
              <>
                {Array(3)
                  .fill(0)
                  .map((_val, index) => (
                    <div className={classes.docRow} key={index}>
                      <div className={`${classes.avatar} ${Classes.SKELETON}`}>
                        <Icon icon="document" color="white" />
                      </div>
                      <div className={classes.metaCol}>
                        <a href="#" className={Classes.SKELETON}>
                          Skeleton Sermon Title Goes Here
                        </a>
                        <div
                          className={`${classes.metaLine} ${Classes.SKELETON}`}
                        >
                          Speaker: speaker name here
                        </div>
                        <div
                          className={`${classes.metaLine} ${Classes.SKELETON}`}
                        >
                          service: Morning Worship
                        </div>
                        <div
                          className={`${classes.metaLine} ${Classes.SKELETON}`}
                        >
                          passage: Romans 8:1-8:2
                        </div>
                        <div className={classes.playLine}>
                          <Button
                            icon="play"
                            minimal={true}
                            className={`${classes.playButton} ${Classes.SKELETON}`}
                          />
                          <span
                            className={`${classes.sermonDate} ${Classes.SKELETON}`}
                          >
                            Mon Jul 4, 2022
                          </span>
                        </div>
                      </div>
                      <Button
                        icon="more"
                        className={`${classes.more} ${Classes.SKELETON}`}
                      />
                    </div>
                  ))}
              </>
            ) : (
              <>
                {sermons.length === 0 ? (
                  <div>
                    <NonIdealState
                      icon="folder-open"
                      title="No Sermons"
                      description="No sermons have been added yet."
                      className={classes.empty}
                    />
                  </div>
                ) : (
                  <>
                    {sermons.map((sermon, i) => {
                      const passage = sermon.getPassage(bibleBooks)
                      return (
                        <div className={classes.docRow} key={sermon.id}>
                          <div className={classes.avatar}>
                            <Icon icon="headset" color="white" />
                          </div>
                          <div className={classes.metaCol}>
                            <a
                              href={`${sermonsUri}${sermon.filename}`}
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              {sermon.title}
                              <Icon
                                className={classes.linkIcon}
                                icon="share"
                                size={12}
                              />
                            </a>
                            <div
                              className={`${classes.metaLine} ${Classes.TEXT_MUTED}`}
                            >
                              Speaker: {sermon.speakerTitle}{' '}
                              {sermon.speakerFirstName} {sermon.speakerLastName}
                            </div>
                            <div
                              className={`${classes.metaLine} ${Classes.TEXT_MUTED}`}
                            >
                              service: {sermon.service}
                            </div>
                            {passage && (
                              <div
                                className={`${classes.metaLine} ${Classes.TEXT_MUTED}`}
                              >
                                passage: {passage}
                              </div>
                            )}
                            {sermon.tags && sermon.tags.length > 0 && (
                              <div
                                className={`${classes.tagLine} ${Classes.TEXT_MUTED}`}
                              >
                                <Icon icon="tag" size={12} />
                                {sermon.tags.map((tag, i) => (
                                  <Tag
                                    key={i}
                                    minimal
                                    round
                                    className={classes.tag}
                                  >
                                    {tag}
                                  </Tag>
                                ))}
                              </div>
                            )}
                            <div
                              className={`${classes.playLine} ${Classes.TEXT_MUTED}`}
                            >
                              {!isActive[i] ? (
                                <Button
                                  icon="play"
                                  minimal={true}
                                  className={classes.playButton}
                                  onClick={() => onTogglePlay(i)}
                                />
                              ) : (
                                <Button
                                  icon="pause"
                                  minimal={true}
                                  className={classes.playButton}
                                  onClick={() => onTogglePlay(i)}
                                />
                              )}
                              <span className={classes.sermonDate}>
                                {sermon
                                  .getSermonDate()
                                  .format('ddd MMM D, YYYY')}
                              </span>
                              {i === playingIndex && (
                                <>
                                  <span className={classes.time}>
                                    {formatTime(i, currentTime)}
                                  </span>
                                  <Slider
                                    min={0}
                                    max={duration}
                                    labelStepSize={duration}
                                    value={currentTime}
                                    intent="primary"
                                    onChange={onSliderChange}
                                    onRelease={onSliderRelease}
                                    labelRenderer={false}
                                  />
                                  <span className={classes.time}>
                                    {formatTime(i, duration)}
                                  </span>
                                </>
                              )}
                              <audio
                                ref={audioRefs[i]}
                                src={`${sermonsUri}${sermon.filename}`}
                              />
                            </div>
                          </div>
                        </div>
                      )
                    })}
                    <ListPager
                      page={page}
                      perPage={perPage}
                      totalRecords={totalSermons}
                      onChange={onPagerChanged}
                    />
                  </>
                )}
              </>
            )}
          </div>
        </Card>
      </div>
      <TagDialog
        tags={tagsTemp}
        isOpen={tagDialogOpen}
        onCancel={() => setTagDialogOpen(false)}
        onApply={onApplyTagChanges}
        onToggleTag={onToggleTag}
      />
    </PublicLayout>
  )
}

export function init(target: string, assetsRoot: string) {
  ReactDOM.render(
    <Sermons assetsRoot={assetsRoot} />,
    document.getElementById(target)
  )
}
