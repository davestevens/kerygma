import React from 'react'
import * as ReactDOM from 'react-dom'
import LazyImage from '../components/lazy-image'
import PublicLayout from '../layouts/public-layout'
import PageFrontmatter from '../common/page-frontmatter'
import LayoutProps from '../layouts/layout-props'
import { parsePhoneNumberFromString } from 'libphonenumber-js'
import Obfuscate from 'react-obfuscate'
import container from '../di-container'
import { KerygmaConfiguration } from '../model/kerygma-configuration'

const config = container.resolve(KerygmaConfiguration)
const kerygmaVersion = container.resolve<string>('kerygmaVersion')

/*
 * Had to make this page a component instead of an MDX file
 * because MDX does not support React hooks and the Obfuscate
 * component uses React hooks.
 */
const ContactUs: React.FC<LayoutProps> = ({ assetsRoot }) => {
  const frontMatter: PageFrontmatter = {
    title: 'Contact Us',
    description: `Contact Information for ${config.churchNameShort}`,
  }
  const phoneNumber = parsePhoneNumberFromString(config.phone, 'US')

  return (
    <PublicLayout
      frontmatter={frontMatter}
      assetsRoot={assetsRoot}
      config={config}
      kerygmaVersion={kerygmaVersion}
      pathHome=""
    >
      <br />
      <LazyImage src="./assets/img/hello-phone.jpg" ht="400" width="wide" />
      <h1>Contact Us</h1>
      <p style={{ textAlign: 'center' }}>
        Have a question? Give us a call or send us an email.
      </p>
      <br />
      <Obfuscate href={phoneNumber.getURI()}>
        {phoneNumber.formatNational()}
      </Obfuscate>
      <Obfuscate email={config.email}>{config.email}</Obfuscate>
      <br />
    </PublicLayout>
  )
}

export function init(target: string, assetsRoot: string) {
  ReactDOM.render(
    <ContactUs assetsRoot={assetsRoot} />,
    document.getElementById(target)
  )
}
