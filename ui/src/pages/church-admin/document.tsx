import {
  AnchorButton,
  BreadcrumbProps,
  Button,
  Card,
  Classes,
  Dialog,
  FileInput,
  FormGroup,
  ProgressBar,
} from '@blueprintjs/core'
import * as React from 'react'
import LayoutProps from '../../layouts/layout-props'
import { useStyles } from './document.styles'
import AdminPage from '../../components/church-admin/admin-page'
import CenterCardLayout from '../../components/church-admin/center-card-layout'
import container from '../../di-container'
import { FormEvent, useEffect, useState } from 'react'
import { User } from '../../model/user'
import ValidationFormGroup from '../../components/church-admin/validation-form-group'
import PageForm from '../../components/church-admin/page-form'
import { UploadedDocument } from '../../model/uploaded-document'
import { StatusCodes } from 'http-status-codes'
import UploadedDocumentDataAccess from '../../services/uploaded-document-data-access'
import UserDataAccess from '../../services/user-data-access'
import { AxiosResponse } from 'axios'
import DarkModeContext from '../../components/church-admin/dark-mode-context'
import { ProgressEvent } from '../../services/data-access'
import * as ReactDOM from 'react-dom'

const docIdStr: string | null = new URLSearchParams(window.location.search).get(
  'docId'
)
const docId: number | null = docIdStr ? parseInt(docIdStr) : null
const mode: 'edit' | 'new' = docId ? 'edit' : 'new'
const title = `${mode === 'new' ? 'Add' : 'Edit'} Document`
const uploadedDocumentDataAccess = container.resolve(UploadedDocumentDataAccess)
const userDataAccess = container.resolve(UserDataAccess)

const breadcrumbs: BreadcrumbProps[] = [
  { href: '/church-admin/documents.html', icon: 'document', text: 'Documents' },
  { icon: mode === 'new' ? 'document-open' : 'edit', text: title },
]

const Document: React.FC<LayoutProps> = ({ assetsRoot }) => {
  const [error, setError] = React.useState<Error>()
  const [loginUser, setLoginUser] = useState<User>()
  const [uploadedDoc, setUploadedDoc] = useState<UploadedDocument>(
    new UploadedDocument()
  )
  const [localFilename, setLocalFilename] = React.useState<string>('')
  const [isProcessing, setIsProcessing] = React.useState<boolean>(false)
  const [isProgressDialogOpen, setIsProgressDialogOpen] =
    React.useState<boolean>(false)
  const [progress, setProgress] = React.useState<number>(0.0)
  const [isLoading, setIsLoading] = React.useState<boolean>(true)
  const fileInputRef = React.useRef<HTMLInputElement>()
  const [uploadError, setUploadError] = React.useState<string>('')

  const classes = useStyles()

  const onFieldChange = (id: string, newValue: string) => {
    uploadedDoc[id] = newValue
    setUploadedDoc(uploadedDoc.clone())
    if (id === 'filename') {
      setUploadError('')
    }
  }

  const onFileInputChange = async (event: FormEvent<HTMLInputElement>) => {
    setLocalFilename(event.currentTarget.value)
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onUploadProgress = (progressEvent: ProgressEvent) => {
    const progress =
      Math.round((progressEvent.loaded * 100) / progressEvent.total) / 100
    setProgress(progress)
  }

  const handleSave = async () => {
    setUploadError('')
    if (fileInputRef.current.files.length === 0 && mode === 'new') {
      return
    }
    const file = fileInputRef.current.files[0]
    try {
      setIsProcessing(true)
      setProgress(0)
      setIsProgressDialogOpen(true)
      let response: AxiosResponse
      if (mode === 'new') {
        response = await uploadedDocumentDataAccess.addDocument(
          uploadedDoc,
          file,
          onUploadProgress
        )
      } else {
        response = await uploadedDocumentDataAccess.saveDocument(
          uploadedDoc,
          file,
          onUploadProgress
        )
      }
      if (response.status === StatusCodes.CONFLICT) {
        setUploadError('A file with that filename already exists')
      } else if (response.status === StatusCodes.REQUEST_TOO_LONG) {
        setUploadError('The size of the file exceeded the allowed max')
      }
    } catch (err) {
      setError(err)
      setUploadError('An error occurred while uploading.')
      setProgress(1.0)
    } finally {
      setIsProcessing(false)
    }
  }

  const clearError = () => {
    setError(undefined)
  }

  const preLoadForm = async () => {
    try {
      const { document, loginUser } =
        await uploadedDocumentDataAccess.getDocument(docId)
      const docForm = new UploadedDocument()
      docForm.filename = document.filename
      docForm.title = document.title
      docForm.id = docId
      setUploadedDoc(docForm)
      setLoginUser(loginUser)
    } catch (err) {
      setError(err)
    } finally {
      setIsLoading(false)
    }
  }

  const onLogin = () => {
    setError(undefined)
    verifyLogin().then()
  }

  const verifyLogin = async () => {
    try {
      const user = await userDataAccess.getUserLoggedIn()
      setLoginUser(user)
    } catch (err) {
      setError(err)
    } finally {
      setIsLoading(false)
    }
  }

  const isFormValid = (): boolean => {
    return (
      uploadedDoc.isValid() &&
      (fileInputRef.current.files.length > 0 || mode === 'edit')
    )
  }

  useEffect(() => {
    if (mode === 'edit') {
      preLoadForm().then()
    } else {
      verifyLogin().then()
    }
  }, [])

  const actionLabel = mode === 'edit' ? 'Save' : 'Add Document'
  const processingLabel = mode === 'edit' ? 'Saving...' : 'Adding Document...'
  const actionIcon = mode === 'edit' ? 'small-tick' : 'plus'

  function closeProgress() {
    setIsProgressDialogOpen(false)
    if (progress >= 1.0 && uploadError === '') {
      window.location.href = 'documents.html'
    } else {
      setProgress(0.0)
    }
  }

  return (
    <AdminPage
      assetsRoot={assetsRoot}
      error={error}
      clearError={clearError}
      onLogin={onLogin}
      loginUser={loginUser}
    >
      <CenterCardLayout breadcrumbs={breadcrumbs}>
        <Card elevation={1} className={classes.card}>
          <div className={`${classes.headerRow} card-header`}>
            <h1 className={classes.header}>{title}</h1>
          </div>
          <div className={classes.body}>
            <PageForm>
              <ValidationFormGroup
                id="filename"
                value={uploadedDoc.filename}
                validator={() => {
                  return uploadedDoc.filenameValidator()
                }}
                setter={onFieldChange}
                label="Web Filename"
                labelInfo="(required)"
                isLoading={isLoading}
                rightText=".pdf"
              />
              {uploadError && (
                <div className={classes.errorMessage}>{uploadError}</div>
              )}
              <ValidationFormGroup
                id="title"
                value={uploadedDoc.title}
                validator={() => {
                  return uploadedDoc.titleValidator()
                }}
                setter={onFieldChange}
                label="Title"
                labelInfo="(required)"
                isLoading={isLoading}
              />
              <FormGroup
                label="Local File to Upload"
                labelInfo="(must be of type PDF)"
                className={`${classes.formGroup} ${
                  isLoading ? Classes.SKELETON : ''
                }`}
              >
                <FileInput
                  text={localFilename}
                  buttonText="Browse"
                  fill={true}
                  onInputChange={onFileInputChange}
                  inputProps={{ ref: fileInputRef, accept: 'application/pdf' }}
                />
              </FormGroup>
            </PageForm>
            <div className={classes.buttonRow}>
              <Button
                className={classes.primaryButton}
                icon={actionIcon}
                text={isProcessing ? processingLabel : actionLabel}
                onClick={handleSave}
                disabled={!isFormValid() || isProcessing}
              />
              <AnchorButton
                icon="small-cross"
                text="Cancel"
                href="/church-admin/documents.html"
              />
            </div>
          </div>
        </Card>
      </CenterCardLayout>
      <DarkModeContext.Consumer>
        {(isDarkMode) => (
          <Dialog
            isOpen={isProgressDialogOpen}
            title="Uploading File"
            className={isDarkMode ? Classes.DARK : ''}
            isCloseButtonShown={false}
            canEscapeKeyClose={false}
            canOutsideClickClose={false}
          >
            <div className={Classes.DIALOG_BODY}>
              <ProgressBar
                value={progress}
                intent={uploadError ? 'danger' : 'primary'}
                stripes={false}
              />
              {progress < 1.0 ? (
                <p className={classes.dialogMessage}>
                  {mode === 'new' ? 'Uploading ' : 'Saving '}
                  {uploadedDoc.filename}.pdf....
                </p>
              ) : (
                <>
                  {uploadError !== '' ? (
                    <p className={classes.dialogMessageError}>{uploadError}</p>
                  ) : (
                    <p className={classes.dialogMessage}>
                      {uploadedDoc.filename}.pdf was successfully
                      {mode === 'new' ? ' uploaded' : ' saved'}.
                    </p>
                  )}
                </>
              )}
            </div>
            {progress >= 1.0 && (
              <div className={Classes.DIALOG_FOOTER}>
                <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                  <Button onClick={closeProgress}>OK</Button>
                </div>
              </div>
            )}
          </Dialog>
        )}
      </DarkModeContext.Consumer>
    </AdminPage>
  )
}

export function init(target: string, assetsRoot: string) {
  ReactDOM.render(
    <Document assetsRoot={assetsRoot} />,
    document.getElementById(target)
  )
}
