import * as React from 'react'
import LayoutProps from '../../layouts/layout-props'
import AdminPage from '../../components/church-admin/admin-page'
import CenterCardLayout from '../../components/church-admin/center-card-layout'
import { Button, NonIdealState } from '@blueprintjs/core'
import { useStyles } from './logged-out.styles'
import LoginDialog from '../../components/church-admin/login-dialog'
import * as ReactDOM from 'react-dom'

const LoggedOut: React.FC<LayoutProps> = ({ assetsRoot }) => {
  const [error, setError] = React.useState<Error>()
  const [showLogin, setShowLogin] = React.useState<boolean>(false)

  const classes = useStyles()

  const openLogin = () => {
    setShowLogin(true)
  }

  const action = (
    <Button icon="log-in" onClick={openLogin}>
      Log Back In
    </Button>
  )

  const onLogin = () => {
    window.location.href = 'index.html'
  }

  return (
    <AdminPage
      assetsRoot={assetsRoot}
      error={error}
      clearError={() => setError(undefined)}
      hideNavLinks={true}
    >
      <CenterCardLayout>
        <NonIdealState
          icon="log-out"
          title="Successfully Logged Out"
          description="You have successfully logged out."
          className={classes.loggedOut}
          action={action}
        />
        <LoginDialog isOpen={showLogin} onLogin={onLogin} />
      </CenterCardLayout>
    </AdminPage>
  )
}

export function init(target: string, assetsRoot: string) {
  ReactDOM.render(
    <LoggedOut assetsRoot={assetsRoot} />,
    document.getElementById(target)
  )
}
