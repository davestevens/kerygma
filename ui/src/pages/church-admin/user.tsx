import * as React from 'react'
import { FormEvent, useState } from 'react'
import {
  FormGroup,
  BreadcrumbProps,
  Button,
  Card,
  RadioGroup,
  Radio,
  AnchorButton,
  Classes,
} from '@blueprintjs/core'
import AdminPage from '../../components/church-admin/admin-page'
import PageForm from '../../components/church-admin/page-form'
import { UserForm } from '../../model/user-form'
import CenterCardLayout from '../../components/church-admin/center-card-layout'
import LayoutProps from '../../layouts/layout-props'
import ValidationFormGroup from '../../components/church-admin/validation-form-group'
import container from '../../di-container'
import UserDataAccess from '../../services/user-data-access'
import { StatusCodes } from 'http-status-codes'
import LocalStorageDataAccess from '../../services/local-storage-data-access'
import { User as UserModel } from '../../model/user'
import { AxiosResponse } from 'axios'
import { useStyles } from './user.styles'
import * as ReactDOM from 'react-dom'

const userId: string | null = new URLSearchParams(window.location.search).get(
  'userId'
)
const mode: 'edit' | 'new' = userId ? 'edit' : 'new'
const title = `${mode === 'new' ? 'Add' : 'Edit'} User`
const userDataAccess = container.resolve(UserDataAccess)
const localStorageDataAccess = container.resolve(LocalStorageDataAccess)

const breadcrumbs: BreadcrumbProps[] = [
  { href: '/church-admin/users.html', icon: 'people', text: 'Users' },
  { icon: mode === 'new' ? 'new-person' : 'edit', text: title },
]

const User: React.FC<LayoutProps> = ({ assetsRoot }) => {
  const [userForm, setUserForm] = React.useState<UserForm>(new UserForm())
  const [usernameError, setUsernameError] = React.useState<string>('')
  const [error, setError] = React.useState<Error>()
  const [isProcessing, setIsProcessing] = React.useState<boolean>(false)
  const [isLoading, setIsLoading] = React.useState<boolean>(true)
  const [loginUser, setLoginUser] = useState<UserModel>()

  const classes = useStyles()

  const handleAuthLevelChange = (event: FormEvent<HTMLInputElement>) => {
    if (event.currentTarget.value === 'sysadmin') {
      userForm.authLevel = 'sysadmin'
    } else {
      userForm.authLevel = 'admin'
    }
    setUserForm(userForm.clone())
  }

  const onFieldChange = (id: string, newValue: string) => {
    userForm[id] = newValue
    setUserForm(userForm.clone())
  }

  const handleSave = async () => {
    setUsernameError('')
    try {
      setIsProcessing(true)
      let response: AxiosResponse
      if (mode === 'new') {
        response = await userDataAccess.addUser(userForm)
      } else {
        response = await userDataAccess.saveUser(userForm)
      }
      if (response.status === StatusCodes.CONFLICT) {
        setUsernameError('Userid already exists')
      } else {
        localStorageDataAccess.setToast(
          `User ${userForm.firstName} ${userForm.lastName} (${
            userForm.username
          }) was successfully ${mode === 'edit' ? 'saved' : 'added'}.`
        )
        window.location.href = 'users.html'
      }
    } catch (err) {
      setError(err)
    } finally {
      setIsProcessing(false)
    }
  }

  const clearError = () => {
    setError(undefined)
  }

  const preLoadForm = async () => {
    try {
      const { user, loginUser } = await userDataAccess.getUser(userId)
      const userForm = new UserForm()
      userForm.mode = 'edit'
      userForm.firstName = user.firstName
      userForm.lastName = user.lastName
      userForm.id = userId
      userForm.username = user.username
      userForm.authLevel = user.authLevel
      setUserForm(userForm)
      setLoginUser(loginUser)
    } catch (err) {
      setError(err)
    } finally {
      setIsLoading(false)
    }
  }

  const verifyLogin = async () => {
    try {
      const user = await userDataAccess.getUserLoggedIn()
      setLoginUser(user)
    } catch (err) {
      setError(err)
    } finally {
      setIsLoading(false)
    }
  }

  React.useEffect(() => {
    if (mode === 'edit') {
      preLoadForm().then()
    } else {
      verifyLogin().then()
    }
  }, [])

  const actionLabel = mode === 'edit' ? 'Save' : 'Add User'
  const processingLabel = mode === 'edit' ? 'Saving...' : 'Adding User...'
  const actionIcon = mode === 'edit' ? 'small-tick' : 'plus'

  return (
    <AdminPage
      assetsRoot={assetsRoot}
      error={error}
      clearError={clearError}
      loginUser={loginUser}
    >
      <CenterCardLayout breadcrumbs={breadcrumbs}>
        <Card elevation={1} className={classes.card}>
          <div className={`${classes.headerRow} card-header`}>
            <h1 className={classes.header}>{title}</h1>
          </div>
          <div className={classes.body}>
            <PageForm>
              <ValidationFormGroup
                id="firstName"
                value={userForm.firstName}
                validator={() => {
                  return userForm.firstNameValidator()
                }}
                setter={onFieldChange}
                label="First Name"
                labelInfo="(required)"
                isLoading={isLoading}
              />
              <ValidationFormGroup
                id="lastName"
                value={userForm.lastName}
                validator={() => {
                  return userForm.lastNameValidator()
                }}
                setter={onFieldChange}
                label="Last Name"
                labelInfo="(required)"
                isLoading={isLoading}
              />
              <ValidationFormGroup
                id="username"
                value={userForm.username}
                validator={() => {
                  return userForm.usernameValidator()
                }}
                setter={onFieldChange}
                label="Username"
                labelInfo="(required)"
                helperText="A valid email address."
                isLoading={isLoading}
              />
              {usernameError && (
                <div className={classes.errorMessage}>{usernameError}</div>
              )}
              <ValidationFormGroup
                id="password"
                value={userForm.password}
                validator={() => {
                  return userForm.passwordValidator()
                }}
                setter={onFieldChange}
                label="Password"
                labelInfo={mode === 'new' ? '(required)' : null}
                helperText="Must be at least 8 characters and contain uppercase, lowercase and numeric characters."
                inputType="password"
                isLoading={isLoading}
              />
              <ValidationFormGroup
                id="passwordConfirm"
                value={userForm.passwordConfirm}
                validator={() => {
                  return userForm.passwordConfirmValidator()
                }}
                setter={onFieldChange}
                label="Confirm Password"
                labelInfo={mode === 'new' ? '(required)' : null}
                inputType="password"
                isLoading={isLoading}
              />
              <FormGroup label="Authorization Level">
                <RadioGroup
                  onChange={handleAuthLevelChange}
                  inline={true}
                  selectedValue={userForm.authLevel}
                  name="auth-level"
                  className={isLoading ? Classes.SKELETON : ''}
                >
                  <Radio label="Admin" value="admin" />
                  <Radio label="System Admin" value="sysadmin" />
                </RadioGroup>
              </FormGroup>
              <div className={classes.buttonRow}>
                <Button
                  icon={actionIcon}
                  text={isProcessing ? processingLabel : actionLabel}
                  className={classes.buttonSpaced}
                  onClick={handleSave}
                  disabled={!userForm.isValid() || isProcessing}
                />
                <AnchorButton
                  icon="small-cross"
                  text="Cancel"
                  href="/church-admin/users.html"
                />
              </div>
            </PageForm>
          </div>
        </Card>
      </CenterCardLayout>
    </AdminPage>
  )
}

export function init(target: string, assetsRoot: string) {
  ReactDOM.render(
    <User assetsRoot={assetsRoot} />,
    document.getElementById(target)
  )
}
