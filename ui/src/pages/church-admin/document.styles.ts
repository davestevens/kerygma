import { Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  card: `
    padding: 0;
  `,
  headerRow: `
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 10px 16px;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
  `,
  header: `
    text-align: left;
    padding: 0;
    width: auto;
  `,
  body: `
    padding: 16px;
  `,
  errorMessage: `
    font-size: 12px;
    color: ${Colors.RED3};
    margin: -8px 0 8px 0;
  `,
  formGroup: `
    width: 100%;
  `,
  buttonRow: `
    display: flex;
  `,
  primaryButton: `
    margin-right: 16px;
  `,
  dialogMessage: `
    margin-top: 16px;
  `,
  dialogMessageError: `
    margin-top: 16px;
    color: ${Colors.RED3};
  `,
})
