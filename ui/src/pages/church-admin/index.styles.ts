import { Colors } from '@blueprintjs/core'
import { globalConstants } from '../../common/global-constants'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  cardContainer: {
    display: 'flex',
    width: '100%',
    maxWidth: '800px',
    padding: '0 8px',
    [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
      flexDirection: 'column',
    },
  },
  header: `
    margin: 40px 8px 20px;
  `,
  intro: `
    flex-direction: column;
  `,
  introParagraph: `
    font-size: 17px;
    margin: 0 8px 40px;
  `,
  card: {
    flex: 1,
    margin: '0 8px',
    textAlign: 'center',
    [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
      marginBottom: '16px',
    },
  },
  cardHeader: `
    margin: 0 0 8px;
  `,
  iconRow: `
    display: flex;
    justify-content: center;
    margin: 20px 0 30px;
  `,
  icon: `
    color: ${Colors.GRAY3};
  `,
  contentRow: `
    display: flex;
    justify-content: center;
    width: 100%;
  `,
})
