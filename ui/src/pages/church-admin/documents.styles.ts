import { Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  card: `
    padding: 0;
  `,
  headerRow: `
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 10px 16px;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
  `,
  header: `
    text-align: left;
    padding: 0;
    width: auto;
  `,
  empty: {
    margin: '40px 0',
    '& h4': `
      margin-bottom: 20px;
    `,
  },
  body: `
    padding: 16px;
  `,
  docRow: {
    display: 'flex',
    alignItems: 'flex-start',
    marginBottom: '16px',
    '&:last-child': `
      margin-bottom: 0;
    `,
  },
  avatar: `
    background-color: ${Colors.GRAY3};
    padding: 6px;
    border-radius: 2px;
    margin: 4px 16px 0 0;
  `,
  metaCol: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginRight: 'auto',
    '& a': `
      display: flex;
      align-items: center;
    `,
  },
  linkIcon: `
    margin-left: 4px;
    padding-bottom: 1px;
  `,
  metaLine: `
    display: flex;
    font-size: 12px;
    margin-top: 2px;
  `,
  more: `
    margin: 4px 0 0 8px;
  `,
})
