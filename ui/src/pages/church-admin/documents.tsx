import {
  AnchorButton,
  Button,
  Card,
  Classes,
  Icon,
  Menu,
  MenuItem,
  NonIdealState,
  Popover,
  Toaster,
} from '@blueprintjs/core'
import React, { useEffect, useState } from 'react'
import LayoutProps from '../../layouts/layout-props'
import AdminPage from '../../components/church-admin/admin-page'
import CenterCardLayout from '../../components/church-admin/center-card-layout'
import container from '../../di-container'
import LocalStorageDataAccess from '../../services/local-storage-data-access'
import UploadedDocumentDataAccess from '../../services/uploaded-document-data-access'
import { User } from '../../model/user'
import { UploadedDocument } from '../../model/uploaded-document'
import { formatBytes } from 'bytes-formatter'
import ConfirmDialog from '../../components/church-admin/confirm-dialog'
import { useStyles } from './documents.styles'
import * as ReactDOM from 'react-dom'
import { KerygmaConfiguration } from '../../model/kerygma-configuration'

const toaster = Toaster.create({ position: 'top' })
const localStorageDataAccess = container.resolve(LocalStorageDataAccess)

const uploadedDocumentDataAccess = container.resolve(UploadedDocumentDataAccess)
const config = container.resolve(KerygmaConfiguration)
const { apiHost, mediaContextRoot } = config.api
const documentUri = `${apiHost}${mediaContextRoot}/documents/`

const Documents: React.FC<LayoutProps> = ({ assetsRoot }) => {
  const [error, setError] = React.useState<Error>()
  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [loginUser, setLoginUser] = useState<User>()
  const [documents, setDocuments] = useState<UploadedDocument[]>([])
  const [isDeleteConfirmOpen, setIsDeleteConfirmOpen] = useState<boolean>(false)
  const [docToDelete, setDocToDelete] = React.useState<UploadedDocument>(null)
  const [isProcessing, setIsProcessing] = useState<boolean>(false)

  const classes = useStyles()

  const onLogin = () => {
    setError(undefined)
    loadDocuments().then()
  }

  const loadDocuments = async (): Promise<void> => {
    try {
      const response = await uploadedDocumentDataAccess.getDocuments()
      setDocuments(response.documents)
      setLoginUser(response.loginUser)
    } catch (err) {
      setError(err)
    } finally {
      setIsLoading(false)
    }
  }

  const onEditDoc = (doc: UploadedDocument) => {
    window.location.href = `document.html?docId=${doc.id}`
  }

  const onConfirmDeleteDoc = (doc: UploadedDocument) => {
    setDocToDelete(doc)
    setIsDeleteConfirmOpen(true)
  }

  const onDeleteDoc = async () => {
    setIsProcessing(true)
    try {
      await uploadedDocumentDataAccess.deleteDocument(docToDelete.id)
      toaster.show({
        message: `Document "${
          docToDelete.title
        }" (${docToDelete.getFilenameWithSuffix()}) was successfully deleted.`,
        intent: 'primary',
        icon: 'small-tick',
      })
      await loadDocuments()
    } catch (err) {
      setError(err)
    } finally {
      setIsDeleteConfirmOpen(false)
      setIsProcessing(false)
      setDocToDelete(null)
    }
  }

  const onCancelDelete = () => {
    setIsDeleteConfirmOpen(false)
    setDocToDelete(null)
  }

  useEffect(() => {
    loadDocuments().then()
    const message = localStorageDataAccess.getToast()
    if (message) {
      toaster.show({
        message: message,
        intent: 'primary',
        icon: 'small-tick',
      })
    }
  }, [])

  return (
    <AdminPage
      assetsRoot={assetsRoot}
      error={error}
      clearError={() => setError(undefined)}
      onLogin={onLogin}
      loginUser={loginUser}
    >
      <CenterCardLayout>
        <Card elevation={1} className={classes.card}>
          <div className={`${classes.headerRow} card-header`}>
            <h1 className={classes.header}>Documents</h1>
            <AnchorButton
              icon="document-open"
              text="Add Document"
              href="document.html"
            />
          </div>
          <div className={classes.body}>
            {isLoading === true ? (
              <div className={classes.docRow}>
                <div className={`${classes.avatar} ${Classes.SKELETON}`}>
                  <Icon icon="document" color="white" />
                </div>
                <div className={classes.metaCol}>
                  <div className={Classes.SKELETON}>Sample Document Title</div>
                  <div className={`${classes.metaLine} ${Classes.SKELETON}`}>
                    ID: sample-document-id
                  </div>
                  <div className={`${classes.metaLine} ${Classes.SKELETON}`}>
                    filename: sample-document-id.pdf (5MB)
                  </div>
                  <div className={`${classes.metaLine} ${Classes.SKELETON}`}>
                    created: Apr 25, 2022 8:34 PM
                  </div>
                </div>
              </div>
            ) : (
              <>
                {documents.length === 0 ? (
                  <div>
                    <NonIdealState
                      icon="folder-open"
                      title="No Documents"
                      description="No documents have been added yet."
                      className={classes.empty}
                    />
                  </div>
                ) : (
                  <>
                    {documents.map((doc) => {
                      const docMenu = (
                        <Menu>
                          <MenuItem
                            icon="edit"
                            text="Edit"
                            onClick={() => onEditDoc(doc)}
                          />
                          <MenuItem
                            icon="remove"
                            text="Delete"
                            onClick={() => onConfirmDeleteDoc(doc)}
                          />
                        </Menu>
                      )
                      return (
                        <div className={classes.docRow} key={doc.id}>
                          <div className={classes.avatar}>
                            <Icon icon="document" color="white" />
                          </div>
                          <div className={classes.metaCol}>
                            <a
                              href={`${documentUri}${doc.getFilenameWithSuffix()}`}
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              {doc.title}
                              <Icon
                                className={classes.linkIcon}
                                icon="share"
                                size={12}
                              />
                            </a>
                            <div
                              className={`${classes.metaLine} ${Classes.TEXT_MUTED}`}
                            >
                              ID: {doc.filename}
                            </div>
                            <div
                              className={`${classes.metaLine} ${Classes.TEXT_MUTED}`}
                            >
                              {doc.getFilenameWithSuffix()} (
                              {formatBytes(doc.size)})
                            </div>
                            <div
                              className={`${classes.metaLine} ${Classes.TEXT_MUTED}`}
                            >
                              created:{' '}
                              {doc.uploadedTimestamp.format(
                                'MMM D, YYYY h:mm A'
                              )}
                            </div>
                          </div>
                          <Popover placement="bottom-end" content={docMenu}>
                            <Button icon="more" className={classes.more} />
                          </Popover>
                        </div>
                      )
                    })}
                  </>
                )}
              </>
            )}
          </div>
        </Card>
      </CenterCardLayout>
      <ConfirmDialog
        isOpen={isDeleteConfirmOpen}
        isProcessing={isProcessing}
        title="Confirm Delete Document"
        message={`Are you sure you want to delete the document "${
          docToDelete?.title
        }" (${docToDelete?.getFilenameWithSuffix()})?`}
        action="Delete Document"
        processingAction="Deleting Document..."
        onConfirm={onDeleteDoc}
        onCancel={onCancelDelete}
      />
    </AdminPage>
  )
}

export function init(target: string, assetsRoot: string) {
  ReactDOM.render(
    <Documents assetsRoot={assetsRoot} />,
    document.getElementById(target)
  )
}
