import React, { ChangeEvent, RefObject, useEffect, useState } from 'react'
import LayoutProps from '../../layouts/layout-props'
import AdminPage from '../../components/church-admin/admin-page'
import { User } from '../../model/user'
import { useStyles } from './sermons.styles'
import {
  AnchorButton,
  Button,
  Card,
  Classes,
  Icon,
  InputGroup,
  Menu,
  MenuItem,
  NonIdealState,
  Popover,
  Slider,
  Tag,
  Toaster,
} from '@blueprintjs/core'
import container from '../../di-container'
import SermonDataAccess from '../../services/sermon-data-access'
import { Sermon } from '../../model/sermon'
import bibleBookData from '../../model/bible-book-data.yaml'
import { BibleBook } from '../../model/bible-book'
import * as ReactDOM from 'react-dom'
import ConfirmDialog from '../../components/church-admin/confirm-dialog'
import ListPager, { perPageOptions } from '../../components/list-pager'
import UserDataAccess from '../../services/user-data-access'
import { KerygmaConfiguration } from '../../model/kerygma-configuration'
import { SermonTag } from '../../model/sermon-tag'
import TagDialog from '../../components/church-admin/tag-dialog'

const toaster = Toaster.create({ position: 'top' })
const bibleBooks = bibleBookData.books as BibleBook[]

const sermonDataAccess = container.resolve(SermonDataAccess)
const userDataAccess = container.resolve(UserDataAccess)
const config = container.resolve(KerygmaConfiguration)
const { apiHost, mediaContextRoot } = config.api
const sermonsUri = `${apiHost}${mediaContextRoot}/sermons/`

const Sermons: React.FC<LayoutProps> = ({ assetsRoot }) => {
  const [error, setError] = React.useState<Error>()
  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [loginUser, setLoginUser] = useState<User>()
  const [sermons, setSermons] = useState<Sermon[]>([])
  const [audioRefs, setAudioRefs] = React.useState<
    RefObject<HTMLMediaElement>[]
  >([])
  const [isActive, setIsActive] = React.useState<boolean[]>([])
  const [intervalId, setIntervalId] = React.useState<number>(0)
  const [currentTime, setCurrentTime] = useState(0)
  const [duration, setDuration] = useState(0)
  const [playingIndex, setPlayingIndex] = useState<number>(null)
  const [isDeleteConfirmOpen, setIsDeleteConfirmOpen] = useState<boolean>(false)
  const [sermonToDelete, setSermonToDelete] = React.useState<Sermon>(null)
  const [isProcessing, setIsProcessing] = useState<boolean>(false)
  const [page, setPage] = React.useState<number>(0)
  const [perPage, setPerPage] = React.useState<number>(perPageOptions[0])
  const [sortOrder, setSortOrder] = React.useState<'latest' | 'oldest'>(
    'latest'
  )
  const [filter, setFilter] = React.useState<string | undefined>(undefined)
  const [tagFilter, setTagFilter] = React.useState<string[]>([])
  const [totalSermons, setTotalSermons] = React.useState<number>(0)
  const [tagDialogOpen, setTagDialogOpen] = React.useState<boolean>(false)
  const [tags, setTags] = React.useState<SermonTag[] | undefined>(undefined)
  const [tagsTemp, setTagsTemp] = React.useState<SermonTag[]>([])

  const classes = useStyles()

  const onLogin = () => {
    setError(undefined)
    loadUser().then()
    loadSermons().then()
  }

  // We need this call because the getSermons call does not return the user
  // since it is also used by the public UI.
  const loadUser = async (): Promise<void> => {
    try {
      const loginUser = await userDataAccess.getUserLoggedIn()
      setLoginUser(loginUser)
    } catch (err) {
      setError(err)
    }
  }

  const loadSermons = async (): Promise<void> => {
    setIsLoading(true)
    try {
      const response = await sermonDataAccess.debouncedGetSermons(
        page,
        perPage,
        sortOrder,
        filter,
        tagFilter
      )
      setSermons(response.sermons)
      setTotalSermons(response.totalRowCount)
      const sermonRefs = response.sermons.map(() => {
        return React.createRef<HTMLMediaElement>()
      })
      const playingStates = response.sermons.map(() => false)
      setAudioRefs(sermonRefs)
      setIsActive(playingStates)
    } catch (err) {
      setError(err)
    } finally {
      setIsLoading(false)
    }
  }

  const loadTags = async (): Promise<void> => {
    const availableTags = await sermonDataAccess.getSermonTags('sermonCount')
    setTags(availableTags)
  }

  const onConfirmDeleteSermon = (sermon: Sermon) => {
    setSermonToDelete(sermon)
    setIsDeleteConfirmOpen(true)
  }

  const onDeleteSermon = async () => {
    setIsProcessing(true)
    try {
      await sermonDataAccess.deleteSermon(sermonToDelete.id)
      toaster.show({
        message: `Sermon "${sermonToDelete.title}" (${sermonToDelete.filename}) was successfully deleted.`,
        intent: 'primary',
        icon: 'small-tick',
      })
      await loadSermons()
    } catch (err) {
      setError(err)
    } finally {
      setIsDeleteConfirmOpen(false)
      setIsProcessing(false)
      setSermonToDelete(null)
    }
  }

  const onCancelDelete = () => {
    setIsDeleteConfirmOpen(false)
    setSermonToDelete(null)
  }

  useEffect(() => {
    if (!loginUser) {
      loadUser().then()
    }
    if (!tags) {
      loadTags().then()
    }
    loadSermons().then()
  }, [sortOrder, page, perPage, filter, tagFilter])

  const onTogglePlay = (index: number) => {
    const playingStates = [...isActive]
    setPlayingIndex(index)
    for (let i = 0; i < audioRefs.length; i++) {
      const audioRef = audioRefs[i]?.current
      if (i === index) {
        if (audioRef.paused) {
          if (currentTime === audioRef.duration) {
            audioRef.currentTime = 0
          }
          audioRef.play().then()
          playingStates[i] = true
          setDuration(audioRef.duration)
          setCurrentTime(audioRef.currentTime)
          clearInterval(intervalId)
          const newIntervalId = window.setInterval(() => {
            setCurrentTime(audioRef.currentTime)
            if (audioRef.paused) {
              const intervalPlayingStates = [...isActive]
              intervalPlayingStates[i] = false
              setIsActive(intervalPlayingStates)
              clearInterval(intervalId)
            }
          }, 1000)
          setIntervalId(newIntervalId)
        } else {
          audioRef.pause()
          playingStates[i] = false
          clearInterval(intervalId)
        }
      } else {
        if (!audioRef.paused) {
          audioRef.pause()
          playingStates[i] = false
        }
      }
    }
    setIsActive(playingStates)
  }

  const onSliderChange = (value: number) => {
    const audioRef = audioRefs[playingIndex]?.current
    if (isActive[playingIndex] && !audioRef.paused) {
      onTogglePlay(playingIndex)
    }
    setCurrentTime(value)
  }

  const onSliderRelease = (value: number) => {
    setCurrentTime(value)
    const audioRef = audioRefs[playingIndex]?.current
    audioRef.currentTime = value
    if (value < duration) {
      onTogglePlay(playingIndex)
    }
  }

  const onSearch = (elem: ChangeEvent<HTMLInputElement>) => {
    const newFilter = elem.target.value ?? ''
    const trimmed = newFilter.trim()
    if (trimmed.length < 3) {
      setFilter(undefined)
      setPage(0)
    } else {
      setFilter(trimmed)
      setPage(0)
    }
  }

  const onPagerChanged = (page: number, perPage: number) => {
    setPage(page)
    setPerPage(perPage)
  }

  const formatTime = (index: number, totalSeconds: number): string => {
    if (index !== playingIndex) {
      return ''
    }
    const hrs = Math.floor(totalSeconds / 3600)
    const hrsPrefix = hrs > 0 ? `${hrs}:` : ''
    totalSeconds = totalSeconds - hrs * 3600
    const min = Math.floor(totalSeconds / 60)
    const minPadded = String(min).padStart(2, '0')
    const sec = Math.floor(totalSeconds - min * 60)
    const secPadded = String(sec).padStart(2, '0')
    return `${hrsPrefix}${minPadded}:${secPadded}`
  }

  const onEditSermon = (sermon: Sermon) => {
    window.location.href = `sermon.html?sermonId=${sermon.id}`
  }

  const onOpenTagDialog = async () => {
    setTagsTemp(tags.map((tag) => tag.clone()))
    setTagDialogOpen(true)
  }

  const onApplyTagChanges = () => {
    setTags(tagsTemp.map((tag) => tag.clone()))
    setTagDialogOpen(false)
    const newFilterTags = tagsTemp
      .filter((tag) => tag.isSelected)
      .map((tag) => tag.tag)
    setTagFilter(newFilterTags)
  }

  const clearTagFilter = () => {
    setTagFilter([])
    for (const tag of tags) {
      tag.isSelected = false
    }
  }

  const onToggleTag = (i: number) => {
    const tagsClone = tagsTemp.map((tag) => tag.clone())
    tagsClone[i].isSelected = !tagsClone[i].isSelected
    setTagsTemp(tagsClone)
  }

  const sortMenu = (
    <Menu>
      <MenuItem
        icon="sort-asc"
        text="Sort latest"
        active={sortOrder === 'latest'}
        onClick={() => setSortOrder('latest')}
      />
      <MenuItem
        icon="sort-desc"
        text="Sort oldest"
        active={sortOrder === 'oldest'}
        onClick={() => setSortOrder('oldest')}
      />
    </Menu>
  )

  return (
    <AdminPage
      assetsRoot={assetsRoot}
      error={error}
      clearError={() => setError(undefined)}
      onLogin={onLogin}
      loginUser={loginUser}
    >
      <div className={classes.centerContainer}>
        <Card elevation={1} className={classes.card}>
          <div className={`${classes.headerRow} card-header`}>
            <h1 className={classes.header}>Sermons</h1>
            <AnchorButton icon="plus" text="Add Sermon" href="sermon.html" />
          </div>
          <div className={`${classes.searchRow} card-header`}>
            <InputGroup
              placeholder="Search sermons on title, speaker, year or passage"
              type="search"
              leftIcon="search"
              className={classes.searchInput}
              onChange={onSearch}
            ></InputGroup>
            {(!tags || tags.length > 0) && (
              <Button
                icon="tag"
                className={classes.sort}
                onClick={onOpenTagDialog}
              />
            )}
            <Popover placement="bottom-end" content={sortMenu}>
              <Button icon="sort" className={classes.sort} />
            </Popover>
          </div>
          {tagFilter.length > 0 && (
            <div className={`${classes.tagFilterRow} card-header`}>
              <Icon icon="tag" size={12} />
              {tagFilter.map((tag, i) => (
                <Tag key={i} minimal round className={classes.tag}>
                  {tag}
                </Tag>
              ))}
              <Button
                icon="cross"
                minimal
                className={classes.clearTags}
                onClick={clearTagFilter}
              />
            </div>
          )}
          <div className={classes.body}>
            {isLoading === true ? (
              <>
                {Array(3)
                  .fill(0)
                  .map((_val, index) => (
                    <div className={classes.docRow} key={index}>
                      <div className={`${classes.avatar} ${Classes.SKELETON}`}>
                        <Icon icon="document" color="white" />
                      </div>
                      <div className={classes.metaCol}>
                        <a href="#" className={Classes.SKELETON}>
                          Skeleton Sermon Title Goes Here
                        </a>
                        <div
                          className={`${classes.metaLine} ${Classes.SKELETON}`}
                        >
                          Speaker: speaker name here
                        </div>
                        <div
                          className={`${classes.metaLine} ${Classes.SKELETON}`}
                        >
                          service: Morning Worship
                        </div>
                        <div
                          className={`${classes.metaLine} ${Classes.SKELETON}`}
                        >
                          passage: Romans 8:1-8:2
                        </div>
                        <div className={classes.playLine}>
                          <Button
                            icon="play"
                            minimal={true}
                            className={`${classes.playButton} ${Classes.SKELETON}`}
                          />
                          <span
                            className={`${classes.sermonDate} ${Classes.SKELETON}`}
                          >
                            Mon Jul 4, 2022
                          </span>
                        </div>
                      </div>
                      <Button
                        icon="more"
                        className={`${classes.more} ${Classes.SKELETON}`}
                      />
                    </div>
                  ))}
              </>
            ) : (
              <>
                {sermons.length === 0 ? (
                  <div>
                    <NonIdealState
                      icon="folder-open"
                      title="No Sermons"
                      description="No sermons have been added yet."
                      className={classes.empty}
                    />
                  </div>
                ) : (
                  <>
                    {sermons.map((sermon, i) => {
                      const docMenu = (
                        <Menu>
                          <MenuItem
                            icon="edit"
                            text="Edit"
                            onClick={() => onEditSermon(sermon)}
                          />
                          <MenuItem
                            icon="remove"
                            text="Delete"
                            onClick={() => onConfirmDeleteSermon(sermon)}
                          />
                        </Menu>
                      )
                      const passage = sermon.getPassage(bibleBooks)
                      return (
                        <div className={classes.docRow} key={sermon.id}>
                          <div className={classes.avatar}>
                            <Icon icon="headset" color="white" />
                          </div>
                          <div className={classes.metaCol}>
                            <a
                              href={`${sermonsUri}${sermon.filename}`}
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              {sermon.title}
                              <Icon
                                className={classes.linkIcon}
                                icon="share"
                                size={12}
                              />
                            </a>
                            <div
                              className={`${classes.metaLine} ${Classes.TEXT_MUTED}`}
                            >
                              Speaker: {sermon.speakerTitle}{' '}
                              {sermon.speakerFirstName} {sermon.speakerLastName}
                            </div>
                            <div
                              className={`${classes.metaLine} ${Classes.TEXT_MUTED}`}
                            >
                              service: {sermon.service}
                            </div>
                            {passage && (
                              <div
                                className={`${classes.metaLine} ${Classes.TEXT_MUTED}`}
                              >
                                passage: {passage}
                              </div>
                            )}
                            {sermon.tags && sermon.tags.length > 0 && (
                              <div
                                className={`${classes.tagLine} ${Classes.TEXT_MUTED}`}
                              >
                                <Icon icon="tag" size={12} />
                                {sermon.tags.map((tag, i) => (
                                  <Tag
                                    key={i}
                                    minimal
                                    round
                                    className={classes.tag}
                                  >
                                    {tag}
                                  </Tag>
                                ))}
                              </div>
                            )}
                            <div
                              className={`${classes.playLine} ${Classes.TEXT_MUTED}`}
                            >
                              {!isActive[i] ? (
                                <Button
                                  icon="play"
                                  minimal={true}
                                  className={classes.playButton}
                                  onClick={() => onTogglePlay(i)}
                                />
                              ) : (
                                <Button
                                  icon="pause"
                                  minimal={true}
                                  className={classes.playButton}
                                  onClick={() => onTogglePlay(i)}
                                />
                              )}
                              <span className={classes.sermonDate}>
                                {sermon
                                  .getSermonDate()
                                  .format('ddd MMM D, YYYY')}
                              </span>
                              {i === playingIndex && (
                                <>
                                  <span className={classes.time}>
                                    {formatTime(i, currentTime)}
                                  </span>
                                  <Slider
                                    min={0}
                                    max={duration}
                                    labelStepSize={duration}
                                    value={currentTime}
                                    intent="primary"
                                    onChange={onSliderChange}
                                    onRelease={onSliderRelease}
                                    labelRenderer={false}
                                  />
                                  <span className={classes.time}>
                                    {formatTime(i, duration)}
                                  </span>
                                </>
                              )}
                              <audio
                                ref={audioRefs[i]}
                                src={`${sermonsUri}${sermon.filename}`}
                              />
                            </div>
                          </div>
                          <Popover placement="bottom-end" content={docMenu}>
                            <Button icon="more" className={classes.more} />
                          </Popover>
                        </div>
                      )
                    })}
                    <ListPager
                      page={page}
                      perPage={perPage}
                      totalRecords={totalSermons}
                      onChange={onPagerChanged}
                    />
                  </>
                )}
              </>
            )}
          </div>
        </Card>
      </div>
      <ConfirmDialog
        isOpen={isDeleteConfirmOpen}
        isProcessing={isProcessing}
        title="Confirm Delete Sermon"
        message={`Are you sure you want to delete the sermon "${sermonToDelete?.title}" (${sermonToDelete?.filename})?`}
        action="Delete Sermon"
        processingAction="Deleting Sermon..."
        onConfirm={onDeleteSermon}
        onCancel={onCancelDelete}
      />
      <TagDialog
        tags={tagsTemp}
        isOpen={tagDialogOpen}
        onCancel={() => setTagDialogOpen(false)}
        onApply={onApplyTagChanges}
        onToggleTag={onToggleTag}
      />
    </AdminPage>
  )
}

export function init(target: string, assetsRoot: string) {
  ReactDOM.render(
    <Sermons assetsRoot={assetsRoot} />,
    document.getElementById(target)
  )
}
