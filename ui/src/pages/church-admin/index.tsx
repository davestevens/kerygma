import React, { useEffect, useState } from 'react'
import AdminPage from '../../components/church-admin/admin-page'
import LayoutProps from '../../layouts/layout-props'
import UserDataAccess from '../../services/user-data-access'
import container from '../../di-container'
import { User } from '../../model/user'
import { Card, Classes, Elevation, Icon } from '@blueprintjs/core'
import { useStyles } from './index.styles'
import * as ReactDOM from 'react-dom'

const userDataAccess = container.resolve(UserDataAccess)

const ChurchAdminHome: React.FC<LayoutProps> = ({ assetsRoot }) => {
  const [loginUser, setLoginUser] = useState<User>()
  const [error, setError] = React.useState<Error>()

  const classes = useStyles()

  const loadUser = async (): Promise<void> => {
    try {
      const loginUser = await userDataAccess.getUserLoggedIn()
      setLoginUser(loginUser)
    } catch (err) {
      setError(err)
    }
  }

  const clearError = () => {
    setError(undefined)
  }

  const onLogin = async () => {
    setError(undefined)
    await loadUser()
  }

  useEffect(() => {
    loadUser().then()
  }, [])

  return (
    <AdminPage
      assetsRoot={assetsRoot}
      error={error}
      clearError={clearError}
      onLogin={onLogin}
      loginUser={loginUser}
    >
      <div className={classes.contentRow}>
        <div className={`${classes.cardContainer} ${classes.intro}`}>
          <h1 className={classes.header}>Church Admin</h1>
          <p className={`${classes.introParagraph} ${Classes.RUNNING_TEXT}`}>
            Welcome to the Kerygma church admin landing page. From here you can
            manage your church's sermons and documents, as well as users if you
            are a system admin.
          </p>
        </div>
      </div>
      <div className={classes.contentRow}>
        <div className={classes.cardContainer}>
          <Card
            interactive={true}
            elevation={Elevation.ONE}
            className={classes.card}
            onClick={() => {
              window.location.href = 'sermons.html'
            }}
          >
            <div className={classes.iconRow}>
              <Icon className={classes.icon} size={40} icon="headset" />
            </div>
            <h3 className={classes.cardHeader}>Manage Sermons</h3>
            <p className={Classes.RUNNING_TEXT}>
              Upload, categorize and manage your church sermons.
            </p>
          </Card>
          <Card
            interactive={true}
            elevation={Elevation.ONE}
            className={classes.card}
            onClick={() => {
              window.location.href = 'documents.html'
            }}
          >
            <div className={classes.iconRow}>
              <Icon className={classes.icon} size={40} icon="document" />
            </div>
            <h3>Manage Documents</h3>
            <p className={Classes.RUNNING_TEXT}>
              Upload PDF documents that you want to share on your web site.
            </p>
          </Card>
          {loginUser && loginUser.authLevel === 'sysadmin' && (
            <Card
              interactive={true}
              elevation={Elevation.ONE}
              className={classes.card}
              onClick={() => {
                window.location.href = 'users.html'
              }}
            >
              <div className={classes.iconRow}>
                <Icon className={classes.icon} size={40} icon="people" />
              </div>
              <h3>Manage Users</h3>
              <p className={Classes.RUNNING_TEXT}>
                For system admins to grant others access to these admin screens.
              </p>
            </Card>
          )}
        </div>
      </div>
    </AdminPage>
  )
}

export function init(target: string, assetsRoot: string) {
  ReactDOM.render(
    <ChurchAdminHome assetsRoot={assetsRoot} />,
    document.getElementById(target)
  )
}
