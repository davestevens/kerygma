import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  loggedOut: `
    margin-top: 40px;
  `,
})
