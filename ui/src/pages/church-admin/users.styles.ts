import { Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  card: `
    padding: 0;
  `,
  headerRow: `
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 10px 16px;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
  `,
  header: `
    text-align: left;
    padding: 0;
    width: auto;
  `,
  body: `
    padding: 16px;
  `,
  userRow: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '16px',
    '&:last-child': `
      margin-bottom: 0;
    `,
  },
  avatar: `
    background-color: ${Colors.GRAY3};
    padding: 6px;
    border-radius: 2px;
    margin-right: 16px;
  `,
  nameCol: `
    display: flex;
    flex-direction: column;
    align-items: flex-start;
  `,
  email: `
    font-size: 12px;
    margin-top: 2px;
  `,
  tag: `
    margin-left: auto;
  `,
  more: `
    margin-left: 8px;
  `,
  subText: `
    font-size: 12px;
  `,
})
