import { Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  card: `
    padding: 0;
  `,
  headerRow: `
    padding: 10px 16px;
    border-bottom: 1px solid ${Colors.LIGHT_GRAY1};
    background-color: ${Colors.LIGHT_GRAY4};
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
  `,
  header: `
    text-align: left;
    padding: 0;
  `,
  body: `
    padding: 16px;
  `,
  errorMessage: `
    font-size: 12px;
    color: ${Colors.RED3};
    margin: -8px 0 8px 0;
  `,
  buttonRow: `
    display: flex;
  `,
  buttonSpaced: `
    margin-right: 16px;
  `,
})
