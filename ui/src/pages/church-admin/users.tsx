import React, { useEffect, useState } from 'react'
import AdminPage from '../../components/church-admin/admin-page'
import CenterCardLayout from '../../components/church-admin/center-card-layout'
import {
  AnchorButton,
  Button,
  Card,
  Classes,
  Icon,
  Menu,
  MenuItem,
  Popover,
  Tag,
  Toaster,
} from '@blueprintjs/core'
import LayoutProps from '../../layouts/layout-props'
import UserDataAccess from '../../services/user-data-access'
import container from '../../di-container'
import LocalStorageDataAccess from '../../services/local-storage-data-access'
import ConfirmDialog from '../../components/church-admin/confirm-dialog'
import { User } from '../../model/user'
import * as ReactDOM from 'react-dom'
import { useStyles } from './users.styles'

const toaster = Toaster.create({ position: 'top' })
const userDataAccess = container.resolve(UserDataAccess)
const localStorageDataAccess = container.resolve(LocalStorageDataAccess)

const Users: React.FC<LayoutProps> = ({ assetsRoot }) => {
  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [users, setUsers] = useState<User[]>([])
  const [loginUser, setLoginUser] = useState<User>()
  const [error, setError] = React.useState<Error>()
  const [isDeleteConfirmOpen, setIsDeleteConfirmOpen] = useState<boolean>(false)
  const [userToDelete, setUserToDelete] = React.useState<User>(null)
  const [isProcessing, setIsProcessing] = useState<boolean>(false)

  const classes = useStyles()

  const loadUsers = async (): Promise<void> => {
    try {
      const response = await userDataAccess.getUsers()
      setUsers(response.users)
      setLoginUser(response.loginUser)
    } catch (err) {
      setError(err)
    } finally {
      setIsLoading(false)
    }
  }

  const clearError = () => {
    setError(undefined)
  }

  const onLogin = () => {
    setError(undefined)
    loadUsers().then()
  }

  const onEditUser = (user: User) => {
    window.location.href = `user.html?userId=${user.id}`
  }

  const onConfirmDeleteUser = (user: User) => {
    setUserToDelete(user)
    setIsDeleteConfirmOpen(true)
  }

  const onDeleteUser = async () => {
    setIsProcessing(true)
    try {
      await userDataAccess.deleteUser(userToDelete.id)
      toaster.show({
        message: `User ${userToDelete.firstName} ${userToDelete.lastName} (${userToDelete.username}) was successfully deleted.`,
        intent: 'primary',
        icon: 'small-tick',
      })
      await loadUsers()
    } catch (err) {
      setError(err)
    } finally {
      setIsDeleteConfirmOpen(false)
      setIsProcessing(false)
      setUserToDelete(null)
    }
  }

  const onCancelDelete = () => {
    setIsDeleteConfirmOpen(false)
    setUserToDelete(null)
  }

  useEffect(() => {
    loadUsers().then()
    const message = localStorageDataAccess.getToast()
    if (message) {
      toaster.show({
        message: message,
        intent: 'primary',
        icon: 'small-tick',
      })
    }
  }, [])

  return (
    <AdminPage
      assetsRoot={assetsRoot}
      error={error}
      clearError={clearError}
      onLogin={onLogin}
      loginUser={loginUser}
    >
      <CenterCardLayout>
        <Card elevation={1} className={classes.card}>
          <div className={`${classes.headerRow} card-header`}>
            <h1 className={classes.header}>Users</h1>
            <AnchorButton icon="new-person" text="Add User" href="user.html" />
          </div>
          <div className={classes.body}>
            {isLoading ? (
              <div className={classes.userRow}>
                <div className={`${classes.avatar} ${Classes.SKELETON}`}>
                  <Icon icon="person" color="white" />
                </div>
                <div className={classes.nameCol}>
                  <div className={Classes.SKELETON}>Sample Name Goes Here</div>
                  <div className={`${classes.email} ${Classes.SKELETON}`}>
                    sample.email@some-domain.com
                  </div>
                </div>
                <Tag
                  round={true}
                  className={`${classes.tag} ${Classes.SKELETON}`}
                >
                  Admin
                </Tag>
                <Button
                  icon="more"
                  className={`${classes.more} ${Classes.SKELETON}`}
                />
              </div>
            ) : (
              <>
                {users.map((user) => {
                  const userMenu = (
                    <Menu>
                      <MenuItem
                        icon="edit"
                        text="Edit"
                        onClick={() => onEditUser(user)}
                      />
                      {user?.id !== loginUser?.id && (
                        <MenuItem
                          icon="remove"
                          text="Delete"
                          onClick={() => onConfirmDeleteUser(user)}
                        />
                      )}
                    </Menu>
                  )
                  return (
                    <div className={classes.userRow} key={user.id}>
                      <div className={classes.avatar}>
                        <Icon icon="person" color="white" />
                      </div>
                      <div className={classes.nameCol}>
                        <div>
                          {user.firstName} {user.lastName}
                          {user?.id === loginUser?.id && (
                            <span
                              className={`${classes.subText} ${Classes.TEXT_MUTED}`}
                            >
                              {' '}
                              (logged in as)
                            </span>
                          )}
                        </div>
                        <div
                          className={`${classes.email} ${Classes.TEXT_MUTED}`}
                        >
                          {user.username}
                        </div>
                      </div>
                      <Tag round={true} className={classes.tag}>
                        {user.authLevel === 'sysadmin'
                          ? 'System Admin'
                          : 'Admin'}
                      </Tag>
                      <Popover placement="bottom-end" content={userMenu}>
                        <Button icon="more" className={classes.more} />
                      </Popover>
                    </div>
                  )
                })}
              </>
            )}
          </div>
        </Card>
      </CenterCardLayout>
      <ConfirmDialog
        isOpen={isDeleteConfirmOpen}
        isProcessing={isProcessing}
        title="Confirm Delete User"
        message={`Are you sure you want to delete user ${userToDelete?.firstName} ${userToDelete?.lastName} (${userToDelete?.username})?`}
        action="Delete User"
        processingAction="Deleting User..."
        onConfirm={onDeleteUser}
        onCancel={onCancelDelete}
      />
    </AdminPage>
  )
}

export function init(target: string, assetsRoot: string) {
  ReactDOM.render(
    <Users assetsRoot={assetsRoot} />,
    document.getElementById(target)
  )
}
