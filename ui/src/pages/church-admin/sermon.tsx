import * as React from 'react'
import { FormEvent, useEffect, useState } from 'react'
import LayoutProps from '../../layouts/layout-props'
import { User } from '../../model/user'
import AdminPage from '../../components/church-admin/admin-page'
import container from '../../di-container'
import UserDataAccess from '../../services/user-data-access'
import {
  AnchorButton,
  BreadcrumbProps,
  Button,
  Card,
  Classes,
  ControlGroup,
  Dialog,
  FileInput,
  FormGroup,
  HTMLSelect,
  ProgressBar,
  TagInput,
} from '@blueprintjs/core'
import { useStyles } from './sermon.styles'
import CenterCardLayout from '../../components/church-admin/center-card-layout'
import ValidationFormGroup from '../../components/church-admin/validation-form-group'
import PageForm from '../../components/church-admin/page-form'
import { Sermon as SermonModel } from '../../model/sermon'
import { DateInput } from '@blueprintjs/datetime'
import dayjs from 'dayjs'
import { KerygmaConfiguration } from '../../model/kerygma-configuration'
import bibleBookData from '../../model/bible-book-data.yaml'
import { BibleBook } from '../../model/bible-book'
import DarkModeContext from '../../components/church-admin/dark-mode-context'
import { AxiosResponse } from 'axios'
import { StatusCodes } from 'http-status-codes'
import SermonDataAccess from '../../services/sermon-data-access'
import { ProgressEvent } from '../../services/data-access'
import * as ReactDOM from 'react-dom'

const sermonDataAccess = container.resolve(SermonDataAccess)
const config = container.resolve(KerygmaConfiguration)
const bibleBooks = bibleBookData.books as BibleBook[]

const speakerOptions = config.defaultSpeakers.map((speaker, index) => {
  const prefix = speaker.title ? `${speaker.title} ` : ''
  const label = `${prefix}${speaker.firstName} ${speaker.lastName}`
  const value = index
  return { label, value }
})
const getSpeakerOption = (sermon: SermonModel) => {
  return speakerOptions.find((speakerOption) => {
    const prefix = sermon.speakerTitle ? `${sermon.speakerTitle} ` : ''
    const label = `${prefix}${sermon.speakerFirstName} ${sermon.speakerLastName}`
    return speakerOption.label === label
  })
}
const otherSpeaker = -1
speakerOptions.push({ label: 'Other', value: otherSpeaker })
const defaultSpeaker = config.defaultSpeakers.findIndex(
  (speaker) => speaker.default
)
const initModel = new SermonModel()
const speaker = config.defaultSpeakers[defaultSpeaker]
initModel.setSermonDate(dayjs())
initModel.speakerTitle = speaker.title
initModel.speakerFirstName = speaker.firstName
initModel.speakerLastName = speaker.lastName
const serviceOptions = config.services.map((service) => ({
  label: service,
  value: service,
}))
initModel.service = serviceOptions[0].value
const bookOptions = [
  { label: '', value: 'Book' },
  ...bibleBooks.map((book) => ({
    label: book.name,
    value: book.code,
  })),
]

const sermonIdStr: string | null = new URLSearchParams(
  window.location.search
).get('sermonId')
const sermonId: number | null = sermonIdStr ? parseInt(sermonIdStr) : null
const mode: 'edit' | 'new' = sermonId ? 'edit' : 'new'
const title = `${mode === 'new' ? 'Add' : 'Edit'} Sermon`
const userDataAccess = container.resolve(UserDataAccess)

const breadcrumbs: BreadcrumbProps[] = [
  { href: '/church-admin/sermons.html', icon: 'headset', text: 'Sermons' },
  { icon: mode === 'new' ? 'plus' : 'edit', text: title },
]

function calcChapters(book: BibleBook): (number | string)[] {
  return book
    ? Array.from(Array(book.chapters.length).keys()).map((chap) => chap + 1)
    : ['Chapter']
}

function calcVerses(book: BibleBook, chapter: number): (number | string)[] {
  return book && chapter
    ? Array.from(Array(book.chapters[chapter - 1]).keys()).map(
        (chap) => chap + 1
      )
    : ['Verse']
}

const Sermon: React.FC<LayoutProps> = ({ assetsRoot }) => {
  const [error, setError] = React.useState<Error>()
  const [loginUser, setLoginUser] = useState<User>()
  const [isProcessing, setIsProcessing] = React.useState<boolean>(false)
  const [isProgressDialogOpen, setIsProgressDialogOpen] =
    React.useState<boolean>(false)
  const [progress, setProgress] = React.useState<number>(0.0)
  const [isLoading, setIsLoading] = React.useState<boolean>(true)
  const [sermonModel, setSermonModel] = React.useState<SermonModel>(initModel)
  const [speakerSelection, setSpeakerSelection] =
    React.useState<number>(defaultSpeaker)
  const [serviceSelection, setServiceSelection] = React.useState<string>(
    serviceOptions[0].value
  )
  const [fromBook, setFromBook] = React.useState<BibleBook>(null)
  const fromChapters = calcChapters(fromBook)
  const fromVerses = calcVerses(fromBook, sermonModel.fromChapter)
  const [thruBook, setThruBook] = React.useState<BibleBook>(null)
  const thruChapters = calcChapters(thruBook)
  const thruVerses = calcVerses(thruBook, sermonModel.thruChapter)
  const [localFilename, setLocalFilename] = React.useState<string>('')
  const fileInputRef = React.useRef<HTMLInputElement>()
  const [uploadError, setUploadError] = React.useState<string>('')

  const classes = useStyles()

  const onFieldChange = (id: string, newValue: string) => {
    sermonModel[id] = newValue
    setSermonModel(sermonModel.clone())
  }

  const onChangeDateInput = async (selectedDate: string) => {
    sermonModel.setSermonDate(dayjs(selectedDate))
    setSermonModel(sermonModel.clone())
  }

  const onChangeTag = (values: string[]) => {
    if (values.length > 20) return
    const tagSet = new Set(values.map((tag) => `${tag}`.toLowerCase()))
    sermonModel.tags = Array.from(tagSet)
    setSermonModel(sermonModel.clone())
  }

  const onChangeSpeaker = (event: FormEvent<HTMLSelectElement>) => {
    const speakerValue = parseInt(event.currentTarget.value)
    setSpeakerSelection(speakerValue)
    if (speakerValue !== otherSpeaker) {
      const speaker = config.defaultSpeakers[speakerValue]
      sermonModel.speakerTitle = speaker.title
      sermonModel.speakerFirstName = speaker.firstName
      sermonModel.speakerLastName = speaker.lastName
    } else {
      sermonModel.speakerTitle = ''
      sermonModel.speakerFirstName = ''
      sermonModel.speakerLastName = ''
    }
    setSermonModel(sermonModel.clone())
  }

  const onChangeService = (event: FormEvent<HTMLSelectElement>) => {
    const service = event.currentTarget.value
    setServiceSelection(service)
    sermonModel.service = service
    setSermonModel(sermonModel.clone())
  }

  const onChangeFromBook = (event: FormEvent<HTMLSelectElement>) => {
    sermonModel.fromBook = event.currentTarget.value
    if (sermonModel.fromBook !== 'Book') {
      const book = bibleBooks.filter(
        (bk) => bk.code === sermonModel.fromBook
      )[0]
      setFromBook(book)
      sermonModel.fromChapter = 1
      sermonModel.fromVerse = 1
    } else {
      setFromBook(null)
      sermonModel.fromBook = undefined
      sermonModel.fromChapter = undefined
      sermonModel.fromVerse = undefined
    }
    setSermonModel(sermonModel.clone())
    onChangeThruBook(event)
  }

  const onChangeThruBook = (event: FormEvent<HTMLSelectElement>) => {
    sermonModel.thruBook = event.currentTarget.value
    if (sermonModel.thruBook !== 'Book') {
      const book = bibleBooks.filter(
        (bk) => bk.code === sermonModel.thruBook
      )[0]
      setThruBook(book)
      sermonModel.thruChapter = 1
      sermonModel.thruVerse = 1
    } else {
      setThruBook(null)
      sermonModel.thruBook = undefined
      sermonModel.thruChapter = undefined
      sermonModel.thruVerse = undefined
    }
    setSermonModel(sermonModel.clone())
  }

  const onChangeFromChapter = (event: FormEvent<HTMLSelectElement>) => {
    sermonModel.fromChapter = parseInt(event.currentTarget.value)
    setSermonModel(sermonModel.clone())
    onChangeThruChapter(event)
  }

  const onChangeFromVerse = (event: FormEvent<HTMLSelectElement>) => {
    sermonModel.fromVerse = parseInt(event.currentTarget.value)
    setSermonModel(sermonModel.clone())
    onChangeThruVerse(event)
  }

  const onChangeThruChapter = (event: FormEvent<HTMLSelectElement>) => {
    sermonModel.thruChapter = parseInt(event.currentTarget.value)
    setSermonModel(sermonModel.clone())
  }

  const onChangeThruVerse = (event: FormEvent<HTMLSelectElement>) => {
    sermonModel.thruVerse = parseInt(event.currentTarget.value)
    setSermonModel(sermonModel.clone())
  }

  const onFileInputChange = async (event: FormEvent<HTMLInputElement>) => {
    setLocalFilename(event.currentTarget.value)
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onUploadProgress = (progressEvent: ProgressEvent) => {
    const progress =
      Math.round((progressEvent.loaded * 100) / progressEvent.total) / 100
    setProgress(progress)
  }

  const clearError = () => {
    setError(undefined)
  }

  const preLoadForm = async () => {
    try {
      const sermon = await sermonDataAccess.getSermon(sermonId)
      const speakerOption = getSpeakerOption(sermon)?.value ?? otherSpeaker
      setSpeakerSelection(speakerOption)
      setServiceSelection(sermon.service)
      const fromBook = bibleBooks.filter((bk) => bk.code === sermon.fromBook)[0]
      setFromBook(fromBook)
      const thruBook = bibleBooks.filter((bk) => bk.code === sermon.thruBook)[0]
      setThruBook(thruBook)
      setSermonModel(sermon)
    } catch (err) {
      setError(err)
    } finally {
      setIsLoading(false)
    }
  }

  const onLogin = () => {
    setError(undefined)
    verifyLogin().then()
  }

  const verifyLogin = async () => {
    try {
      const user = await userDataAccess.getUserLoggedIn()
      setLoginUser(user)
    } catch (err) {
      setError(err)
    } finally {
      setIsLoading(false)
    }
  }

  const isFormValid = (): boolean => {
    return (
      sermonModel.isValid() &&
      (fileInputRef.current.files.length > 0 || mode === 'edit')
    )
  }

  const handleSave = async () => {
    setUploadError('')
    if (fileInputRef.current.files.length === 0 && mode === 'new') {
      return
    }
    const file = fileInputRef.current.files[0]
    try {
      setIsProcessing(true)
      setProgress(0)
      setIsProgressDialogOpen(true)
      let response: AxiosResponse
      if (mode === 'new') {
        response = await sermonDataAccess.addSermon(
          sermonModel,
          file,
          onUploadProgress
        )
      } else {
        response = await sermonDataAccess.saveSermon(
          sermonModel,
          file,
          onUploadProgress
        )
      }
      if (response.status === StatusCodes.CONFLICT) {
        setUploadError('A file with that filename already exists')
      } else if (response.status === StatusCodes.REQUEST_TOO_LONG) {
        setUploadError('The size of the file exceeded the allowed max')
      }
    } catch (err) {
      setError(err)
      setUploadError('An error occurred while uploading.')
      setProgress(1.0)
    } finally {
      setIsProcessing(false)
    }
  }

  function closeProgress() {
    setIsProgressDialogOpen(false)
    if (progress >= 1.0 && uploadError === '') {
      window.location.href = 'sermons.html'
    } else {
      setProgress(0.0)
    }
  }

  useEffect(() => {
    if (mode === 'edit') {
      preLoadForm().then()
    }
    verifyLogin().then()
  }, [])

  const actionLabel = mode === 'edit' ? 'Save' : 'Add Sermon'
  const processingLabel = mode === 'edit' ? 'Saving...' : 'Adding Sermon...'
  const actionIcon = mode === 'edit' ? 'small-tick' : 'plus'

  return (
    <AdminPage
      assetsRoot={assetsRoot}
      error={error}
      clearError={clearError}
      onLogin={onLogin}
      loginUser={loginUser}
    >
      <CenterCardLayout breadcrumbs={breadcrumbs}>
        <Card elevation={1} className={classes.card}>
          <div className={`${classes.headerRow} card-header`}>
            <h1 className={classes.header}>{title}</h1>
          </div>
          <div className={classes.body}>
            <PageForm>
              <FormGroup
                label="Sermon Date"
                className={`${classes.formGroup} ${
                  isLoading && Classes.SKELETON
                }`}
              >
                <DateInput
                  value={sermonModel.getSermonDate().toISOString()}
                  onChange={onChangeDateInput}
                  formatDate={(date) => date.toLocaleDateString()}
                  parseDate={(str) => new Date(str)}
                  fill={true}
                />
              </FormGroup>
              <FormGroup
                label="Speaker"
                className={`${classes.formGroup} ${
                  isLoading && Classes.SKELETON
                }`}
              >
                <HTMLSelect
                  onChange={onChangeSpeaker}
                  options={speakerOptions}
                  fill={true}
                  value={speakerSelection}
                />
              </FormGroup>
              {speakerSelection === otherSpeaker && (
                <FormGroup
                  label="Other Speaker"
                  className={`${classes.formGroup} ${
                    isLoading && Classes.SKELETON
                  }`}
                >
                  <ControlGroup fill={true}>
                    <ValidationFormGroup
                      id="speakerTitle"
                      label="Title"
                      placeholder="Title"
                      value={sermonModel.speakerTitle}
                      setter={onFieldChange}
                      hideFormGroup={true}
                      noFill={true}
                      className={classes.nameTitle}
                    />
                    <ValidationFormGroup
                      id="speakerFirstName"
                      label="First Name"
                      placeholder="First Name"
                      value={sermonModel.speakerFirstName}
                      validator={sermonModel.speakerFirstNameValidator}
                      setter={onFieldChange}
                      hideFormGroup={true}
                      noFill={true}
                    />
                    <ValidationFormGroup
                      id="speakerLastName"
                      label="Last Name"
                      placeholder="Last Name"
                      value={sermonModel.speakerLastName}
                      validator={sermonModel.speakerLastNameValidator}
                      setter={onFieldChange}
                      hideFormGroup={true}
                      noFill={true}
                    />
                  </ControlGroup>
                </FormGroup>
              )}
              <ValidationFormGroup
                id="title"
                value={sermonModel.title}
                validator={() => {
                  return sermonModel.titleValidator()
                }}
                setter={onFieldChange}
                label="Title"
                labelInfo="(required)"
                isLoading={isLoading}
              />
              <FormGroup
                label="Search Tags"
                className={`${classes.formGroup} ${
                  isLoading && Classes.SKELETON
                }`}
              >
                <TagInput
                  onChange={onChangeTag}
                  values={sermonModel.tags}
                  fill={true}
                  leftIcon="tag"
                  placeholder="Separate each tag with a comma and hit enter..."
                  tagProps={{ round: true, minimal: true }}
                />
              </FormGroup>
              <FormGroup
                label="Service"
                className={`${classes.formGroup} ${
                  isLoading && Classes.SKELETON
                }`}
              >
                <HTMLSelect
                  onChange={onChangeService}
                  options={serviceOptions}
                  fill={true}
                  value={serviceSelection}
                />
              </FormGroup>
              <FormGroup
                label="From Passage"
                className={`${classes.formGroup} ${
                  isLoading && Classes.SKELETON
                }`}
              >
                <ControlGroup fill={true}>
                  <HTMLSelect
                    onChange={onChangeFromBook}
                    options={bookOptions}
                    value={sermonModel.fromBook}
                  />
                  <HTMLSelect
                    onChange={onChangeFromChapter}
                    options={fromChapters}
                    value={sermonModel.fromChapter}
                  />
                  <HTMLSelect
                    onChange={onChangeFromVerse}
                    options={fromVerses}
                    value={sermonModel.fromVerse}
                  />
                </ControlGroup>
              </FormGroup>
              <FormGroup
                label="To Passage"
                className={`${classes.formGroup} ${
                  isLoading && Classes.SKELETON
                }`}
              >
                <ControlGroup fill={true}>
                  <HTMLSelect
                    onChange={onChangeThruBook}
                    options={bookOptions}
                    value={sermonModel.thruBook}
                  />
                  <HTMLSelect
                    onChange={onChangeThruChapter}
                    options={thruChapters}
                    value={sermonModel.thruChapter}
                  />
                  <HTMLSelect
                    onChange={onChangeThruVerse}
                    options={thruVerses}
                    value={sermonModel.thruVerse}
                  />
                </ControlGroup>
              </FormGroup>
              <FormGroup
                label="Local File to Upload"
                labelInfo="(must be of type MP3)"
                className={`${classes.formGroup} ${
                  isLoading ? Classes.SKELETON : ''
                }`}
              >
                <FileInput
                  text={localFilename}
                  buttonText="Browse"
                  fill={true}
                  onInputChange={onFileInputChange}
                  inputProps={{ ref: fileInputRef, accept: 'audio/mpeg' }}
                />
              </FormGroup>
            </PageForm>
            <div className={classes.buttonRow}>
              <Button
                icon={actionIcon}
                text={isProcessing ? processingLabel : actionLabel}
                className={classes.saveButton}
                onClick={handleSave}
                disabled={!isFormValid() || isProcessing}
              />
              <AnchorButton
                icon="small-cross"
                text="Cancel"
                href="/church-admin/sermons.html"
              />
            </div>
          </div>
        </Card>
      </CenterCardLayout>
      <DarkModeContext.Consumer>
        {(isDarkMode) => (
          <Dialog
            isOpen={isProgressDialogOpen}
            title="Uploading File"
            className={isDarkMode ? Classes.DARK : ''}
            isCloseButtonShown={false}
            canEscapeKeyClose={false}
            canOutsideClickClose={false}
          >
            <div className={Classes.DIALOG_BODY}>
              <ProgressBar
                value={progress}
                intent={uploadError ? 'danger' : 'primary'}
                stripes={false}
              />
              {progress < 1.0 ? (
                <p className={classes.dialogMessage}>
                  {mode === 'new' ? 'Uploading ' : 'Saving '}
                  {localFilename}....
                </p>
              ) : (
                <>
                  {uploadError !== '' ? (
                    <p className={classes.dialogMessageError}>{uploadError}</p>
                  ) : (
                    <p className={classes.dialogMessage}>
                      {localFilename} was successfully
                      {mode === 'new' ? ' uploaded' : ' saved'}.
                    </p>
                  )}
                </>
              )}
            </div>
            {progress >= 1.0 && (
              <div className={Classes.DIALOG_FOOTER}>
                <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                  <Button onClick={closeProgress}>OK</Button>
                </div>
              </div>
            )}
          </Dialog>
        )}
      </DarkModeContext.Consumer>
    </AdminPage>
  )
}

export function init(target: string, assetsRoot: string) {
  ReactDOM.render(
    <Sermon assetsRoot={assetsRoot} />,
    document.getElementById(target)
  )
}
