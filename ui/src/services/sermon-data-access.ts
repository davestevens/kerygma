import DataAccess, { Fetcher, OnUploadProgress } from './data-access'
import { JsonConvert } from 'json2typescript'
import LocalStorageDataAccess from './local-storage-data-access'
import { AxiosResponse } from 'axios'
import { StatusCodes } from 'http-status-codes'
import { Sermon } from '../model/sermon'
import { inject, injectable } from 'tsyringe'
import pDebounce from 'p-debounce'
import { KerygmaConfiguration } from '../model/kerygma-configuration'
import { SermonTag } from '../model/sermon-tag'

@injectable()
export default class SermonDataAccess extends DataAccess {
  constructor(
    private readonly jsonConvert: JsonConvert,
    config: KerygmaConfiguration,
    @inject('fetcher') fetcher: Fetcher,
    localStorageDataAccess: LocalStorageDataAccess
  ) {
    super(config, fetcher, localStorageDataAccess)
  }

  public async addSermon(
    sermon: Sermon,
    file: File,
    onUploadProgress?: OnUploadProgress
  ): Promise<AxiosResponse> {
    const data = new FormData()
    data.append('sermonDate', sermon.sermonDate)
    data.append('speakerTitle', sermon.speakerTitle)
    data.append('speakerFirstName', sermon.speakerFirstName)
    data.append('speakerLastName', sermon.speakerLastName)
    data.append('title', sermon.title)
    data.append('tags', JSON.stringify(sermon.tags))
    data.append('service', sermon.service)
    data.append('fromBook', sermon.fromBook)
    data.append('fromChapter', `${sermon.fromChapter}`)
    data.append('fromVerse', `${sermon.fromVerse}`)
    data.append('thruBook', sermon.thruBook)
    data.append('thruChapter', `${sermon.thruChapter}`)
    data.append('thruVerse', `${sermon.thruVerse}`)
    data.append('file', file)
    return await this.postFormDataAuthorized(
      'POST',
      'api/sermons',
      data,
      [StatusCodes.CONFLICT, StatusCodes.REQUEST_TOO_LONG],
      onUploadProgress
    )
  }

  public async saveSermon(
    sermon: Sermon,
    file: File,
    onUploadProgress?: OnUploadProgress
  ): Promise<AxiosResponse> {
    if (file) {
      const data = new FormData()
      data.append('sermonDate', sermon.sermonDate)
      data.append('speakerTitle', sermon.speakerTitle)
      data.append('speakerFirstName', sermon.speakerFirstName)
      data.append('speakerLastName', sermon.speakerLastName)
      data.append('title', sermon.title)
      data.append('tags', JSON.stringify(sermon.tags))
      data.append('service', sermon.service)
      data.append('fromBook', sermon.fromBook)
      data.append('fromChapter', `${sermon.fromChapter}`)
      data.append('fromVerse', `${sermon.fromVerse}`)
      data.append('thruBook', sermon.thruBook)
      data.append('thruChapter', `${sermon.thruChapter}`)
      data.append('thruVerse', `${sermon.thruVerse}`)
      data.append('file', file)
      return await this.postFormDataAuthorized(
        'PUT',
        `api/sermons/${sermon.id}`,
        data,
        [StatusCodes.CONFLICT, StatusCodes.REQUEST_TOO_LONG],
        onUploadProgress
      )
    } else {
      const response = await this.putAuthorized(
        `api/sermons/${sermon.id}`,
        JSON.stringify(sermon),
        [StatusCodes.CONFLICT]
      )
      onUploadProgress({ loaded: 10, total: 10 })
      return response
    }
  }

  public debouncedGetSermons = pDebounce(this.getSermons, 500)

  private async getSermons(
    page: number,
    perPage: number,
    sortOrder: 'latest' | 'oldest',
    filter: string,
    tags: string[]
  ): Promise<{
    sermons: Sermon[]
    totalRowCount: number
  }> {
    const params = {
      page,
      perPage,
      sortOrder,
      filter,
      tags,
    }
    const response = await this.get('api/sermons', [], undefined, params)
    const body = await response.data
    const sermons = this.jsonConvert.deserializeArray(body.sermons, Sermon)
    return { sermons, totalRowCount: body.totalRowCount }
  }

  public async getSermon(sermonId: number): Promise<Sermon> {
    const response = await this.get(`api/sermons/${sermonId}`)
    const body = await response.data
    return this.jsonConvert.deserializeObject(body.sermon, Sermon)
  }

  public async deleteSermon(sermonId: number): Promise<AxiosResponse> {
    return await this.deleteAuthorized(`api/sermons/${sermonId}`)
  }

  public async getSermonTags(
    sortOrder: 'tag' | 'sermonCount'
  ): Promise<SermonTag[]> {
    const params = { sortOrder }
    const response = await this.get('api/sermon-tags', [], undefined, params)
    const body = await response.data
    return this.jsonConvert.deserializeArray(body.tags, SermonTag)
  }
}
