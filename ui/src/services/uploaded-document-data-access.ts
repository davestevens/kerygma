import DataAccess, { Fetcher, OnUploadProgress } from './data-access'
import { JsonConvert } from 'json2typescript'
import LocalStorageDataAccess from './local-storage-data-access'
import { UploadedDocument } from '../model/uploaded-document'
import { StatusCodes } from 'http-status-codes'
import { AxiosResponse } from 'axios'
import { User } from '../model/user'
import { injectable, inject } from 'tsyringe'
import { KerygmaConfiguration } from '../model/kerygma-configuration'

@injectable()
export default class UploadedDocumentDataAccess extends DataAccess {
  constructor(
    private readonly jsonConvert: JsonConvert,
    config: KerygmaConfiguration,
    @inject('fetcher') fetcher: Fetcher,
    localStorageDataAccess: LocalStorageDataAccess
  ) {
    super(config, fetcher, localStorageDataAccess)
  }

  public async addDocument(
    document: UploadedDocument,
    file: File,
    onUploadProgress?: OnUploadProgress
  ): Promise<AxiosResponse> {
    const data = new FormData()
    data.append('filename', document.filename)
    data.append('title', document.title)
    data.append('file', file)
    return await this.postFormDataAuthorized(
      'POST',
      'api/documents',
      data,
      [StatusCodes.CONFLICT, StatusCodes.REQUEST_TOO_LONG],
      onUploadProgress
    )
  }

  public async getDocuments(): Promise<{
    documents: UploadedDocument[]
    loginUser: User
  }> {
    const response = await this.getAuthorized('api/documents')
    const body = await response.data
    const documents = this.jsonConvert.deserializeArray(
      body.documents,
      UploadedDocument
    )
    const loginUser = this.jsonConvert.deserializeObject(body.loginUser, User)
    return { documents, loginUser }
  }

  public async getDocument(documentId: number): Promise<{
    document: UploadedDocument
    loginUser: User
  }> {
    const response = await this.getAuthorized(`api/documents/${documentId}`)
    const body = await response.data
    const document = this.jsonConvert.deserializeObject(
      body.document,
      UploadedDocument
    )
    const loginUser = this.jsonConvert.deserializeObject(body.loginUser, User)
    return { document, loginUser }
  }

  public async deleteDocument(docId: number): Promise<AxiosResponse> {
    return await this.deleteAuthorized(`api/documents/${docId}`)
  }

  async saveDocument(
    document: UploadedDocument,
    file: File,
    onUploadProgress?: OnUploadProgress
  ): Promise<AxiosResponse> {
    if (file) {
      const data = new FormData()
      data.append('id', `${document.id}`)
      data.append('filename', document.filename)
      data.append('title', document.title)
      data.append('file', file)
      return await this.postFormDataAuthorized(
        'PUT',
        `api/documents/${document.id}`,
        data,
        [StatusCodes.CONFLICT, StatusCodes.REQUEST_TOO_LONG],
        onUploadProgress
      )
    } else {
      const response = await this.putAuthorized(
        `api/documents/${document.id}`,
        JSON.stringify(document),
        [StatusCodes.CONFLICT]
      )
      onUploadProgress({ loaded: 10, total: 10 })
      return response
    }
  }
}
