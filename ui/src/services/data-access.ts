import UnauthorizedError from '../model/unauthorized-error'
import LocalStorageDataAccess from './local-storage-data-access'
import { StatusCodes } from 'http-status-codes'
import { AxiosPromise, AxiosResponse } from 'axios'
import { KerygmaConfiguration } from '../model/kerygma-configuration'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Fetcher = (url: string, settings: any) => AxiosPromise
export type ProgressEvent = {
  loaded: number
  total: number
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type OnUploadProgress = (progressEvent: ProgressEvent) => void

type FetchSettings = {
  method: 'GET' | 'PUT' | 'POST' | 'DELETE'
  data?: string | FormData
  headers?: {
    Accept: string
    'content-type'?: string
    Authorization: string
    _csrf?: string
  }
  params?: object
  onUploadProgress?: OnUploadProgress
  withCredentials?: boolean
}

export default class DataAccess {
  constructor(
    private readonly config: KerygmaConfiguration,
    private readonly fetcher: Fetcher,
    private readonly localStorageDataAccess: LocalStorageDataAccess
  ) {}

  protected async get(
    path: string,
    acceptableStatuses: number[] = [],
    bearerToken?: string,
    params?: object
  ): Promise<AxiosResponse> {
    const method = 'GET'
    const settings: FetchSettings = {
      method: method,
      params: params,
      withCredentials: true,
    }
    if (bearerToken) {
      settings.headers = {
        Accept: 'application/json',
        'content-type': 'application/json',
        Authorization: `Bearer ${bearerToken}`,
      }
    }
    try {
      return await this.fetcher(`${this.config.api.apiHost}${path}`, settings)
    } catch (error) {
      if (error.response) {
        return DataAccess.verifyResponse(
          error.response,
          method,
          path,
          acceptableStatuses
        )
      }
      throw error
    }
  }

  protected async getAuthorized(
    path: string,
    acceptableStatuses: number[] = [],
    params?: object
  ): Promise<AxiosResponse> {
    const token = this.localStorageDataAccess.getToken()
    return this.get(path, acceptableStatuses, token, params)
  }

  protected async post(
    path: string,
    body?: string,
    acceptableStatuses: number[] = [],
    bearerToken?: string
  ): Promise<AxiosResponse> {
    return this.postOrPut('POST', path, body, acceptableStatuses, bearerToken)
  }

  protected async put(
    path: string,
    body?: string,
    acceptableStatuses: number[] = [],
    bearerToken?: string
  ): Promise<AxiosResponse> {
    return this.postOrPut('PUT', path, body, acceptableStatuses, bearerToken)
  }

  private async postOrPut(
    method: 'PUT' | 'POST',
    path: string,
    data?: string,
    acceptableStatuses: number[] = [],
    bearerToken?: string
  ): Promise<AxiosResponse> {
    const csrfToken = this.localStorageDataAccess.getCsrfToken()
    const settings: FetchSettings = {
      method,
      data,
      headers: {
        Accept: 'application/json',
        Authorization: bearerToken ? `Bearer ${bearerToken}` : '',
        _csrf: csrfToken,
      },
      withCredentials: true,
    }
    if (data) {
      settings.headers['Content-Type'] = 'application/json'
    }

    try {
      return await this.fetcher(`${this.config.api.apiHost}${path}`, settings)
    } catch (error) {
      if (error.response) {
        return DataAccess.verifyResponse(
          error.response,
          method,
          path,
          acceptableStatuses
        )
      }
      throw error
    }
  }

  public async postFormDataAuthorized(
    method: 'PUT' | 'POST',
    path: string,
    data: FormData,
    acceptableStatuses: number[] = [],
    onUploadProgress?: OnUploadProgress
  ): Promise<AxiosResponse> {
    const token = this.localStorageDataAccess.getToken()
    const csrfToken = this.localStorageDataAccess.getCsrfToken()
    const contentType = data.has('file')
      ? 'multipart/form-data'
      : 'application/x-www-form-urlencoded'
    const settings: FetchSettings = {
      method,
      data,
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
        'content-type': contentType,
        _csrf: csrfToken,
      },
      withCredentials: true,
      onUploadProgress,
    }

    try {
      return await this.fetcher(`${this.config.api.apiHost}${path}`, settings)
    } catch (error) {
      if (error.response) {
        return DataAccess.verifyResponse(
          error.response,
          method,
          path,
          acceptableStatuses
        )
      }
      throw error
    }
  }

  protected async delete(
    path: string,
    acceptableStatuses: number[] = [],
    bearerToken?: string
  ): Promise<AxiosResponse> {
    const csrfToken = this.localStorageDataAccess.getCsrfToken()
    const method = 'DELETE'
    const settings: FetchSettings = {
      method: method,
      headers: {
        Accept: 'application/json',
        Authorization: bearerToken ? `Bearer ${bearerToken}` : '',
        _csrf: csrfToken,
      },
      withCredentials: true,
    }

    try {
      return await this.fetcher(`${this.config.api.apiHost}${path}`, settings)
    } catch (error) {
      if (error.response) {
        return DataAccess.verifyResponse(
          error.response,
          method,
          path,
          acceptableStatuses
        )
      }
      throw error
    }
  }

  protected async postAuthorized(
    path: string,
    body?: string,
    acceptableStatuses: number[] = []
  ): Promise<AxiosResponse> {
    const token = this.localStorageDataAccess.getToken()
    return this.post(path, body, acceptableStatuses, token)
  }

  protected async putAuthorized(
    path: string,
    body?: string,
    acceptableStatuses: number[] = []
  ): Promise<AxiosResponse> {
    const token = this.localStorageDataAccess.getToken()
    return this.put(path, body, acceptableStatuses, token)
  }

  protected async deleteAuthorized(
    path: string,
    acceptableStatuses: number[] = []
  ): Promise<AxiosResponse> {
    const token = this.localStorageDataAccess.getToken()
    return this.delete(path, acceptableStatuses, token)
  }

  private static verifyResponse(
    response: AxiosResponse,
    method: 'POST' | 'PUT' | 'GET' | 'DELETE',
    path: string,
    acceptableStatuses: number[] = []
  ): AxiosResponse {
    const statusWasAcceptable =
      acceptableStatuses.filter((status) => status === response.status).length >
      0
    if (response.status === StatusCodes.UNAUTHORIZED) {
      throw new UnauthorizedError()
    }
    if (!statusWasAcceptable && response.status >= 300) {
      throw new Error(
        `Non 200 level status (${response.status}) returned from ${method} ${path}`
      )
    }
    return response
  }
}
