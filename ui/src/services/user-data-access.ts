import { User } from '../model/user'
import { JsonConvert } from 'json2typescript'
import DataAccess, { Fetcher } from './data-access'
import { UserForm } from '../model/user-form'
import LocalStorageDataAccess from './local-storage-data-access'
import { StatusCodes } from 'http-status-codes'
import { AxiosResponse } from 'axios'
import { injectable, inject } from 'tsyringe'
import { KerygmaConfiguration } from '../model/kerygma-configuration'

@injectable()
export default class UserDataAccess extends DataAccess {
  constructor(
    private readonly jsonConvert: JsonConvert,
    config: KerygmaConfiguration,
    @inject('fetcher') fetcher: Fetcher,
    localStorageDataAccess: LocalStorageDataAccess
  ) {
    super(config, fetcher, localStorageDataAccess)
  }

  public async getUsers(): Promise<{ users: User[]; loginUser: User }> {
    const response = await this.getAuthorized('api/users')
    const body = await response.data
    const users = this.jsonConvert.deserializeArray(body.users, User)
    const loginUser = this.jsonConvert.deserializeObject(body.loginUser, User)
    return { users, loginUser }
  }

  public async getUser(
    userId: string
  ): Promise<{ user: User; loginUser: User }> {
    const response = await this.getAuthorized(`api/users/${userId}`)
    const body = await response.data
    const user = this.jsonConvert.deserializeObject(body.user, User)
    const loginUser = this.jsonConvert.deserializeObject(body.loginUser, User)
    return { user, loginUser }
  }

  public async addUser(userForm: UserForm): Promise<AxiosResponse> {
    return await this.postAuthorized('api/users', JSON.stringify(userForm), [
      StatusCodes.CONFLICT,
    ])
  }

  public async saveUser(userForm: UserForm): Promise<AxiosResponse> {
    return await this.putAuthorized(
      `api/users/${userForm.id}`,
      JSON.stringify(userForm),
      [StatusCodes.CONFLICT]
    )
  }

  public async deleteUser(userId: string): Promise<AxiosResponse> {
    return await this.deleteAuthorized(`api/users/${userId}`)
  }

  async getUserLoggedIn(): Promise<User> {
    const response = await this.getAuthorized('api/users/logged-in')
    const body = await response.data
    return this.jsonConvert.deserializeObject(body.user, User)
  }
}
