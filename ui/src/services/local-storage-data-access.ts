import { inject, injectable } from 'tsyringe'

const tokenKey = 'kerygma-api-token'
const csrfTokenKey = 'kerygma-csrf-token'
const settingsKey = 'kerygma-settings'
const toastKey = 'kerygma-toast'

type AppSettings = {
  isDarkMode: boolean
}

@injectable()
export default class LocalStorageDataAccess {
  constructor(
    @inject('localStorage') private localStorage: Storage,
    @inject('sessionStorage') private sessionStorage: Storage
  ) {}

  public saveToken(token: string): void {
    this.sessionStorage.setItem(tokenKey, token)
  }

  public getToken(): string {
    return this.sessionStorage.getItem(tokenKey)
  }

  public saveCsrfToken(csrfToken: string): void {
    this.sessionStorage.setItem(csrfTokenKey, csrfToken)
  }

  public getCsrfToken(): string {
    return this.sessionStorage.getItem(csrfTokenKey)
  }

  public saveDarkMode(isDarkMode: boolean): void {
    const settingsValue = JSON.stringify({ isDarkMode: isDarkMode })
    this.localStorage.setItem(settingsKey, settingsValue)
  }

  public getDarkMode(): boolean {
    const settings: AppSettings | null = JSON.parse(
      this.localStorage.getItem(settingsKey)
    )
    return settings?.isDarkMode ?? false
  }

  public setToast(message: string): void {
    this.sessionStorage.setItem(toastKey, message)
  }

  // this is a destructive read
  public getToast(): string {
    const message: string = this.sessionStorage.getItem(toastKey) ?? ''
    this.sessionStorage.setItem(toastKey, '')
    return message
  }
}
