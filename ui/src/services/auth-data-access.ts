import DataAccess, { Fetcher } from './data-access'
import LocalStorageDataAccess from './local-storage-data-access'
import { StatusCodes } from 'http-status-codes'
import { injectable, inject } from 'tsyringe'
import { KerygmaConfiguration } from '../model/kerygma-configuration'

@injectable()
export default class AuthDataAccess extends DataAccess {
  constructor(
    config: KerygmaConfiguration,
    @inject('fetcher') fetcher: Fetcher,
    localStorageDataAccess: LocalStorageDataAccess
  ) {
    super(config, fetcher, localStorageDataAccess)
  }

  public async generateCsrfToken(): Promise<string | undefined> {
    const response = await this.get('api/generate-csrf')
    if (response.status === StatusCodes.OK) {
      return response.data.token
    }
  }

  public async login(
    username: string,
    password: string
  ): Promise<string | StatusCodes.FORBIDDEN | StatusCodes.LOCKED> {
    const response = await this.post(
      'api/login',
      JSON.stringify({ username, password }),
      [StatusCodes.FORBIDDEN, StatusCodes.LOCKED]
    )
    if (response.status === StatusCodes.OK) {
      const loginBody: { token: string } = await response.data
      return loginBody.token
    }
    return response.status === StatusCodes.LOCKED
      ? StatusCodes.LOCKED
      : StatusCodes.FORBIDDEN
  }

  public async logout(): Promise<void> {
    await this.deleteAuthorized('api/logout')
  }
}
