export default interface PageFrontmatter {
  layout?: string
  suppressBottomMargin?: boolean
  title?: string
  description?: string
}
