export const globalConstants = {
  textMaxWidth: '680px',
  formMaxWidth: '400px',
  bp: {
    phoneMax: '576px',
    tabletMin: '577px',
    tabletMax: '768px',
    mdMin: '769px',
    mdMax: '991px',
    lgMin: '992px',
    lgMax: '1199px',
    xlMin: '1200px',
    xlMax: '1399px',
    xxlMin: '1400px',
  },
  churchAdmin: {
    appName: 'Kerygma',
    version: '0.1 beta',
  },
  kerygmaUrl: 'https://gitlab.com/davestevens/kerygma',
}
