import { KerygmaConfiguration } from '../model/kerygma-configuration'

// KERYGMA_CONFIG is set by the WebPack DefinePlugin.
// See https://webpack.js.org/plugins/define-plugin/
declare const KERYGMA_CONFIG: unknown
export const kerygmaConfig = KERYGMA_CONFIG as KerygmaConfiguration

declare const KERYGMA_VERSION: unknown
const kerygmaVersionObj = KERYGMA_VERSION as { version: string }
export const kerygmaVersion = kerygmaVersionObj.version
