import { JsonObject, JsonProperty } from 'json2typescript'

@JsonObject('SermonTag')
export class SermonTag {
  @JsonProperty('tag', String)
  tag = ''
  @JsonProperty('sermonCount', Number)
  sermonCount = 0
  isSelected = false

  public clone(): SermonTag {
    const clone = new SermonTag()
    clone.tag = this.tag
    clone.sermonCount = this.sermonCount
    clone.isSelected = this.isSelected
    return clone
  }
}
