import * as yup from 'yup'

export class LoginForm {
  [key: string]: string | (() => string) | (() => boolean) | (() => LoginForm)
  username = ''
  password = ''

  usernameValidator = (): string => {
    return yup.string().trim().required().isValidSync(this.username)
      ? ''
      : 'is required'
  }

  passwordValidator = (): string => {
    return yup.string().trim().required().isValidSync(this.password)
      ? ''
      : 'is required'
  }

  isValid(): boolean {
    for (const key in this) {
      const validator = this[key]
      if (
        key.endsWith('Validator') &&
        typeof validator === 'function' &&
        validator() !== ''
      ) {
        return false
      }
    }
    return true
  }

  /*
   * This is necessary for the useState react hook to modify the state
   * of the form.
   */
  clone(): LoginForm {
    const clone = new LoginForm()
    for (const key of Object.keys(this)) {
      if (typeof this[key] === 'string') {
        clone[key] = this[key]
      }
    }
    return clone
  }
}
