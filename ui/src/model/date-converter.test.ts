import DateConverter from './date-converter'
import dayjs from 'dayjs'

test('serialize', () => {
  const converter = new DateConverter()
  const dateStr = converter.serialize(dayjs('2022-04-01T00:00:00.000Z'))
  expect(dateStr).toBe('2022-04-01T00:00:00.000Z')
})

test('deserialize', () => {
  const converter = new DateConverter()
  const day = converter.deserialize('2022-04-01T15:32:14.000Z')
  expect(day.toISOString()).toBe('2022-04-01T15:32:14.000Z')
})
