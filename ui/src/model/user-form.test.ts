import { UserForm } from './user-form'

test('first name validator', () => {
  const userForm = new UserForm()
  expect(userForm.firstNameValidator()).toBe('is required')
  userForm.firstName = 'First Name'
  expect(userForm.firstNameValidator()).toBe('')
})

test('last name validator', () => {
  const userForm = new UserForm()
  expect(userForm.lastNameValidator()).toBe('is required')
  userForm.lastName = 'Last Name'
  expect(userForm.lastNameValidator()).toBe('')
})

test('username validator', () => {
  const userForm = new UserForm()
  expect(userForm.usernameValidator()).toBe('is required')
  userForm.username = 'user'
  expect(userForm.usernameValidator()).toBe('must be at least 8 characters')
  userForm.username = 'adminuser'
  expect(userForm.usernameValidator()).toBe('')
})

test('password validator', () => {
  const userForm = new UserForm()
  expect(userForm.passwordValidator()).toBe('is required')
  userForm.mode = 'edit'
  expect(userForm.passwordValidator()).toBe('')
  userForm.mode = 'new'
  userForm.password = 'Short12'
  expect(userForm.passwordValidator()).toBe('must be at least 8 characters')
  userForm.password = 'lowercaseUPPERCASE'
  expect(userForm.passwordValidator()).toBe(
    'must contain uppercase, lowercase, and numeric characters'
  )
  userForm.password = 'lowercase12345'
  expect(userForm.passwordValidator()).toBe(
    'must contain uppercase, lowercase, and numeric characters'
  )
  userForm.password = 'UPPERCASE12345'
  expect(userForm.passwordValidator()).toBe(
    'must contain uppercase, lowercase, and numeric characters'
  )
  userForm.password = 'Passw0rd'
  expect(userForm.passwordValidator()).toBe('')
})

test('password confirm validator', () => {
  const userForm = new UserForm()
  userForm.password = 'Password1'
  userForm.passwordConfirm = 'Password2'
  expect(userForm.passwordConfirmValidator()).toBe('does not match')
  userForm.passwordConfirm = 'Password1'
  expect(userForm.passwordConfirmValidator()).toBe('')
  userForm.password = ''
  userForm.mode = 'edit'
  expect(userForm.passwordConfirmValidator()).toBe('')
})

test('auth level validator', () => {
  const userForm = new UserForm()
  userForm.authLevel = 'blah blah blah'
  expect(userForm.authLevelValidator()).toBe('invalid auth level')
  userForm.authLevel = 'sysadmin'
  expect(userForm.authLevelValidator()).toBe('')
})

test('entire form is valid', () => {
  const userForm = new UserForm()
  userForm.lastName = 'Last'
  userForm.username = 'adminuser'
  userForm.password = 'Password1'
  userForm.passwordConfirm = 'Password1'
  expect(userForm.isValid()).toBeFalsy()
  userForm.firstName = 'First'
  userForm.lastName = ''
  expect(userForm.isValid()).toBeFalsy()
  userForm.lastName = 'Last'
  userForm.username = 'me'
  expect(userForm.isValid()).toBeFalsy()
  userForm.username = 'adminuser'
  userForm.password = 'Short1'
  expect(userForm.isValid()).toBeFalsy()
  userForm.password = 'Password1'
  userForm.passwordConfirm = 'Password2'
  expect(userForm.isValid()).toBeFalsy()
  userForm.passwordConfirm = 'Password1'
  userForm.authLevel = 'blah'
  expect(userForm.isValid()).toBeFalsy()
  userForm.authLevel = 'admin'
  expect(userForm.isValid()).toBeTruthy()
})

test('clone', () => {
  const userForm = new UserForm()
  userForm.lastName = 'Last2'
  userForm.username = 'adminuser'
  userForm.password = 'Password2'
  const clone = userForm.clone()
  expect(clone.lastName).toBe('Last2')
  expect(clone.username).toBe('adminuser')
  expect(clone.password).toBe('Password2')
})
