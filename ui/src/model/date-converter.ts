import { JsonConverter, JsonCustomConvert } from 'json2typescript'
import dayjs, { Dayjs } from 'dayjs'

@JsonConverter
export default class DateConverter implements JsonCustomConvert<Dayjs> {
  deserialize(data: string): Dayjs {
    return dayjs(data)
  }

  serialize(data: Dayjs): string {
    return data.toISOString()
  }
}
