import { JsonObject, JsonProperty } from 'json2typescript'

@JsonObject('User')
export class User {
  @JsonProperty('id', String)
  id = ''
  @JsonProperty('firstName', String)
  firstName = ''
  @JsonProperty('lastName', String)
  lastName = ''
  @JsonProperty('username', String)
  username = ''
  @JsonProperty('authLevel', String)
  authLevel = 'admin'
}
