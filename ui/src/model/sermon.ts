import { JsonObject, JsonProperty } from 'json2typescript'
import * as yup from 'yup'
import DateConverter from './date-converter'
import dayjs, { Dayjs } from 'dayjs'
import { BibleBook } from './bible-book'

@JsonObject('Sermon')
export class Sermon {
  @JsonProperty('id', Number)
  id = 0
  @JsonProperty('sermonDate', String)
  sermonDate = ''
  public setSermonDate(dt: Dayjs) {
    this.sermonDate = dt.format('YYYY-MM-DD')
  }
  public getSermonDate(): Dayjs {
    return dayjs(this.sermonDate, 'YYYY-MM-DD')
  }
  @JsonProperty('speakerTitle', String, true)
  speakerTitle? = ''
  @JsonProperty('speakerFirstName', String)
  speakerFirstName = ''
  @JsonProperty('speakerLastName', String)
  speakerLastName = ''
  @JsonProperty('title', String)
  title = ''
  @JsonProperty('tags', [String], true)
  tags: string[] = []
  @JsonProperty('service', String, true)
  service? = ''
  @JsonProperty('fromBook', String, true)
  fromBook? = ''
  @JsonProperty('fromChapter', Number, true)
  fromChapter? = 0
  @JsonProperty('fromVerse', Number, true)
  fromVerse? = 0
  @JsonProperty('thruBook', String, true)
  thruBook? = ''
  @JsonProperty('thruChapter', Number, true)
  thruChapter? = 0
  @JsonProperty('thruVerse', Number, true)
  thruVerse? = 0
  @JsonProperty('filename', String)
  filename = ''
  @JsonProperty('uploadedTimestamp', DateConverter, true)
  uploadedTimestamp = dayjs()
  @JsonProperty('size', Number, true)
  size = 0

  speakerFirstNameValidator = (): string => {
    return !yup.string().trim().required().isValidSync(this.speakerFirstName)
      ? 'is required'
      : ''
  }

  speakerLastNameValidator = (): string => {
    return !yup.string().trim().required().isValidSync(this.speakerLastName)
      ? 'is required'
      : ''
  }

  titleValidator = (): string => {
    return !yup.string().trim().required().isValidSync(this.title)
      ? 'is required'
      : ''
  }

  isValid(): boolean {
    for (const key in this) {
      const validator = this[key]
      if (
        key.endsWith('Validator') &&
        typeof validator === 'function' &&
        validator() !== ''
      ) {
        return false
      }
    }
    return true
  }

  getFilename(bibleBooks: BibleBook[]): string {
    const dt = this.getSermonDate().format('YYYY-MM-DD')
    const svc = this.service.replace(' ', '_').toLowerCase()
    let suffix: string | null = null
    const fromBook = this.getFromBook(bibleBooks)
    if (fromBook) {
      suffix = `${fromBook.abbrev}${this.fromChapter ?? ''}v${this.fromVerse}-${
        this.thruChapter
      }v${this.thruVerse}`
    } else {
      suffix = this.title.replace(/\W/g, '')
    }
    return `${dt}_${this.speakerFirstName}_${this.speakerLastName}_${svc}_${suffix}.mp3`
  }

  getPassage(bibleBooks: BibleBook[]): string | null {
    const fromBook = this.getFromBook(bibleBooks)
    if (!fromBook) {
      return null
    }
    return `${fromBook.name} ${this.fromChapter}:${this.fromVerse}-${this.thruChapter}:${this.thruVerse}`
  }

  private getFromBook(bibleBooks: BibleBook[]): BibleBook | null {
    if (!this.fromBook) {
      return null
    }
    const fromBooks = bibleBooks.filter((book) => book.code === this.fromBook)
    return fromBooks.length > 0 ? fromBooks[0] : null
  }

  /*
   * This is necessary for the useState react hook to modify the state
   * of the form.
   */
  clone(): Sermon {
    const clone = new Sermon()
    for (const key of Object.keys(this)) {
      if (typeof this[key] === 'string' || typeof this[key] === 'number') {
        clone[key] = this[key]
      } else if (Array.isArray(this[key])) {
        clone[key] = [...this[key]]
      } else if (dayjs.isDayjs(this[key])) {
        clone[key] = this[key].clone()
      }
    }
    return clone
  }
}
