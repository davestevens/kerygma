export class BibleBook {
  name: string
  abbrev: string
  code: string
  chapters: number[]
}
