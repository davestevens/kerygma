import * as yup from 'yup'
import { User } from './user'
import { JsonObject, JsonProperty } from 'json2typescript'

const passwordPattern = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).*$/

@JsonObject('UserForm')
export class UserForm extends User {
  [key: string]: string | (() => string) | (() => boolean) | (() => UserForm)

  @JsonProperty('password', String)
  password = ''
  @JsonProperty('passwordConfirm', String)
  passwordConfirm = ''
  mode: 'new' | 'edit' = 'new'

  // Field Validators
  firstNameValidator = (): string => {
    return yup.string().trim().required().isValidSync(this.firstName)
      ? ''
      : 'is required'
  }

  lastNameValidator = (): string => {
    return yup.string().trim().required().isValidSync(this.lastName)
      ? ''
      : 'is required'
  }

  usernameValidator = (): string => {
    if (!yup.string().trim().required().isValidSync(this.username)) {
      return 'is required'
    }
    return yup.string().min(8).isValidSync(this.username)
      ? ''
      : 'must be at least 8 characters'
  }

  passwordValidator = (): string => {
    if (this.mode === 'edit' && !this.isPasswordSet()) {
      // not required for edit mode
      return ''
    }
    if (!yup.string().trim().required().isValidSync(this.password)) {
      return 'is required'
    }
    if (!yup.string().trim().min(8).isValidSync(this.password)) {
      return 'must be at least 8 characters'
    }
    return yup
      .string()
      .trim()
      .matches(passwordPattern)
      .isValidSync(this.password)
      ? ''
      : 'must contain uppercase, lowercase, and numeric characters'
  }

  passwordConfirmValidator = (): string => {
    if (this.mode === 'edit' && !this.isPasswordSet()) {
      // not required for edit mode
      return ''
    }
    return this.password === this.passwordConfirm ? '' : 'does not match'
  }

  authLevelValidator = (): string => {
    return this.authLevel === 'admin' || this.authLevel === 'sysadmin'
      ? ''
      : 'invalid auth level'
  }

  isValid(): boolean {
    for (const key in this) {
      const validator = this[key]
      if (
        key.endsWith('Validator') &&
        typeof validator === 'function' &&
        validator() !== ''
      ) {
        return false
      }
    }
    return true
  }

  isPasswordSet(): boolean {
    return this.password.trim().length > 0
  }

  /*
   * This is necessary for the useState react hook to modify the state
   * of the form.
   */
  clone(): UserForm {
    const clone = new UserForm()
    for (const key of Object.keys(this)) {
      if (typeof this[key] === 'string') {
        clone[key] = this[key]
      }
    }
    return clone
  }
}
