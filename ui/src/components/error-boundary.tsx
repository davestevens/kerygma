import * as React from 'react'
import { Alert, Classes } from '@blueprintjs/core'
import UnauthorizedError from '../model/unauthorized-error'
import LoginDialog from './church-admin/login-dialog'
import DarkModeContext from './church-admin/dark-mode-context'

export default class ErrorBoundary extends React.Component<
  {
    error?: Error
    clearError: () => void
    onLogin?: () => void
  },
  { showError: boolean; showLogin: boolean }
> {
  constructor(props) {
    super(props)
    this.state = { showError: false, showLogin: false }
  }

  static getDerivedStateFromProps(props) {
    if (props.error) {
      if (props.error instanceof UnauthorizedError) {
        return { showLogin: true }
      } else {
        console.error(
          'ErrorBoundary was passed the following error. Showing error alert.'
        )
        console.error(props.error)
        return { showError: true }
      }
    }
    return { showLogin: false, showError: false }
  }

  componentDidCatch(error, info) {
    console.error(
      'ErrorBoundary caught the following error. Showing error alert.'
    )
    console.error(error)
    console.error(info)
    this.setState({ showError: true })
  }

  cancel = () => {
    this.props.clearError()
  }

  render() {
    return (
      <>
        <DarkModeContext.Consumer>
          {(isDarkMode) => (
            <Alert
              isOpen={this.state.showError}
              onConfirm={this.cancel}
              icon="error"
              className={isDarkMode ? Classes.DARK : ''}
            >
              Oops! Something went wrong with that last request.
            </Alert>
          )}
        </DarkModeContext.Consumer>
        <LoginDialog
          isOpen={this.state.showLogin}
          onLogin={this.props.onLogin}
        />
        {this.props.children}
      </>
    )
  }
}
