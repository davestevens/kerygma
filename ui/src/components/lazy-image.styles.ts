import { globalConstants } from '../common/global-constants'
import preset from 'jss-preset-default'
import jss, { Styles } from 'jss'
/*
 * full = 100%
 * wide = full size of reading area with rounded corners
 * half = 50% of reading area with rounded corners
 */

const containerBase = {
  display: 'flex !important',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  backgroundSize: 'cover !important',
  backgroundPosition: 'center !important',
}

jss.setup(preset())
export const createClasses = (objectPosition?: string) => {
  const imageBase: Partial<Styles> = {
    objectFit: 'cover',
    width: '100%',
    height: '100%',
  }
  if (objectPosition) {
    imageBase.objectPosition = objectPosition
  }

  const { classes } = jss
    .createStyleSheet({
      containerFull: {
        ...containerBase,
        width: '100%',
        '& img': imageBase,
      },
      containerPadding: `
      display: flex;
      padding: 0 16px;
    `,
      containerWide: {
        ...containerBase,
        maxWidth: globalConstants.textMaxWidth,
        borderRadius: '4px',
        '& img': imageBase,
      },
      containerHalf: {
        ...containerBase,
        width: 'calc(${globalConstants.textMaxWidth} / 2)',
        borderRadius: '4px',
        '& img': imageBase,
      },
    })
    .attach()
  return classes
}
