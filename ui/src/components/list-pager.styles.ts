import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  buttonRow: `
    display: flex;
    justify-content: flex-end;
    align-items: center;
  `,
  label: `
    font-size: 12px;
    margin-right: 8px;
  `,
  range: `
    font-size: 12px;
    margin: 0 4px 0 8px;
  `,
  button: `
    margin-left: 4px;
  `,
})
