import { globalConstants } from '../common/global-constants'
import { createUseStyles } from 'react-jss'
import { KerygmaTheme } from '../model/kerygma-configuration'

export const useStylesThemed = (theme: KerygmaTheme) => {
  return createUseStyles({
    footer: `
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      width: 100%;
      margin-top: auto;
      min-height: 300px;
      background-color: ${theme.colors.footerBackground};
      color: ${theme.colors.footerFont};
      font-weight: ${theme.fonts.weightFooter};
    `,
    address: {
      fontSize: '14px',
      marginBottom: '8px',
      textAlign: 'center',
      [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
        fontSize: '16px',
      },
    },
    addressPart: {
      display: 'inline-block',
      [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
        display: 'block',
      },
    },
    spacer: {
      margin: '0 15px',
      display: 'inline-block',
      [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
        display: 'none',
      },
    },
    contactLink: {
      display: 'inline',
    },
    obfuscate: {
      color: theme.colors.footerFont,
      textDecoration: 'none',
      '&:hover': {
        color: theme.colors.footerFontLink,
      },
    },
    footerText: {
      fontSize: '12px',
      marginBottom: '8px',
      textAlign: 'center',
      [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
        fontSize: '14px',
        padding: '0 16px',
      },
    },
    attributionText: {
      fontSize: '12px',
      textAlign: 'center',
      [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
        fontSize: '14px',
        padding: '0 16px',
      },
    },
    attributionLink: {
      color: theme.colors.footerFont,
      textDecoration: 'none',
      '&:hover': {
        color: theme.colors.footerFontLink,
      },
    },
  })
}
