import * as React from 'react'
import { classes } from './location-map.styles'
import L from 'leaflet'
import { kerygmaConfig } from '../common/declare-build-constants'

class LocationMap extends React.Component {
  componentDidMount() {
    const { worshipLocation } = kerygmaConfig
    const loc = worshipLocation.location
    const map = new L.Map('__kerygma_loc_map__', {
      touchZoom: true,
      dragging: false,
      tap: false,
      scrollWheelZoom: false,
      center: new L.LatLng(loc.lat, loc.lng),
      zoom: worshipLocation.zoom,
    })
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution:
        '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    }).addTo(map)
    L.marker([loc.lat, loc.lng]).addTo(map)
  }

  render() {
    return (
      <div id="__kerygma_loc_map__" className={classes.mapContainerRow}></div>
    )
  }
}

export default LocationMap
