import * as React from 'react'
import { useStylesThemed } from './header-nav.styles'
import { Button, Menu, MenuItem, Popover, Position } from '@blueprintjs/core'
import {
  KerygmaConfiguration,
  Navigation,
} from '../model/kerygma-configuration'
import { Classes } from 'jss'

const NavigationMenuItems: React.FC<{
  navigations: Navigation[]
  classes: Classes<'navLink' | 'navMenu'>
  pathHome: string
}> = ({ navigations, classes, pathHome }) => {
  return (
    <Menu className={classes.navMenu}>
      {navigations.map((navigation, i) => {
        if (navigation.path) {
          const path = `${pathHome}${navigation.path}`
          return <MenuItem text={navigation.label} href={path} key={i} />
        } else {
          return (
            <MenuItem text={navigation.label} key={i}>
              <NavigationMenuItems
                navigations={navigation.children}
                classes={classes}
                pathHome={pathHome}
              />
            </MenuItem>
          )
        }
      })}
    </Menu>
  )
}

const NavigationItems: React.FC<{
  navigations: Navigation[]
  classes: Classes<'navLink' | 'navMenu'>
  pathHome: string
}> = ({ navigations, classes, pathHome }) => {
  return (
    <>
      {navigations.map((navigation, i) => {
        if (navigation.path) {
          const path = `${pathHome}${navigation.path}`
          return (
            <a className={classes.navLink} href={path} key={i}>
              {navigation.label}
            </a>
          )
        } else {
          const childrenMenu = (
            <NavigationMenuItems
              navigations={navigation.children}
              classes={classes}
              pathHome={pathHome}
            />
          )
          return (
            <Popover
              content={childrenMenu}
              position={Position.BOTTOM_LEFT}
              modifiers={{ arrow: { enabled: false } }}
              usePortal={false}
              key={i}
            >
              <a className={classes.navLink}>{navigation.label}</a>
            </Popover>
          )
        }
      })}
    </>
  )
}

const HeaderNav: React.FC<{
  assetsRoot: string
  config: KerygmaConfiguration
  pathHome: string
}> = ({ assetsRoot, config, pathHome }) => {
  const classes = useStylesThemed(config.theme)()
  const fullMenu = (
    <NavigationMenuItems
      navigations={config.navigation}
      classes={classes}
      pathHome={pathHome}
    />
  )

  return (
    <div className={classes.container}>
      <div className={classes.logoRow}>
        <a href={`${pathHome}index.html`}>
          <img
            alt="Church Logo"
            src={`${pathHome}${assetsRoot}${config.logo.path}`}
            style={{ height: config.logo.height }}
          />
        </a>
        <div className={classes.navButton}>
          <Popover content={fullMenu} position={Position.BOTTOM_LEFT}>
            <Button icon="menu" text="Menu" />
          </Popover>
        </div>
      </div>
      <div className={classes.navRow}>
        <NavigationItems
          navigations={config.navigation}
          classes={classes}
          pathHome={pathHome}
        />
      </div>
    </div>
  )
}

export default HeaderNav
