import { Classes, Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

const header = {
  fontFamily: "'Ubuntu', sans-serif",
  textTransform: 'none',
  margin: 0,
}

export const useStyles = createUseStyles({
  pageContainer: {
    display: 'flex',
    flexDirection: 'column',
    fontSize: '14px',
    minHeight: '100vh',
    fontFamily: "'Ubuntu', sans-serif",
    [`&.${Classes.DARK}`]: `
      background-color: ${Colors.DARK_GRAY2};
    `,
    [`&.${Classes.DARK} .admin-footer`]: `
      border-top: 1px solid ${Colors.GRAY1};
      background-color: ${Colors.DARK_GRAY5};
    `,
  },
  '@global': {
    body: `
      margin: 0;
    `,
    [`.${Classes.DARK}`]: {
      '& .card-header': `
        background-color: ${Colors.DARK_GRAY5};
        border: 1px solid #555;
      `,
    },
    h1: { ...header, fontSize: '24px' },
    h2: { ...header, fontSize: '22px' },
    h3: { ...header, fontSize: '20px' },
    h4: { ...header, fontSize: '18px' },
    h5: { ...header, fontSize: '16px', fontWeight: 'bold' },
    '.card-header': `
      border-bottom: 1px solid ${Colors.LIGHT_GRAY1};
      background-color: ${Colors.LIGHT_GRAY4};
      border-top-left-radius: 2px;
      border-top-right-radius: 2px;
    `,
    '.card-header h1': { fontSize: '18px' },
    '.card-header h2': { fontSize: '18px' },
  },
  contentBody: `
    display: flex;
    flex-direction: column;
    padding: 50px 0 20px;
  `,
  footer: `
    margin-top: auto;
    background-color: ${Colors.LIGHT_GRAY4};
    padding: 8px 16px;
    border-top: 1px solid ${Colors.LIGHT_GRAY1};
    font-size: 12px;
    font-style: italic;
  `,
})
