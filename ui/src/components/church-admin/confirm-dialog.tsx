import * as React from 'react'
import DarkModeContext from './dark-mode-context'
import { Button, Classes, Dialog } from '@blueprintjs/core'

const ConfirmDialog: React.FC<{
  isOpen: boolean
  isProcessing?: boolean
  title: string
  message: string
  action: string
  processingAction?: string
  onConfirm: () => void
  onCancel: () => void
}> = ({
  isOpen,
  isProcessing,
  title,
  message,
  action,
  processingAction,
  onConfirm,
  onCancel,
}) => {
  const isDarkMode = React.useContext<boolean>(DarkModeContext)

  return (
    <Dialog
      isOpen={isOpen}
      title={title}
      className={isDarkMode ? Classes.DARK : ''}
      onClose={onCancel}
    >
      <div className={Classes.DIALOG_BODY}>
        <p className={Classes.RUNNING_TEXT}>{message}</p>
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button onClick={onCancel} disabled={isProcessing === true}>
            Cancel
          </Button>
          <Button
            onClick={onConfirm}
            disabled={isProcessing === true}
            intent="primary"
          >
            {isProcessing === true ? processingAction : action}
          </Button>
        </div>
      </div>
    </Dialog>
  )
}

export default ConfirmDialog
