import { Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  breadcrumbs: `
    padding: 8px 16px;
    border-bottom: 1px solid ${Colors.LIGHT_GRAY1};
    margin-bottom: 16px;
  `,
})
