import * as React from 'react'
import { useStyles } from './page-form.styles'

const PageForm: React.FC = ({ children }) => {
  const classes = useStyles()
  return <div className={classes.formContainer}>{children}</div>
}

export default PageForm
