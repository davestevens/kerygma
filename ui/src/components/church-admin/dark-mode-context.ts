import * as React from 'react'

const DarkModeContext = React.createContext<boolean>(false)

export default DarkModeContext
