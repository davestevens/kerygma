import * as React from 'react'
import AdminNavbar from './admin-navbar'
import AdminBreadcrumbs from './admin-breadcrumbs'
import { useStyles } from './admin-page.styles'
import { globalConstants } from '../../common/global-constants'
import { BreadcrumbProps, Classes } from '@blueprintjs/core'
import { Helmet } from 'react-helmet'
import ErrorBoundary from '../error-boundary'
import container from '../../di-container'
import LocalStorageDataAccess from '../../services/local-storage-data-access'
import DarkModeContext from './dark-mode-context'
import { User } from '../../model/user'

const localStorageDataAccess = container.resolve(LocalStorageDataAccess)
const kerygmaVersion = container.resolve<string>('kerygmaVersion')
const initDarkMode = localStorageDataAccess.getDarkMode()

const AdminPage: React.FC<{
  assetsRoot: string
  breadcrumbs?: BreadcrumbProps[]
  error?: Error
  clearError: () => void
  onLogin?: () => void
  loginUser?: User
  hideNavLinks?: boolean
}> = ({
  breadcrumbs,
  error,
  clearError,
  onLogin,
  children,
  loginUser,
  hideNavLinks,
}) => {
  // replace this with local storage
  const [isDarkMode, setIsDarkMode] = React.useState<boolean>(initDarkMode)

  const classes = useStyles()

  const onSwitchMode = () => {
    localStorageDataAccess.saveDarkMode(!isDarkMode)
    setIsDarkMode(!isDarkMode)
  }

  return (
    <div className={`${classes.pageContainer} ${isDarkMode && Classes.DARK}`}>
      <DarkModeContext.Provider value={isDarkMode}>
        <ErrorBoundary error={error} clearError={clearError} onLogin={onLogin}>
          <Helmet>
            <meta charSet="utf-8" />
            <title>Church Admin - {globalConstants.churchAdmin.appName}</title>
            <meta
              name="description"
              content="This is where you manage your church content."
            />
            <meta name="robots" content="noindex" />
          </Helmet>
          <AdminNavbar
            hideNavLinks={hideNavLinks}
            isDarkMode={isDarkMode}
            onSwitchMode={onSwitchMode}
            loginUser={loginUser}
          />
          <div className={classes.contentBody}>
            {breadcrumbs && <AdminBreadcrumbs items={breadcrumbs} />}
            {children}
          </div>
          <div className={`${classes.footer} admin-footer`}>
            Powered by{' '}
            <a href={globalConstants.kerygmaUrl}>
              {globalConstants.churchAdmin.appName}
            </a>{' '}
            vs. {kerygmaVersion}.
          </div>
        </ErrorBoundary>
      </DarkModeContext.Provider>
    </div>
  )
}

export default AdminPage
