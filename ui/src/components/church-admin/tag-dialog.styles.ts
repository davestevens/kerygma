import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  tagContainer: `
    display: flex;
    flex-direction: column;
    padding: 16px 0;
  `,
  tagRow: `
    display: flex;
    align-items: center;
    padding: 8px;
    font-size: 14px;
  `,
  tagSelect: `
    display: flex;
    margin-bottom: 0;
  `,
  tagCount: `
    margin-left: auto;
    text-align: right;
  `,
  tagCountLabel: `
    margin-left: 4px;
  `,
})
