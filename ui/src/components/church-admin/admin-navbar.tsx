import * as React from 'react'
import container from '../../di-container'
import { useStyles } from './admin-navbar.styles'
import {
  Navbar,
  NavbarHeading,
  Classes,
  NavbarDivider,
  Button,
  NavbarGroup,
  AnchorButton,
  Icon,
} from '@blueprintjs/core'
import AuthDataAccess from '../../services/auth-data-access'
import { User } from '../../model/user'

const authDataAccess = container.resolve(AuthDataAccess)

const AdminNavbar: React.FC<{
  isDarkMode: boolean
  onSwitchMode: () => void
  loginUser?: User
  hideNavLinks?: boolean
}> = ({ isDarkMode, onSwitchMode, loginUser, hideNavLinks }) => {
  const [isProcessing, setIsProcessing] = React.useState<boolean>(false)

  const classes = useStyles()

  const onLogout = async () => {
    setIsProcessing(true)
    await authDataAccess.logout()
    setIsProcessing(false)
    window.location.href = 'logged-out.html'
  }

  const loginUserSection = (
    <div className={`${classes.user} ${Classes.TEXT_MUTED}`}>
      {loginUser !== undefined && (
        <>
          <Icon icon="person" size={10} className={classes.icon} />
          {loginUser?.firstName} {loginUser?.lastName}
        </>
      )}
    </div>
  )

  return (
    <Navbar
      fixedToTop={true}
      className={`${Classes.DARK} ${classes.navbarCustom}`}
    >
      <div className={classes.navContainer}>
        <NavbarGroup align="left">
          <NavbarHeading>
            <a className={classes.homeLink} href="index.html">
              <span className={classes.homeLinkSpan}>Church Admin</span>
            </a>
          </NavbarHeading>
          {!hideNavLinks && (
            <>
              <NavbarDivider />
              <AnchorButton
                href="sermons.html"
                icon="headset"
                text="Sermons"
                minimal={true}
              />
              <AnchorButton
                href="documents.html"
                icon="document"
                text="Documents"
                minimal={true}
              />
              {loginUser && loginUser.authLevel === 'sysadmin' && (
                <AnchorButton
                  href="users.html"
                  icon="people"
                  text="Users"
                  minimal={true}
                />
              )}
            </>
          )}
        </NavbarGroup>
        <NavbarGroup align="right">
          <Button
            icon={isDarkMode ? 'flash' : 'moon'}
            minimal={true}
            className={classes.buttonRM}
            title={isDarkMode ? 'Light Mode' : 'Dark Mode'}
            onClick={onSwitchMode}
            outlined={false}
          />
          {!hideNavLinks && (
            <>
              {loginUserSection}
              <Button
                icon="log-out"
                text={isProcessing ? 'Logging out...' : 'Logout'}
                minimal={true}
                onClick={onLogout}
                disabled={isProcessing}
              />
            </>
          )}
        </NavbarGroup>
      </div>
    </Navbar>
  )
}

export default AdminNavbar
