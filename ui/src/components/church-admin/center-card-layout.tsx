import * as React from 'react'
import { useStyles } from './center-card-layout.styles'
import { BreadcrumbProps, Breadcrumbs } from '@blueprintjs/core'

const CenterCardLayout: React.FC<{ breadcrumbs?: BreadcrumbProps[] }> = ({
  breadcrumbs,
  children,
}) => {
  const classes = useStyles()
  return (
    <div className={classes.centerContainer}>
      <div
        className={`
          ${classes.centerContent}
          ${breadcrumbs && classes.centerContentBreadcrumb}
        `}
      >
        {breadcrumbs && (
          <div className={classes.breadcrumbs}>
            <Breadcrumbs items={breadcrumbs} />
          </div>
        )}
        {children}
      </div>
    </div>
  )
}

export default CenterCardLayout
