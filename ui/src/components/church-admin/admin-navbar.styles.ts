import { Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  homeLink: {
    '&:hover': `
      text-decoration: none;
    `,
  },
  homeLinkSpan: `
    color: ${Colors.LIGHT_GRAY5};
  `,
  navbarCustom: `
    box-shadow: none !important;
  `,
  navContainer: `
    display: flex;
    justify-content: space-between;
  `,
  buttonRM: `
    margin-right: 16px;
  `,
  user: `
    display: flex;
    font-size: 12px;
    margin-right: 16px;
    align-items: center;
  `,
  icon: `
    margin-right: 8px;
  `,
})
