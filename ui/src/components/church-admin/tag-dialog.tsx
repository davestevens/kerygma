import React from 'react'
import { SermonTag } from '../../model/sermon-tag'
import DarkModeContext from './dark-mode-context'
import {
  Button,
  Checkbox,
  Classes,
  Dialog,
  DialogBody,
  DialogFooter,
} from '@blueprintjs/core'
import { useStyles } from './tag-dialog.styles'

interface Props {
  tags: SermonTag[]
  isOpen: boolean
  onCancel: () => void
  onApply: () => void
  onToggleTag: (i: number) => void
}

const TagDialog: React.FC<Props> = ({
  tags,
  isOpen,
  onCancel,
  onApply,
  onToggleTag,
}) => {
  const classes = useStyles()

  return (
    <DarkModeContext.Consumer>
      {(isDarkMode) => (
        <Dialog
          isOpen={isOpen}
          title="Filter by Tag"
          className={isDarkMode ? Classes.DARK : ''}
          onClose={onCancel}
        >
          <DialogBody useOverflowScrollContainer>
            {tags.map((tag, i) => (
              <div key={i} className={classes.tagRow}>
                <Checkbox
                  className={classes.tagSelect}
                  inline
                  checked={tag.isSelected}
                  onChange={() => onToggleTag(i)}
                >
                  {tag.tag}
                </Checkbox>
                <div className={classes.tagCount}>
                  {tag.sermonCount}
                  <span
                    className={`${classes.tagCountLabel} bp5-text-muted bp5-text-small`}
                  >
                    sermons
                  </span>
                </div>
              </div>
            ))}
          </DialogBody>
          <DialogFooter
            actions={
              <>
                <Button onClick={() => onCancel()}>Cancel</Button>
                <Button onClick={() => onApply()} intent="primary">
                  Apply
                </Button>
              </>
            }
          />
        </Dialog>
      )}
    </DarkModeContext.Consumer>
  )
}

export default TagDialog
