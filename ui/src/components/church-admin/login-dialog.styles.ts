import { Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  error: `
    color: ${Colors.RED2};
    font-size: 14px;
    margin-bottom: 16px;
  `,
  loginButton: `
    margin-bottom: 16px;
  `,
})
