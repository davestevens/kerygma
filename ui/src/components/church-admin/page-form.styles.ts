import { globalConstants } from '../../common/global-constants'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  formContainer: `
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    width: 100%;
    max-width: ${globalConstants.bp.phoneMax};
  `,
})
