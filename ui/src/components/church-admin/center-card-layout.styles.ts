import { globalConstants } from '../../common/global-constants'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  centerContainer: `
    display: flex;
    justify-content: center;
    padding: 0 16px;
  `,
  centerContent: `
    display: flex;
    flex-direction: column;
    max-width: ${globalConstants.bp.phoneMax};
    width: 100%;
    margin-top: 30px;
  `,
  centerContentBreadcrumb: `
    margin-top: 16px;
  `,
  breadcrumbs: `
    padding: 0 0 8px 0;
  `,
})
