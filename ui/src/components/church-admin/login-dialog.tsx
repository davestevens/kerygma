import React, { useEffect } from 'react'
import { Button, Classes, Dialog } from '@blueprintjs/core'
import ValidationFormGroup from './validation-form-group'
import { LoginForm } from '../../model/login-form'
import container from '../../di-container'
import AuthDataAccess from '../../services/auth-data-access'
import LocalStorageDataAccess from '../../services/local-storage-data-access'
import { useStyles } from './login-dialog.styles'
import DarkModeContext from './dark-mode-context'
import { StatusCodes } from 'http-status-codes'

const authDataAccess = container.resolve(AuthDataAccess)
const localStorageDataAccess = container.resolve(LocalStorageDataAccess)

const LoginDialog: React.FC<{
  isOpen: boolean
  onLogin: () => void
}> = ({ isOpen, onLogin }) => {
  const classes = useStyles()
  const [loginForm, setLoginForm] = React.useState<LoginForm>(new LoginForm())
  const [isProcessing, setIsProcessing] = React.useState<boolean>(false)
  const [usernameRef, setUsernameRef] = React.useState<HTMLInputElement | null>(
    null
  )
  const [errorMessage, setErrorMessage] = React.useState<string>('')
  const isDarkMode = React.useContext<boolean>(DarkModeContext)

  const onFieldChange = (id: string, newValue: string) => {
    loginForm[id] = newValue
    setLoginForm(loginForm.clone())
    setErrorMessage('')
  }

  const handleLogin = async () => {
    setErrorMessage('')
    setIsProcessing(true)
    const token = await authDataAccess.login(
      loginForm.username,
      loginForm.password
    )
    setIsProcessing(false)
    switch (token) {
      case StatusCodes.FORBIDDEN:
        setErrorMessage('Incorrect username or password.')
        break
      case StatusCodes.LOCKED:
        setErrorMessage(
          'Username has been suspended for 30 minutes due to failed login attempts.'
        )
        break
      default: {
        localStorageDataAccess.saveToken(token)
        onLogin()
      }
    }
  }

  const focusOnUsername = () => {
    if (usernameRef) {
      usernameRef.focus()
    }
  }

  useEffect(() => {
    if (isOpen) {
      authDataAccess.generateCsrfToken().then((csrfToken) => {
        localStorageDataAccess.saveCsrfToken(csrfToken)
      })
    }
  }, [isOpen])

  return (
    <Dialog
      isOpen={isOpen}
      title="Church Admin Login"
      isCloseButtonShown={false}
      onOpened={focusOnUsername}
      className={isDarkMode ? Classes.DARK : ''}
    >
      <form className={Classes.DIALOG_BODY} style={{ marginBottom: 0 }}>
        <ValidationFormGroup
          id="username"
          value={loginForm.username}
          validator={() => {
            return loginForm.usernameValidator()
          }}
          setter={onFieldChange}
          placeholder="Username"
          leftIcon="person"
          setInputRef={setUsernameRef}
        />
        <ValidationFormGroup
          id="password"
          value={loginForm.password}
          validator={() => {
            return loginForm.passwordValidator()
          }}
          setter={onFieldChange}
          placeholder="Password"
          leftIcon="lock"
          inputType="password"
        />
        {errorMessage !== '' && (
          <div className={classes.error}>{errorMessage}</div>
        )}
        <Button
          text={isProcessing ? 'Logging In...' : 'Log In'}
          onClick={handleLogin}
          disabled={!loginForm.isValid() || isProcessing}
          fill={true}
          large={true}
          intent="primary"
          type="submit"
          className={classes.loginButton}
        />
      </form>
    </Dialog>
  )
}

export default LoginDialog
