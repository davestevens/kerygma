import * as React from 'react'
import { useStyles } from './admin-breadcrumbs.styles'
import { BreadcrumbProps, Breadcrumbs } from '@blueprintjs/core'

const AdminBreadcrumbs: React.FC<{ items: BreadcrumbProps[] }> = ({
  items,
}) => {
  const classes = useStyles()
  return (
    <div className={classes.breadcrumbs}>
      <Breadcrumbs items={items} />
    </div>
  )
}

export default AdminBreadcrumbs
