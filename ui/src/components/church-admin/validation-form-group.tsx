import React, { FormEvent } from 'react'
import {
  Button,
  Classes,
  FormGroup,
  InputGroup,
  Tag,
  Tooltip,
} from '@blueprintjs/core'
import { useStyles } from './validation-form-group.styles'
import { IconName } from '@blueprintjs/core/lib/esm/components/icon/icon'
import DarkModeContext from './dark-mode-context'

const ValidationFormGroup: React.FC<{
  id: string
  value: string
  validator?: () => string
  setter: (id: string, newValue: string) => void
  label?: string
  labelInfo?: string
  helperText?: string
  inputType?: 'password'
  placeholder?: string
  leftIcon?: IconName
  setInputRef?: (elem: HTMLInputElement) => void
  isLoading?: boolean
  rightText?: string
  noFill?: boolean
  hideFormGroup?: boolean
  className?: string
}> = ({
  id,
  value,
  validator,
  setter,
  label,
  labelInfo,
  helperText,
  inputType,
  placeholder,
  leftIcon,
  setInputRef,
  isLoading,
  rightText,
  noFill,
  hideFormGroup,
  className,
}) => {
  const classes = useStyles()
  const [showPassword, setShowPassword] = React.useState<boolean>(false)
  const [errorMessage, setErrorMessage] = React.useState<string>('')
  const helper = errorMessage === '' ? helperText : errorMessage
  const isDarkMode = React.useContext<boolean>(DarkModeContext)
  const errorClass = isDarkMode
    ? classes.errorMessageDark
    : classes.errorMessage

  const handleLockClick = () => {
    if (showPassword) {
      setShowPassword(false)
    } else {
      setShowPassword(true)
    }
  }

  const toolTip = (
    <div className={classes.toolTip}>
      {`${showPassword ? 'Hide' : 'Show'} Password`}
    </div>
  )

  const showButton = (
    <Tooltip content={toolTip} usePortal={false}>
      <Button
        icon={showPassword ? 'eye-open' : 'eye-off'}
        minimal={true}
        tabIndex={999}
        onClick={handleLockClick}
      />
    </Tooltip>
  )

  const onChangeInput = async (event: FormEvent<HTMLInputElement>) => {
    setter(id, event.currentTarget.value)
    const errorMessage = validator ? validator() : ''
    if (errorMessage !== '') {
      const prefix = label ? label : placeholder
      setErrorMessage(`${prefix} ${errorMessage}`)
    } else {
      setErrorMessage('')
    }
  }

  const fieldType =
    inputType === 'password' ? (showPassword ? 'text' : 'password') : 'text'

  const rightTag =
    inputType === 'password' ? (
      showButton
    ) : rightText ? (
      <Tag minimal={true}>{rightText}</Tag>
    ) : undefined

  const inputGroup = (
    <InputGroup
      id={id}
      onChange={onChangeInput}
      intent={errorMessage ? 'danger' : 'none'}
      value={value}
      type={fieldType}
      rightElement={rightTag}
      fill={!noFill}
      placeholder={placeholder}
      leftIcon={leftIcon}
      inputRef={(input) => {
        if (setInputRef) {
          setInputRef(input)
        }
        return input
      }}
    />
  )

  if (hideFormGroup) {
    return (
      <div className={className}>
        {inputGroup}
        {errorMessage && <div className={errorClass}>{errorMessage}</div>}
      </div>
    )
  }

  return (
    <FormGroup
      label={label}
      labelInfo={labelInfo}
      intent={errorMessage ? 'danger' : 'none'}
      helperText={helper}
      className={`
        ${classes.formGroup}
        ${isLoading && Classes.SKELETON}
        ${className ?? ''}
      `}
    >
      {inputGroup}
    </FormGroup>
  )
}

export default ValidationFormGroup
