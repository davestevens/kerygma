import { Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  formGroup: `
    width: 100%;
  `,
  toolTip: `
    font-size: 12px;
  `,
  errorMessage: `
    font-size: 12px;
    color: ${Colors.RED3};
    margin-top: 5px;
  `,
  errorMessageDark: `
    font-size: 12px;
    color: ${Colors.RED5};
    margin-top: 5px;
  `,
})
