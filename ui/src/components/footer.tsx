import * as React from 'react'
import { useStylesThemed } from './footer.styles'
import { parsePhoneNumberFromString } from 'libphonenumber-js'
import Obfuscate from 'react-obfuscate'
import { KerygmaConfiguration } from '../model/kerygma-configuration'
import { globalConstants } from '../common/global-constants'

const Footer: React.FC<{
  config: KerygmaConfiguration
  kerygmaVersion: string
}> = ({ config, kerygmaVersion }) => {
  const classes = useStylesThemed(config.theme)()
  const phoneNumber = parsePhoneNumberFromString(config.phone, 'US')

  return (
    <div className={classes.footer}>
      <div className={classes.address}>
        <span className={classes.addressPart}>
          {config.worshipLocation.street1},&nbsp;
          {config.worshipLocation.street2 && (
            <>{config.worshipLocation.street2}&nbsp;</>
          )}
          {config.worshipLocation.city}&nbsp;
          {config.worshipLocation.state}&nbsp;
          {config.worshipLocation.postalCode}
        </span>
        {phoneNumber !== undefined && (
          <div className={classes.addressPart}>
            <div className={classes.spacer}>•</div>
            <div className={classes.contactLink}>
              <Obfuscate
                href={phoneNumber.getURI()}
                className={classes.obfuscate}
              >
                {phoneNumber.formatNational()}
              </Obfuscate>
            </div>
          </div>
        )}
        <div className={classes.addressPart}>
          <div className={classes.spacer}>•</div>
          <div className={classes.contactLink}>
            <Obfuscate email={config.email} className={classes.obfuscate}>
              {config.email}
            </Obfuscate>
          </div>
        </div>
      </div>
      <div className={classes.footerText}>{config.footerText}</div>
      <div className={classes.attributionText}>
        Powered by{' '}
        <a
          href={globalConstants.kerygmaUrl}
          className={classes.attributionLink}
        >
          {globalConstants.churchAdmin.appName}
        </a>{' '}
        vs. {kerygmaVersion}.
      </div>
    </div>
  )
}

export default Footer
