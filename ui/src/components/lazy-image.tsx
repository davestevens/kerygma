import * as React from 'react'
import { createClasses } from './lazy-image.styles'
import { LazyLoadImage } from 'react-lazy-load-image-component'

const LazyImage: React.FC<{
  src: string
  ht: string
  width: 'full' | 'wide' | 'half'
  position?: string
}> = ({ src, ht, width, position }) => {
  const classes = createClasses(position)
  const containerClass =
    width === 'half'
      ? classes.containerHalf
      : width === 'wide'
      ? classes.containerWide
      : classes.containerFull
  const placeholder = src
    .replace('.jpg', '-placeholder.jpg')
    .replace('.jpeg', '-placeholder.jpeg')
  const image = (
    <LazyLoadImage
      src={src}
      placeholderSrc={placeholder}
      effect="blur"
      threshold={100}
      height={ht}
      wrapperClassName={containerClass}
    />
  )
  return (
    <>
      {width === 'full' ? (
        image
      ) : (
        <div className={classes.containerPadding}>{image}</div>
      )}
    </>
  )
}

export default LazyImage
