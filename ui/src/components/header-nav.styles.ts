import { globalConstants } from '../common/global-constants'
import { lighten } from 'polished'
import { createUseStyles } from 'react-jss'
import { KerygmaTheme } from '../model/kerygma-configuration'

export const useStylesThemed = (theme: KerygmaTheme) => {
  return createUseStyles({
    container: `
      display: flex;
      flex-direction: column;
      align-items: center;
      width: 100%;
    `,
    logoRow: `
      display: flex;
      justify-content: center;
      align-items: center;
      height: 120px;
      width: 100%;
      background-color: ${theme.colors.headerBackground};
    `,
    navRow: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '40px',
      width: '100%',
      backgroundColor: theme.colors.primary,
      color: theme.colors.primaryText,
      [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
        height: '8px',
      },
    },
    navLink: {
      color: theme.colors.primaryText,
      padding: '0 16px',
      height: '40px',
      display: 'flex',
      alignItems: 'center',
      fontSize: '16px',
      fontWeight: theme.fonts.weightDefault,
      textDecoration: 'none',
      '&:hover': {
        textDecoration: 'none',
        color: theme.colors.primaryText,
        backgroundColor: lighten(0.1, theme.colors.primary),
      },
      [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
        display: 'none',
      },
    },
    navButton: {
      display: 'none',
      position: 'absolute',
      left: '16px',
      top: '16px',
      [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
        display: 'flex',
      },
    },
    navMenu: `
      font-size: 14px;
    `,
  })
}
