import jss from 'jss'
import preset from 'jss-preset-default'

jss.setup(preset())
export const { classes } = jss
  .createStyleSheet({
    mapContainerRow: `
      display: flex;
      width: 100%;
      min-height: 300px;
    `,
    '@global': {
      '.leaflet-attribution-flag': `
        display: none !important;
      `,
    },
  })
  .attach()
