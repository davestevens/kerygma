import * as React from 'react'
import { useStyles } from './list-pager.styles'
import { Button, Classes, HTMLSelect } from '@blueprintjs/core'
import { FormEvent, useEffect, useRef } from 'react'

export const perPageOptions = [20, 50, 100]

const ListPager: React.FC<{
  page: number
  perPage: number
  totalRecords: number
  onChange: (page: number, perPage: number) => void
}> = (props) => {
  const { totalRecords, onChange } = props
  const isMounted = useRef(false)
  const classes = useStyles()
  const [page, setPage] = React.useState<number>(props.page)
  const [perPage, setPerPage] = React.useState<number>(props.perPage)

  const onChangePerPage = (event: FormEvent<HTMLSelectElement>) => {
    const perPage = parseInt(event.currentTarget.value)
    setPerPage(perPage)
    setPage(0)
  }

  const calcRange = (): string => {
    if (totalRecords === 0) {
      return 'no records'
    }
    const from = page * perPage + 1
    const thru = Math.min(page * perPage + perPage, totalRecords)
    return `${from} - ${thru} of ${totalRecords}`
  }

  const onPrev = (): void => {
    if (page > 0) {
      setPage(page - 1)
    }
  }

  const maxPage = (): number => {
    return Math.ceil(totalRecords / perPage - 1)
  }

  const onNext = (): void => {
    if (page < maxPage()) {
      setPage(page + 1)
    }
  }

  const onFirst = (): void => {
    setPage(0)
  }

  const onLast = (): void => {
    setPage(maxPage())
  }

  useEffect(() => {
    // Using isMounted ref to not execute the onChange callback
    // when first mounting the component.
    if (isMounted.current) {
      onChange(page, perPage)
    } else {
      isMounted.current = true
    }
  }, [page, perPage])

  return (
    <div className={classes.buttonRow}>
      <label className={`${classes.label} ${Classes.TEXT_MUTED}`}>
        Items per page:
      </label>
      <HTMLSelect
        onChange={onChangePerPage}
        options={perPageOptions}
        value={perPage}
      />
      <span className={`${classes.range} ${Classes.TEXT_MUTED}`}>
        {calcRange()}
      </span>
      <Button
        icon="chevron-backward"
        className={classes.button}
        onClick={onFirst}
        disabled={page === 0}
      />
      <Button
        icon="chevron-left"
        className={classes.button}
        onClick={onPrev}
        disabled={page === 0}
      />
      <Button
        icon="chevron-right"
        className={classes.button}
        onClick={onNext}
        disabled={page >= maxPage()}
      />
      <Button
        icon="chevron-forward"
        className={classes.button}
        onClick={onLast}
        disabled={page >= maxPage()}
      />
    </div>
  )
}

export default ListPager
