import PageFrontmatter from '../common/page-frontmatter'
import { KerygmaConfiguration } from '../model/kerygma-configuration'

interface LayoutProps {
  frontmatter?: PageFrontmatter
  assetsRoot?: string
  config?: KerygmaConfiguration
  kerygmaVersion?: string
  pathHome?: string
}

export default LayoutProps
