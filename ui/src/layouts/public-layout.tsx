import * as React from 'react'
import LayoutProps from './layout-props'
import { useStylesThemed } from './public-layout.styles'
import Footer from '../components/footer'
import HeaderNav from '../components/header-nav'
import { Helmet } from 'react-helmet'

const PublicLayout: React.FC<LayoutProps> = ({
  children,
  frontmatter,
  assetsRoot,
  config,
  kerygmaVersion,
  pathHome,
}) => {
  const classes = useStylesThemed(config.theme)()
  const bodyClasses =
    frontmatter?.suppressBottomMargin === true
      ? classes.contentBody
      : `${classes.contentBody} ${classes.padContentBody}`
  const title = frontmatter?.title
    ? `${frontmatter?.title} - ${config.churchNameShort}`
    : config.churchNameShort
  const assetsPath = `${pathHome}${assetsRoot}`
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>{title}</title>
        {frontmatter.description && (
          <meta name="description" content={frontmatter.description} />
        )}
        <link
          rel="apple-touch-icon"
          href={`${assetsPath}${config.favicon.path}`}
        />
        <link
          rel="shortcut icon"
          type={config.favicon.fileType}
          href={`${assetsPath}${config.favicon.path}`}
        />
        <style type="text/css">{`
          @import url('${config.theme.fonts.default.url}');
          @import url('${config.theme.fonts.titles.url}');
        `}</style>
      </Helmet>
      <div className={classes.container}>
        <HeaderNav
          assetsRoot={assetsRoot}
          config={config}
          pathHome={pathHome}
        />
        <div className={bodyClasses}>{children}</div>
        <Footer config={config} kerygmaVersion={kerygmaVersion} />
      </div>
    </>
  )
}

export default PublicLayout
