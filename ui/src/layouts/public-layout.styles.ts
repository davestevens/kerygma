import { globalConstants } from '../common/global-constants'
import { createUseStyles } from 'react-jss'
import { KerygmaTheme } from '../model/kerygma-configuration'

export const useStylesThemed = (theme: KerygmaTheme) => {
  const header = {
    fontFamily: theme.fonts.titles.family,
    fontWeight: theme.fonts.weightHeaders,
    color: theme.colors.fontHeaders,
    textTransform: theme.fonts.textTransformHeaders,
    width: '100%',
    boxSizing: 'border-box',
    maxWidth: globalConstants.textMaxWidth,
    padding: '0 16px',
  }

  return createUseStyles({
    '@global': {
      body: {
        display: 'flex',
        flexDirection: 'column',
        margin: 0,
        fontFamily: theme.fonts.default.family,
        fontSize: theme.fonts.sizeDefault,
        minHeight: '100vh',
        color: theme.colors.fontDefault,
        [`@media (max-width: ${globalConstants.bp.phoneMax})`]: {
          fontSize: `calc(${theme.fonts.sizeDefault} + 2px)`,
        },
      },
      h1: { extend: header, textAlign: 'center' },
      h2: { extend: header },
      h3: { extend: header },
      h4: { extend: header },
      p: {
        width: '100%',
        boxSizing: 'border-box',
        maxWidth: globalConstants.textMaxWidth,
        padding: '0 20px',
        margin: '16px 0',
        lineHeight: '1.5',
        color: theme.colors.fontDefault,
        fontWeight: theme.fonts.weightDefault,
      },
      small: {
        width: '100%',
        boxSizing: 'border-box',
        maxWidth: globalConstants.textMaxWidth,
        padding: '0 20px',
        margin: '16px 0',
        fontSize: '13px',
        lineHeight: '1.5',
      },
      center: {
        width: '100%',
        boxSizing: 'border-box',
        maxWidth: globalConstants.textMaxWidth,
        padding: '0 20px',
        margin: '16px 0',
        lineHeight: '1.5',
        color: theme.colors.fontDefault,
        fontWeight: theme.fonts.weightDefault,
        textAlign: 'center',
      },
      ul: {
        width: '100%',
        boxSizing: 'border-box',
        maxWidth: globalConstants.textMaxWidth,
        '& li': {
          lineHeight: '1.5',
          color: theme.colors.fontDefault,
          fontWeight: theme.fonts.weightDefault,
        },
      },
    },
    container: `
      display: flex;
      flex-direction: column;
      align-items: center;
      min-height: 100vh;
    `,
    contentBody: `
      display: flex;
      flex-direction: column;
      align-items: center;
      width: 100%;
    `,
    padContentBody: `
      padding-bottom: 20px;
    `,
  })
}
