## [1.0.0] - 2024-04-26

### Added
- Added new tag search functionality. This requires a DB migration. So, the major version was increased to 1.0.0.

## [0.1.0] - 2024-04-06

### Added
- `npx kerygma init` command to init a new site.
- Added documentation
