#!/usr/bin/env node

import 'reflect-metadata'
import container from './di-container'
import { StaticSiteGenerator } from './commands/static-site-generator'

const staticSiteGenerator = container.resolve(StaticSiteGenerator)

export async function generateStaticPages(
  kerygmaConfigPath: string
): Promise<void> {
  return staticSiteGenerator.generateStaticPages(kerygmaConfigPath)
}
