import { inject, injectable } from 'tsyringe'
import { Logger } from 'pino'
import { EntryPointFactory } from '../services/entry-point-factory'
import { ConfigParser } from '../services/config-parser'
import { WebpackService } from '../services/webpack-service'
import { FileService } from '../services/file-service'
import { HtmlGenerator } from '../services/html-generator'

@injectable()
export class StaticSiteGenerator {
  constructor(
    @inject('Logger') private logger: Logger,
    private configParser: ConfigParser,
    private entryPointFactory: EntryPointFactory,
    private fileService: FileService,
    private webpackService: WebpackService,
    private htmlGenerator: HtmlGenerator
  ) {}

  public async generateStaticPages(kerygmaConfigPath: string): Promise<void> {
    this.logger.info('gathering entry points...')
    const config = this.configParser.parseConfig(kerygmaConfigPath)
    if (!config) {
      return
    }
    const { tsxEntries, mdxAppEntry } =
      this.entryPointFactory.createTsxEntryPoints(config)
    const mdxEntries = this.entryPointFactory.createMdxEntryPoints(config)

    this.logger.info('cleaning outDir...')
    this.fileService.cleanOutDir(config)
    this.logger.info('copying assets...')
    const outCssFiles = this.fileService.copyAssets(config)
    this.logger.info('compiling jsx and mdx files...')
    const jsFileMap = await this.webpackService.compilejs(
      [mdxAppEntry, ...tsxEntries, ...mdxEntries],
      config
    )
    this.logger.info('generating church admin HTML pages...')
    this.htmlGenerator.generateHtmlForTsxPages(
      tsxEntries,
      config,
      outCssFiles,
      jsFileMap
    )

    this.logger.info('generating church public HTML pages...')
    this.htmlGenerator.generateHtmlForMdxPages(
      mdxEntries,
      config,
      outCssFiles,
      jsFileMap
    )
  }
}
