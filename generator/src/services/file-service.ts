import { inject, injectable } from 'tsyringe'
import { PlatformPath } from 'path'
import { KerygmaConfiguration } from '../model/kerygma-configuration'
import { rimrafSync as rimrafSyncType } from 'rimraf'
import * as fsType from 'fs'
import { glob as globLib } from 'glob'
import sharp from 'sharp'

const cssAssets = [
  {
    sourcePath: 'node_modules/@blueprintjs/core/lib/css',
    file: 'blueprint.css',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/core/lib/css',
    file: 'blueprint.css.map',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/icons/lib/css',
    file: 'blueprint-icons.css',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/icons/lib/css',
    file: 'blueprint-icons.css.map',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/icons/lib/css',
    file: 'blueprint-icons.css.map',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/icons/lib/css',
    file: 'blueprint-icons-20.ttf',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/icons/lib/css',
    file: 'blueprint-icons-20.woff',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/icons/lib/css',
    file: 'blueprint-icons-20.woff2',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/icons/lib/css',
    file: 'blueprint-icons-16.ttf',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/icons/lib/css',
    file: 'blueprint-icons-16.woff',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/icons/lib/css',
    file: 'blueprint-icons-16.woff2',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/datetime/lib/css',
    file: 'blueprint-datetime.css',
  },
  {
    sourcePath: 'node_modules/@blueprintjs/datetime/lib/css',
    file: 'blueprint-datetime.css.map',
  },
  {
    sourcePath: 'node_modules/leaflet/dist',
    file: 'leaflet.css',
  },
  {
    sourcePath: 'node_modules/leaflet/dist',
    file: 'images/layers.png',
  },
  {
    sourcePath: 'node_modules/leaflet/dist',
    file: 'images/layers-2x.png',
  },
  {
    sourcePath: 'node_modules/leaflet/dist',
    file: 'images/marker-icon.png',
  },
  {
    sourcePath: 'node_modules/leaflet/dist',
    file: 'images/marker-icon-2x.png',
  },
  {
    sourcePath: 'node_modules/leaflet/dist',
    file: 'images/marker-shadow.png',
  },
]

@injectable()
export class FileService {
  constructor(
    @inject('path') private path: PlatformPath,
    @inject('rimrafSync') private rimrafSync: typeof rimrafSyncType,
    @inject('fs') private fs: typeof fsType,
    @inject('glob') private glob: typeof globLib.sync,
    @inject('sharp') private sharpFile: typeof sharp
  ) {}

  public cleanOutDir(config: KerygmaConfiguration): void {
    const outDir = this.path.resolve(config.generator?.outDir ?? '')
    this.rimrafSync(outDir)
  }

  public copyAssets(config: KerygmaConfiguration): string[] {
    const outDir = config.generator?.outDir ?? ''
    const outCssDir = this.path.join(outDir, '/assets/css')
    this.fs.mkdirSync(outCssDir, { recursive: true })
    this.fs.mkdirSync(`${outCssDir}/images`) // needed for leaflet css images
    const outCssFiles: string[] = []

    // Copying css from node_modules
    for (const asset of cssAssets) {
      const sourceFile = this.path.resolve(`${asset.sourcePath}/${asset.file}`)
      const outCssFile = this.path.join(outCssDir, asset.file)
      outCssFiles.push(outCssFile)
      this.fs.copyFileSync(sourceFile, outCssFile)
    }

    // Copying assets folder listed in kerygma-config
    const sourceAssetsDir = this.path.resolve(
      config?.generator?.assetsDir ?? ''
    )
    const outAssetsDir = this.path.join(outDir, '/assets')
    this.fs.cpSync(sourceAssetsDir, outAssetsDir, { recursive: true })

    // Generate placeholder images for jpeg files
    const jpegFiles = this.glob(`${outAssetsDir}/**/*+(.jpg|.jpeg)`)
    for (const file of jpegFiles) {
      const fileParts = this.path.parse(file)
      const destFile = `${fileParts.dir}/${fileParts.name}-placeholder${fileParts.ext}`
      this.sharpFile(file).resize(300).blur(5).toFile(destFile).then()
    }

    return outCssFiles
  }
}
