import { inject, injectable } from 'tsyringe'
import { Logger } from 'pino'
import { PlatformPath } from 'path'
import EntryPointTsx from '../model/entry-point-tsx'
import EntryPointMdx from '../model/entry-point-mdx'
import { KerygmaConfiguration } from '../model/kerygma-configuration'
import * as fsType from 'fs'
import matter from 'gray-matter'

@injectable()
export class HtmlGenerator {
  constructor(
    @inject('Logger') private logger: Logger,
    @inject('path') private path: PlatformPath,
    @inject('fs') private fs: typeof fsType
  ) {}

  public generateHtmlForTsxPages(
    tsxEntryPoints: EntryPointTsx[],
    config: KerygmaConfiguration,
    cssFiles: string[],
    jsFileMap: Map<string, string[] | undefined>
  ): void {
    const outDir = this.path.resolve(config.generator?.outDir ?? '')
    const assetsDir = config.generator?.assetsDir ?? 'assets'
    for (const tsxEntry of tsxEntryPoints) {
      const jsFiles = jsFileMap.get(tsxEntry.entryKey)
      if (jsFiles) {
        this.generateHtmlForTsxPage(
          tsxEntry,
          jsFiles,
          cssFiles,
          outDir,
          assetsDir
        )
      }
    }
  }

  private generateHtmlForTsxPage(
    tsxPage: EntryPointTsx,
    jsFiles: string[],
    cssFiles: string[],
    outDir: string,
    assetsDir: string
  ) {
    let cssLinks = ''
    for (const cssFile of cssFiles) {
      const cssRelPath = this.path.relative(tsxPage.htmlPath, cssFile)
      cssLinks += `<link href="${cssRelPath}" rel="stylesheet" />`
      cssLinks += '\r\n'
    }
    let jsScripts = ''
    for (const jsFile of jsFiles) {
      const jsFilename = `${outDir}/${jsFile}`
      const jsRelPath = this.path.relative(tsxPage.htmlPath, jsFilename)
      jsScripts += `<script src="${jsRelPath}"></script>`
      jsScripts += '\r\n'
    }

    // See https://stackoverflow.com/questions/49064060/unicode-characters-not-showed-when-bundling-and-deploying-react-app-with-webpack
    // for why we need to add the charset meta tag.
    const html = `<!DOCTYPE html><html lang="en"><head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta charset="UTF-8">
      ${cssLinks}
    </head><body>
    <div id="app"></div>
    ${jsScripts}
    <script>
      const assetsRoot = '${assetsDir}/'
      ${tsxPage.entryKey}.init("app", assetsRoot)
    </script>
    </body></html>`
    this.logger.info(`  writing out ${tsxPage.htmlFile}...`)
    this.fs.writeFileSync(tsxPage.htmlFile, Buffer.from(html))
  }

  generateHtmlForMdxPages(
    mdxEntries: EntryPointMdx[],
    config: KerygmaConfiguration,
    cssFiles: string[],
    jsFileMap: Map<string, string[] | undefined>
  ) {
    const outDir = this.path.resolve(config.generator?.outDir ?? '')
    const assetsDir = config.generator?.assetsDir ?? 'assets'
    const appJsFiles = jsFileMap.get('App')
    for (const mdxEntry of mdxEntries) {
      const jsFiles = jsFileMap.get(mdxEntry.entryKey)
      if (jsFiles && appJsFiles) {
        this.generateHtmlForMdxPage(
          config,
          mdxEntry,
          jsFiles,
          appJsFiles,
          cssFiles,
          outDir,
          assetsDir
        )
      }
    }
  }

  private generateHtmlForMdxPage(
    config: KerygmaConfiguration,
    mdxPage: EntryPointMdx,
    jsFiles: string[],
    appJsFiles: string[],
    cssFiles: string[],
    outDir: string,
    assetsDir: string
  ) {
    let cssLinks = ''
    // TODO remove dup code
    for (const cssFile of cssFiles) {
      const cssRelPath = this.path.relative(mdxPage.htmlPath, cssFile)
      cssLinks += `<link href="${cssRelPath}" rel="stylesheet" />`
      cssLinks += '\r\n'
    }
    // Using set to remove duplicate vendors.js file.
    const jsFileSet = new Set<string>([...jsFiles, ...appJsFiles])
    let jsScripts = ''
    for (const jsFile of jsFileSet.values()) {
      const jsFilename = `${outDir}/${jsFile}`
      const jsRelPath = this.path.relative(mdxPage.htmlPath, jsFilename)
      jsScripts += `<script src="${jsRelPath}"></script>`
      jsScripts += '\r\n'
    }
    const frontMatter = matter.read(mdxPage.sourcePath, {
      delimiters: ['{/*', '*/}'],
    })
    const frontMatterStr = JSON.stringify(frontMatter)
    const frontMatterEncoded = Buffer.from(frontMatterStr).toString('base64')
    const title = frontMatter?.data?.title ?? config.churchNameFull
    // See https://stackoverflow.com/questions/49064060/unicode-characters-not-showed-when-bundling-and-deploying-react-app-with-webpack
    // for why we need to add the charset meta tag.
    const html = `<!DOCTYPE html><html lang="en"><head>
      <title>${title}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta charset="UTF-8">
      ${cssLinks}
    </head><body>
    <div id="app"></div>
    ${jsScripts}
    <script>
      const assetsRoot = '${assetsDir}/'
      const pathHome = '${mdxPage.pathHome}'
      App.init(${mdxPage.entryKey}.default, "app", "${frontMatterEncoded}", assetsRoot, pathHome)
    </script>
    </body></html>`
    this.logger.info(`  writing out ${mdxPage.htmlFile} from MDX...`)
    this.fs.writeFileSync(mdxPage.htmlFile, Buffer.from(html))
  }
}
