import { inject, injectable } from 'tsyringe'
import { glob as globLib } from 'glob'
import { PlatformPath } from 'path'
import EntryPointTsx from '../model/entry-point-tsx'
import { Logger } from 'pino'
import { KerygmaConfiguration } from '../model/kerygma-configuration'
import EntryPointMdx from '../model/entry-point-mdx'

const mdxAppPath = 'src/pages/App.tsx'

@injectable()
export class EntryPointFactory {
  constructor(
    @inject('Logger') private logger: Logger,
    @inject('glob') private glob: typeof globLib.sync,
    @inject('path') private path: PlatformPath
  ) {}

  public createTsxEntryPoints(config: KerygmaConfiguration): {
    tsxEntries: EntryPointTsx[]
    mdxAppEntry: EntryPointTsx
  } {
    const uiSource = this.path.join(__dirname, '../../../ui')
    const tsxPagePaths = this.glob('src/pages/**/*+(.tsx)', {
      cwd: uiSource,
      ignore: mdxAppPath,
    })
    const tsxEntries = tsxPagePaths.map((uiPath) => {
      const fullPath = this.path.join(uiSource, uiPath)
      return this.createEntryPoint(
        fullPath,
        uiPath,
        config.generator?.outDir ?? ''
      )
    })
    const appPath = this.path.join(uiSource, mdxAppPath)
    const mdxAppEntry = this.createEntryPoint(
      appPath,
      mdxAppPath,
      config.generator?.outDir ?? ''
    )
    return { tsxEntries, mdxAppEntry }
  }

  private createEntryPoint(
    fullPath: string,
    uiPath: string,
    outDir: string
  ): EntryPointTsx {
    // parsing the sections of the uiPath
    const parsed = this.path.parse(uiPath)
    return new EntryPointTsx(fullPath, parsed, outDir)
  }

  public createMdxEntryPoints(config: KerygmaConfiguration): EntryPointMdx[] {
    const mdxPagePaths = this.glob(`${config?.generator?.pagesDir}/**/*+(.mdx)`)

    return mdxPagePaths.map((uiPath) => {
      const fullPath = this.path.resolve(uiPath)
      const parsed = this.path.parse(uiPath)
      const prefixToRemove = new RegExp(
        `^(${config.generator?.pagesDir ?? ''})`,
        'gi'
      )
      const dirTrimmed = parsed.dir.replace(prefixToRemove, '')
      const depth = (dirTrimmed.match(/\//g) || []).length
      const pathHome = '../'.repeat(depth)
      return new EntryPointMdx(
        fullPath,
        parsed,
        config.generator?.outDir ?? '',
        config.generator?.pagesDir ?? '',
        pathHome
      )
    })
  }
}
