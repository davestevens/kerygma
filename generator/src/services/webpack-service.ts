import {inject, injectable} from 'tsyringe'
import {Logger} from 'pino'
import EntryPointTsx from '../model/entry-point-tsx'
import webpackLib, {DefinePlugin} from 'webpack'
import {PlatformPath} from 'path'
import {KerygmaConfiguration} from '../model/kerygma-configuration'
import fsType from "fs";

class EntryObject {
  [index: string]: { import: string; filename: string }
}

@injectable()
export class WebpackService {
  constructor(
    @inject('Logger') private logger: Logger,
    @inject('webpack') private webpack: typeof webpackLib,
    @inject('path') private path: PlatformPath,
    @inject('fs') private fs: typeof fsType
  ) {}

  public async compilejs(
    tsxEntryPoints: EntryPointTsx[],
    config: KerygmaConfiguration
  ): Promise<Map<string, string[] | undefined>> {
    const version = this.getPackageVersion()
    const entries = this.createEntries(tsxEntryPoints)
    const definePlugin = new DefinePlugin({
      KERYGMA_VERSION: JSON.stringify({ version }),
      KERYGMA_CONFIG: JSON.stringify(config),
    })
    const compiler = this.webpack({
      mode: 'development',
      entry: entries,
      module: {
        rules: [
          {
            test: /\.tsx?$/,
            use: 'ts-loader',
          },
          {
            test: /\.ya?ml$/,
            use: 'yaml-loader',
          },
          {
            test: /\.mdx?$/,
            use: [
              {
                loader: '@mdx-js/loader',
                options: {},
              },
            ],
          },
        ],
      },
      resolve: {
        extensions: ['.tsx', '.ts', '.js', '.mdx'],
      },
      output: {
        filename: '[name].[contenthash].js',
        path: this.path.resolve(config.generator?.outDir ?? ''),
        library: '[name]',
      },
      optimization: {
        splitChunks: {
          chunks: 'all',
          cacheGroups: {
            commons: {
              test: /[\\/]node_modules[\\/]/,
              name: 'vendors',
              chunks: 'all',
            },
          },
        },
      },
      devtool: false,
      plugins: [definePlugin],
    })

    const files = new Map<string, string[] | undefined>()
    return new Promise((resolve, reject) => {
      compiler.run((err, stats) => {
        if (err || stats?.hasErrors()) {
          console.log(stats?.toString({ colors: true }))
          reject('Errors encountered in run.')
        } else {
          const entryNames = stats?.compilation.entrypoints.keys() ?? []
          for (const entryName of entryNames) {
            const compiledFiles = stats?.compilation.entrypoints
              ?.get(entryName)
              ?.getFiles()
            files.set(entryName, compiledFiles)
          }
          resolve(files)
        }
        compiler.close(() => files)
      })
    })
  }

  private createEntries(tsxEntryPoints: EntryPointTsx[]): EntryObject {
    const entries = new EntryObject()
    for (const entryPoint of tsxEntryPoints) {
      entries[entryPoint.entryKey] = {
        import: entryPoint.sourcePath,
        filename: entryPoint.entry,
      }
    }
    return entries
  }

  private getPackageVersion(): string {
    const packagePath = this.path.join(__dirname, '../../../package.json')
    const packageJsonStr = this.fs.readFileSync(packagePath, 'utf8')
    const packageInfo = JSON.parse(packageJsonStr)
    return packageInfo.version
  }
}
