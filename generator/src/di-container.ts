import pino, { Logger } from 'pino'
import { container } from 'tsyringe'
import { glob } from 'glob'
import path, { PlatformPath } from 'path'
import * as fs from 'fs'
import yaml from 'js-yaml'
import { JsonConvert } from 'json2typescript'
import webpack from 'webpack'
import { rimrafSync } from 'rimraf'
import sharp from 'sharp'

const logger = pino({
  transport: {
    target: 'pino-pretty',
    options: { colorize: true, singleLine: true },
  },
})

container.register<Logger>('Logger', { useValue: logger })
container.register<typeof glob.sync>('glob', { useValue: glob.sync })
container.register<typeof yaml>('yaml', { useValue: yaml })
container.register<typeof fs>('fs', { useValue: fs })
container.register<PlatformPath>('path', { useValue: path })
container.register<typeof rimrafSync>('rimrafSync', { useValue: rimrafSync })
container.register<JsonConvert>('JsonConvert', { useValue: new JsonConvert() })
container.register<typeof webpack>('webpack', { useValue: webpack })
container.register<typeof sharp>('sharp', { useValue: sharp })

export default container
