import { ParsedPath } from 'path'

export default class EntryPointMdx {
  sourcePath: string
  parsed: ParsedPath
  entry: string
  entryKey: string
  htmlPath: string
  htmlFile: string
  pathHome: string
  constructor(
    sourcePath: string,
    parsed: ParsedPath,
    outDir: string,
    pagesDir: string,
    pathHome: string
  ) {
    this.sourcePath = sourcePath
    this.parsed = parsed
    const relativeDir = parsed.dir
      .replace(`${pagesDir}/`, '')
      .replace(pagesDir, '')
    const pathSep = relativeDir === '' ? '' : '/'
    const nameSep = relativeDir === '' ? '' : '-'
    this.entry = `${relativeDir}${pathSep}[name].[contenthash].js`
    const entryKey = `${relativeDir}${nameSep}${parsed.name}`
    // converting from kebab case to camel case
    this.entryKey = entryKey.replace(/-./g, (x) => x[1].toUpperCase())
    this.htmlPath = relativeDir === '' ? outDir : `${outDir}/${relativeDir}`
    this.htmlFile = `${this.htmlPath}/${parsed.name}.html`
    this.pathHome = pathHome
  }
}
