import { ParsedPath } from 'path'

export default class EntryPointTsx {
  sourcePath: string
  parsed: ParsedPath
  entry: string
  entryKey: string
  htmlPath: string
  htmlFile: string
  constructor(sourcePath: string, parsed: ParsedPath, outDir: string) {
    this.sourcePath = sourcePath
    this.parsed = parsed
    // This is the location of the admin pages within the ui project
    const relativeDir = parsed.dir
      .replace('src/pages/', '')
      .replace('src/pages', '')
    const pathSep = relativeDir === '' ? '' : '/'
    const nameSep = relativeDir === '' ? '' : '-'
    this.entry = `${relativeDir}${pathSep}[name].[contenthash].js`
    const entryKey = `${relativeDir}${nameSep}${parsed.name}`
    // converting from kebab case to camel case
    this.entryKey = entryKey.replace(/-./g, (x) => x[1].toUpperCase())
    this.htmlPath = relativeDir === '' ? outDir : `${outDir}/${relativeDir}`
    this.htmlFile = `${this.htmlPath}/${parsed.name}.html`
  }
}
