import { inject, injectable } from 'tsyringe'
import Fastify from 'fastify'
import UserController from '../controllers/user-controller'
import AuthController from '../controllers/auth-controller'
import DocumentController from '../controllers/document-controller'
import SermonController from '../controllers/sermon-controller'
import { fastifyRequestContext as FastifyRequestContext } from '@fastify/request-context'
import FastifyCookie from '@fastify/cookie'
import FastifyMultipart from '@fastify/multipart'
import FastifyStatic from '@fastify/static'
import { PlatformPath } from 'path'
import CsrfProtectHook from '../handlers/csrf-protect-hook'
import PodcastController from '../controllers/podcast-controller'

@injectable()
export default class StartServerCommand {
  constructor(
    @inject('Fastify') private fastify: typeof Fastify,
    @inject('fastifyRequestContext')
    private fastifyRequestContext: typeof FastifyRequestContext,
    @inject('fastifyCookie') private fastifyCookie: typeof FastifyCookie,
    @inject('fastifyMultipart')
    private fastifyMultipart: typeof FastifyMultipart,
    @inject('fastifyStatic') private fastifyStatic: typeof FastifyStatic,
    private userController: UserController,
    private authController: AuthController,
    private documentController: DocumentController,
    private sermonController: SermonController,
    private podcastController: PodcastController,
    @inject('maxFileSize') private maxFileSize: number,
    @inject('path') private path: PlatformPath,
    @inject('staticServe') private staticServe: boolean,
    @inject('staticPath') private staticPath: string,
    @inject('mediaServe') private mediaServe: boolean,
    @inject('mediaPath') private mediaPath: string,
    @inject('mediaServeContextRoot') private mediaServeContextRoot: string,
    @inject('apiPort') private apiPort: number,
    private csrfProtectHook: CsrfProtectHook
  ) {}

  public async start(): Promise<void> {
    const fastify = this.fastify({
      logger: {
        transport: {
          target: 'pino-pretty',
          options: { colorize: true, singleLine: true },
        },
      },
    })

    // Setting up CORS
    // Note: We cannot use CORS and get cookies to work for CSRF protection.
    //       Cookies can be set but modern browsers will not send them back
    //       unless you have SameSite set to none and HTTPS turned on.
    //       I don't want to require HTTPS only for local testing.
    //       Therefore, I am not going to enable CORS.
    /*
    fastify.register(this.fastifyCors, {
      origin: this.apiCorsOrigin,
      methods: ['GET', 'PUT', 'POST', 'DELETE', 'HEAD', 'OPTIONS'],
      preflightContinue: true, // Required to allow cookies to be set
      credentials: true, // Required to allow cookies to be set
      allowedHeaders: [
        'Content-Type',
        'Authorization',
        'Accept',
        'Origin',
      ],
    })
     */
    fastify.register(this.fastifyRequestContext)
    fastify.register(this.fastifyCookie)

    // Setting up file upload settings
    fastify.register(this.fastifyMultipart, {
      limits: {
        fileSize: this.maxFileSize,
        files: 1,
      },
    })

    // Add CSRF token validation hook
    this.csrfProtectHook.registerHook(fastify)

    // Registering API routes
    this.userController.registerRoutes(fastify)
    this.authController.registerRoutes(fastify)
    this.documentController.registerRoutes(fastify)
    this.sermonController.registerRoutes(fastify)
    this.podcastController.registerRoutes(fastify)

    // Registering static content routes
    if (this.staticServe) {
      const root = this.path.resolve(this.staticPath)
      fastify.log.info(`Serving static html with prefix /* from ${root}`)
      fastify.register(this.fastifyStatic, { root })
    }
    if (this.mediaServe) {
      const root = this.path.resolve(this.mediaPath)
      const prefix = `/${this.mediaServeContextRoot}/`
      fastify.log.info(`Serving media with prefix ${prefix} from ${root}`)
      const decorateReply = !this.staticServe
      fastify.register(this.fastifyStatic, { root, prefix, decorateReply })
    }

    try {
      await fastify.listen({ port: this.apiPort })
    } catch (err) {
      fastify.log.error(err)
      process.exit(1)
    }
  }
}
