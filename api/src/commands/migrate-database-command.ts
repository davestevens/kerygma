import { injectable, inject } from 'tsyringe'
import dbmigrateType from 'db-migrate'
import { PlatformPath } from 'path'
import { Logger } from 'pino'

@injectable()
export default class MigrateDatabaseCommand {
  constructor(
    @inject('dbmigrate') private dbmigrate: typeof dbmigrateType,
    @inject('path') private path: PlatformPath,
    @inject('Logger') private readonly logger: Logger
  ) {}

  public up() {
    this.logger.info('Running database migration up scripts...')
    this.dbmigrate.getInstance(true, this.getOptions()).up()
  }

  public down() {
    this.logger.info('Running database migration down script...')
    this.dbmigrate.getInstance(true, this.getOptions()).down()
  }

  private getOptions(): { cwd: string; config: string } {
    const cwd = this.path.resolve(`${__dirname}/../..`)
    const config = this.path.resolve(`${__dirname}/../../database.json`)
    return { cwd, config }
  }
}
