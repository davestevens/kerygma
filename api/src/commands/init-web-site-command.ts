import { injectable } from 'tsyringe'
import * as fs from 'fs'
import path from 'path'

@injectable()
export default class InitWebSiteCommand {
  public init(
    configFile: string,
    pagesPath: string,
    assetsPath: string,
    mediaPath: string
  ) {
    // Create kerygma-config.yaml file.
    const configTemplatePath = path.join(
      __dirname,
      '../model/kerygma-config-template.yaml'
    )
    const templateYaml = fs.readFileSync(configTemplatePath, 'utf8')
    const configYaml = templateYaml
      .replace('[PAGES_PATH]', pagesPath)
      .replace('[ASSETS_PATH]', assetsPath)
    const configPath = path.resolve(configFile)
    fs.writeFileSync(configPath, configYaml)

    // Create .env file.
    const envTemplatePath = path.join(__dirname, '../model/.env-template')
    const envTemplateContents = fs.readFileSync(envTemplatePath, 'utf8')
    const envSettings = envTemplateContents.replace('[MEDIA_PATH]', mediaPath)
    const envPath = path.resolve('.env')
    fs.writeFileSync(envPath, envSettings)

    // Populate demo site pages, assets and media folders.
    const demoPagesPath = path.join(__dirname, '../../../pages')
    const assetsPagesPath = path.join(__dirname, '../../../assets')
    fs.cpSync(demoPagesPath, pagesPath, { recursive: true })
    fs.cpSync(assetsPagesPath, assetsPath, { recursive: true })
    if (!fs.existsSync(mediaPath)) {
      fs.mkdirSync(mediaPath)
    }
    console.log(
      'Your example source markdown pages have been created. Now use [npx kerygma generate] to generate your static html pages and [npx kerygma start] to view your site locally.'
    )
  }
}
