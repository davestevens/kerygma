declare module 'db-migrate' {
  export class DbMigrateInstance {
    up: () => void
    down: () => void
  }

  export function getInstance(
    isModule: boolean,
    options: {
      cwd: string
      config: string
    }
  ): DbMigrateInstance
}
