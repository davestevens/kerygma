declare module 'bible-passage-reference-parser/js/en_bcv_parser' {
  export class bcv_parser {
    set_options: (options: unknown) => void
    parse: (passage: string) => {
      osis: () => string
    }
  }
}
