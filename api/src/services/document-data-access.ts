import { Pool } from 'pg'
import { UploadedDocument } from '../model/uploaded-document'
import dayjs from 'dayjs'
import { inject, injectable } from 'tsyringe'

@injectable()
export default class DocumentDataAccess {
  constructor(@inject('Pool') private readonly pool: Pool) {}

  public async getAll(): Promise<UploadedDocument[]> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT * FROM documents
        ORDER BY uploaded_ts DESC
      `
    )
    if (result.rowCount === 0) {
      return []
    }
    client.release()
    return result.rows.map<UploadedDocument>((row) =>
      DocumentDataAccess.mapRow2Doc(row)
    )
  }

  public async get(documentId: number): Promise<UploadedDocument | null> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT * FROM documents WHERE id = $1
      `,
      [documentId]
    )
    client.release()
    if (result.rowCount === 0) {
      return null
    }
    return DocumentDataAccess.mapRow2Doc(result.rows[0])
  }

  public async add(doc: UploadedDocument): Promise<void> {
    const client = await this.pool.connect()
    await client.query(
      `
      INSERT INTO documents(
        filename,
        type,
        title,
        uploaded_ts,
        size
      ) VALUES ($1, $2, $3, $4, $5)
    `,
      [
        doc.filename.trim(),
        doc.type.trim(),
        doc.title.trim(),
        doc.uploadedTimestamp.toDate(),
        doc.size,
      ]
    )
    client.release()
  }

  public async getByFilename(
    filename: string
  ): Promise<UploadedDocument | null> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT * FROM documents WHERE filename = $1
      `,
      [filename]
    )
    client.release()
    if (result.rowCount === 0) {
      return null
    }
    return DocumentDataAccess.mapRow2Doc(result.rows[0])
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static mapRow2Doc(row: any): UploadedDocument {
    const doc = new UploadedDocument()
    doc.id = row.id
    doc.filename = row.filename
    doc.type = row.type
    doc.title = row.title
    doc.size = row.size
    doc.uploadedTimestamp = dayjs(row.uploaded_ts)
    return doc
  }

  public async delete(documentId: number): Promise<void> {
    const client = await this.pool.connect()
    await client.query(
      `
        DELETE FROM documents 
        WHERE id = $1
      `,
      [documentId]
    )
    client.release()
  }

  async save(doc: UploadedDocument) {
    const client = await this.pool.connect()
    await client.query(
      `
      UPDATE documents
      SET
        filename = $2,
        type = $3,
        title = $4,
        uploaded_ts = $5,
        size = $6
      WHERE id = $1
    `,
      [
        doc.id,
        doc.filename.trim(),
        doc.type.trim(),
        doc.title.trim(),
        doc.uploadedTimestamp.toDate(),
        doc.size,
      ]
    )
    client.release()
  }
}
