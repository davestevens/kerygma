import { Pool } from 'pg'
import { User } from '../model/user'
import { UserForm } from '../model/user-form'
import { inject, injectable } from 'tsyringe'

@injectable()
export default class UserDataAccess {
  constructor(@inject('Pool') private readonly pool: Pool) {}

  public async getAll(): Promise<User[]> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT * FROM users
        ORDER BY first_name, last_name
      `
    )
    if (result.rowCount === 0) {
      return []
    }
    client.release()
    return result.rows.map<User>((row) => UserDataAccess.mapRow2User(row))
  }

  public async getByUsername(username: string): Promise<User | null> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT * FROM users WHERE username = $1
      `,
      [username]
    )
    if (result.rowCount === 0) {
      return null
    }
    client.release()
    return UserDataAccess.mapRow2User(result.rows[0])
  }

  public async get(userId: string): Promise<User | null> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT * FROM users WHERE id = $1
      `,
      [userId]
    )
    if (result.rowCount === 0) {
      return null
    }
    client.release()
    return UserDataAccess.mapRow2User(result.rows[0])
  }

  public async add(user: UserForm): Promise<void> {
    const client = await this.pool.connect()
    await client.query(
      `
      INSERT INTO users(
        id,
        first_name,
        last_name,
        username,
        password_hash,
        auth_level,
        failed_attempt_count
      ) VALUES (
        uuid_generate_v4(),
        $1, $2, $3,
        crypt($4, gen_salt('md5')),
        $5,
        0
      )
    `,
      [
        user.firstName.trim(),
        user.lastName.trim(),
        user.username.trim(),
        user.password.trim(),
        user.authLevel.trim(),
      ]
    )
    client.release()
  }

  public async save(user: UserForm): Promise<void> {
    const client = await this.pool.connect()
    const values = [
      user.id,
      user.firstName.trim(),
      user.lastName.trim(),
      user.username.trim(),
      user.authLevel.trim(),
    ]
    if (user.isPasswordSet()) {
      values.push(user.password.trim())
    }

    await client.query(
      `
      UPDATE users
      SET
        first_name = $2,
        last_name = $3,
        username = $4,
        auth_level = $5,
        ${
          user.isPasswordSet()
            ? "password_hash = crypt($6, gen_salt('md5')),"
            : ''
        }
        failed_attempt_count = 0
      WHERE id = $1
    `,
      values
    )
    client.release()
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static mapRow2User(row: any): User {
    const user = new User()
    user.id = row.id
    user.firstName = row.first_name
    user.lastName = row.last_name
    user.username = row.username
    user.authLevel = row.auth_level
    return user
  }

  public async addFailedAttempt(username: string): Promise<boolean> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT failed_attempt_count FROM users WHERE username = $1
      `,
      [username]
    )
    if (result.rowCount === 0) {
      return false
    }
    const failedAttemptCount = result.rows[0].failed_attempt_count + 1
    const isSuspended = failedAttemptCount > 3
    const sql = `
      UPDATE users 
      SET failed_attempt_count = $1
      ${isSuspended ? ', suspend_ts = current_timestamp' : ''}
      WHERE username = $2
    `
    await client.query(sql, [failedAttemptCount, username])
    client.release()
    return isSuspended
  }

  public async clearFailedAttempts(username: string): Promise<void> {
    const client = await this.pool.connect()
    await client.query(
      `
        UPDATE users 
        SET failed_attempt_count = 0, suspend_ts = null
        WHERE username = $1
      `,
      [username]
    )
    client.release()
  }

  public async delete(userId: string): Promise<void> {
    const client = await this.pool.connect()
    await client.query(
      `
        DELETE FROM users 
        WHERE id = $1
      `,
      [userId]
    )
    client.release()
  }
}
