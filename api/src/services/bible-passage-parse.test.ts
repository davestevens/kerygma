import BiblePassageParser from './bible-passage-parser'
import { bcv_parser } from 'bible-passage-reference-parser/js/en_bcv_parser'
import Path from 'path'
import fs from 'fs'
import { BibleBook } from '../model/bible-book'
import yaml from 'js-yaml'

const bibleBookPath = Path.resolve(`${__dirname}/../model/bible-book-data.yaml`)
const bibleBookYaml = fs.readFileSync(bibleBookPath, 'utf8')
const bibleBookData = yaml.load(bibleBookYaml) as { books: BibleBook[] }
const bibleBooks = bibleBookData.books

test('BiblePassageParser - full range', async () => {
  const parser = new BiblePassageParser(new bcv_parser(), bibleBooks)

  const range = parser.parsePassage('Genesis 5:1-6:10; John 1-3')
  expect(range.length).toEqual(2)
  expect(range[0].start.bookOsisId).toEqual('Gen')
  expect(range[0].start.dbBookCode).toEqual('01_Gen')
  expect(range[0].start.chapter).toEqual(5)
  expect(range[0].start.verse).toEqual(1)
  expect(range[0].end?.bookOsisId).toEqual('Gen')
  expect(range[0].end?.dbBookCode).toEqual('01_Gen')
  expect(range[0].end?.chapter).toEqual(6)
  expect(range[0].end?.verse).toEqual(10)
  expect(range[1].start.bookOsisId).toEqual('John')
  expect(range[1].start.dbBookCode).toEqual('43_John')
  expect(range[1].start.chapter).toEqual(1)
  expect(range[1].start.verse).toBeNull()
  expect(range[1].end?.bookOsisId).toEqual('John')
  expect(range[1].end?.dbBookCode).toEqual('43_John')
  expect(range[1].end?.chapter).toEqual(3)
  expect(range[1].end?.verse).toBeNull()
})

test('BiblePassageParser - single chapter', async () => {
  const parser = new BiblePassageParser(new bcv_parser(), bibleBooks)

  const range = parser.parsePassage('Rom. 8')
  expect(range.length).toEqual(1)
  expect(range[0].start.bookOsisId).toEqual('Rom')
  expect(range[0].start.dbBookCode).toEqual('45_Rom')
  expect(range[0].start.chapter).toEqual(8)
  expect(range[0].start.verse).toBeNull()
  expect(range[0].end).toBeNull()
})

test('BiblePassageParser - not in the bible', async () => {
  const parser = new BiblePassageParser(new bcv_parser(), bibleBooks)

  const range = parser.parsePassage('Invalid Passage')
  expect(range.length).toEqual(0)
})
