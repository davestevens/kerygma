import * as td from 'testdouble'
import { Pool, PoolClient, QueryResult } from 'pg'

export function mockQuery(rows: unknown[], rowCount: number): Pool {
  const queryResult = td.object<QueryResult>()
  queryResult.rowCount = rowCount
  queryResult.rows = rows
  const client = td.object<PoolClient>()
  td.when(
    client.query(td.matchers.anything(), td.matchers.anything())
  ).thenResolve(queryResult)
  const pool = td.object<Pool>()
  td.when(pool.connect()).thenResolve(client)
  return pool
}

export function mockQueryWithClient(): { pool: Pool; client: PoolClient } {
  const client = td.object<PoolClient>()
  const pool = td.object<Pool>()
  td.when(pool.connect()).thenResolve(client)
  return { pool, client }
}
