import * as crypto from 'crypto'
import { injectable } from 'tsyringe'

@injectable()
export default class AuthTokenGenerator {
  async generateLoginToken(): Promise<string> {
    const token = await crypto.randomBytes(64)
    return token.toString('base64')
  }
}
