import AuthTokenGenerator from './auth-token-generator'

test('token is generated', async () => {
  const authTokenGenerator = new AuthTokenGenerator()
  const token1 = await authTokenGenerator.generateLoginToken()
  expect(token1.length).toBeGreaterThan(0)
  const token2 = await authTokenGenerator.generateLoginToken()
  expect(token2.length).toBeGreaterThan(0)
  expect(token1 === token2).toBeFalsy()
})
