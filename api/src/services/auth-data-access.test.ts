import AuthDataAccess from './auth-data-access'
import { mockQuery, mockQueryWithClient } from './pool-mocks'
import * as td from 'testdouble'

test('AuthDataAccess - verifyCredentials valid', async () => {
  const dataAccess = new AuthDataAccess(mockQuery([{ id: 'id' }], 1))
  const res = await dataAccess.verifyCredentials('uid', 'pwd')
  expect(res).toBe('id')
})

test('AuthDataAccess - verifyCredentials not valid', async () => {
  const dataAccess = new AuthDataAccess(mockQuery([], 0))
  const res = await dataAccess.verifyCredentials('uid', 'pwd')
  expect(res).toBeNull()
})

test('AuthDataAccess - verifyToken valid', async () => {
  const dataAccess = new AuthDataAccess(
    mockQuery([{ has_expired: false, user_id: 'uid' }], 1)
  )
  const res = await dataAccess.verifyToken('token')
  expect(res).toBe('uid')
})

test('AuthDataAccess - verifyToken not valid', async () => {
  const dataAccess = new AuthDataAccess(mockQuery([], 0))
  const res = await dataAccess.verifyToken('token')
  expect(res).toBeNull()
})

test('AuthDataAccess - verifyToken expired', async () => {
  const dataAccess = new AuthDataAccess(mockQuery([{ has_expired: true }], 1))
  const res = await dataAccess.verifyToken('token')
  expect(res).toBeNull()
})

test('AuthDataAccess - addLoginSession valid', async () => {
  const { pool, client } = mockQueryWithClient()
  const dataAccess = new AuthDataAccess(pool)
  await dataAccess.addLoginSession('uid', 'token')
  td.verify(client.query(td.matchers.anything(), ['uid', 'token']), {
    times: 1,
  })
})

test('AuthDataAccess - logout valid', async () => {
  const { pool, client } = mockQueryWithClient()
  const dataAccess = new AuthDataAccess(pool)
  await dataAccess.logout('token')
  td.verify(client.query(td.matchers.anything(), ['token']), {
    times: 1,
  })
})

test('AuthDataAccess - extendSession valid', async () => {
  const { pool, client } = mockQueryWithClient()
  const dataAccess = new AuthDataAccess(pool)
  await dataAccess.extendSession('token')
  td.verify(client.query(td.matchers.anything(), ['token']), {
    times: 1,
  })
})
