import { Pool } from 'pg'
import { Sermon } from '../model/sermon'
import dayjs from 'dayjs'
import { inject, injectable } from 'tsyringe'
import BiblePassageParser from './bible-passage-parser'
import { KerygmaConfiguration } from '../model/kerygma-configuration'
import { SermonTag } from '../model/sermon-tag'

@injectable()
export default class SermonDataAccess {
  constructor(
    @inject('Pool') private readonly pool: Pool,
    private readonly biblePassageParser: BiblePassageParser,
    @inject('KerygmaConfiguration')
    private readonly kerygmaConfig: KerygmaConfiguration
  ) {}

  public async getAll(
    page: number,
    perPage: number,
    sortOrder: 'latest' | 'oldest',
    filter?: string,
    tags?: string[]
  ): Promise<{ sermons: Sermon[]; totalRowCount: number }> {
    const offset = page * perPage
    const client = await this.pool.connect()
    const orderBy = sortOrder === 'latest' ? 'DESC' : 'ASC'
    const values: (number | string | string[])[] = [offset, perPage]
    let whereClause = ''
    if (filter) {
      const bcvRange = this.biblePassageParser.parsePassage(filter)
      if (bcvRange.length > 0) {
        whereClause = `WHERE
          (sermon_title ILIKE $3 OR speaker_first_name ILIKE $3 OR speaker_last_name ILIKE $3
          OR (from_book_cd = $4`
        values.push(`%${filter}%`)
        values.push(bcvRange[0].start.dbBookCode)
        if (bcvRange[0].start.chapter && bcvRange[0].end?.chapter) {
          whereClause +=
            ' AND (from_chapter BETWEEN $5 AND $6 OR from_chapter IS NULL)))'
          values.push(bcvRange[0].start.chapter)
          values.push(bcvRange[0].end.chapter)
        } else if (bcvRange[0].start.chapter) {
          whereClause += ' AND (from_chapter = $5 OR from_chapter IS NULL)))'
          values.push(bcvRange[0].start.chapter)
        } else {
          whereClause += '))'
        }
      } else if (/^\d+$/.test(filter)) {
        whereClause = 'WHERE EXTRACT(YEAR FROM sermon_date) = $3'
        const year = parseInt(filter)
        values.push(year)
      } else {
        whereClause =
          'WHERE (sermon_title ILIKE $3 OR speaker_first_name ILIKE $3 OR speaker_last_name ILIKE $3)'
        values.push(`%${filter}%`)
      }
    }

    if (tags && tags.length > 0) {
      let andClause = ' AND'
      if (whereClause === '') {
        whereClause += ' WHERE'
        andClause = ''
      }
      for (const tag of tags) {
        whereClause += andClause
        values.push(tag)
        whereClause += `
        sermons.id IN (
          SELECT sermon_id
          FROM sermon_tags
          WHERE tag = $${values.length}
        )`
        andClause = ' AND'
      }
    }

    let secondarySortCol = 'CASE service '
    for (const [i, service] of this.kerygmaConfig.services.entries()) {
      secondarySortCol += `WHEN '${service}' THEN ${i} `
    }
    secondarySortCol += `ELSE ${this.kerygmaConfig.services.length} END as secondary_sort`

    const sql = `
      SELECT sermons.*, tags.tags, ${secondarySortCol}, COUNT(*) OVER() AS total_row_count
      FROM sermons
        LEFT OUTER JOIN (
          SELECT sermon_id, STRING_AGG(tag, ',') as tags
          FROM sermon_tags
          GROUP BY sermon_id
        ) tags ON (sermons.id = tags.sermon_id)
      ${whereClause}
      ORDER BY sermon_date ${orderBy}, secondary_sort asc
      OFFSET $1
      LIMIT $2
    `
    const result = await client.query(sql, values)
    if (result.rowCount === 0) {
      return { sermons: [], totalRowCount: 0 }
    }
    client.release()
    const totalRowCount = parseInt(result.rows[0].total_row_count)
    const sermons = result.rows.map<Sermon>((row) =>
      SermonDataAccess.mapRow2Sermon(row)
    )
    return { sermons, totalRowCount }
  }

  async add(sermon: Sermon, filename: string): Promise<void> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
      INSERT INTO sermons(
        sermon_date,
        speaker_title,
        speaker_first_name,
        speaker_last_name,
        sermon_title,
        service,
        from_book_cd,
        from_chapter,
        from_verse,
        thru_book_cd,
        thru_chapter,
        thru_verse,
        file_name,
        uploaded_ts,
        size
      ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)
      RETURNING id
    `,
      [
        sermon.getSermonDate().toDate(),
        sermon.speakerTitle,
        sermon.speakerFirstName,
        sermon.speakerLastName,
        sermon.title,
        sermon.service,
        sermon.fromBook,
        sermon.fromChapter,
        sermon.fromVerse,
        sermon.thruBook,
        sermon.thruChapter,
        sermon.thruVerse,
        filename,
        sermon.uploadedTimestamp.toDate(),
        sermon.size,
      ]
    )
    const sermonId = result.rows[0].id
    for (const tag of sermon.tags) {
      await client.query(
        ' INSERT INTO sermon_tags (sermon_id, tag) VALUES ($1, $2)',
        [sermonId, tag]
      )
    }
    client.release()
  }

  async save(sermon: Sermon) {
    const client = await this.pool.connect()
    await client.query(
      `
        UPDATE sermons
        SET
          sermon_date = $2,
          speaker_title = $3,
          speaker_first_name = $4,
          speaker_last_name = $5,
          sermon_title = $6,
          service = $7,
          from_book_cd = $8,
          from_chapter = $9,
          from_verse = $10,
          thru_book_cd = $11,
          thru_chapter = $12,
          thru_verse = $13,
          file_name = $14,
          uploaded_ts = $15,
          size = $16
        WHERE id = $1
      `,
      [
        sermon.id,
        sermon.getSermonDate().toDate(),
        sermon.speakerTitle,
        sermon.speakerFirstName,
        sermon.speakerLastName,
        sermon.title,
        sermon.service,
        sermon.fromBook,
        sermon.fromChapter,
        sermon.fromVerse,
        sermon.thruBook,
        sermon.thruChapter,
        sermon.thruVerse,
        sermon.filename,
        sermon.uploadedTimestamp.toDate(),
        sermon.size,
      ]
    )
    await client.query('DELETE FROM sermon_tags WHERE sermon_id = $1', [
      sermon.id,
    ])
    for (const tag of sermon.tags) {
      await client.query(
        ' INSERT INTO sermon_tags (sermon_id, tag) VALUES ($1, $2)',
        [sermon.id, tag]
      )
    }
    client.release()
  }

  public async get(sermonId: number): Promise<Sermon | null> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT sermons.*, tags.tags
        FROM sermons
          LEFT OUTER JOIN (
            SELECT sermon_id, STRING_AGG(tag, ',') as tags
            FROM sermon_tags
            GROUP BY sermon_id
          ) tags ON (sermons.id = tags.sermon_id)
        WHERE id = $1
      `,
      [sermonId]
    )
    client.release()
    if (result.rowCount === 0) {
      return null
    }
    return SermonDataAccess.mapRow2Sermon(result.rows[0])
  }

  public async getByFilename(filename: string): Promise<Sermon | null> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT * FROM sermons WHERE file_name = $1
      `,
      [filename]
    )
    client.release()
    if (result.rowCount === 0) {
      return null
    }
    return SermonDataAccess.mapRow2Sermon(result.rows[0])
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static mapRow2Sermon(row: any): Sermon {
    const sermon = new Sermon()
    sermon.id = row.id
    sermon.setSermonDate(dayjs(row.sermon_date))
    sermon.speakerTitle = row.speaker_title
    sermon.speakerFirstName = row.speaker_first_name
    sermon.speakerLastName = row.speaker_last_name
    sermon.title = row.sermon_title
    sermon.tags = row.tags ? row.tags.split(',') : []
    sermon.service = row.service
    sermon.fromBook = row.from_book_cd
    sermon.fromChapter = row.from_chapter
    sermon.fromVerse = row.from_verse
    sermon.thruBook = row.thru_book_cd
    sermon.thruChapter = row.thru_chapter
    sermon.thruVerse = row.thru_verse
    sermon.filename = row.file_name
    sermon.uploadedTimestamp = dayjs(row.uploaded_ts)
    sermon.size = row.size
    return sermon
  }

  public async delete(sermonId: number): Promise<void> {
    const client = await this.pool.connect()
    await client.query(
      `
        DELETE FROM sermons 
        WHERE id = $1
      `,
      [sermonId]
    )
    client.release()
  }

  public async getSermonTags(
    sortOrder: 'tag' | 'sermonCount'
  ): Promise<SermonTag[]> {
    const client = await this.pool.connect()
    const orderBy = sortOrder === 'tag' ? 'tag asc' : 'sermon_count desc'
    const result = await client.query(
      `
        SELECT tag, count(*) as sermon_count
        FROM sermon_tags
        GROUP BY tag
        ORDER BY ${orderBy}
      `
    )
    client.release()
    if (result.rowCount === 0) {
      return []
    }
    return result.rows.map((row) => {
      const tag = new SermonTag()
      tag.tag = row.tag
      tag.sermonCount = parseInt(row.sermon_count)
      return tag
    })
  }
}
