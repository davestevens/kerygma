import { inject, injectable } from 'tsyringe'
import { bcv_parser } from 'bible-passage-reference-parser/js/en_bcv_parser'
import ModuleReference from '../model/module-reference'
import { BibleBook } from '../model/bible-book'

@injectable()
export default class BiblePassageParser {
  constructor(
    @inject('bcv_parser') private readonly bcvParser: bcv_parser,
    @inject('bibleBooks') private readonly bibleBooks: BibleBook[]
  ) {}

  parsePassage(passage: string): {
    start: ModuleReference
    end: ModuleReference | null
  }[] {
    const parsedVerses = this.bcvParser.parse(passage)
    const osis = parsedVerses.osis()
    if (osis.length === 0) {
      return []
    }
    return osis.split(',').map((osisValue) => {
      const rangeParts = osisValue.split('-')
      const refs: ModuleReference[] = rangeParts.map((rangePart) => {
        const refParts = rangePart.split('.')
        const bookOsisId = refParts[0]
        const dbBibleBook = this.bibleBooks.find(
          (book) => book.abbrev === bookOsisId
        )
        const dbBookCode = dbBibleBook ? dbBibleBook.code : ''
        return {
          bookOsisId: refParts[0],
          dbBookCode: dbBookCode,
          chapter: parseInt(refParts[1]),
          verse: refParts[2] ? parseInt(refParts[2]) : null,
          verseId: 0,
        }
      })
      return { start: refs[0], end: refs?.[1] ?? null }
    })
  }
}
