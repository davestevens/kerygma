import { Pool } from 'pg'
import { inject, injectable } from 'tsyringe'

const LOGIN_TIMEOUT = '30 minutes'
const SUSPEND_DURATION = '30 minutes'

export type AuthLevel = 'sysadmin' | 'admin'

@injectable()
export default class AuthDataAccess {
  constructor(@inject('Pool') private readonly pool: Pool) {}

  public async verifyCredentials(
    username: string,
    password: string
  ): Promise<string | null> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT id FROM users
        WHERE username = $1
          AND crypt($2, password_hash) = password_hash
          AND (suspend_ts IS NULL
            OR now() > suspend_ts + interval '${SUSPEND_DURATION}'
          )
      `,
      [username, password]
    )
    client.release()
    if (result.rowCount === 0) {
      return null
    }
    return result.rows[0].id as string
  }

  public async verifyToken(token: string): Promise<string | null> {
    const client = await this.pool.connect()
    const result = await client.query(
      `
        SELECT *,
        CASE WHEN last_used < (now() - interval '${LOGIN_TIMEOUT}') THEN TRUE ELSE FALSE END as has_expired
        FROM login_sessions
        WHERE token = $1
      `,
      [token]
    )
    client.release()
    if (result.rowCount === 0) {
      return null
    }
    const session = result.rows[0]
    if (session.has_expired) {
      return null
    }
    return session.user_id as string
  }

  public async addLoginSession(userId: string, token: string): Promise<void> {
    const client = await this.pool.connect()
    await client.query(`
      DELETE FROM login_sessions
      WHERE last_used <
        (now() - interval '${LOGIN_TIMEOUT}')
    `)
    await client.query(
      `
      INSERT INTO login_sessions(
        user_id,
        token,
        last_used
      ) VALUES (
        $1,
        $2,
        current_timestamp
      )
    `,
      [userId, token]
    )
    client.release()
  }

  public async logout(token: string): Promise<void> {
    const client = await this.pool.connect()
    await client.query(
      `
        DELETE FROM login_sessions
        WHERE token = $1
      `,
      [token]
    )
    client.release()
  }

  public async extendSession(token: string) {
    const client = await this.pool.connect()
    await client.query(
      `
        UPDATE login_sessions SET last_used = now() where token = $1
      `,
      [token]
    )
    client.release()
  }
}
