import { inject, injectable } from 'tsyringe'
import { Sermon } from '../model/sermon'
import { KerygmaConfiguration } from '../model/kerygma-configuration'
import { FeedItunesCategory, FeedOptions, Podcast } from 'podcast'
import { BibleBook } from '../model/bible-book'

@injectable()
export default class FeedDataAccess {
  constructor(
    @inject('PodcastFactory')
    private podcastFactory: (options: FeedOptions) => Podcast,
    @inject('bibleBooks') private bibleBooks: BibleBook[]
  ) {}

  public async getPodcastFeed(
    sermons: Sermon[],
    config: KerygmaConfiguration,
    feedUrl: string,
    sermonsUri: string,
    faviconUrl: string
  ): Promise<string> {
    const itunesCategory = this.getItunesCategories(
      config.podcast?.categories ?? []
    )
    const podcast = this.podcastFactory({
      title: config.podcast?.title ?? '',
      feedUrl,
      siteUrl: config.podcast?.siteUrl ?? '',
      imageUrl: faviconUrl,
      author: config.podcast?.author ?? '',
      description: config.podcast?.description ?? '',
      copyright: config.podcast?.copyright ?? '',
      itunesType: 'episodic',
      language: config.podcast?.language,
      categories: config.podcast?.categories,
      itunesCategory,
    })
    for (const sermon of sermons) {
      let description = ''
      if (sermon.fromBook) {
        description += `passage: ${sermon.getPassage(this.bibleBooks)} `
      }
      description += `service: ${sermon.service}`
      const sermonUrl = `${sermonsUri}${sermon.filename}`

      podcast.addItem({
        guid: `${sermon.id}`,
        title: sermon.title,
        description: description,
        url: sermonUrl,
        enclosure: { url: sermonUrl },
        date: sermon.sermonDate,
        author: `${sermon.speakerFirstName} ${sermon.speakerLastName}`,
        itunesTitle: sermon.title,
        itunesExplicit: false,
      })
    }
    return podcast.buildXml()
  }

  private getItunesCategories(
    categories: string[]
  ): FeedItunesCategory[] | undefined {
    if (categories.length === 0) {
      return
    }
    const subcats = this.getItunesCategories(categories.slice(1))
    return [
      {
        text: categories[0],
        subcats,
      },
    ]
  }
}
