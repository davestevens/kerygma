import { inject, injectable } from 'tsyringe'
import { JsonConvert } from 'json2typescript'
import { PlatformPath } from 'path'
import * as yamlLib from 'js-yaml'
import * as fsLib from 'fs'
import { KerygmaConfiguration } from '../model/kerygma-configuration'
import { Logger } from 'pino'

@injectable()
export class ConfigParser {
  constructor(
    @inject('Logger') private logger: Logger,
    @inject('JsonConvert') private jsonConvert: JsonConvert,
    @inject('path') private path: PlatformPath,
    @inject('yaml') private yaml: typeof yamlLib,
    @inject('fs') private fs: typeof fsLib
  ) {}

  parseConfig(configPath: string): KerygmaConfiguration | undefined {
    const path = this.path.resolve(configPath)
    if (!this.fs.existsSync(path)) {
      this.logger.error({ msg: 'Kerygma config file could not be found', path })
      return
    }
    const configObj = this.yaml.load(
      this.fs.readFileSync(path, 'utf8')
    ) as object
    try {
      return this.jsonConvert.deserializeObject<KerygmaConfiguration>(
        configObj,
        KerygmaConfiguration
      )
    } catch (err) {
      this.logger.error({ msg: 'Kerygma config file was invalid', path, err })
    }
  }
}
