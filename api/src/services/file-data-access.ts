import { Stats, WriteStream } from 'fs'
import Path from 'path'
import { MultipartFile } from '@fastify/multipart'
import { UploadedDocument } from '../model/uploaded-document'
import { promisify } from 'util'
import { pipeline } from 'stream'
import { inject, injectable } from 'tsyringe'
import InputSanitizer from '../common/input-sanitizer'
const pump = promisify(pipeline)

export type FileSystem = {
  createWriteStream: (file: string) => WriteStream
  statSync: (file: string) => Stats
  existsSync: (file: string) => boolean
  mkdirSync: (dir: string, options: { recursive: boolean }) => void
  unlinkSync: (file: string) => void
  renameSync: (oldPath: string, newPath: string) => void
}

@injectable()
export default class FileDataAccess {
  constructor(
    private readonly inputSanitizer: InputSanitizer,
    @inject('FileSystem') private readonly fs: FileSystem,
    @inject('mediaPath') private readonly mediaPath: string
  ) {}

  public async saveDocumentFile(
    data: MultipartFile,
    doc: UploadedDocument
  ): Promise<number> {
    const docsPath = Path.resolve(`${this.mediaPath}/documents`)
    const pathExists = this.fs.existsSync(docsPath)
    if (!pathExists) {
      this.fs.mkdirSync(docsPath, { recursive: true })
    }

    const filename = this.inputSanitizer.sanitizeFilename(
      doc.getFilenameWithSuffix()
    )
    const serverFile = Path.resolve(`${docsPath}/${filename}`)
    await pump(data.file, this.fs.createWriteStream(serverFile))
    const stats = this.fs.statSync(serverFile)
    return stats.size
  }

  public async saveAudioFile(
    data: MultipartFile,
    filename: string
  ): Promise<number> {
    const docsPath = Path.resolve(`${this.mediaPath}/sermons`)
    const pathExists = this.fs.existsSync(docsPath)
    if (!pathExists) {
      this.fs.mkdirSync(docsPath, { recursive: true })
    }

    const sanitizedFilename = this.inputSanitizer.sanitizeFilename(filename)
    const serverFile = Path.resolve(`${docsPath}/${sanitizedFilename}`)
    await pump(data.file, this.fs.createWriteStream(serverFile))
    const stats = this.fs.statSync(serverFile)
    return stats.size
  }

  public deleteDocumentFile(doc: UploadedDocument): void {
    const docsPath = Path.resolve(`${this.mediaPath}/documents`)
    const filename = this.inputSanitizer.sanitizeFilename(
      doc.getFilenameWithSuffix()
    )
    const serverFile = Path.resolve(`${docsPath}/${filename}`)
    try {
      this.fs.unlinkSync(serverFile)
    } catch (_e) {
      /* intentionally ignored */
    }
  }

  public deleteAudioFile(filename: string): void {
    const docsPath = Path.resolve(`${this.mediaPath}/sermons`)
    const sanitizedFilename = this.inputSanitizer.sanitizeFilename(filename)
    const serverFile = Path.resolve(`${docsPath}/${sanitizedFilename}`)
    try {
      this.fs.unlinkSync(serverFile)
    } catch (_e) {
      /* intentionally ignored */
    }
  }

  public renameDocumentFile(oldFilename: string, newFilename: string): void {
    this.renameFile(oldFilename, newFilename, 'documents')
  }

  public renameAudioFile(oldFilename: string, newFilename: string): void {
    this.renameFile(oldFilename, newFilename, 'sermons')
  }

  private renameFile(
    oldFilename: string,
    newFilename: string,
    path: 'sermons' | 'documents'
  ): void {
    const docsPath = Path.resolve(`${this.mediaPath}/${path}`)
    const oldFilenameSanitized =
      this.inputSanitizer.sanitizeFilename(oldFilename)
    const oldServerFile = Path.resolve(`${docsPath}/${oldFilenameSanitized}`)
    const newFilenameSanitized =
      this.inputSanitizer.sanitizeFilename(newFilename)
    const newServerFile = Path.resolve(`${docsPath}/${newFilenameSanitized}`)
    try {
      this.fs.renameSync(oldServerFile, newServerFile)
    } catch (_e) {
      /* intentionally ignored */
    }
  }
}
