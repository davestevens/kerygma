import { FastifyReply, FastifyRequest } from 'fastify'

type PreHandler = (request: FastifyRequest, reply: FastifyReply) => void

export default PreHandler
