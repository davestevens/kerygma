import { FastifyReply, FastifyRequest } from 'fastify'
import AuthDataAccess, { AuthLevel } from '../services/auth-data-access'
import UserDataAccess from '../services/user-data-access'
import { StatusCodes } from 'http-status-codes'
import { RequestContext } from '@fastify/request-context'
import { inject, injectable } from 'tsyringe'

@injectable()
export default class AuthPreHandlerFactory {
  constructor(
    private readonly authDataAccess: AuthDataAccess,
    private readonly userDataAccess: UserDataAccess,
    @inject('requestContext') private readonly requestContext: RequestContext
  ) {}

  public createAdminAuthPreHandler() {
    return async (request: FastifyRequest, reply: FastifyReply) => {
      await this.authPreHandler(request, reply, 'admin')
    }
  }

  public createSysAdminAuthPreHandler() {
    return async (request: FastifyRequest, reply: FastifyReply) => {
      await this.authPreHandler(request, reply, 'sysadmin')
    }
  }

  private async authPreHandler(
    request: FastifyRequest,
    reply: FastifyReply,
    authLevel: AuthLevel
  ) {
    const authorizationHeader = request.headers.authorization
    if (!authorizationHeader || !authorizationHeader.startsWith('Bearer ')) {
      reply.status(StatusCodes.UNAUTHORIZED).send()
      return
    }
    const token = authorizationHeader.replace('Bearer ', '')
    const userId = await this.authDataAccess.verifyToken(token)
    if (userId === null) {
      reply.status(StatusCodes.UNAUTHORIZED).send()
      return
    }

    const user = await this.userDataAccess.get(userId)
    if (!user || (user.authLevel === 'admin' && authLevel === 'sysadmin')) {
      reply.status(StatusCodes.UNAUTHORIZED).send()
      return
    }

    await this.authDataAccess.extendSession(token)

    this.requestContext.set('user', user)
    this.requestContext.set('authToken', token)
  }
}
