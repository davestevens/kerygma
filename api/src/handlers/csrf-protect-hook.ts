import { inject, injectable } from 'tsyringe'
import { FastifyInstance } from 'fastify/types/instance'
import { jwtVerify as JwtVerify } from 'jose'
import { StatusCodes } from 'http-status-codes'

@injectable()
export default class CsrfProtectHook {
  constructor(
    @inject('jwtVerify') private jwtVerify: typeof JwtVerify,
    @inject('csrfSignedTokenSecret') private csrfSignedTokenSecret: string
  ) {}

  registerHook(fastify: FastifyInstance) {
    fastify.addHook('onRequest', async (request, reply) => {
      const { method } = request
      if (method === 'POST' || method === 'PUT' || method === 'DELETE') {
        const tokenFromCookie = request.cookies._csrf
        const tokenFromHeader = request.headers._csrf
        if (!tokenFromCookie || tokenFromCookie !== tokenFromHeader) {
          reply.status(StatusCodes.FORBIDDEN).send()
          return
        }
        const secret = new TextEncoder().encode(this.csrfSignedTokenSecret)
        const verifyResult = await this.jwtVerify(tokenFromCookie, secret)
        if (verifyResult.payload?.iss !== 'kerygma:api') {
          reply.status(StatusCodes.FORBIDDEN).send()
          return
        }
      }
    })
  }
}
