import { injectable } from 'tsyringe'

@injectable()
export default class InputSanitizer {
  sanitizeFilename(filename: string): string {
    return filename.replaceAll(/[^a-z0-9._-]/gi, '')
  }
}
