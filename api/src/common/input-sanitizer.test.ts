import InputSanitizer from './input-sanitizer'

test('InputSanitizer - sanitizeFilename', async () => {
  const sanitizer = new InputSanitizer()
  expect(sanitizer.sanitizeFilename('test file')).toBe('testfile')
  expect(sanitizer.sanitizeFilename('dir/hack/ing')).toBe('dirhacking')
  expect(sanitizer.sanitizeFilename('A_Val1d-file.pdf')).toBe(
    'A_Val1d-file.pdf'
  )
})
