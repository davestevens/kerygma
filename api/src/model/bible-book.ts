export class BibleBook {
  public name = ''
  public abbrev = ''
  public code = ''
  public chapters: number[] = []
}
