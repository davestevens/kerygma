import { JsonObject, JsonProperty } from 'json2typescript'

@JsonObject('SermonTag')
export class SermonTag {
  @JsonProperty('tag', String)
  tag = ''
  @JsonProperty('sermonCount', Number)
  sermonCount = 0
}
