import { JsonObject, JsonProperty } from 'json2typescript'
import * as yup from 'yup'
import DateConverter from './date-converter'
import dayjs, { Dayjs } from 'dayjs'
import { BibleBook } from './bible-book'

@JsonObject('Sermon')
export class Sermon {
  @JsonProperty('id', Number)
  id = 0
  @JsonProperty('sermonDate', String)
  sermonDate = ''
  public setSermonDate(dt: Dayjs) {
    this.sermonDate = dt.format('YYYY-MM-DD')
  }
  public getSermonDate(): Dayjs {
    return dayjs(this.sermonDate, 'YYYY-MM-DD')
  }
  @JsonProperty('speakerTitle', String)
  speakerTitle = ''
  @JsonProperty('speakerFirstName', String)
  speakerFirstName = ''
  @JsonProperty('speakerLastName', String)
  speakerLastName = ''
  @JsonProperty('title', String)
  title = ''
  @JsonProperty('tags', [String])
  tags: string[] = []
  @JsonProperty('service', String)
  service = ''
  @JsonProperty('fromBook', String)
  fromBook = ''
  @JsonProperty('fromChapter', Number)
  fromChapter = 0
  @JsonProperty('fromVerse', Number)
  fromVerse = 0
  @JsonProperty('thruBook', String)
  thruBook = ''
  @JsonProperty('thruChapter', Number)
  thruChapter = 0
  @JsonProperty('thruVerse', Number)
  thruVerse = 0
  @JsonProperty('filename', String)
  filename = ''
  @JsonProperty('uploadedTimestamp', DateConverter, true)
  uploadedTimestamp = dayjs()
  @JsonProperty('size', Number, true)
  size = 0

  speakerFirstNameValidator = (): string => {
    return !yup.string().trim().required().isValidSync(this.speakerFirstName)
      ? 'is required'
      : ''
  }

  speakerLastNameValidator = (): string => {
    return !yup.string().trim().required().isValidSync(this.speakerLastName)
      ? 'is required'
      : ''
  }

  titleValidator = (): string => {
    return !yup.string().trim().required().isValidSync(this.title)
      ? 'is required'
      : ''
  }

  tagValidator = (): string => {
    return this.tags.length > 20 ? 'too many tags' : ''
  }

  isValid(): boolean {
    for (const key in this) {
      const validator = this[key]
      if (
        key.endsWith('Validator') &&
        typeof validator === 'function' &&
        validator() !== ''
      ) {
        return false
      }
    }
    return true
  }

  getFilename(bibleBooks: BibleBook[]): string {
    const dt = this.getSermonDate().format('YYYY-MM-DD')
    const svc = this.service.replace(' ', '_').toLowerCase()
    let suffix: string | null = null
    const fromBook = this.getFromBook(bibleBooks)
    if (fromBook) {
      suffix = `${fromBook.abbrev}${this.fromChapter ?? ''}v${this.fromVerse}-${
        this.thruChapter
      }v${this.thruVerse}`
    } else {
      suffix = this.title.replace(/\W/g, '')
    }
    return `${dt}_${this.speakerFirstName}_${this.speakerLastName}_${svc}_${suffix}.mp3`
  }

  getPassage(bibleBooks: BibleBook[]): string | null {
    const fromBook = this.getFromBook(bibleBooks)
    if (!fromBook) {
      return null
    }
    return `${fromBook.name} ${this.fromChapter}:${this.fromVerse}-${this.thruChapter}:${this.thruVerse}`
  }

  public getFromBook(bibleBooks: BibleBook[]): BibleBook | null {
    if (!this.fromBook) {
      return null
    }
    const fromBooks = bibleBooks.filter((book) => book.code === this.fromBook)
    return fromBooks.length > 0 ? fromBooks[0] : null
  }

  /*
   * This is necessary for the useState react hook to modify the state
   * of the form.
   */
  clone(): Sermon {
    const clone = new Sermon()
    Object.assign(clone, this)
    return clone
  }
}
