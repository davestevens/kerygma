import { JsonObject, JsonProperty } from 'json2typescript'

@JsonObject('GeneratorSettings')
export class GeneratorSettings {
  @JsonProperty('outDir', String) outDir = ''
  @JsonProperty('pagesDir', String) pagesDir = ''
  @JsonProperty('assetsDir', String) assetsDir = ''
}

@JsonObject('ApiSettings')
export class ApiSettings {
  @JsonProperty('apiHost', String) apiHost = ''
  @JsonProperty('mediaContextRoot', String) mediaContextRoot = ''
}

@JsonObject('ConfigLogo')
export class ConfigLogo {
  @JsonProperty('path', String) path = ''
  @JsonProperty('height', String) height = ''
}

@JsonObject('ConfigFavicon')
export class ConfigFavicon {
  @JsonProperty('path', String) path = ''
  @JsonProperty('fileType', String) fileType = ''
}

@JsonObject('LatLng')
export class LatLng {
  @JsonProperty('lat', Number) lat = 0
  @JsonProperty('lng', Number) lng = 0
}

@JsonObject('ConfigLocation')
export class ConfigLocation {
  @JsonProperty('street1', String) street1 = ''
  @JsonProperty('street2', String, true) street2 = ''
  @JsonProperty('city', String) city = ''
  @JsonProperty('state', String) state = ''
  @JsonProperty('postalCode', String) postalCode = ''
  @JsonProperty('directionsUrl', String) directionsUrl = ''
  @JsonProperty('location', LatLng) location?: LatLng = undefined
  @JsonProperty('zoom', Number) zoom = 16
}

@JsonObject('ConfigSpeaker')
export class ConfigSpeaker {
  @JsonProperty('title', String) title = ''
  @JsonProperty('firstName', String) firstName = ''
  @JsonProperty('lastName', String) lastName = ''
  @JsonProperty('default', Boolean, true) default = false
}

@JsonObject('FontFamily')
export class FontFamily {
  @JsonProperty('url', String) url = ''
  @JsonProperty('family', String) family = ''
}

@JsonObject('KerygmaThemeFonts')
export class KerygmaThemeFonts {
  @JsonProperty('default', FontFamily) default?: FontFamily = undefined
  @JsonProperty('titles', FontFamily) titles?: FontFamily = undefined
  @JsonProperty('weightDefault', Number) weightDefault = 300
  @JsonProperty('weightHeaders', Number) weightHeaders = 400
  @JsonProperty('weightFooter', Number) weightFooter = 300
  @JsonProperty('sizeDefault', String) sizeDefault = ''
}

@JsonObject('KerygmaThemeColors')
export class KerygmaThemeColors {
  @JsonProperty('primary', String) primary = ''
  @JsonProperty('primaryText', String) primaryText = ''
  @JsonProperty('links', String) links = ''
  @JsonProperty('headerBackground', String) headerBackground = ''
  @JsonProperty('bodyBackground', String) bodyBackground = ''
  @JsonProperty('footerBackground', String) footerBackground = ''
  @JsonProperty('footerFont', String) footerFont = ''
  @JsonProperty('footerFontLink', String) footerFontLink = ''
  @JsonProperty('hr', String) hr = ''
  @JsonProperty('fontDefault', String) fontDefault = ''
  @JsonProperty('fontHeaders', String) fontHeaders = ''
  @JsonProperty('fontHeadersSmaller', String) fontHeadersSmaller = ''
  @JsonProperty('border', String) border = ''
}

@JsonObject('KerygmaTheme')
export class KerygmaTheme {
  @JsonProperty('fonts', KerygmaThemeFonts) fonts?: KerygmaThemeFonts =
    undefined
  @JsonProperty('colors', KerygmaThemeColors) colors?: KerygmaThemeColors =
    undefined
}

@JsonObject('Podcast')
export class Podcast {
  @JsonProperty('length', Number) length?: number = undefined
  @JsonProperty('siteUrl', String) siteUrl?: string = undefined
  @JsonProperty('title', String) title?: string = undefined
  @JsonProperty('description', String) description?: string = undefined
  @JsonProperty('author', String) author?: string = undefined
  @JsonProperty('copyright', String) copyright?: string = undefined
  @JsonProperty('language', String) language?: string = undefined
  @JsonProperty('categories', [String]) categories?: string[] = []
}

@JsonObject('Navigation')
export class Navigation {
  @JsonProperty('label', String) label?: string = undefined
  @JsonProperty('path', String, true) path?: string = undefined
  @JsonProperty('children', [Navigation], true) children?: Navigation[] = []
}

@JsonObject('KerygmaConfiguration')
export class KerygmaConfiguration {
  @JsonProperty('generator', GeneratorSettings) generator?: GeneratorSettings =
    undefined
  @JsonProperty('api', ApiSettings) api?: ApiSettings = undefined
  @JsonProperty('churchNameFull', String) churchNameFull = ''
  @JsonProperty('churchNameShort', String) churchNameShort = ''
  @JsonProperty('phone', String) phone = ''
  @JsonProperty('email', String) email = ''
  @JsonProperty('footerText', String) footerText = ''
  @JsonProperty('logo', ConfigLogo) logo?: ConfigLogo = undefined
  @JsonProperty('favicon', ConfigFavicon) favicon?: ConfigFavicon = undefined
  @JsonProperty('worshipLocation', ConfigLocation)
  worshipLocation?: ConfigLocation = undefined
  @JsonProperty('defaultSpeakers', [ConfigSpeaker])
  defaultSpeakers: ConfigSpeaker[] = []
  @JsonProperty('services', [String]) services = ['Morning Worship']
  @JsonProperty('podcast', Podcast) podcast?: Podcast = undefined
  @JsonProperty('theme', KerygmaTheme) theme?: KerygmaTheme = undefined
  @JsonProperty('navigation', [Navigation]) navigation?: Navigation[] = []
}
