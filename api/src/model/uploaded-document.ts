import { JsonObject, JsonProperty } from 'json2typescript'
import * as yup from 'yup'
import DateConverter from './date-converter'
import dayjs from 'dayjs'

@JsonObject('Document')
export class UploadedDocument {
  @JsonProperty('id', Number)
  id = 0

  @JsonProperty('filename', String)
  filename = ''

  @JsonProperty('type', String)
  type = 'pdf'

  @JsonProperty('title', String)
  title = ''

  @JsonProperty('url', String, true)
  url = ''

  @JsonProperty('size', Number, true)
  size = 0

  @JsonProperty('uploadedTimestamp', DateConverter, true)
  uploadedTimestamp = dayjs()

  // Field Validators
  filenameValidator = (): string => {
    if (!yup.string().trim().required().isValidSync(this.filename)) {
      return 'is required'
    }
    if (!yup.string().trim().min(3).isValidSync(this.filename)) {
      return 'must be at least 3 characters'
    }
    return !yup
      .string()
      .trim()
      .matches(/[^a-z0-9_-]/)
      .isValidSync(this.filename)
      ? ''
      : 'can only contain lowercase alpha numeric characters, underscores or dashes.'
  }

  titleValidator = (): string => {
    return !yup.string().trim().required().isValidSync(this.title)
      ? 'is required'
      : ''
  }

  isValid(): boolean {
    return this.filenameValidator() === '' && this.titleValidator() === ''
  }

  getFilenameWithSuffix(): string {
    return `${this.filename}.${this.type}`
  }

  /*
   * This is necessary for the useState react hook to modify the state
   * of the form.
   */
  clone(): UploadedDocument {
    const clone = new UploadedDocument()
    Object.assign(clone, this)
    return clone
  }
}
