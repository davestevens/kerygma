import { UploadedDocument } from './uploaded-document'

test('filename validator', () => {
  const doc = new UploadedDocument()
  expect(doc.filenameValidator()).toBe('is required')
  doc.filename = 'a'
  expect(doc.filenameValidator()).toBe('must be at least 3 characters')
  doc.filename = 'Invalid@document-name.pdf'
  expect(doc.filenameValidator()).toBe(
    'can only contain lowercase alpha numeric characters, underscores or dashes.'
  )
  doc.filename = 'Invalid/document-name.pdf'
  expect(doc.filenameValidator()).toBe(
    'can only contain lowercase alpha numeric characters, underscores or dashes.'
  )
  doc.filename = 'Invaliddocument-name'
  expect(doc.filenameValidator()).toBe(
    'can only contain lowercase alpha numeric characters, underscores or dashes.'
  )
  doc.filename = 'this_isavaliddocument-namepdf'
  expect(doc.filenameValidator()).toBe('')
})

test('title validator', () => {
  const doc = new UploadedDocument()
  expect(doc.titleValidator()).toBe('is required')
  doc.title = 'A Title'
  expect(doc.titleValidator()).toBe('')
})

test('entire form is valid', () => {
  const doc = new UploadedDocument()
  expect(doc.isValid()).toBeFalsy()
  doc.filename = 'short'
  expect(doc.isValid()).toBeFalsy()
  doc.filename = 'Invalid@document-name'
  expect(doc.isValid()).toBeFalsy()
  doc.filename = 'Invalid/document-name.pdf'
  expect(doc.isValid()).toBeFalsy()
  doc.filename = 'Invalid$document-name'
  expect(doc.isValid()).toBeFalsy()
  doc.filename = 'this_is-a-validDocument-name'
  expect(doc.isValid()).toBeFalsy()
  doc.filename = 'this_is-a-validdocument-name'
  doc.title = 'Title of Document'
  expect(doc.isValid()).toBeTruthy()
})

test('getFilenameWithSuffix', () => {
  const doc = new UploadedDocument()
  doc.filename = 'test-file'
  expect(doc.getFilenameWithSuffix()).toBe('test-file.pdf')
})

test('clone', () => {
  const doc = new UploadedDocument()
  doc.filename = 'filenameABC'
  doc.title = 'Title ABC'
  const doc2 = doc.clone()
  doc2.filename = 'filenameEFG'
  doc2.title = 'Title EFG'
  expect(doc.filename).toBe('filenameABC')
  expect(doc.title).toBe('Title ABC')
})
