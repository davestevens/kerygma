export default interface ModuleReference {
  bookOsisId: string
  dbBookCode: string
  chapter: number
  verse: number | null
  verseId: number
}
