import pino, { Logger } from 'pino'
import { container } from 'tsyringe'
import { Pool } from 'pg'
import { JsonConvert } from 'json2typescript'
import PreHandler from './handlers/pre-handler'
import { config } from 'dotenv'
import * as fs from 'fs'
import { FileSystem } from './services/file-data-access'
import { BibleBook } from './model/bible-book'
import { bcv_parser } from 'bible-passage-reference-parser/js/en_bcv_parser'
import Fastify from 'fastify'
import {
  RequestContext,
  requestContext,
  fastifyRequestContext,
} from '@fastify/request-context'
import fastifyCookie from '@fastify/cookie'
import fastifyMultipart from '@fastify/multipart'
import fastifyStatic from '@fastify/static'
import path, { PlatformPath } from 'path'
import dbmigrate from 'db-migrate'
import { SignJWT, jwtVerify } from 'jose'
import AuthPreHandlerFactory from './handlers/auth-pre-handler-factory'
import { FeedOptions, Podcast } from 'podcast'
import yaml from 'js-yaml'

const bibleBookPath = path.resolve(`${__dirname}/model/bible-book-data.yaml`)
const bibleBookYaml = fs.readFileSync(bibleBookPath, 'utf8')
const bibleBookData = yaml.load(bibleBookYaml) as { books: BibleBook[] }
const bibleBooks = bibleBookData.books

config()

container.register<PlatformPath>('path', { useValue: path })
container.register<Pool>('Pool', { useValue: new Pool() })
container.register<JsonConvert>('JsonConvert', { useValue: new JsonConvert() })
const logger = pino({
  transport: {
    target: 'pino-pretty',
    options: { colorize: true, singleLine: true },
  },
})
container.register<Logger>('Logger', { useValue: logger })
const staticServe = process.env.STATIC_HTML_SERVE === 'true'
container.register<boolean>('staticServe', { useValue: staticServe })
const staticPath = process.env.STATIC_HTML_PATH ?? 'STATIC_HTML_PATH not set'
container.register<string>('staticPath', { useValue: staticPath })
const mediaServe = process.env.MEDIA_SERVE === 'true'
container.register<boolean>('mediaServe', { useValue: mediaServe })
const mediaPath = process.env.MEDIA_PATH ?? 'MEDIA_PATH not set'
container.register<string>('mediaPath', { useValue: mediaPath })
const mediaServeContextRoot =
  process.env.MEDIA_SERVE_CONTEXT_ROOT ?? 'MEDIA_SERVE_CONTEXT_ROOT not set'
container.register<string>('mediaServeContextRoot', {
  useValue: mediaServeContextRoot,
})
const apiPort = process.env.API_PORT ?? '9000'
container.register<number>('apiPort', { useValue: parseInt(apiPort) })
// const apiCorsOrigin = process.env.API_CORS_ORIGIN
//container.register<string | undefined>('apiCorsOrigin', {
//  useValue: apiCorsOrigin,
//})
const maxFileSize =
  process.env.MEDIA_MAX_FILE_SIZE ?? 'MEDIA_MAX_FILE_SIZE not set'
container.register<typeof Fastify>('Fastify', { useValue: Fastify })
container.register<RequestContext>('requestContext', {
  useValue: requestContext,
})
container.register<typeof fastifyRequestContext>('fastifyRequestContext', {
  useValue: fastifyRequestContext,
})
container.register<typeof fastifyCookie>('fastifyCookie', {
  useValue: fastifyCookie,
})
container.register<typeof fastifyMultipart>('fastifyMultipart', {
  useValue: fastifyMultipart,
})
container.register<typeof fastifyStatic>('fastifyStatic', {
  useValue: fastifyStatic,
})
container.register<SignJWT>('SignJWT', {
  useValue: new SignJWT({ appName: 'kerygma-api' }),
})
container.register<typeof jwtVerify>('jwtVerify', { useValue: jwtVerify })
const csrfSignedTokenSecret =
  process.env.CSRF_SIGNED_TOKEN_SECRET ?? 'CSRF_SIGNED_TOKEN_SECRET not set'
container.register<string>('csrfSignedTokenSecret', {
  useValue: csrfSignedTokenSecret,
})
container.register<number>('maxFileSize', { useValue: parseInt(maxFileSize) })
container.register<BibleBook[]>('bibleBooks', { useValue: bibleBooks })
container.register<FileSystem>('FileSystem', { useValue: fs })
const authPreHandlerFactory = container.resolve(AuthPreHandlerFactory)
const adminPreHandler = authPreHandlerFactory.createAdminAuthPreHandler()
const sysAdminPreHandler = authPreHandlerFactory.createSysAdminAuthPreHandler()
container.register<PreHandler>('adminPreHandler', { useValue: adminPreHandler })
container.register<PreHandler>('sysAdminPreHandler', {
  useValue: sysAdminPreHandler,
})
const bcv = new bcv_parser()
bcv.set_options({
  versification_system: 'kjv',
  osis_compaction_strategy: 'bcv',
  book_alone_strategy: 'full',
})
container.register<bcv_parser>('bcv_parser', { useValue: bcv })
container.register<typeof dbmigrate>('dbmigrate', { useValue: dbmigrate })
container.register<typeof fs>('fs', { useValue: fs })
container.register<typeof yaml>('yaml', { useValue: yaml })
container.register<(options: FeedOptions) => Podcast>('PodcastFactory', {
  useValue: (options: FeedOptions) => new Podcast(options),
})

export default container
