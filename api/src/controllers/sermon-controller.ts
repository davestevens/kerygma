import Controller from './controller'
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify'
import SermonDataAccess from '../services/sermon-data-access'
import FileDataAccess from '../services/file-data-access'
import { JsonConvert } from 'json2typescript'
import PreHandler from '../handlers/pre-handler'
import { StatusCodes } from 'http-status-codes'
import {
  MultipartFields,
  MultipartFile,
  MultipartValue,
} from '@fastify/multipart'
import { Sermon } from '../model/sermon'
import dayjs from 'dayjs'
import { BibleBook } from '../model/bible-book'
import { inject, injectable } from 'tsyringe'
import { Logger } from 'pino'

type GetSermonsQString = {
  page: string
  perPage: string
  sortOrder: 'latest' | 'oldest'
  filter?: string
  'tags[]'?: string | string[]
}

type GetSermonTagsQString = {
  sortOrder: 'tag' | 'sermonCount'
}

@injectable()
export default class SermonController implements Controller {
  private fastify?: FastifyInstance

  constructor(
    private readonly sermonDataAccess: SermonDataAccess,
    private readonly fileDataAccess: FileDataAccess,
    @inject('Logger') private logger: Logger,
    @inject('JsonConvert') private readonly jsonConvert: JsonConvert,
    @inject('adminPreHandler') private readonly adminPreHandler: PreHandler,
    @inject('bibleBooks') private readonly bibleBooks: BibleBook[]
  ) {}

  registerRoutes(fastify: FastifyInstance): void {
    this.fastify = fastify
    fastify.post(
      '/api/sermons',
      { preHandler: this.adminPreHandler },
      async (request, reply) => {
        await this.addSermon(request, reply)
      }
    )
    fastify.put(
      '/api/sermons/:sermonId',
      { preHandler: this.adminPreHandler },
      async (request, reply) => {
        await this.saveSermon(request, reply)
      }
    )
    // get is unsecure so the public site can query the sermons
    fastify.get(
      '/api/sermons',
      async (
        request: FastifyRequest<{ Querystring: GetSermonsQString }>,
        reply
      ) => {
        await this.getSermons(request, reply)
      }
    )
    fastify.get(
      '/api/sermons/:sermonId',
      async (request: FastifyRequest, reply) => {
        await this.getSermon(request, reply)
      }
    )
    fastify.get(
      '/api/sermon-tags',
      async (
        request: FastifyRequest<{ Querystring: GetSermonTagsQString }>,
        reply
      ) => {
        await this.getSermonTags(request, reply)
      }
    )
    fastify.delete(
      '/api/sermons/:sermonId',
      { preHandler: this.adminPreHandler },
      async (request, reply) => {
        await this.deleteSermon(request, reply)
      }
    )
  }

  public async getSermons(
    request: FastifyRequest<{ Querystring: GetSermonsQString }>,
    reply: FastifyReply
  ): Promise<void> {
    const { page, perPage, sortOrder, filter } = request.query
    const tagsQuery = request.query['tags[]']
    const tags = typeof tagsQuery === 'string' ? [tagsQuery] : tagsQuery
    const pg = parseInt(page)
    const per = parseInt(perPage)

    const results = await this.sermonDataAccess.getAll(
      pg,
      per,
      sortOrder,
      filter,
      tags
    )
    reply.send({ status: 'ok', ...results })
  }

  public async getSermon(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const params = request.params as { sermonId: string }
    const sermonId = parseInt(params.sermonId)
    try {
      const sermon = await this.sermonDataAccess.get(sermonId)
      reply.status(StatusCodes.OK).send({ status: 'ok', sermon })
    } catch (error) {
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }

  public async addSermon(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    this.jsonConvert.ignorePrimitiveChecks = false
    try {
      const data = await request.file()
      if (!data) {
        reply
          .status(StatusCodes.BAD_REQUEST)
          .send({ status: 'error', message: 'File not found' })
        return
      }
      if (data.mimetype !== 'audio/mpeg') {
        reply
          .status(StatusCodes.BAD_REQUEST)
          .send({ status: 'error', message: 'Invalid mimetype' })
        return
      }

      const sermon = SermonController.createSermon(data.fields)
      const filename = sermon.getFilename(this.bibleBooks)
      sermon.filename = data.filename

      if (!sermon.isValid()) {
        reply.status(StatusCodes.BAD_REQUEST).send({
          status: 'error',
          message: 'Sermon meta data was invalid',
        })
        return
      }

      const size = await this.fileDataAccess.saveAudioFile(data, filename)
      if (data.file.truncated) {
        reply
          .status(StatusCodes.REQUEST_TOO_LONG)
          .send({ status: 'error', message: 'File size is too large.' })
        return
      }

      sermon.size = size
      await this.sermonDataAccess.add(sermon, filename)

      reply.status(StatusCodes.OK).send()
    } catch (error) {
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }

  public async saveSermon(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    this.jsonConvert.ignorePrimitiveChecks = false

    try {
      const params = request.params as { sermonId: string }
      const sermonId = parseInt(params.sermonId)
      const isMultipart = !request.body

      let sermon = await this.sermonDataAccess.get(sermonId)
      if (sermon === null) {
        reply.status(StatusCodes.NOT_FOUND).send({
          status: 'error',
          message: 'Cannot update a resource that doesn’t exist',
        })
        return
      }
      const priorFilename = sermon.filename

      let file: MultipartFile | undefined
      if (isMultipart) {
        file = await request.file()
        if (!file) {
          reply
            .status(StatusCodes.BAD_REQUEST)
            .send({ status: 'error', message: 'File not found' })
          return
        }
        if (file.mimetype !== 'audio/mpeg') {
          reply
            .status(StatusCodes.BAD_REQUEST)
            .send({ status: 'error', message: 'Invalid mimetype' })
          return
        }
        sermon = SermonController.createSermon(file.fields, sermon)
      } else {
        const sermonChanges = this.jsonConvert.deserializeObject(
          request.body,
          Sermon
        )
        sermon.setSermonDate(sermonChanges.getSermonDate())
        sermon.speakerTitle = sermonChanges.speakerTitle
        sermon.speakerFirstName = sermonChanges.speakerFirstName
        sermon.speakerLastName = sermonChanges.speakerLastName
        sermon.title = sermonChanges.title
        sermon.tags = sermonChanges.tags
        sermon.service = sermonChanges.service
        sermon.fromBook = sermonChanges.fromBook
        sermon.fromChapter = sermonChanges.fromChapter
        sermon.fromVerse = sermonChanges.fromVerse
        sermon.thruBook = sermonChanges.thruBook
        sermon.thruChapter = sermonChanges.thruChapter
        sermon.thruVerse = sermonChanges.thruVerse
        sermon.uploadedTimestamp = dayjs()
      }
      sermon.filename = sermon.getFilename(this.bibleBooks)

      if (!sermon.isValid()) {
        reply.status(StatusCodes.BAD_REQUEST).send({
          status: 'error',
          message: 'Sermon meta data was invalid',
        })
        return
      }

      const dup = await this.sermonDataAccess.getByFilename(sermon.filename)
      if (dup !== null && dup.id !== sermon.id) {
        reply
          .status(StatusCodes.CONFLICT)
          .send({ status: 'error', message: 'Filename already in use.' })
        return
      }

      if (isMultipart && file) {
        sermon.size = await this.fileDataAccess.saveAudioFile(
          file,
          sermon.filename
        )
        if (priorFilename !== sermon.filename) {
          await this.fileDataAccess.deleteAudioFile(priorFilename)
        }
      } else if (priorFilename !== sermon.filename) {
        this.fileDataAccess.renameAudioFile(priorFilename, sermon.filename)
      }

      await this.sermonDataAccess.save(sermon)
      reply.status(StatusCodes.OK).send()
    } catch (error) {
      if (
        this.fastify &&
        error instanceof this.fastify.multipartErrors.RequestFileTooLargeError
      ) {
        reply
          .status(StatusCodes.REQUEST_TOO_LONG)
          .send({ status: 'error', message: 'File size is too large.' })
      }
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }

  private static createSermon(
    fields: MultipartFields,
    sermon = new Sermon()
  ): Sermon {
    const sermonDateField =
      fields.sermonDate as unknown as MultipartValue<string>
    const speakerTitleField =
      fields.speakerTitle as unknown as MultipartValue<string>
    const speakerFirstNameField =
      fields.speakerFirstName as unknown as MultipartValue<string>
    const speakerLastNameField =
      fields.speakerLastName as unknown as MultipartValue<string>
    const serviceField = fields.service as unknown as MultipartValue<string>
    const titleField = fields.title as unknown as MultipartValue<string>
    const tagsField = fields.tags as unknown as MultipartValue<string>
    const fromBookField = fields.fromBook as unknown as MultipartValue<string>
    const fromChapterField =
      fields.fromChapter as unknown as MultipartValue<string>
    const fromVerseField = fields.fromVerse as unknown as MultipartValue<string>
    const thruBookField = fields.thruBook as unknown as MultipartValue<string>
    const thruChapterField =
      fields.thruChapter as unknown as MultipartValue<string>
    const thruVerseField = fields.thruVerse as unknown as MultipartValue<string>
    sermon.setSermonDate(dayjs(sermonDateField.value))
    sermon.speakerTitle = speakerTitleField.value
    sermon.speakerFirstName = speakerFirstNameField.value
    sermon.speakerLastName = speakerLastNameField.value
    sermon.title = titleField.value
    sermon.tags = JSON.parse(tagsField.value)
    sermon.service = serviceField.value
    sermon.fromBook = fromBookField.value
    sermon.fromChapter = parseInt(fromChapterField.value)
    sermon.fromVerse = parseInt(fromVerseField.value)
    sermon.thruBook = thruBookField.value
    sermon.thruChapter = parseInt(thruChapterField.value)
    sermon.thruVerse = parseInt(thruVerseField.value)
    sermon.uploadedTimestamp = dayjs()
    return sermon
  }

  public async deleteSermon(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const params = request.params as { sermonId: string }
    const sermonId = parseInt(params.sermonId)
    try {
      const sermon = await this.sermonDataAccess.get(sermonId)
      if (sermon) {
        this.fileDataAccess.deleteAudioFile(sermon.filename)
      }
      await this.sermonDataAccess.delete(sermonId)
      reply.status(StatusCodes.OK).send({ status: 'ok' })
    } catch (error) {
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }

  public async getSermonTags(
    request: FastifyRequest<{ Querystring: GetSermonTagsQString }>,
    reply: FastifyReply
  ): Promise<void> {
    const { sortOrder } = request.query

    const tags = await this.sermonDataAccess.getSermonTags(sortOrder)

    reply.send({ status: 'ok', tags })
  }
}
