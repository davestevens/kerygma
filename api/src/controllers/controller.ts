import { FastifyInstance } from 'fastify'

export default interface Controller {
  registerRoutes(fastify: FastifyInstance): void
}
