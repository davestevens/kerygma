import Controller from './controller'
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify'
import PreHandler from '../handlers/pre-handler'
import { JsonConvert } from 'json2typescript'
import { StatusCodes } from 'http-status-codes'
import { UploadedDocument } from '../model/uploaded-document'
import { MultipartFile, MultipartValue } from '@fastify/multipart'
import DocumentDataAccess from '../services/document-data-access'
import FileDataAccess from '../services/file-data-access'
import dayjs from 'dayjs'
import { inject, injectable } from 'tsyringe'
import { Logger } from 'pino'

@injectable()
export default class DocumentController implements Controller {
  private fastify?: FastifyInstance

  constructor(
    private readonly documentDataAccess: DocumentDataAccess,
    private readonly fileDataAccess: FileDataAccess,
    @inject('Logger') private readonly logger: Logger,
    @inject('JsonConvert') private readonly jsonConvert: JsonConvert,
    @inject('adminPreHandler') private readonly adminPreHandler: PreHandler
  ) {}

  registerRoutes(fastify: FastifyInstance): void {
    this.fastify = fastify
    fastify.post(
      '/api/documents',
      { preHandler: this.adminPreHandler },
      async (request, reply) => {
        await this.addDocument(request, reply)
      }
    )
    fastify.put(
      '/api/documents/:documentId',
      { preHandler: this.adminPreHandler },
      async (request, reply) => {
        await this.saveDocument(request, reply)
      }
    )
    fastify.get(
      '/api/documents',
      { preHandler: this.adminPreHandler },
      async (request, reply) => {
        await this.getDocuments(request, reply)
      }
    )
    fastify.get(
      '/api/documents/:documentId',
      { preHandler: this.adminPreHandler },
      async (request, reply) => {
        await this.getDocument(request, reply)
      }
    )
    fastify.delete(
      '/api/documents/:documentId',
      { preHandler: this.adminPreHandler },
      async (request, reply) => {
        await this.deleteDocument(request, reply)
      }
    )
  }

  public async getDocuments(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const loginUser = request.requestContext.get('user')
    const documents = await this.documentDataAccess.getAll()
    reply.send({ status: 'ok', documents, loginUser })
  }

  public async getDocument(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const loginUser = request.requestContext.get('user')
    const params = request.params as { documentId: string }
    const docId = parseInt(params.documentId)
    const document = await this.documentDataAccess.get(docId)
    if (document === null) {
      reply
        .status(StatusCodes.NOT_FOUND)
        .send({ status: 'error', message: 'Document not found.' })
      return
    }
    reply.send({ status: 'ok', document, loginUser })
  }

  public async addDocument(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    this.jsonConvert.ignorePrimitiveChecks = false
    try {
      const data = await request.file()
      if (!data) {
        reply
          .status(StatusCodes.BAD_REQUEST)
          .send({ status: 'error', message: 'File not found' })
        return
      }
      if (data.mimetype !== 'application/pdf') {
        reply
          .status(StatusCodes.BAD_REQUEST)
          .send({ status: 'error', message: 'Invalid mimetype' })
        return
      }

      const filenameField = data.fields
        .filename as unknown as MultipartValue<string>
      const titleField = data.fields.title as unknown as MultipartValue<string>
      const doc = new UploadedDocument()
      doc.filename = filenameField.value
      doc.title = titleField.value
      if (!doc.isValid()) {
        reply.status(StatusCodes.BAD_REQUEST).send({
          status: 'error',
          message: 'Uploaded document meta data was invalid',
        })
        return
      }

      const dup = await this.documentDataAccess.getByFilename(doc.filename)
      if (dup !== null) {
        reply
          .status(StatusCodes.CONFLICT)
          .send({ status: 'error', message: 'Filename already in use.' })
        return
      }

      doc.size = await this.fileDataAccess.saveDocumentFile(data, doc)
      if (data.file.truncated) {
        reply
          .status(StatusCodes.REQUEST_TOO_LONG)
          .send({ status: 'error', message: 'File size is too large.' })
        return
      }

      await this.documentDataAccess.add(doc)

      reply.status(StatusCodes.OK).send()
    } catch (error) {
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }

  public async saveDocument(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    this.jsonConvert.ignorePrimitiveChecks = false
    try {
      const params = request.params as { documentId: string }
      const docId = parseInt(params.documentId)
      const isMultipart = !request.body

      const doc = await this.documentDataAccess.get(docId)
      if (doc === null) {
        reply.status(StatusCodes.NOT_FOUND).send({
          status: 'error',
          message: 'Cannot update a resource that doesn’t exist',
        })
        return
      }
      const priorDoc = doc.clone()

      let file: MultipartFile | undefined
      if (isMultipart) {
        file = await request.file()
        if (!file) {
          reply
            .status(StatusCodes.BAD_REQUEST)
            .send({ status: 'error', message: 'File not found' })
          return
        }
        if (file.mimetype !== 'application/pdf') {
          reply
            .status(StatusCodes.BAD_REQUEST)
            .send({ status: 'error', message: 'Invalid mimetype' })
          return
        }

        const filenameField = file.fields
          .filename as unknown as MultipartValue<string>
        const titleField = file.fields
          .title as unknown as MultipartValue<string>
        doc.filename = filenameField.value
        doc.title = titleField.value
        doc.uploadedTimestamp = dayjs()
      } else {
        const docChanges = this.jsonConvert.deserializeObject(
          request.body,
          UploadedDocument
        )
        doc.filename = docChanges.filename
        doc.title = docChanges.title
      }

      if (!doc.isValid()) {
        reply.status(StatusCodes.BAD_REQUEST).send({
          status: 'error',
          message: 'Uploaded document meta data was invalid',
        })
        return
      }

      const dup = await this.documentDataAccess.getByFilename(doc.filename)
      if (dup !== null && dup.id !== doc.id) {
        reply
          .status(StatusCodes.CONFLICT)
          .send({ status: 'error', message: 'Filename already in use.' })
        return
      }

      if (isMultipart && file) {
        doc.size = await this.fileDataAccess.saveDocumentFile(file, doc)
        if (priorDoc.getFilenameWithSuffix() !== doc.getFilenameWithSuffix()) {
          await this.fileDataAccess.deleteDocumentFile(priorDoc)
        }
      } else if (
        priorDoc.getFilenameWithSuffix() !== doc.getFilenameWithSuffix()
      ) {
        await this.fileDataAccess.renameDocumentFile(
          priorDoc.getFilenameWithSuffix(),
          doc.getFilenameWithSuffix()
        )
      }

      await this.documentDataAccess.save(doc)
      reply.status(StatusCodes.OK).send()
    } catch (error) {
      if (
        this.fastify &&
        error instanceof this.fastify.multipartErrors.RequestFileTooLargeError
      ) {
        reply
          .status(StatusCodes.REQUEST_TOO_LONG)
          .send({ status: 'error', message: 'File size is too large.' })
      }
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }

  public async deleteDocument(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const params = request.params as { documentId: string }
    const docId = parseInt(params.documentId)
    try {
      const doc = await this.documentDataAccess.get(docId)
      if (doc) {
        this.fileDataAccess.deleteDocumentFile(doc)
      }
      await this.documentDataAccess.delete(docId)
      reply.status(StatusCodes.OK).send({ status: 'ok' })
    } catch (error) {
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }
}
