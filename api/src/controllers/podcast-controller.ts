import { inject, injectable } from 'tsyringe'
import Controller from './controller'
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify'
import SermonDataAccess from '../services/sermon-data-access'
import FeedDataAccess from '../services/feed-data-access'
import { KerygmaConfiguration } from '../model/kerygma-configuration'

@injectable()
export default class PodcastController implements Controller {
  private fastify?: FastifyInstance

  constructor(
    private readonly sermonDataAccess: SermonDataAccess,
    private readonly feedDataAccess: FeedDataAccess,
    @inject('KerygmaConfiguration')
    private readonly kerygmaConfig: KerygmaConfiguration,
    @inject('mediaPath') private readonly mediaPath: string
  ) {}

  registerRoutes(fastify: FastifyInstance): void {
    this.fastify = fastify
    // get is unsecure so the podcast services can query the rss feed
    fastify.get('/api/podcast', async (request: FastifyRequest, reply) => {
      await this.getPodcastFeed(request, reply)
    })
  }

  public async getPodcastFeed(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const config = this.kerygmaConfig
    const sermons = await this.sermonDataAccess.getAll(0, 50, 'latest')
    const apiHost = this.kerygmaConfig.podcast?.siteUrl ?? ''
    const feedUrl = `${apiHost}/api/podcast`
    const sermonsUri = `${apiHost}/${this.mediaPath}/sermons/`
    const faviconUrl = `${apiHost}/${config.generator?.assetsDir}/${config.favicon?.path}`
    const podcastFeed = await this.feedDataAccess.getPodcastFeed(
      sermons.sermons,
      config,
      feedUrl,
      sermonsUri,
      faviconUrl
    )
    reply.send(podcastFeed)
  }
}
