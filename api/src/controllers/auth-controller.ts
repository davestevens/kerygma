import AuthDataAccess from '../services/auth-data-access'
import AuthTokenGenerator from '../services/auth-token-generator'
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify'
import Controller from './controller'
import PreHandler from '../handlers/pre-handler'
import UserDataAccess from '../services/user-data-access'
import { StatusCodes } from 'http-status-codes'
import { RequestContext } from '@fastify/request-context'
import { inject, injectable } from 'tsyringe'
import { Logger } from 'pino'
import { SignJWT } from 'jose'

@injectable()
export default class AuthController implements Controller {
  constructor(
    private readonly authDataAccess: AuthDataAccess,
    private readonly authTokenGenerator: AuthTokenGenerator,
    @inject('adminPreHandler') private readonly adminPreHandler: PreHandler,
    private readonly userDataAccess: UserDataAccess,
    @inject('Logger') private logger: Logger,
    @inject('requestContext') private readonly requestContext: RequestContext,
    @inject('SignJWT') private signJWT: SignJWT,
    @inject('csrfSignedTokenSecret') private csrfSignedTokenSecret: string
  ) {}

  public registerRoutes(fastify: FastifyInstance): void {
    fastify.get('/api/generate-csrf', async (request, reply) => {
      await this.generateCsrfToken(request, reply)
    })
    fastify.post('/api/login', async (request, reply) => {
      await this.login(request, reply)
    })
    fastify.delete(
      '/api/logout',
      { preHandler: this.adminPreHandler },
      async (request, reply) => {
        await this.logout(request, reply)
      }
    )
  }

  public async generateCsrfToken(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const secret = new TextEncoder().encode(this.csrfSignedTokenSecret)
    const alg = 'HS256'
    const token = await this.signJWT
      .setProtectedHeader({ alg })
      .setIssuedAt()
      .setIssuer('kerygma:api')
      .setAudience('kerygma:web-user')
      .setExpirationTime('2h')
      .sign(secret)
    // TODO: remove underscore from token. See the following
    // https://serverfault.com/questions/586970/nginx-is-not-forwarding-a-header-value-when-using-proxy-pass
    reply
      .setCookie('_csrf', token, {
        path: '/',
        sameSite: 'strict',
        httpOnly: true,
      })
      .status(200)
      .send({ token })
  }

  public async login(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    try {
      const login = request.body as { username: string; password: string }
      const username = login.username?.trim()
      const password = login.password?.trim()
      const userId = await this.authDataAccess.verifyCredentials(
        username,
        password
      )
      if (userId === null) {
        const isSuspended = await this.userDataAccess.addFailedAttempt(username)
        if (isSuspended) {
          request.log.info(`${username} failed log in and is suspended.`)
        } else {
          request.log.info(`${username} failed log in.`)
        }
        const statusCode = isSuspended
          ? StatusCodes.LOCKED
          : StatusCodes.FORBIDDEN
        reply
          .status(statusCode)
          .send({ status: 'error', message: 'invalid credentials' })
        return
      }
      const loginToken = await this.authTokenGenerator.generateLoginToken()
      await this.authDataAccess.addLoginSession(userId, loginToken)
      await this.userDataAccess.clearFailedAttempts(username)
      request.log.info(`${username} successfully logged in.`)
      reply.status(StatusCodes.OK).send({ status: 'ok', token: loginToken })
    } catch (error) {
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }

  public async logout(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const authToken = this.requestContext.get('authToken')
    await this.authDataAccess.logout(authToken)
    reply
      .status(StatusCodes.OK)
      .send({ status: 'ok', message: 'Successfully logged out.' })
    return
  }
}
