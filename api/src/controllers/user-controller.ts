import UserDataAccess from '../services/user-data-access'
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify'
import { JsonConvert } from 'json2typescript'
import { UserForm } from '../model/user-form'
import Controller from './controller'
import PreHandler from '../handlers/pre-handler'
import { StatusCodes } from 'http-status-codes'
import { RequestContext } from '@fastify/request-context'
import { inject, injectable } from 'tsyringe'
import { Logger } from 'pino'

@injectable()
export default class UserController implements Controller {
  constructor(
    private readonly userDataAccess: UserDataAccess,
    @inject('JsonConvert') private readonly jsonConvert: JsonConvert,
    @inject('Logger') private logger: Logger,
    @inject('sysAdminPreHandler')
    private readonly sysAdminPreHandler: PreHandler,
    @inject('adminPreHandler') private readonly adminPreHandler: PreHandler,
    @inject('requestContext') private readonly requestContext: RequestContext
  ) {}

  public registerRoutes(fastify: FastifyInstance): void {
    fastify.get(
      '/api/users',
      { preHandler: this.sysAdminPreHandler },
      async (request, reply) => {
        await this.getUsers(request, reply)
      }
    )
    fastify.get(
      '/api/users/logged-in',
      { preHandler: this.adminPreHandler },
      async (request, reply) => {
        await this.getUserLoginIn(request, reply)
      }
    )
    fastify.get(
      '/api/users/:userId',
      { preHandler: this.sysAdminPreHandler },
      async (request, reply) => {
        await this.getUser(request, reply)
      }
    )
    fastify.post(
      '/api/users',
      { preHandler: this.sysAdminPreHandler },
      async (request, reply) => {
        await this.addUser(request, reply)
      }
    )
    fastify.put(
      '/api/users/:userId',
      { preHandler: this.sysAdminPreHandler },
      async (request, reply) => {
        await this.saveUser(request, reply)
      }
    )
    fastify.delete(
      '/api/users/:userId',
      { preHandler: this.sysAdminPreHandler },
      async (request, reply) => {
        await this.deleteUser(request, reply)
      }
    )
  }

  public async getUsers(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const loginUser = this.requestContext.get('user')
    const users = await this.userDataAccess.getAll()
    reply.send({ status: 'ok', users, loginUser })
  }

  public async getUser(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const loginUser = this.requestContext.get('user')
    const params = request.params as { userId: string }
    const user = await this.userDataAccess.get(params.userId)
    if (user === null) {
      reply
        .status(StatusCodes.NOT_FOUND)
        .send({ status: 'error', message: 'User not found.' })
      return
    }
    reply.send({ status: 'ok', user, loginUser })
  }

  public async addUser(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    this.jsonConvert.ignorePrimitiveChecks = false
    try {
      const userForm = this.jsonConvert.deserializeObject(
        request.body,
        UserForm
      )

      if (!userForm.isValid()) {
        reply
          .status(StatusCodes.UNPROCESSABLE_ENTITY)
          .send({ status: 'error', message: 'Validation failed.' })
        return
      }

      const alreadyExists = await this.userDataAccess.getByUsername(
        userForm.username
      )
      if (alreadyExists !== null) {
        reply
          .status(StatusCodes.CONFLICT)
          .send({ status: 'error', message: 'Userid already in use.' })
        return
      }

      await this.userDataAccess.add(userForm)
      reply.status(StatusCodes.OK).send({ status: 'ok' })
      return
    } catch (error) {
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }

  public async saveUser(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    try {
      const params = request.params as { userId: string }
      const loginUser = this.requestContext.get('user')
      const userForm = this.jsonConvert.deserializeObject(
        request.body,
        UserForm
      )
      userForm.mode = 'edit'

      if (!userForm.isValid()) {
        reply
          .status(StatusCodes.UNPROCESSABLE_ENTITY)
          .send({ status: 'error', message: 'Validation failed.' })
        return
      }

      if (
        loginUser.id === params.userId &&
        loginUser.authLevel === 'sysadmin' &&
        userForm.authLevel === 'admin'
      ) {
        reply
          .status(StatusCodes.UNPROCESSABLE_ENTITY)
          .send({ status: 'error', message: 'Cannot demote self.' })
        return
      }

      const alreadyExists = await this.userDataAccess.getByUsername(
        userForm.username
      )
      if (alreadyExists && alreadyExists.id !== userForm.id) {
        reply
          .status(StatusCodes.CONFLICT)
          .send({ status: 'error', message: 'Userid already in use.' })
        return
      }

      await this.userDataAccess.save(userForm)
      reply.status(StatusCodes.OK).send({ status: 'ok' })
      return Promise.resolve()
    } catch (error) {
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }

  public async deleteUser(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const params = request.params as { userId: string }
    const loginUser = this.requestContext.get('user')
    if (loginUser.id === params.userId) {
      reply
        .status(StatusCodes.BAD_REQUEST)
        .send({ status: 'error', message: 'Cannot delete logged in user.' })
      return
    }
    try {
      await this.userDataAccess.delete(params.userId)
      reply.status(StatusCodes.OK).send({ status: 'ok' })
    } catch (error) {
      const err = error as Error
      this.logger.error(err.message, err)
      reply
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ status: 'error', message: err.message })
      return
    }
  }

  public async getUserLoginIn(
    request: FastifyRequest,
    reply: FastifyReply
  ): Promise<void> {
    const user = this.requestContext.get('user')
    if (user === null) {
      reply
        .status(StatusCodes.NOT_FOUND)
        .send({ status: 'error', message: 'User not found.' })
      return
    }
    reply.send({ status: 'ok', user })
  }
}
