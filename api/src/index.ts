#!/usr/bin/env node

import 'reflect-metadata'
import container from './di-container'
import StartServerCommand from './commands/start-server-command'
import MigrateDatabaseCommand from './commands/migrate-database-command'
import { ConfigParser } from './services/config-parser'
import { KerygmaConfiguration } from './model/kerygma-configuration'
import InitWebSiteCommand from './commands/init-web-site-command'

export async function startServer(kerygmaConfigPath: string): Promise<void> {
  const configParser = container.resolve(ConfigParser)
  const config = configParser.parseConfig(kerygmaConfigPath)
  if (!config) {
    return
  }

  container.register<KerygmaConfiguration>('KerygmaConfiguration', {
    useValue: config,
  })
  const startServerCommand = container.resolve(StartServerCommand)
  return startServerCommand.start()
}

export function migrateDatabase(direction: 'up' | 'down'): void {
  const migrateDatabaseCommand = container.resolve(MigrateDatabaseCommand)
  if (direction === 'down') {
    migrateDatabaseCommand.down()
  } else {
    migrateDatabaseCommand.up()
  }
}

export function initWebSite(
  configFile: string,
  pagesPath: string,
  assetsPath: string,
  mediaPath: string
): void {
  const initWebSiteCommand = container.resolve(InitWebSiteCommand)
  initWebSiteCommand.init(configFile, pagesPath, assetsPath, mediaPath)
}
