/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ['**/*.test.ts'],
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.ts',
    '!src/index.ts',
    '!src/injector.ts',
    '!src/handlers/*-handler.ts',
  ],
  setupFilesAfterEnv: ['<rootDir>/src/jest-setup.ts'],
}
