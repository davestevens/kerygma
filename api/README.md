# Kerygma API

The Kerygma API contains the API functions for Kerygma, a static page generator
and sermon management tool for church websites.

## Local Initial Setup
### Setting Up the Database Locally

1. Download PostgreSQL 14.x from https://www.postgresql.org/download/.
2. Follow the install instructions for your OS.
3. Verify the install with the following command.
   ```
   $ postgres --version
   postgres (PostgreSQL) 14.1 (Ubuntu 14.1-1.pgdg21.10+1)
   ```
4. Create the kerygma database and user.
   ```
   $ sudo -u postgres psql
   postgres=# create database kerygma;
   postgres=# create user kerygma with encrypted password 'kerygma';
   postgres=# grant all privileges on database kerygma to kerygma;
   postgres=# exit
   $ sudo -u postgres psql -d kerygma
   postgres=# CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
   postgres=# CREATE EXTENSION IF NOT EXISTS "pgcrypto";
   postgres=# exit 
   ```
   uuid-ossp is used for the uuid_generate_v4 function.
   pgcrypto is used for password hashing (crypt and gen_salt).
6. Test out the newly created database and user.
   ```
   $ psql --host=localhost --dbname=kerygma --username=kerygma
   ```

### Running Database Migrations

Note: The init migration will enable the `uuid-ossp` and `pgcrypto`
extensions, if not already added, for use by add-user.
```
npx db-migrate up
npx db-migrate down
npx db-migrate create [script_name]
```

### API Project Setup

```
nvm use v16 # Switch to using Node v16. (or node version manager of choice)
npm i
cp .env.dist .env
```

## Local Build, Lint and Test

```
npm run build
npm start
npm run eslint
npm test
```

## Publishing the Package

```
npm login --auth-type=legacy # This may be required if you are not logged in
npm publish
```
