'use strict'

exports.up = async function (db) {
  // uuid-ossp is used for the uuid_generate_v4 function.
  // pgcrypto is used for password hashing (crypt and gen_salt)
  // await db.runSql('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')
  // await db.runSql('CREATE EXTENSION IF NOT EXISTS "pgcrypto"')
  await db.createTable('sermons', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    sermon_date: { type: 'date', notNull: true },
    speaker_title: { type: 'string', notNull: true },
    speaker_first_name: { type: 'string', notNull: true },
    speaker_last_name: { type: 'string', notNull: true },
    sermon_title: { type: 'string', notNull: true },
    service: { type: 'string', notNull: true },
    from_book_cd: { type: 'string' },
    from_chapter: { type: 'int' },
    from_verse: { type: 'int' },
    thru_book_cd: { type: 'string' },
    thru_chapter: { type: 'int' },
    thru_verse: { type: 'int' },
    file_name: { type: 'string', noNull: true },
    uploaded_ts: { type: 'timestamp', notNull: true },
    size: { type: 'int', notNull: true },
  })
  await db.addIndex('sermons', 'sermon_date_index', ['sermon_date'], false)
  await db.createTable('documents', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    filename: { type: 'string', notNull: true },
    type: { type: 'string', notNull: true },
    title: { type: 'string', notNull: true },
    uploaded_ts: { type: 'timestamp', notNull: true },
    size: { type: 'int', notNull: true },
  })
  await db.createTable('users', {
    id: { type: 'string', primaryKey: true },
    first_name: { type: 'string', notNull: true },
    last_name: { type: 'string', notNull: true },
    username: {
      type: 'string',
      notNull: true,
      unique: true,
    },
    password_hash: { type: 'string', notNull: true },
    auth_level: { type: 'string', notNull: true },
    failed_attempt_count: { type: 'int', notNull: true },
    suspend_ts: { type: 'timestamp' },
  })
  await db.createTable('login_sessions', {
    user_id: { type: 'string', notNull: true },
    token: { type: 'string', notNull: true },
    last_used: { type: 'timestamp' },
  })
  await db.addForeignKey(
    'login_sessions',
    'users',
    'login_sessions_user_id_foreign',
    {
      user_id: 'id',
    },
    {
      onDelete: 'CASCADE',
      onUpdate: 'RESTRICT',
    }
  )
}

exports.down = async function (db) {
  await db.dropTable('sermons')
  await db.dropTable('documents')
  await db.dropTable('login_sessions')
  await db.dropTable('users')
}

exports._meta = {
  version: 1,
}
