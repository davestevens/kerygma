'use strict'

exports.up = async function (db) {
  await db.createTable('sermon_tags', {
    sermon_id: { type: 'int', notNull: true },
    tag: { type: 'string', notNull: true },
  })
  await db.addIndex(
    'sermon_tags',
    'sermon_tags_sermon_id_index',
    ['sermon_id'],
    false
  )
  await db.addIndex('sermon_tags', 'sermon_tags_tag_index', ['tag'], false)
  await db.addForeignKey(
    'sermon_tags',
    'sermons',
    'sermon_tags_sermon_id_foreign',
    {
      sermon_id: 'id',
    },
    {
      onDelete: 'CASCADE',
      onUpdate: 'RESTRICT',
    }
  )
}

exports.down = async function (db) {
  await db.dropTable('sermon_tags')
}
