'use strict'

exports.up = async function (db) {
  await db.runSql(
    `
      INSERT INTO users(
        id,
        first_name,
        last_name,
        username,
        password_hash,
        auth_level,
        failed_attempt_count
      ) VALUES (
        uuid_generate_v4(),
        'Admin',
        'User',
        'adminuser',
        crypt('ChangeMe123', gen_salt('md5')),
        'sysadmin',
        0
      )
    `
  )
}

exports.down = async function (db) {
  await db.runSql(
    `
      DELETE FROM users
      WHERE username = 'adminuser'
    `
  )
}
